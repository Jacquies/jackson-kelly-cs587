#ifndef INCLUDED_Projectile
#define INCLUDED_Projectile

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <ITickable.h>
#include <IRenderable.h>
#include <ICollidable.h>
HX_DECLARE_CLASS0(Animation)
HX_DECLARE_CLASS0(Collider)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(Projectile)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,geom,Point)


class Projectile_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Projectile_obj OBJ_;
		Projectile_obj();
		Void __construct(Float x,Float y,::TileRenderer tileRenderer,int ID);

	public:
		static hx::ObjectPtr< Projectile_obj > __new(Float x,Float y,::TileRenderer tileRenderer,int ID);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Projectile_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::ITickable_obj *()
			{ return new ::ITickable_delegate_< Projectile_obj >(this); }
		inline operator ::IRenderable_obj *()
			{ return new ::IRenderable_delegate_< Projectile_obj >(this); }
		inline operator ::ICollidable_obj *()
			{ return new ::ICollidable_delegate_< Projectile_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Projectile"); }

		virtual Void hitWall( ::ICollidable wallCollidable);
		Dynamic hitWall_dyn();

		virtual bool isActivated( );
		Dynamic isActivated_dyn();

		virtual Void tick( Float deltaTime);
		Dynamic tick_dyn();

		virtual Void render( Float deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual ::Collider getCollider( );
		Dynamic getCollider_dyn();

		virtual Void setRotation( Float rot);
		Dynamic setRotation_dyn();

		virtual Void setY( Float y);
		Dynamic setY_dyn();

		virtual Void setX( Float x);
		Dynamic setX_dyn();

		virtual Void deinitialize( );
		Dynamic deinitialize_dyn();

		virtual Void initialize( );
		Dynamic initialize_dyn();

		bool activated; /* REM */ 
		int poolID; /* REM */ 
		Float rotation; /* REM */ 
		::native::geom::Point position; /* REM */ 
		::TileRenderer renderer; /* REM */ 
		::Animation animation; /* REM */ 
		::Collider collider; /* REM */ 
};


#endif /* INCLUDED_Projectile */ 
