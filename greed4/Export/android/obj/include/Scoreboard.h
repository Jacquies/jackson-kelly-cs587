#ifndef INCLUDED_Scoreboard
#define INCLUDED_Scoreboard

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Scoreboard)
HX_DECLARE_CLASS0(TileRenderer)


class Scoreboard_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Scoreboard_obj OBJ_;
		Scoreboard_obj();
		Void __construct(::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Scoreboard_obj > __new(::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Scoreboard_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Scoreboard"); }

		virtual Void renderNumber( Float x,Float y,Float scale,int num);
		Dynamic renderNumber_dyn();

		virtual Void update( Float gold);
		Dynamic update_dyn();

		::TileRenderer renderer; /* REM */ 
		Array< int > spriteIDs; /* REM */ 
};


#endif /* INCLUDED_Scoreboard */ 
