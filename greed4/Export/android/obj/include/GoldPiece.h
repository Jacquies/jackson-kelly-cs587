#ifndef INCLUDED_GoldPiece
#define INCLUDED_GoldPiece

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IRenderable.h>
#include <ICollidable.h>
HX_DECLARE_CLASS0(Animation)
HX_DECLARE_CLASS0(Collider)
HX_DECLARE_CLASS0(GoldPiece)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,geom,Point)


class GoldPiece_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef GoldPiece_obj OBJ_;
		GoldPiece_obj();
		Void __construct(::native::geom::Point spawnPoint,::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< GoldPiece_obj > __new(::native::geom::Point spawnPoint,::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~GoldPiece_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IRenderable_obj *()
			{ return new ::IRenderable_delegate_< GoldPiece_obj >(this); }
		inline operator ::ICollidable_obj *()
			{ return new ::ICollidable_delegate_< GoldPiece_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("GoldPiece"); }

		virtual Void collide( ::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual ::Collider getCollider( );
		Dynamic getCollider_dyn();

		int value; /* REM */ 
		::TileRenderer renderer; /* REM */ 
		bool collected; /* REM */ 
		::Animation animation; /* REM */ 
		::Collider collider; /* REM */ 
};


#endif /* INCLUDED_GoldPiece */ 
