#ifndef INCLUDED_engine_interfaces_ICollidable
#define INCLUDED_engine_interfaces_ICollidable

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,objects,Collider)
namespace engine{
namespace interfaces{


class ICollidable_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef ICollidable_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual Void collide( ::engine::interfaces::ICollidable otherCollidable)=0;
		Dynamic collide_dyn();
virtual ::engine::objects::Collider getCollider( )=0;
		Dynamic getCollider_dyn();
};

#define DELEGATE_engine_interfaces_ICollidable \
virtual Void collide( ::engine::interfaces::ICollidable otherCollidable) { return mDelegate->collide(otherCollidable);}  \
virtual Dynamic collide_dyn() { return mDelegate->collide_dyn();}  \
virtual ::engine::objects::Collider getCollider( ) { return mDelegate->getCollider();}  \
virtual Dynamic getCollider_dyn() { return mDelegate->getCollider_dyn();}  \


template<typename IMPL>
class ICollidable_delegate_ : public ICollidable_obj
{
	protected:
		IMPL *mDelegate;
	public:
		ICollidable_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_engine_interfaces_ICollidable
};

} // end namespace engine
} // end namespace interfaces

#endif /* INCLUDED_engine_interfaces_ICollidable */ 
