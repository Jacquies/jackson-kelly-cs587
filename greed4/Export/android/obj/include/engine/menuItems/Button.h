#ifndef INCLUDED_engine_menuItems_Button
#define INCLUDED_engine_menuItems_Button

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,menuItems,Button)
HX_DECLARE_CLASS2(native,geom,Point)
namespace engine{
namespace menuItems{


class Button_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Button_obj OBJ_;
		Button_obj();
		Void __construct(Float xPos,Float yPos,int buttonSpriteID,Float scale,::engine::managers::TileRenderer tileRenderer,Dynamic func);

	public:
		static hx::ObjectPtr< Button_obj > __new(Float xPos,Float yPos,int buttonSpriteID,Float scale,::engine::managers::TileRenderer tileRenderer,Dynamic func);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Button_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Button"); }

		virtual Void onClick( Float clickedX,Float clickedY);
		Dynamic onClick_dyn();

		Dynamic onPressFunction; /* REM */ 
		Dynamic &onPressFunction_dyn() { return onPressFunction;}
		::engine::managers::TileRenderer renderer; /* REM */ 
		Float height; /* REM */ 
		Float width; /* REM */ 
		::native::geom::Point position; /* REM */ 
};

} // end namespace engine
} // end namespace menuItems

#endif /* INCLUDED_engine_menuItems_Button */ 
