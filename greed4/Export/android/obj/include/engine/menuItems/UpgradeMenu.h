#ifndef INCLUDED_engine_menuItems_UpgradeMenu
#define INCLUDED_engine_menuItems_UpgradeMenu

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <native/display/Sprite.h>
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,menuItems,Button)
HX_DECLARE_CLASS2(engine,menuItems,UpgradeMenu)
HX_DECLARE_CLASS2(engine,objects,HealthBar)
HX_DECLARE_CLASS2(engine,objects,Scoreboard)
HX_DECLARE_CLASS2(native,display,DisplayObject)
HX_DECLARE_CLASS2(native,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(native,display,IBitmapDrawable)
HX_DECLARE_CLASS2(native,display,InteractiveObject)
HX_DECLARE_CLASS2(native,display,Sprite)
HX_DECLARE_CLASS2(native,events,Event)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,events,MouseEvent)
HX_DECLARE_CLASS2(native,events,TouchEvent)
HX_DECLARE_CLASS2(native,net,SharedObject)
namespace engine{
namespace menuItems{


class UpgradeMenu_obj : public ::native::display::Sprite_obj{
	public:
		typedef ::native::display::Sprite_obj super;
		typedef UpgradeMenu_obj OBJ_;
		UpgradeMenu_obj();
		Void __construct(::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< UpgradeMenu_obj > __new(::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~UpgradeMenu_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("UpgradeMenu"); }

		virtual Void dummyFunction( );
		Dynamic dummyFunction_dyn();

		virtual Void onTouchBegin( ::native::events::TouchEvent event);
		Dynamic onTouchBegin_dyn();

		virtual Void nextLevel( );
		Dynamic nextLevel_dyn();

		virtual Void saveAndExit( );
		Dynamic saveAndExit_dyn();

		virtual Void upgradeAggroRadius( );
		Dynamic upgradeAggroRadius_dyn();

		virtual Void upgradeCollectRadius( );
		Dynamic upgradeCollectRadius_dyn();

		virtual Void upgradeSpeed( );
		Dynamic upgradeSpeed_dyn();

		virtual Void upgradeHealth( );
		Dynamic upgradeHealth_dyn();

		virtual Void refresh( );
		Dynamic refresh_dyn();

		virtual Void onAddedToStage( ::native::events::Event event);
		Dynamic onAddedToStage_dyn();

		Float scale; /* REM */ 
		::engine::objects::HealthBar healthBar; /* REM */ 
		::engine::objects::Scoreboard scoreboard; /* REM */ 
		::native::net::SharedObject localData; /* REM */ 
		Array< ::engine::menuItems::Button > buttons; /* REM */ 
		::engine::managers::TileRenderer renderer; /* REM */ 
};

} // end namespace engine
} // end namespace menuItems

#endif /* INCLUDED_engine_menuItems_UpgradeMenu */ 
