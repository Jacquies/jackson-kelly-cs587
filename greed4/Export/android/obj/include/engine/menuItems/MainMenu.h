#ifndef INCLUDED_engine_menuItems_MainMenu
#define INCLUDED_engine_menuItems_MainMenu

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <native/display/Sprite.h>
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,menuItems,Button)
HX_DECLARE_CLASS2(engine,menuItems,MainMenu)
HX_DECLARE_CLASS2(native,display,DisplayObject)
HX_DECLARE_CLASS2(native,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(native,display,IBitmapDrawable)
HX_DECLARE_CLASS2(native,display,InteractiveObject)
HX_DECLARE_CLASS2(native,display,Sprite)
HX_DECLARE_CLASS2(native,events,Event)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,events,MouseEvent)
HX_DECLARE_CLASS2(native,events,TouchEvent)
namespace engine{
namespace menuItems{


class MainMenu_obj : public ::native::display::Sprite_obj{
	public:
		typedef ::native::display::Sprite_obj super;
		typedef MainMenu_obj OBJ_;
		MainMenu_obj();
		Void __construct(::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< MainMenu_obj > __new(::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~MainMenu_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("MainMenu"); }

		virtual Void dummyFunction( );
		Dynamic dummyFunction_dyn();

		virtual Void onTouchEnd( ::native::events::TouchEvent event);
		Dynamic onTouchEnd_dyn();

		virtual Void onTouchBegin( ::native::events::TouchEvent event);
		Dynamic onTouchBegin_dyn();

		virtual Void exit( );
		Dynamic exit_dyn();

		virtual Void newGame( );
		Dynamic newGame_dyn();

		virtual Void resumeGame( );
		Dynamic resumeGame_dyn();

		virtual Void refresh( );
		Dynamic refresh_dyn();

		virtual Void onAddedToStage( ::native::events::Event event);
		Dynamic onAddedToStage_dyn();

		bool clickable; /* REM */ 
		Float scale; /* REM */ 
		Array< ::engine::menuItems::Button > buttons; /* REM */ 
		::engine::managers::TileRenderer renderer; /* REM */ 
		static bool initialized; /* REM */ 
};

} // end namespace engine
} // end namespace menuItems

#endif /* INCLUDED_engine_menuItems_MainMenu */ 
