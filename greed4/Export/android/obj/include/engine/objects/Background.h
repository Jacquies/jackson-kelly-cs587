#ifndef INCLUDED_engine_objects_Background
#define INCLUDED_engine_objects_Background

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <engine/interfaces/IRenderable.h>
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,objects,Animation)
HX_DECLARE_CLASS2(engine,objects,Background)
HX_DECLARE_CLASS2(native,geom,Point)
namespace engine{
namespace objects{


class Background_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Background_obj OBJ_;
		Background_obj();
		Void __construct(::native::geom::Point spawnPoint,::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Background_obj > __new(::native::geom::Point spawnPoint,::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Background_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::engine::interfaces::IRenderable_obj *()
			{ return new ::engine::interfaces::IRenderable_delegate_< Background_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Background"); }

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		::engine::objects::Animation animation; /* REM */ 
};

} // end namespace engine
} // end namespace objects

#endif /* INCLUDED_engine_objects_Background */ 
