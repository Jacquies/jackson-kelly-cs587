#ifndef INCLUDED_engine_objects_HealthBar
#define INCLUDED_engine_objects_HealthBar

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,objects,HealthBar)
namespace engine{
namespace objects{


class HealthBar_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef HealthBar_obj OBJ_;
		HealthBar_obj();
		Void __construct(::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< HealthBar_obj > __new(::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~HealthBar_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("HealthBar"); }

		virtual Void update( int health);
		Dynamic update_dyn();

		::engine::managers::TileRenderer renderer; /* REM */ 
		Array< int > spriteIDs; /* REM */ 
};

} // end namespace engine
} // end namespace objects

#endif /* INCLUDED_engine_objects_HealthBar */ 
