#ifndef INCLUDED_engine_managers_SceneManager
#define INCLUDED_engine_managers_SceneManager

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,interfaces,ITickable)
HX_DECLARE_CLASS2(engine,managers,SceneManager)
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,objects,HealthBar)
HX_DECLARE_CLASS2(engine,objects,Scoreboard)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,geom,Point)
HX_DECLARE_CLASS2(native,net,SharedObject)
namespace engine{
namespace managers{


class SceneManager_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef SceneManager_obj OBJ_;
		SceneManager_obj();
		Void __construct(::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< SceneManager_obj > __new(::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~SceneManager_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("SceneManager"); }

		virtual Void setLevelOver( bool isLevelOver);
		Dynamic setLevelOver_dyn();

		virtual Void setIdle( bool idleState);
		Dynamic setIdle_dyn();

		virtual bool isIdle( );
		Dynamic isIdle_dyn();

		virtual ::native::net::SharedObject getSharedObject( );
		Dynamic getSharedObject_dyn();

		virtual Void clearTickableSet( );
		Dynamic clearTickableSet_dyn();

		virtual Void removeFromTickableSet( ::engine::interfaces::ITickable tickable);
		Dynamic removeFromTickableSet_dyn();

		virtual Void addToTickableSet( ::engine::interfaces::ITickable tickable);
		Dynamic addToTickableSet_dyn();

		virtual Void clearRenderSet( );
		Dynamic clearRenderSet_dyn();

		virtual Void removeFromRenderSet( ::engine::interfaces::IRenderable renderable);
		Dynamic removeFromRenderSet_dyn();

		virtual Void addToRenderSet( ::engine::interfaces::IRenderable renderable);
		Dynamic addToRenderSet_dyn();

		virtual Void clearCollisionSet( );
		Dynamic clearCollisionSet_dyn();

		virtual Void removeFromCollisionSet( ::engine::interfaces::ICollidable collidable);
		Dynamic removeFromCollisionSet_dyn();

		virtual Void addToCollisionSet( ::engine::interfaces::ICollidable collidable);
		Dynamic addToCollisionSet_dyn();

		virtual Void setCameraY( Float y);
		Dynamic setCameraY_dyn();

		virtual Void setCameraX( Float x);
		Dynamic setCameraX_dyn();

		virtual ::native::geom::Point getCameraPosition( );
		Dynamic getCameraPosition_dyn();

		virtual ::native::geom::Point getMousePosition( );
		Dynamic getMousePosition_dyn();

		virtual Void setMousePosition( Float x,Float y);
		Dynamic setMousePosition_dyn();

		virtual bool getMouseDown( );
		Dynamic getMouseDown_dyn();

		virtual Void setMouseDown( bool isMouseDown);
		Dynamic setMouseDown_dyn();

		virtual Void updateHealthBar( );
		Dynamic updateHealthBar_dyn();

		virtual Void updateScoreboard( );
		Dynamic updateScoreboard_dyn();

		virtual Void tick( int deltaTime);
		Dynamic tick_dyn();

		virtual Void initialize( );
		Dynamic initialize_dyn();

		bool levelOver; /* REM */ 
		::engine::objects::HealthBar healthBar; /* REM */ 
		::engine::objects::Scoreboard scoreboard; /* REM */ 
		bool idle; /* REM */ 
		::native::net::SharedObject localData; /* REM */ 
		::native::geom::Point mousePosition; /* REM */ 
		::native::geom::Point camera; /* REM */ 
		bool mouseDown; /* REM */ 
		Array< ::engine::interfaces::ITickable > tickableSet; /* REM */ 
		Array< ::engine::interfaces::IRenderable > renderSet; /* REM */ 
		Array< ::engine::interfaces::ICollidable > collisionSet; /* REM */ 
		::engine::managers::TileRenderer renderer; /* REM */ 
		static Float scale; /* REM */ 
		static int usingFlash; /* REM */ 
		static ::engine::managers::SceneManager currentSceneManager; /* REM */ 
		static Void collide( ::engine::interfaces::ICollidable collidable1,::engine::interfaces::ICollidable collidable2);
		static Dynamic collide_dyn();

		static Float distance( ::native::geom::Point point1,::native::geom::Point point2);
		static Dynamic distance_dyn();

		static Float squareDistance( ::native::geom::Point point1,::native::geom::Point point2);
		static Dynamic squareDistance_dyn();

};

} // end namespace engine
} // end namespace managers

#endif /* INCLUDED_engine_managers_SceneManager */ 
