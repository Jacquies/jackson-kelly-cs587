#ifndef INCLUDED_engine_managers_TileRenderer
#define INCLUDED_engine_managers_TileRenderer

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(native,display,DisplayObject)
HX_DECLARE_CLASS2(native,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(native,display,Graphics)
HX_DECLARE_CLASS2(native,display,IBitmapDrawable)
HX_DECLARE_CLASS2(native,display,InteractiveObject)
HX_DECLARE_CLASS2(native,display,Stage)
HX_DECLARE_CLASS2(native,display,Tilesheet)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
namespace engine{
namespace managers{


class TileRenderer_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef TileRenderer_obj OBJ_;
		TileRenderer_obj();
		Void __construct(::native::display::Graphics inGraphics,::native::display::Stage displayStage);

	public:
		static hx::ObjectPtr< TileRenderer_obj > __new(::native::display::Graphics inGraphics,::native::display::Stage displayStage);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~TileRenderer_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("TileRenderer"); }

		virtual Void loadAllTiles( );
		Dynamic loadAllTiles_dyn();

		virtual Void clear( );
		Dynamic clear_dyn();

		virtual Void draw( );
		Dynamic draw_dyn();

		virtual Void updateObject( int ID,Float x,Float y,int SpriteID,Float rotation,Float scale,int layer);
		Dynamic updateObject_dyn();

		virtual Void remove( int ID,int layer);
		Dynamic remove_dyn();

		virtual int addTile( Float x,Float y,int SpriteID,Float rot,Float scale,int layer);
		Dynamic addTile_dyn();

		virtual int loadTile( Float x,Float y,Float width,Float height,Float centerX,Float centerY);
		Dynamic loadTile_dyn();

		Array< Array< int > > freeIndices; /* REM */ 
		Array< Array< Float > > tileData; /* REM */ 
		::native::display::Graphics graphics; /* REM */ 
		int numTiles; /* REM */ 
		::native::display::Tilesheet tilesheet; /* REM */ 
		static Float stageWidth; /* REM */ 
		static Float stageHeight; /* REM */ 
		static Array< Float > widths; /* REM */ 
		static Array< Float > heights; /* REM */ 
		static Float DPI; /* REM */ 
};

} // end namespace engine
} // end namespace managers

#endif /* INCLUDED_engine_managers_TileRenderer */ 
