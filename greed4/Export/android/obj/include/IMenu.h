#ifndef INCLUDED_IMenu
#define INCLUDED_IMenu

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(IMenu)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,net,SharedObject)


class IMenu_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef IMenu_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual ::native::net::SharedObject getSharedObject( )=0;
		Dynamic getSharedObject_dyn();
virtual Void options( )=0;
		Dynamic options_dyn();
virtual Void back( )=0;
		Dynamic back_dyn();
virtual Void forward( )=0;
		Dynamic forward_dyn();
virtual Void refresh( )=0;
		Dynamic refresh_dyn();
};

#define DELEGATE_IMenu \
virtual ::native::net::SharedObject getSharedObject( ) { return mDelegate->getSharedObject();}  \
virtual Dynamic getSharedObject_dyn() { return mDelegate->getSharedObject_dyn();}  \
virtual Void options( ) { return mDelegate->options();}  \
virtual Dynamic options_dyn() { return mDelegate->options_dyn();}  \
virtual Void back( ) { return mDelegate->back();}  \
virtual Dynamic back_dyn() { return mDelegate->back_dyn();}  \
virtual Void forward( ) { return mDelegate->forward();}  \
virtual Dynamic forward_dyn() { return mDelegate->forward_dyn();}  \
virtual Void refresh( ) { return mDelegate->refresh();}  \
virtual Dynamic refresh_dyn() { return mDelegate->refresh_dyn();}  \


template<typename IMPL>
class IMenu_delegate_ : public IMenu_obj
{
	protected:
		IMPL *mDelegate;
	public:
		IMenu_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_IMenu
};


#endif /* INCLUDED_IMenu */ 
