#ifndef INCLUDED_ChasingState
#define INCLUDED_ChasingState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IState.h>
HX_DECLARE_CLASS0(ChasingState)
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(IState)
HX_DECLARE_CLASS0(ITickable)


class ChasingState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef ChasingState_obj OBJ_;
		ChasingState_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< ChasingState_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~ChasingState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IState_obj *()
			{ return new ::IState_delegate_< ChasingState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("ChasingState"); }

		virtual Void tick( Float deltaTime,::Enemy enemy);
		Dynamic tick_dyn();

};


#endif /* INCLUDED_ChasingState */ 
