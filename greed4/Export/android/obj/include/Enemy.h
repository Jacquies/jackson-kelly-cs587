#ifndef INCLUDED_Enemy
#define INCLUDED_Enemy

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <ITickable.h>
#include <IRenderable.h>
#include <ICollidable.h>
HX_DECLARE_CLASS0(Animation)
HX_DECLARE_CLASS0(Collider)
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IEnemyState)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(Player)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,geom,Point)


class Enemy_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Enemy_obj OBJ_;
		Enemy_obj();
		Void __construct(::native::geom::Point spawnPoint,Array< ::IEnemyState > states,Array< ::Animation > enemyAnimations,::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Enemy_obj > __new(::native::geom::Point spawnPoint,Array< ::IEnemyState > states,Array< ::Animation > enemyAnimations,::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Enemy_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::ITickable_obj *()
			{ return new ::ITickable_delegate_< Enemy_obj >(this); }
		inline operator ::IRenderable_obj *()
			{ return new ::IRenderable_delegate_< Enemy_obj >(this); }
		inline operator ::ICollidable_obj *()
			{ return new ::ICollidable_delegate_< Enemy_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Enemy"); }

		virtual Void collide( ::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual Void switchAnimation( int ID);
		Dynamic switchAnimation_dyn();

		virtual Void exitState( ::IEnemyState state);
		Dynamic exitState_dyn();

		virtual Void enterState( ::IEnemyState state);
		Dynamic enterState_dyn();

		virtual Void tick( int deltaTime);
		Dynamic tick_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual ::Collider getCollider( );
		Dynamic getCollider_dyn();

		virtual Void setRotation( Float rot);
		Dynamic setRotation_dyn();

		virtual Float getRotation( );
		Dynamic getRotation_dyn();

		virtual Void setY( Float y);
		Dynamic setY_dyn();

		virtual Void setX( Float x);
		Dynamic setX_dyn();

		virtual ::native::geom::Point getPosition( );
		Dynamic getPosition_dyn();

		Array< ::Animation > animations; /* REM */ 
		int currentAnimationID; /* REM */ 
		Float rotation; /* REM */ 
		::native::geom::Point position; /* REM */ 
		Array< ::IEnemyState > enemyStates; /* REM */ 
		::Animation animation; /* REM */ 
		::Collider collider; /* REM */ 
		static int TYPE_CHASING_ENEMY; /* REM */ 
		static int TYPE_RANGED_ENEMY; /* REM */ 
		static int TYPE_CHARGING_ENEMY; /* REM */ 
		static ::Player player; /* REM */ 
};


#endif /* INCLUDED_Enemy */ 
