#ifndef INCLUDED_IEnemyState
#define INCLUDED_IEnemyState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IEnemyState)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)


class IEnemyState_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef IEnemyState_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual Void hitWall( ::ICollidable wallCollidable)=0;
		Dynamic hitWall_dyn();
virtual Void tick( int deltaTime,::Enemy enemy)=0;
		Dynamic tick_dyn();
};

#define DELEGATE_IEnemyState \
virtual Void hitWall( ::ICollidable wallCollidable) { return mDelegate->hitWall(wallCollidable);}  \
virtual Dynamic hitWall_dyn() { return mDelegate->hitWall_dyn();}  \
virtual Void tick( int deltaTime,::Enemy enemy) { return mDelegate->tick(deltaTime,enemy);}  \
virtual Dynamic tick_dyn() { return mDelegate->tick_dyn();}  \


template<typename IMPL>
class IEnemyState_delegate_ : public IEnemyState_obj
{
	protected:
		IMPL *mDelegate;
	public:
		IEnemyState_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_IEnemyState
};


#endif /* INCLUDED_IEnemyState */ 
