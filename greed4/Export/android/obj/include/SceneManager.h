#ifndef INCLUDED_SceneManager
#define INCLUDED_SceneManager

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(HealthBar)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(SceneManager)
HX_DECLARE_CLASS0(Scoreboard)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,geom,Point)
HX_DECLARE_CLASS2(native,net,SharedObject)


class SceneManager_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef SceneManager_obj OBJ_;
		SceneManager_obj();
		Void __construct(::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< SceneManager_obj > __new(::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~SceneManager_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("SceneManager"); }

		virtual Void setLevelOver( bool isLevelOver);
		Dynamic setLevelOver_dyn();

		virtual Void setIdle( bool idleState);
		Dynamic setIdle_dyn();

		virtual bool isIdle( );
		Dynamic isIdle_dyn();

		virtual ::native::net::SharedObject getSharedObject( );
		Dynamic getSharedObject_dyn();

		virtual Void clearTickableSet( );
		Dynamic clearTickableSet_dyn();

		virtual Void removeFromTickableSet( ::ITickable tickable);
		Dynamic removeFromTickableSet_dyn();

		virtual Void addToTickableSet( ::ITickable tickable);
		Dynamic addToTickableSet_dyn();

		virtual Void clearRenderSet( );
		Dynamic clearRenderSet_dyn();

		virtual Void removeFromRenderSet( ::IRenderable renderable);
		Dynamic removeFromRenderSet_dyn();

		virtual Void addToRenderSet( ::IRenderable renderable);
		Dynamic addToRenderSet_dyn();

		virtual Void clearCollisionSet( );
		Dynamic clearCollisionSet_dyn();

		virtual Void removeFromCollisionSet( ::ICollidable collidable);
		Dynamic removeFromCollisionSet_dyn();

		virtual Void addToCollisionSet( ::ICollidable collidable);
		Dynamic addToCollisionSet_dyn();

		virtual Void setCameraY( Float y);
		Dynamic setCameraY_dyn();

		virtual Void setCameraX( Float x);
		Dynamic setCameraX_dyn();

		virtual ::native::geom::Point getCameraPosition( );
		Dynamic getCameraPosition_dyn();

		virtual ::native::geom::Point getMousePosition( );
		Dynamic getMousePosition_dyn();

		virtual Void setMousePosition( Float x,Float y);
		Dynamic setMousePosition_dyn();

		virtual bool getMouseDown( );
		Dynamic getMouseDown_dyn();

		virtual Void setMouseDown( bool isMouseDown);
		Dynamic setMouseDown_dyn();

		virtual Void updateHealthBar( );
		Dynamic updateHealthBar_dyn();

		virtual Void updateScoreboard( );
		Dynamic updateScoreboard_dyn();

		virtual Void tick( int deltaTime);
		Dynamic tick_dyn();

		virtual Void initialize( );
		Dynamic initialize_dyn();

		bool levelOver; /* REM */ 
		::HealthBar healthBar; /* REM */ 
		::Scoreboard scoreboard; /* REM */ 
		bool idle; /* REM */ 
		::native::net::SharedObject localData; /* REM */ 
		::native::geom::Point mousePosition; /* REM */ 
		::native::geom::Point camera; /* REM */ 
		bool mouseDown; /* REM */ 
		Array< ::ITickable > tickableSet; /* REM */ 
		Array< ::IRenderable > renderSet; /* REM */ 
		Array< ::ICollidable > collisionSet; /* REM */ 
		::TileRenderer renderer; /* REM */ 
		static Float scale; /* REM */ 
		static int usingFlash; /* REM */ 
		static ::SceneManager currentSceneManager; /* REM */ 
		static Void collide( ::ICollidable collidable1,::ICollidable collidable2);
		static Dynamic collide_dyn();

		static Float distance( ::native::geom::Point point1,::native::geom::Point point2);
		static Dynamic distance_dyn();

		static Float squareDistance( ::native::geom::Point point1,::native::geom::Point point2);
		static Dynamic squareDistance_dyn();

};


#endif /* INCLUDED_SceneManager */ 
