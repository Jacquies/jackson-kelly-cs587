#ifndef INCLUDED_Player
#define INCLUDED_Player

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <ITickable.h>
#include <IRenderable.h>
#include <ICollidable.h>
HX_DECLARE_CLASS0(Animation)
HX_DECLARE_CLASS0(Collider)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(Player)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,geom,Point)


class Player_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Player_obj OBJ_;
		Player_obj();
		Void __construct(::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Player_obj > __new(::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Player_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::ITickable_obj *()
			{ return new ::ITickable_delegate_< Player_obj >(this); }
		inline operator ::IRenderable_obj *()
			{ return new ::IRenderable_delegate_< Player_obj >(this); }
		inline operator ::ICollidable_obj *()
			{ return new ::ICollidable_delegate_< Player_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Player"); }

		virtual Void collide( ::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual int getHitCountdown( );
		Dynamic getHitCountdown_dyn();

		virtual Void decreaseHealth( );
		Dynamic decreaseHealth_dyn();

		virtual Void hitWall( ::ICollidable wallCollidable);
		Dynamic hitWall_dyn();

		virtual Void hit( );
		Dynamic hit_dyn();

		virtual Void switchAnimation( int ID);
		Dynamic switchAnimation_dyn();

		virtual Void tick( int deltaTime);
		Dynamic tick_dyn();

		virtual Void translateY( Float dy);
		Dynamic translateY_dyn();

		virtual Void translateX( Float dx);
		Dynamic translateX_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual Void setRotation( Float rot);
		Dynamic setRotation_dyn();

		virtual Float getRotation( );
		Dynamic getRotation_dyn();

		virtual Void setY( Float y);
		Dynamic setY_dyn();

		virtual Void setX( Float x);
		Dynamic setX_dyn();

		virtual ::native::geom::Point getPosition( );
		Dynamic getPosition_dyn();

		virtual ::Collider getCollider( );
		Dynamic getCollider_dyn();

		::TileRenderer renderer; /* REM */ 
		Array< ::Animation > animations; /* REM */ 
		int hitCountdown; /* REM */ 
		int currentAnimationID; /* REM */ 
		Float rotation; /* REM */ 
		::native::geom::Point position; /* REM */ 
		::Animation animation; /* REM */ 
		::Collider collider; /* REM */ 
};


#endif /* INCLUDED_Player */ 
