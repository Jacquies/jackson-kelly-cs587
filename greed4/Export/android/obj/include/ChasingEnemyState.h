#ifndef INCLUDED_ChasingEnemyState
#define INCLUDED_ChasingEnemyState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IEnemyState.h>
HX_DECLARE_CLASS0(ChasingEnemyState)
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IEnemyState)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)


class ChasingEnemyState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef ChasingEnemyState_obj OBJ_;
		ChasingEnemyState_obj();
		Void __construct(int difficulty);

	public:
		static hx::ObjectPtr< ChasingEnemyState_obj > __new(int difficulty);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~ChasingEnemyState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IEnemyState_obj *()
			{ return new ::IEnemyState_delegate_< ChasingEnemyState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("ChasingEnemyState"); }

		virtual Void hitWall( ::ICollidable wallCollidable);
		Dynamic hitWall_dyn();

		virtual Void tick( int deltaTime,::Enemy enemy);
		Dynamic tick_dyn();

		Float speed; /* REM */ 
};


#endif /* INCLUDED_ChasingEnemyState */ 
