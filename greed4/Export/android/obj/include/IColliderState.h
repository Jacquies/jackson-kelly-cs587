#ifndef INCLUDED_IColliderState
#define INCLUDED_IColliderState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IColliderState)


class IColliderState_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef IColliderState_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual Void collide( ::ICollidable collidingObject,::ICollidable otherCollidingObject)=0;
		Dynamic collide_dyn();
};

#define DELEGATE_IColliderState \
virtual Void collide( ::ICollidable collidingObject,::ICollidable otherCollidingObject) { return mDelegate->collide(collidingObject,otherCollidingObject);}  \
virtual Dynamic collide_dyn() { return mDelegate->collide_dyn();}  \


template<typename IMPL>
class IColliderState_delegate_ : public IColliderState_obj
{
	protected:
		IMPL *mDelegate;
	public:
		IColliderState_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_IColliderState
};


#endif /* INCLUDED_IColliderState */ 
