#ifndef INCLUDED_StrafingState
#define INCLUDED_StrafingState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IState.h>
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(IState)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(StrafingState)


class StrafingState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef StrafingState_obj OBJ_;
		StrafingState_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< StrafingState_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~StrafingState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IState_obj *()
			{ return new ::IState_delegate_< StrafingState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("StrafingState"); }

		virtual Void tick( Float deltaTime,::Enemy enemy);
		Dynamic tick_dyn();

};


#endif /* INCLUDED_StrafingState */ 
