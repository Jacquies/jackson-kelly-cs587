#ifndef INCLUDED_Animation
#define INCLUDED_Animation

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Animation)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,geom,Point)


class Animation_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Animation_obj OBJ_;
		Animation_obj();
		Void __construct(int startingFrame,int length,int framesPerSecond,Float x,Float y,Float rot,int displayLayer,::TileRenderer tileRenderer,hx::Null< bool >  __o_loop);

	public:
		static hx::ObjectPtr< Animation_obj > __new(int startingFrame,int length,int framesPerSecond,Float x,Float y,Float rot,int displayLayer,::TileRenderer tileRenderer,hx::Null< bool >  __o_loop);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Animation_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Animation"); }

		virtual Void restartAnimation( );
		Dynamic restartAnimation_dyn();

		virtual bool isFinished( );
		Dynamic isFinished_dyn();

		virtual int getCurrentFrame( );
		Dynamic getCurrentFrame_dyn();

		virtual ::native::geom::Point getPosition( );
		Dynamic getPosition_dyn();

		virtual Void setY( Float y);
		Dynamic setY_dyn();

		virtual Void setX( Float x);
		Dynamic setX_dyn();

		virtual Float getRotation( );
		Dynamic getRotation_dyn();

		virtual Void setRotation( Float rot);
		Dynamic setRotation_dyn();

		virtual Void addToRenderSet( );
		Dynamic addToRenderSet_dyn();

		virtual Void removeFromRenderSet( );
		Dynamic removeFromRenderSet_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual Void update( Float x,Float y,Float rot);
		Dynamic update_dyn();

		bool rendering; /* REM */ 
		bool loops; /* REM */ 
		int ID; /* REM */ 
		::TileRenderer renderer; /* REM */ 
		int layer; /* REM */ 
		Float rotation; /* REM */ 
		::native::geom::Point position; /* REM */ 
		Float currentTime; /* REM */ 
		int currentFrame; /* REM */ 
		int animationSpeed; /* REM */ 
		int animationLength; /* REM */ 
		int startFrame; /* REM */ 
};


#endif /* INCLUDED_Animation */ 
