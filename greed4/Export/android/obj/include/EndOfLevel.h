#ifndef INCLUDED_EndOfLevel
#define INCLUDED_EndOfLevel

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IRenderable.h>
#include <ICollidable.h>
HX_DECLARE_CLASS0(Animation)
HX_DECLARE_CLASS0(Collider)
HX_DECLARE_CLASS0(EndOfLevel)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(Level)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,display,DisplayObject)
HX_DECLARE_CLASS2(native,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(native,display,IBitmapDrawable)
HX_DECLARE_CLASS2(native,display,InteractiveObject)
HX_DECLARE_CLASS2(native,display,Sprite)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,geom,Point)


class EndOfLevel_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef EndOfLevel_obj OBJ_;
		EndOfLevel_obj();
		Void __construct(::native::geom::Point spawnPoint,::TileRenderer tileRenderer,::Level parentLevel);

	public:
		static hx::ObjectPtr< EndOfLevel_obj > __new(::native::geom::Point spawnPoint,::TileRenderer tileRenderer,::Level parentLevel);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~EndOfLevel_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IRenderable_obj *()
			{ return new ::IRenderable_delegate_< EndOfLevel_obj >(this); }
		inline operator ::ICollidable_obj *()
			{ return new ::ICollidable_delegate_< EndOfLevel_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("EndOfLevel"); }

		virtual Void collide( ::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual Void endLevel( );
		Dynamic endLevel_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual ::Collider getCollider( );
		Dynamic getCollider_dyn();

		::Level parent; /* REM */ 
		::Animation animation; /* REM */ 
		::Collider collider; /* REM */ 
};


#endif /* INCLUDED_EndOfLevel */ 
