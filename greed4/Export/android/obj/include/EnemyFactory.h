#ifndef INCLUDED_EnemyFactory
#define INCLUDED_EnemyFactory

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(EnemyFactory)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,geom,Point)


class EnemyFactory_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef EnemyFactory_obj OBJ_;
		EnemyFactory_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< EnemyFactory_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~EnemyFactory_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("EnemyFactory"); }

		static ::Enemy getEnemy( ::native::geom::Point position,int type,::TileRenderer tileRenderer);
		static Dynamic getEnemy_dyn();

};


#endif /* INCLUDED_EnemyFactory */ 
