#ifndef INCLUDED_IdleState
#define INCLUDED_IdleState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IState.h>
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(IState)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(IdleState)


class IdleState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef IdleState_obj OBJ_;
		IdleState_obj();
		Void __construct(int type);

	public:
		static hx::ObjectPtr< IdleState_obj > __new(int type);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~IdleState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IState_obj *()
			{ return new ::IState_delegate_< IdleState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("IdleState"); }

		virtual Void tick( Float deltaTime,::Enemy enemy);
		Dynamic tick_dyn();

		Array< ::IState > nextStates; /* REM */ 
};


#endif /* INCLUDED_IdleState */ 
