#ifndef INCLUDED_ChargingEnemyState
#define INCLUDED_ChargingEnemyState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IEnemyState.h>
HX_DECLARE_CLASS0(ChargingEnemyState)
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IEnemyState)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)


class ChargingEnemyState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef ChargingEnemyState_obj OBJ_;
		ChargingEnemyState_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< ChargingEnemyState_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~ChargingEnemyState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IEnemyState_obj *()
			{ return new ::IEnemyState_delegate_< ChargingEnemyState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("ChargingEnemyState"); }

		virtual Void hitWall( ::ICollidable wallCollidable);
		Dynamic hitWall_dyn();

		virtual Void tick( int deltaTime,::Enemy enemy);
		Dynamic tick_dyn();

		Float rotation; /* REM */ 
		Float speed; /* REM */ 
};


#endif /* INCLUDED_ChargingEnemyState */ 
