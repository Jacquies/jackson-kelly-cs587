#ifndef INCLUDED_IdleEnemyState
#define INCLUDED_IdleEnemyState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IEnemyState.h>
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IEnemyState)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(IdleEnemyState)


class IdleEnemyState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef IdleEnemyState_obj OBJ_;
		IdleEnemyState_obj();
		Void __construct(Array< ::IEnemyState > states);

	public:
		static hx::ObjectPtr< IdleEnemyState_obj > __new(Array< ::IEnemyState > states);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~IdleEnemyState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IEnemyState_obj *()
			{ return new ::IEnemyState_delegate_< IdleEnemyState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("IdleEnemyState"); }

		virtual Void hitWall( ::ICollidable wallCollidable);
		Dynamic hitWall_dyn();

		virtual Void tick( int _tmp_deltaTime,::Enemy enemy);
		Dynamic tick_dyn();

		Array< ::IEnemyState > nextStates; /* REM */ 
};


#endif /* INCLUDED_IdleEnemyState */ 
