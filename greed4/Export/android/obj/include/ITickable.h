#ifndef INCLUDED_ITickable
#define INCLUDED_ITickable

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(ITickable)


class ITickable_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef ITickable_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual Void tick( int deltaTime)=0;
		Dynamic tick_dyn();
};

#define DELEGATE_ITickable \
virtual Void tick( int deltaTime) { return mDelegate->tick(deltaTime);}  \
virtual Dynamic tick_dyn() { return mDelegate->tick_dyn();}  \


template<typename IMPL>
class ITickable_delegate_ : public ITickable_obj
{
	protected:
		IMPL *mDelegate;
	public:
		ITickable_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_ITickable
};


#endif /* INCLUDED_ITickable */ 
