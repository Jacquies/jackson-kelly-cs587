#ifndef INCLUDED_ColliderFactory
#define INCLUDED_ColliderFactory

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Collider)
HX_DECLARE_CLASS0(ColliderFactory)


class ColliderFactory_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef ColliderFactory_obj OBJ_;
		ColliderFactory_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< ColliderFactory_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~ColliderFactory_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("ColliderFactory"); }

		static ::Collider getCollider( Float x,Float y,Float width,Float height,int type);
		static Dynamic getCollider_dyn();

};


#endif /* INCLUDED_ColliderFactory */ 
