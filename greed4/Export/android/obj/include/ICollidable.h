#ifndef INCLUDED_ICollidable
#define INCLUDED_ICollidable

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Collider)
HX_DECLARE_CLASS0(ICollidable)


class ICollidable_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef ICollidable_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual Void collide( ::ICollidable otherCollidable)=0;
		Dynamic collide_dyn();
virtual ::Collider getCollider( )=0;
		Dynamic getCollider_dyn();
};

#define DELEGATE_ICollidable \
virtual Void collide( ::ICollidable otherCollidable) { return mDelegate->collide(otherCollidable);}  \
virtual Dynamic collide_dyn() { return mDelegate->collide_dyn();}  \
virtual ::Collider getCollider( ) { return mDelegate->getCollider();}  \
virtual Dynamic getCollider_dyn() { return mDelegate->getCollider_dyn();}  \


template<typename IMPL>
class ICollidable_delegate_ : public ICollidable_obj
{
	protected:
		IMPL *mDelegate;
	public:
		ICollidable_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_ICollidable
};


#endif /* INCLUDED_ICollidable */ 
