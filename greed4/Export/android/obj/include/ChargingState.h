#ifndef INCLUDED_ChargingState
#define INCLUDED_ChargingState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IState.h>
HX_DECLARE_CLASS0(ChargingState)
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(IState)
HX_DECLARE_CLASS0(ITickable)


class ChargingState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef ChargingState_obj OBJ_;
		ChargingState_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< ChargingState_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~ChargingState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IState_obj *()
			{ return new ::IState_delegate_< ChargingState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("ChargingState"); }

		virtual Void tick( Float deltaTime,::Enemy enemy);
		Dynamic tick_dyn();

		Float rotation; /* REM */ 
		Float speed; /* REM */ 
};


#endif /* INCLUDED_ChargingState */ 
