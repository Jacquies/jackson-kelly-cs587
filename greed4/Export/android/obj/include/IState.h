#ifndef INCLUDED_IState
#define INCLUDED_IState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(IState)
HX_DECLARE_CLASS0(ITickable)


class IState_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef IState_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual Void tick( Float deltaTime,::Enemy enemy)=0;
		Dynamic tick_dyn();
};

#define DELEGATE_IState \
virtual Void tick( Float deltaTime,::Enemy enemy) { return mDelegate->tick(deltaTime,enemy);}  \
virtual Dynamic tick_dyn() { return mDelegate->tick_dyn();}  \


template<typename IMPL>
class IState_delegate_ : public IState_obj
{
	protected:
		IMPL *mDelegate;
	public:
		IState_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_IState
};


#endif /* INCLUDED_IState */ 
