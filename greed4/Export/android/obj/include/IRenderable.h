#ifndef INCLUDED_IRenderable
#define INCLUDED_IRenderable

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Animation)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS2(native,geom,Point)


class IRenderable_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef IRenderable_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual Void render( int deltaTime,::native::geom::Point camera)=0;
		Dynamic render_dyn();
};

#define DELEGATE_IRenderable \
virtual Void render( int deltaTime,::native::geom::Point camera) { return mDelegate->render(deltaTime,camera);}  \
virtual Dynamic render_dyn() { return mDelegate->render_dyn();}  \


template<typename IMPL>
class IRenderable_delegate_ : public IRenderable_obj
{
	protected:
		IMPL *mDelegate;
	public:
		IRenderable_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_IRenderable
};


#endif /* INCLUDED_IRenderable */ 
