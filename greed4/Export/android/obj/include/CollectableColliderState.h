#ifndef INCLUDED_CollectableColliderState
#define INCLUDED_CollectableColliderState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IColliderState.h>
HX_DECLARE_CLASS0(CollectableColliderState)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IColliderState)


class CollectableColliderState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef CollectableColliderState_obj OBJ_;
		CollectableColliderState_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< CollectableColliderState_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~CollectableColliderState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IColliderState_obj *()
			{ return new ::IColliderState_delegate_< CollectableColliderState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("CollectableColliderState"); }

		virtual Void collide( ::ICollidable collidingObject,::ICollidable otherCollidingObject);
		Dynamic collide_dyn();

};


#endif /* INCLUDED_CollectableColliderState */ 
