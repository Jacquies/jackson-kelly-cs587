#ifndef INCLUDED_Background
#define INCLUDED_Background

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IRenderable.h>
HX_DECLARE_CLASS0(Animation)
HX_DECLARE_CLASS0(Background)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,geom,Point)


class Background_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Background_obj OBJ_;
		Background_obj();
		Void __construct(::native::geom::Point spawnPoint,::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Background_obj > __new(::native::geom::Point spawnPoint,::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Background_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IRenderable_obj *()
			{ return new ::IRenderable_delegate_< Background_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Background"); }

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		::Animation animation; /* REM */ 
};


#endif /* INCLUDED_Background */ 
