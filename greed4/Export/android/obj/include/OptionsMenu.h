#ifndef INCLUDED_OptionsMenu
#define INCLUDED_OptionsMenu

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <native/display/Sprite.h>
#include <IMenu.h>
HX_DECLARE_CLASS0(Button)
HX_DECLARE_CLASS0(IMenu)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(OptionsMenu)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS2(native,display,DisplayObject)
HX_DECLARE_CLASS2(native,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(native,display,IBitmapDrawable)
HX_DECLARE_CLASS2(native,display,InteractiveObject)
HX_DECLARE_CLASS2(native,display,Sprite)
HX_DECLARE_CLASS2(native,events,Event)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,events,MouseEvent)
HX_DECLARE_CLASS2(native,net,SharedObject)


class OptionsMenu_obj : public ::native::display::Sprite_obj{
	public:
		typedef ::native::display::Sprite_obj super;
		typedef OptionsMenu_obj OBJ_;
		OptionsMenu_obj();
		Void __construct(::TileRenderer inRenderer);

	public:
		static hx::ObjectPtr< OptionsMenu_obj > __new(::TileRenderer inRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~OptionsMenu_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IMenu_obj *()
			{ return new ::IMenu_delegate_< OptionsMenu_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("OptionsMenu"); }

		virtual ::native::net::SharedObject getSharedObject( );
		Dynamic getSharedObject_dyn();

		virtual Void setParent( ::String parent);
		Dynamic setParent_dyn();

		virtual Void options( );
		Dynamic options_dyn();

		virtual Void back( );
		Dynamic back_dyn();

		virtual Void forward( );
		Dynamic forward_dyn();

		virtual Void onMouseDown( ::native::events::MouseEvent event);
		Dynamic onMouseDown_dyn();

		virtual Void refresh( );
		Dynamic refresh_dyn();

		virtual Void onAddedToStage( ::native::events::Event event);
		Dynamic onAddedToStage_dyn();

		bool fromMain; /* REM */ 
		bool mouseDown; /* REM */ 
		::native::net::SharedObject sharedObj; /* REM */ 
		Array< ::Button > buttons; /* REM */ 
		::TileRenderer renderer; /* REM */ 
};


#endif /* INCLUDED_OptionsMenu */ 
