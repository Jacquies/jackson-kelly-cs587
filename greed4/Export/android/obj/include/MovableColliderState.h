#ifndef INCLUDED_MovableColliderState
#define INCLUDED_MovableColliderState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IColliderState.h>
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IColliderState)
HX_DECLARE_CLASS0(MovableColliderState)


class MovableColliderState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef MovableColliderState_obj OBJ_;
		MovableColliderState_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< MovableColliderState_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~MovableColliderState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IColliderState_obj *()
			{ return new ::IColliderState_delegate_< MovableColliderState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("MovableColliderState"); }

		virtual Void collide( ::ICollidable collidingObject,::ICollidable otherCollidingObject);
		Dynamic collide_dyn();

};


#endif /* INCLUDED_MovableColliderState */ 
