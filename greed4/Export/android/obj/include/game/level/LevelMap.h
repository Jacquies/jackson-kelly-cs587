#ifndef INCLUDED_game_level_LevelMap
#define INCLUDED_game_level_LevelMap

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(game,level,LevelMap)
HX_DECLARE_CLASS2(native,geom,Point)
namespace game{
namespace level{


class LevelMap_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef LevelMap_obj OBJ_;
		LevelMap_obj();
		Void __construct(int difficulty);

	public:
		static hx::ObjectPtr< LevelMap_obj > __new(int difficulty);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~LevelMap_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("LevelMap"); }

		virtual Void printMap( );
		Dynamic printMap_dyn();

		virtual Void removeIslands( int i,int j,Array< Array< bool > > tempMap);
		Dynamic removeIslands_dyn();

		virtual Array< ::native::geom::Point > generateBackgroundPositions( );
		Dynamic generateBackgroundPositions_dyn();

		virtual Array< ::native::geom::Point > generateWallPositions( Array< int > wallDirections);
		Dynamic generateWallPositions_dyn();

		virtual Array< ::native::geom::Point > generateEnemiesOrCoins( int difficulty,int denseness);
		Dynamic generateEnemiesOrCoins_dyn();

		virtual ::native::geom::Point generateLevelEnd( Dynamic difficulty);
		Dynamic generateLevelEnd_dyn();

		virtual Array< ::native::geom::Point > generateCoins( int difficulty);
		Dynamic generateCoins_dyn();

		virtual Array< ::native::geom::Point > generateEnemies( int difficulty);
		Dynamic generateEnemies_dyn();

		int mapRadius; /* REM */ 
		Array< Array< bool > > map; /* REM */ 
		static int mapScale; /* REM */ 
};

} // end namespace game
} // end namespace level

#endif /* INCLUDED_game_level_LevelMap */ 
