#ifndef INCLUDED_game_level_Level
#define INCLUDED_game_level_Level

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <native/display/Sprite.h>
HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,interfaces,ITickable)
HX_DECLARE_CLASS2(engine,managers,SceneManager)
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,menuItems,Button)
HX_DECLARE_CLASS2(engine,objects,Background)
HX_DECLARE_CLASS2(game,level,Level)
HX_DECLARE_CLASS2(game,level,LevelMap)
HX_DECLARE_CLASS2(game,objects,EndOfLevel)
HX_DECLARE_CLASS2(game,objects,GoldPiece)
HX_DECLARE_CLASS2(game,objects,Player)
HX_DECLARE_CLASS2(game,objects,Wall)
HX_DECLARE_CLASS3(game,objects,enemy,Enemy)
HX_DECLARE_CLASS2(native,display,DisplayObject)
HX_DECLARE_CLASS2(native,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(native,display,IBitmapDrawable)
HX_DECLARE_CLASS2(native,display,InteractiveObject)
HX_DECLARE_CLASS2(native,display,Sprite)
HX_DECLARE_CLASS2(native,events,Event)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,events,MouseEvent)
HX_DECLARE_CLASS2(native,events,TouchEvent)
HX_DECLARE_CLASS2(native,net,SharedObject)
namespace game{
namespace level{


class Level_obj : public ::native::display::Sprite_obj{
	public:
		typedef ::native::display::Sprite_obj super;
		typedef Level_obj OBJ_;
		Level_obj();
		Void __construct(::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Level_obj > __new(::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Level_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Level"); }

		virtual Void dummyFunction( );
		Dynamic dummyFunction_dyn();

		virtual Void cleanup( );
		Dynamic cleanup_dyn();

		virtual Void returnToMainMenu( );
		Dynamic returnToMainMenu_dyn();

		virtual Void endLevel( );
		Dynamic endLevel_dyn();

		virtual Void onTouchMove( ::native::events::TouchEvent event);
		Dynamic onTouchMove_dyn();

		virtual Void onTouchEnd( ::native::events::TouchEvent event);
		Dynamic onTouchEnd_dyn();

		virtual Void onTouchBegin( ::native::events::TouchEvent event);
		Dynamic onTouchBegin_dyn();

		virtual Void onEnterFrame( ::native::events::Event event);
		Dynamic onEnterFrame_dyn();

		virtual Void onAddedToStage( ::native::events::Event event);
		Dynamic onAddedToStage_dyn();

		int previousTickTime; /* REM */ 
		::engine::menuItems::Button mainMenuButton; /* REM */ 
		int deathScreenCountdown; /* REM */ 
		Array< ::engine::objects::Background > backgrounds; /* REM */ 
		Array< ::game::objects::Wall > walls; /* REM */ 
		::game::objects::EndOfLevel levelEnd; /* REM */ 
		Array< ::game::objects::GoldPiece > coins; /* REM */ 
		Array< ::game::objects::enemy::Enemy > enemies; /* REM */ 
		::game::objects::Player player; /* REM */ 
		::game::level::LevelMap map; /* REM */ 
		::engine::managers::SceneManager sceneManager; /* REM */ 
		::engine::managers::TileRenderer renderer; /* REM */ 
		static Void initializeLocalData( ::native::net::SharedObject localData);
		static Dynamic initializeLocalData_dyn();

		static Void newGame( ::native::net::SharedObject localData);
		static Dynamic newGame_dyn();

};

} // end namespace game
} // end namespace level

#endif /* INCLUDED_game_level_Level */ 
