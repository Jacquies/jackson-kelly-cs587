#ifndef INCLUDED_game_objects_GoldPiece
#define INCLUDED_game_objects_GoldPiece

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <engine/interfaces/IRenderable.h>
#include <engine/interfaces/ICollidable.h>
HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,objects,Animation)
HX_DECLARE_CLASS2(engine,objects,Collider)
HX_DECLARE_CLASS2(game,objects,GoldPiece)
HX_DECLARE_CLASS2(native,geom,Point)
namespace game{
namespace objects{


class GoldPiece_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef GoldPiece_obj OBJ_;
		GoldPiece_obj();
		Void __construct(::native::geom::Point spawnPoint,::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< GoldPiece_obj > __new(::native::geom::Point spawnPoint,::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~GoldPiece_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::engine::interfaces::IRenderable_obj *()
			{ return new ::engine::interfaces::IRenderable_delegate_< GoldPiece_obj >(this); }
		inline operator ::engine::interfaces::ICollidable_obj *()
			{ return new ::engine::interfaces::ICollidable_delegate_< GoldPiece_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("GoldPiece"); }

		virtual Void collide( ::engine::interfaces::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual ::engine::objects::Collider getCollider( );
		Dynamic getCollider_dyn();

		int value; /* REM */ 
		::engine::managers::TileRenderer renderer; /* REM */ 
		bool collected; /* REM */ 
		::engine::objects::Animation animation; /* REM */ 
		::engine::objects::Collider collider; /* REM */ 
};

} // end namespace game
} // end namespace objects

#endif /* INCLUDED_game_objects_GoldPiece */ 
