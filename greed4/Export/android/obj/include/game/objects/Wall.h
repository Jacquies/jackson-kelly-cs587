#ifndef INCLUDED_game_objects_Wall
#define INCLUDED_game_objects_Wall

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <engine/interfaces/ICollidable.h>
#include <engine/interfaces/IRenderable.h>
HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,objects,Animation)
HX_DECLARE_CLASS2(engine,objects,Collider)
HX_DECLARE_CLASS2(game,objects,Wall)
HX_DECLARE_CLASS2(native,geom,Point)
namespace game{
namespace objects{


class Wall_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Wall_obj OBJ_;
		Wall_obj();
		Void __construct(::native::geom::Point spawnPoint,int facingDirection,::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Wall_obj > __new(::native::geom::Point spawnPoint,int facingDirection,::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Wall_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::engine::interfaces::ICollidable_obj *()
			{ return new ::engine::interfaces::ICollidable_delegate_< Wall_obj >(this); }
		inline operator ::engine::interfaces::IRenderable_obj *()
			{ return new ::engine::interfaces::IRenderable_delegate_< Wall_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Wall"); }

		virtual Void collide( ::engine::interfaces::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual int getDirection( );
		Dynamic getDirection_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual ::engine::objects::Collider getCollider( );
		Dynamic getCollider_dyn();

		int direction; /* REM */ 
		::engine::objects::Animation animation; /* REM */ 
		::engine::objects::Collider collider; /* REM */ 
};

} // end namespace game
} // end namespace objects

#endif /* INCLUDED_game_objects_Wall */ 
