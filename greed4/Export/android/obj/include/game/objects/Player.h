#ifndef INCLUDED_game_objects_Player
#define INCLUDED_game_objects_Player

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <engine/interfaces/ITickable.h>
#include <engine/interfaces/IRenderable.h>
#include <engine/interfaces/ICollidable.h>
HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,interfaces,ITickable)
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,objects,Animation)
HX_DECLARE_CLASS2(engine,objects,Collider)
HX_DECLARE_CLASS2(game,objects,Player)
HX_DECLARE_CLASS2(native,geom,Point)
namespace game{
namespace objects{


class Player_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Player_obj OBJ_;
		Player_obj();
		Void __construct(::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Player_obj > __new(::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Player_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::engine::interfaces::ITickable_obj *()
			{ return new ::engine::interfaces::ITickable_delegate_< Player_obj >(this); }
		inline operator ::engine::interfaces::IRenderable_obj *()
			{ return new ::engine::interfaces::IRenderable_delegate_< Player_obj >(this); }
		inline operator ::engine::interfaces::ICollidable_obj *()
			{ return new ::engine::interfaces::ICollidable_delegate_< Player_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Player"); }

		virtual Void collide( ::engine::interfaces::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual int getHitCountdown( );
		Dynamic getHitCountdown_dyn();

		virtual Void decreaseHealth( );
		Dynamic decreaseHealth_dyn();

		virtual Void hitWall( ::engine::interfaces::ICollidable wallCollidable);
		Dynamic hitWall_dyn();

		virtual Void hit( );
		Dynamic hit_dyn();

		virtual Void switchAnimation( int ID);
		Dynamic switchAnimation_dyn();

		virtual Void tick( int deltaTime);
		Dynamic tick_dyn();

		virtual Void translateY( Float dy);
		Dynamic translateY_dyn();

		virtual Void translateX( Float dx);
		Dynamic translateX_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual Void setRotation( Float rot);
		Dynamic setRotation_dyn();

		virtual Float getRotation( );
		Dynamic getRotation_dyn();

		virtual Void setY( Float y);
		Dynamic setY_dyn();

		virtual Void setX( Float x);
		Dynamic setX_dyn();

		virtual ::native::geom::Point getPosition( );
		Dynamic getPosition_dyn();

		virtual ::engine::objects::Collider getCollider( );
		Dynamic getCollider_dyn();

		::engine::managers::TileRenderer renderer; /* REM */ 
		Array< ::engine::objects::Animation > animations; /* REM */ 
		int hitCountdown; /* REM */ 
		int currentAnimationID; /* REM */ 
		Float rotation; /* REM */ 
		::native::geom::Point position; /* REM */ 
		::engine::objects::Animation animation; /* REM */ 
		::engine::objects::Collider collider; /* REM */ 
};

} // end namespace game
} // end namespace objects

#endif /* INCLUDED_game_objects_Player */ 
