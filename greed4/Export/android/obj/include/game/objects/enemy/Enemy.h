#ifndef INCLUDED_game_objects_enemy_Enemy
#define INCLUDED_game_objects_enemy_Enemy

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <engine/interfaces/ITickable.h>
#include <engine/interfaces/IRenderable.h>
#include <engine/interfaces/ICollidable.h>
HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,interfaces,ITickable)
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,objects,Animation)
HX_DECLARE_CLASS2(engine,objects,Collider)
HX_DECLARE_CLASS2(game,objects,Player)
HX_DECLARE_CLASS3(game,objects,enemy,Enemy)
HX_DECLARE_CLASS4(game,objects,enemy,ai,IEnemyState)
HX_DECLARE_CLASS2(native,geom,Point)
namespace game{
namespace objects{
namespace enemy{


class Enemy_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Enemy_obj OBJ_;
		Enemy_obj();
		Void __construct(::native::geom::Point spawnPoint,Array< ::game::objects::enemy::ai::IEnemyState > states,Array< ::engine::objects::Animation > enemyAnimations,::engine::managers::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Enemy_obj > __new(::native::geom::Point spawnPoint,Array< ::game::objects::enemy::ai::IEnemyState > states,Array< ::engine::objects::Animation > enemyAnimations,::engine::managers::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Enemy_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::engine::interfaces::ITickable_obj *()
			{ return new ::engine::interfaces::ITickable_delegate_< Enemy_obj >(this); }
		inline operator ::engine::interfaces::IRenderable_obj *()
			{ return new ::engine::interfaces::IRenderable_delegate_< Enemy_obj >(this); }
		inline operator ::engine::interfaces::ICollidable_obj *()
			{ return new ::engine::interfaces::ICollidable_delegate_< Enemy_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Enemy"); }

		virtual Void collide( ::engine::interfaces::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual Void switchAnimation( int ID);
		Dynamic switchAnimation_dyn();

		virtual Void exitState( ::game::objects::enemy::ai::IEnemyState state);
		Dynamic exitState_dyn();

		virtual Void enterState( ::game::objects::enemy::ai::IEnemyState state);
		Dynamic enterState_dyn();

		virtual Void tick( int deltaTime);
		Dynamic tick_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual ::engine::objects::Collider getCollider( );
		Dynamic getCollider_dyn();

		virtual Void setRotation( Float rot);
		Dynamic setRotation_dyn();

		virtual Float getRotation( );
		Dynamic getRotation_dyn();

		virtual Void setY( Float y);
		Dynamic setY_dyn();

		virtual Void setX( Float x);
		Dynamic setX_dyn();

		virtual ::native::geom::Point getPosition( );
		Dynamic getPosition_dyn();

		Array< ::engine::objects::Animation > animations; /* REM */ 
		int currentAnimationID; /* REM */ 
		Float rotation; /* REM */ 
		::native::geom::Point position; /* REM */ 
		Array< ::game::objects::enemy::ai::IEnemyState > enemyStates; /* REM */ 
		::engine::objects::Animation animation; /* REM */ 
		::engine::objects::Collider collider; /* REM */ 
		static int TYPE_CHASING_ENEMY; /* REM */ 
		static int TYPE_RANGED_ENEMY; /* REM */ 
		static int TYPE_CHARGING_ENEMY; /* REM */ 
		static ::game::objects::Player player; /* REM */ 
};

} // end namespace game
} // end namespace objects
} // end namespace enemy

#endif /* INCLUDED_game_objects_enemy_Enemy */ 
