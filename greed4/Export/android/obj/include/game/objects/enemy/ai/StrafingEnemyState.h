#ifndef INCLUDED_game_objects_enemy_ai_StrafingEnemyState
#define INCLUDED_game_objects_enemy_ai_StrafingEnemyState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <game/objects/enemy/ai/IEnemyState.h>
HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,interfaces,ITickable)
HX_DECLARE_CLASS3(game,objects,enemy,Enemy)
HX_DECLARE_CLASS4(game,objects,enemy,ai,IEnemyState)
HX_DECLARE_CLASS4(game,objects,enemy,ai,StrafingEnemyState)
namespace game{
namespace objects{
namespace enemy{
namespace ai{


class StrafingEnemyState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef StrafingEnemyState_obj OBJ_;
		StrafingEnemyState_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< StrafingEnemyState_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~StrafingEnemyState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::game::objects::enemy::ai::IEnemyState_obj *()
			{ return new ::game::objects::enemy::ai::IEnemyState_delegate_< StrafingEnemyState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("StrafingEnemyState"); }

		virtual Void hitWall( ::engine::interfaces::ICollidable wallCollidable);
		Dynamic hitWall_dyn();

		virtual Void tick( int deltaTime,::game::objects::enemy::Enemy enemy);
		Dynamic tick_dyn();

		Float strafeAngle; /* REM */ 
		int strafeDirection; /* REM */ 
};

} // end namespace game
} // end namespace objects
} // end namespace enemy
} // end namespace ai

#endif /* INCLUDED_game_objects_enemy_ai_StrafingEnemyState */ 
