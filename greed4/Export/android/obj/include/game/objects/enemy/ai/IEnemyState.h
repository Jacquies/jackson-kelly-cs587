#ifndef INCLUDED_game_objects_enemy_ai_IEnemyState
#define INCLUDED_game_objects_enemy_ai_IEnemyState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,interfaces,ITickable)
HX_DECLARE_CLASS3(game,objects,enemy,Enemy)
HX_DECLARE_CLASS4(game,objects,enemy,ai,IEnemyState)
namespace game{
namespace objects{
namespace enemy{
namespace ai{


class IEnemyState_obj : public hx::Interface{
	public:
		typedef hx::Interface super;
		typedef IEnemyState_obj OBJ_;
		HX_DO_INTERFACE_RTTI;
		static void __boot();
virtual Void hitWall( ::engine::interfaces::ICollidable wallCollidable)=0;
		Dynamic hitWall_dyn();
virtual Void tick( int deltaTime,::game::objects::enemy::Enemy enemy)=0;
		Dynamic tick_dyn();
};

#define DELEGATE_game_objects_enemy_ai_IEnemyState \
virtual Void hitWall( ::engine::interfaces::ICollidable wallCollidable) { return mDelegate->hitWall(wallCollidable);}  \
virtual Dynamic hitWall_dyn() { return mDelegate->hitWall_dyn();}  \
virtual Void tick( int deltaTime,::game::objects::enemy::Enemy enemy) { return mDelegate->tick(deltaTime,enemy);}  \
virtual Dynamic tick_dyn() { return mDelegate->tick_dyn();}  \


template<typename IMPL>
class IEnemyState_delegate_ : public IEnemyState_obj
{
	protected:
		IMPL *mDelegate;
	public:
		IEnemyState_delegate_(IMPL *inDelegate) : mDelegate(inDelegate) {}
		hx::Object *__GetRealObject() { return mDelegate; }
		void __Visit(HX_VISIT_PARAMS) { HX_VISIT_OBJECT(mDelegate); }
		DELEGATE_game_objects_enemy_ai_IEnemyState
};

} // end namespace game
} // end namespace objects
} // end namespace enemy
} // end namespace ai

#endif /* INCLUDED_game_objects_enemy_ai_IEnemyState */ 
