#ifndef INCLUDED_game_objects_enemy_EnemyFactory
#define INCLUDED_game_objects_enemy_EnemyFactory

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,interfaces,ITickable)
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS3(game,objects,enemy,Enemy)
HX_DECLARE_CLASS3(game,objects,enemy,EnemyFactory)
HX_DECLARE_CLASS2(native,geom,Point)
namespace game{
namespace objects{
namespace enemy{


class EnemyFactory_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef EnemyFactory_obj OBJ_;
		EnemyFactory_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< EnemyFactory_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~EnemyFactory_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("EnemyFactory"); }

		static ::game::objects::enemy::Enemy getEnemy( ::native::geom::Point position,int type,::engine::managers::TileRenderer tileRenderer);
		static Dynamic getEnemy_dyn();

};

} // end namespace game
} // end namespace objects
} // end namespace enemy

#endif /* INCLUDED_game_objects_enemy_EnemyFactory */ 
