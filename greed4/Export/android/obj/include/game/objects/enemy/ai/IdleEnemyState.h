#ifndef INCLUDED_game_objects_enemy_ai_IdleEnemyState
#define INCLUDED_game_objects_enemy_ai_IdleEnemyState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <game/objects/enemy/ai/IEnemyState.h>
HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,interfaces,ITickable)
HX_DECLARE_CLASS3(game,objects,enemy,Enemy)
HX_DECLARE_CLASS4(game,objects,enemy,ai,IEnemyState)
HX_DECLARE_CLASS4(game,objects,enemy,ai,IdleEnemyState)
namespace game{
namespace objects{
namespace enemy{
namespace ai{


class IdleEnemyState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef IdleEnemyState_obj OBJ_;
		IdleEnemyState_obj();
		Void __construct(Array< ::game::objects::enemy::ai::IEnemyState > states);

	public:
		static hx::ObjectPtr< IdleEnemyState_obj > __new(Array< ::game::objects::enemy::ai::IEnemyState > states);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~IdleEnemyState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::game::objects::enemy::ai::IEnemyState_obj *()
			{ return new ::game::objects::enemy::ai::IEnemyState_delegate_< IdleEnemyState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("IdleEnemyState"); }

		virtual Void hitWall( ::engine::interfaces::ICollidable wallCollidable);
		Dynamic hitWall_dyn();

		virtual Void tick( int _tmp_deltaTime,::game::objects::enemy::Enemy enemy);
		Dynamic tick_dyn();

		Array< ::game::objects::enemy::ai::IEnemyState > nextStates; /* REM */ 
};

} // end namespace game
} // end namespace objects
} // end namespace enemy
} // end namespace ai

#endif /* INCLUDED_game_objects_enemy_ai_IdleEnemyState */ 
