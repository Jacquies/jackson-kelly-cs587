#ifndef INCLUDED_game_objects_enemy_Projectile
#define INCLUDED_game_objects_enemy_Projectile

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <engine/interfaces/ITickable.h>
#include <engine/interfaces/IRenderable.h>
#include <engine/interfaces/ICollidable.h>
HX_DECLARE_CLASS2(engine,interfaces,ICollidable)
HX_DECLARE_CLASS2(engine,interfaces,IRenderable)
HX_DECLARE_CLASS2(engine,interfaces,ITickable)
HX_DECLARE_CLASS2(engine,managers,TileRenderer)
HX_DECLARE_CLASS2(engine,objects,Animation)
HX_DECLARE_CLASS2(engine,objects,Collider)
HX_DECLARE_CLASS3(game,objects,enemy,Projectile)
HX_DECLARE_CLASS2(native,geom,Point)
namespace game{
namespace objects{
namespace enemy{


class Projectile_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Projectile_obj OBJ_;
		Projectile_obj();
		Void __construct(Float x,Float y,::engine::managers::TileRenderer tileRenderer,int ID);

	public:
		static hx::ObjectPtr< Projectile_obj > __new(Float x,Float y,::engine::managers::TileRenderer tileRenderer,int ID);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Projectile_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::engine::interfaces::ITickable_obj *()
			{ return new ::engine::interfaces::ITickable_delegate_< Projectile_obj >(this); }
		inline operator ::engine::interfaces::IRenderable_obj *()
			{ return new ::engine::interfaces::IRenderable_delegate_< Projectile_obj >(this); }
		inline operator ::engine::interfaces::ICollidable_obj *()
			{ return new ::engine::interfaces::ICollidable_delegate_< Projectile_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Projectile"); }

		virtual Void collide( ::engine::interfaces::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual Void tick( int deltaTime);
		Dynamic tick_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual ::engine::objects::Collider getCollider( );
		Dynamic getCollider_dyn();

		virtual Void setRotation( Float rot);
		Dynamic setRotation_dyn();

		virtual Void setY( Float y);
		Dynamic setY_dyn();

		virtual Void setX( Float x);
		Dynamic setX_dyn();

		virtual Void deinitialize( );
		Dynamic deinitialize_dyn();

		virtual Void initialize( );
		Dynamic initialize_dyn();

		int poolID; /* REM */ 
		Float rotation; /* REM */ 
		::native::geom::Point position; /* REM */ 
		::engine::managers::TileRenderer renderer; /* REM */ 
		::engine::objects::Animation animation; /* REM */ 
		::engine::objects::Collider collider; /* REM */ 
};

} // end namespace game
} // end namespace objects
} // end namespace enemy

#endif /* INCLUDED_game_objects_enemy_Projectile */ 
