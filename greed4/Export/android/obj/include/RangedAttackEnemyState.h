#ifndef INCLUDED_RangedAttackEnemyState
#define INCLUDED_RangedAttackEnemyState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <IEnemyState.h>
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IEnemyState)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(RangedAttackEnemyState)


class RangedAttackEnemyState_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef RangedAttackEnemyState_obj OBJ_;
		RangedAttackEnemyState_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< RangedAttackEnemyState_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~RangedAttackEnemyState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::IEnemyState_obj *()
			{ return new ::IEnemyState_delegate_< RangedAttackEnemyState_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("RangedAttackEnemyState"); }

		virtual Void hitWall( ::ICollidable wallCollidable);
		Dynamic hitWall_dyn();

		virtual Void tick( int deltaTime,::Enemy enemy);
		Dynamic tick_dyn();

		int fireCooldown; /* REM */ 
};


#endif /* INCLUDED_RangedAttackEnemyState */ 
