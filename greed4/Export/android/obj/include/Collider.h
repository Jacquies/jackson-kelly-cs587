#ifndef INCLUDED_Collider
#define INCLUDED_Collider

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(Collider)
HX_DECLARE_CLASS2(native,geom,Rectangle)


class Collider_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Collider_obj OBJ_;
		Collider_obj();
		Void __construct(Float x,Float y,Float width,Float height,int colliderType);

	public:
		static hx::ObjectPtr< Collider_obj > __new(Float x,Float y,Float width,Float height,int colliderType);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Collider_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Collider"); }

		virtual bool collides( ::Collider otherCollider);
		Dynamic collides_dyn();

		virtual Void setY( Float y);
		Dynamic setY_dyn();

		virtual Void setX( Float x);
		Dynamic setX_dyn();

		virtual int getType( );
		Dynamic getType_dyn();

		virtual ::native::geom::Rectangle getBoundingBox( );
		Dynamic getBoundingBox_dyn();

		int type; /* REM */ 
		::native::geom::Rectangle boundingBox; /* REM */ 
		static int TYPE_PLAYER; /* REM */ 
		static int TYPE_ENEMY; /* REM */ 
		static int TYPE_COIN; /* REM */ 
		static int TYPE_WALL; /* REM */ 
		static int TYPE_END; /* REM */ 
		static int TYPE_PROJECTILE; /* REM */ 
};


#endif /* INCLUDED_Collider */ 
