#ifndef INCLUDED_UpgradeMenu
#define INCLUDED_UpgradeMenu

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <native/display/Sprite.h>
HX_DECLARE_CLASS0(Button)
HX_DECLARE_CLASS0(HealthBar)
HX_DECLARE_CLASS0(Scoreboard)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS0(UpgradeMenu)
HX_DECLARE_CLASS2(native,display,DisplayObject)
HX_DECLARE_CLASS2(native,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(native,display,IBitmapDrawable)
HX_DECLARE_CLASS2(native,display,InteractiveObject)
HX_DECLARE_CLASS2(native,display,Sprite)
HX_DECLARE_CLASS2(native,events,Event)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,events,MouseEvent)
HX_DECLARE_CLASS2(native,events,TouchEvent)
HX_DECLARE_CLASS2(native,net,SharedObject)


class UpgradeMenu_obj : public ::native::display::Sprite_obj{
	public:
		typedef ::native::display::Sprite_obj super;
		typedef UpgradeMenu_obj OBJ_;
		UpgradeMenu_obj();
		Void __construct(::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< UpgradeMenu_obj > __new(::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~UpgradeMenu_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("UpgradeMenu"); }

		virtual Void dummyFunction( );
		Dynamic dummyFunction_dyn();

		virtual Void onTouchBegin( ::native::events::TouchEvent event);
		Dynamic onTouchBegin_dyn();

		virtual Void nextLevel( );
		Dynamic nextLevel_dyn();

		virtual Void saveAndExit( );
		Dynamic saveAndExit_dyn();

		virtual Void upgradeAggroRadius( );
		Dynamic upgradeAggroRadius_dyn();

		virtual Void upgradeCollectRadius( );
		Dynamic upgradeCollectRadius_dyn();

		virtual Void upgradeSpeed( );
		Dynamic upgradeSpeed_dyn();

		virtual Void upgradeHealth( );
		Dynamic upgradeHealth_dyn();

		virtual Void refresh( );
		Dynamic refresh_dyn();

		virtual Void onAddedToStage( ::native::events::Event event);
		Dynamic onAddedToStage_dyn();

		Float scale; /* REM */ 
		::HealthBar healthBar; /* REM */ 
		::Scoreboard scoreboard; /* REM */ 
		::native::net::SharedObject localData; /* REM */ 
		Array< ::Button > buttons; /* REM */ 
		::TileRenderer renderer; /* REM */ 
};


#endif /* INCLUDED_UpgradeMenu */ 
