#ifndef INCLUDED_Wall
#define INCLUDED_Wall

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <ICollidable.h>
#include <IRenderable.h>
HX_DECLARE_CLASS0(Animation)
HX_DECLARE_CLASS0(Collider)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS0(Wall)
HX_DECLARE_CLASS2(native,geom,Point)


class Wall_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Wall_obj OBJ_;
		Wall_obj();
		Void __construct(::native::geom::Point spawnPoint,int facingDirection,::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Wall_obj > __new(::native::geom::Point spawnPoint,int facingDirection,::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Wall_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		inline operator ::ICollidable_obj *()
			{ return new ::ICollidable_delegate_< Wall_obj >(this); }
		inline operator ::IRenderable_obj *()
			{ return new ::IRenderable_delegate_< Wall_obj >(this); }
		hx::Object *__ToInterface(const type_info &inType);
		::String __ToString() const { return HX_CSTRING("Wall"); }

		virtual Void collide( ::ICollidable otherCollidable);
		Dynamic collide_dyn();

		virtual int getDirection( );
		Dynamic getDirection_dyn();

		virtual Void render( int deltaTime,::native::geom::Point camera);
		Dynamic render_dyn();

		virtual ::Collider getCollider( );
		Dynamic getCollider_dyn();

		int direction; /* REM */ 
		::Animation animation; /* REM */ 
		::Collider collider; /* REM */ 
};


#endif /* INCLUDED_Wall */ 
