#ifndef INCLUDED_Level
#define INCLUDED_Level

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <native/display/Sprite.h>
HX_DECLARE_CLASS0(Background)
HX_DECLARE_CLASS0(Button)
HX_DECLARE_CLASS0(EndOfLevel)
HX_DECLARE_CLASS0(Enemy)
HX_DECLARE_CLASS0(GoldPiece)
HX_DECLARE_CLASS0(ICollidable)
HX_DECLARE_CLASS0(IRenderable)
HX_DECLARE_CLASS0(ITickable)
HX_DECLARE_CLASS0(Level)
HX_DECLARE_CLASS0(LevelMap)
HX_DECLARE_CLASS0(Player)
HX_DECLARE_CLASS0(SceneManager)
HX_DECLARE_CLASS0(TileRenderer)
HX_DECLARE_CLASS0(Wall)
HX_DECLARE_CLASS2(native,display,DisplayObject)
HX_DECLARE_CLASS2(native,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(native,display,IBitmapDrawable)
HX_DECLARE_CLASS2(native,display,InteractiveObject)
HX_DECLARE_CLASS2(native,display,Sprite)
HX_DECLARE_CLASS2(native,events,Event)
HX_DECLARE_CLASS2(native,events,EventDispatcher)
HX_DECLARE_CLASS2(native,events,IEventDispatcher)
HX_DECLARE_CLASS2(native,events,MouseEvent)
HX_DECLARE_CLASS2(native,events,TouchEvent)
HX_DECLARE_CLASS2(native,net,SharedObject)


class Level_obj : public ::native::display::Sprite_obj{
	public:
		typedef ::native::display::Sprite_obj super;
		typedef Level_obj OBJ_;
		Level_obj();
		Void __construct(::TileRenderer tileRenderer);

	public:
		static hx::ObjectPtr< Level_obj > __new(::TileRenderer tileRenderer);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~Level_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Level"); }

		virtual Void dummyFunction( );
		Dynamic dummyFunction_dyn();

		virtual Void cleanup( );
		Dynamic cleanup_dyn();

		virtual Void returnToMainMenu( );
		Dynamic returnToMainMenu_dyn();

		virtual Void endLevel( );
		Dynamic endLevel_dyn();

		virtual Void onTouchMove( ::native::events::TouchEvent event);
		Dynamic onTouchMove_dyn();

		virtual Void onTouchEnd( ::native::events::TouchEvent event);
		Dynamic onTouchEnd_dyn();

		virtual Void onTouchBegin( ::native::events::TouchEvent event);
		Dynamic onTouchBegin_dyn();

		virtual Void onEnterFrame( ::native::events::Event event);
		Dynamic onEnterFrame_dyn();

		virtual Void onAddedToStage( ::native::events::Event event);
		Dynamic onAddedToStage_dyn();

		int previousTickTime; /* REM */ 
		::Button mainMenuButton; /* REM */ 
		int deathScreenCountdown; /* REM */ 
		Array< ::Background > backgrounds; /* REM */ 
		Array< ::Wall > walls; /* REM */ 
		::EndOfLevel levelEnd; /* REM */ 
		Array< ::GoldPiece > coins; /* REM */ 
		Array< ::Enemy > enemies; /* REM */ 
		::Player player; /* REM */ 
		::LevelMap map; /* REM */ 
		::SceneManager sceneManager; /* REM */ 
		::TileRenderer renderer; /* REM */ 
		static Void initializeLocalData( ::native::net::SharedObject localData);
		static Dynamic initializeLocalData_dyn();

		static Void newGame( ::native::net::SharedObject localData);
		static Dynamic newGame_dyn();

};


#endif /* INCLUDED_Level */ 
