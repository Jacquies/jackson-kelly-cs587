#include <hxcpp.h>

#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IEnemyState
#include <IEnemyState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif

HX_DEFINE_DYNAMIC_FUNC1(IEnemyState_obj,hitWall,)

HX_DEFINE_DYNAMIC_FUNC2(IEnemyState_obj,tick,)


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IEnemyState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IEnemyState_obj::__mClass,"__mClass");
};

Class IEnemyState_obj::__mClass;

void IEnemyState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("IEnemyState"), hx::TCanCast< IEnemyState_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IEnemyState_obj::__boot()
{
}

