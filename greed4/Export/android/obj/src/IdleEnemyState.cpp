#include <hxcpp.h>

#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IEnemyState
#include <IEnemyState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_IdleEnemyState
#include <IdleEnemyState.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif

Void IdleEnemyState_obj::__construct(Array< ::IEnemyState > states)
{
HX_STACK_PUSH("IdleEnemyState::new","IdleEnemyState.hx",10);
{
	HX_STACK_LINE(10)
	this->nextStates = states;
}
;
	return null();
}

IdleEnemyState_obj::~IdleEnemyState_obj() { }

Dynamic IdleEnemyState_obj::__CreateEmpty() { return  new IdleEnemyState_obj; }
hx::ObjectPtr< IdleEnemyState_obj > IdleEnemyState_obj::__new(Array< ::IEnemyState > states)
{  hx::ObjectPtr< IdleEnemyState_obj > result = new IdleEnemyState_obj();
	result->__construct(states);
	return result;}

Dynamic IdleEnemyState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< IdleEnemyState_obj > result = new IdleEnemyState_obj();
	result->__construct(inArgs[0]);
	return result;}

hx::Object *IdleEnemyState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IEnemyState_obj)) return operator ::IEnemyState_obj *();
	return super::__ToInterface(inType);
}

Void IdleEnemyState_obj::hitWall( ::ICollidable wallCollidable){
{
		HX_STACK_PUSH("IdleEnemyState::hitWall","IdleEnemyState.hx",28);
		HX_STACK_THIS(this);
		HX_STACK_ARG(wallCollidable,"wallCollidable");
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(IdleEnemyState_obj,hitWall,(void))

Void IdleEnemyState_obj::tick( int _tmp_deltaTime,::Enemy enemy){
{
		HX_STACK_PUSH("IdleEnemyState::tick","IdleEnemyState.hx",14);
		HX_STACK_THIS(this);
		HX_STACK_ARG(_tmp_deltaTime,"_tmp_deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(14)
		Float deltaTime = _tmp_deltaTime;		HX_STACK_VAR(deltaTime,"deltaTime");
		HX_STACK_LINE(14)
		if (((bool((::SceneManager_obj::squareDistance(enemy->getPosition(),::Enemy_obj::player->getPosition()) < ::Math_obj::pow(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("aggroRadius"),true),(int)2))) && bool(!(::SceneManager_obj::currentSceneManager->isIdle()))))){
			HX_STACK_LINE(18)
			enemy->exitState(hx::ObjectPtr<OBJ_>(this));
			HX_STACK_LINE(20)
			enemy->switchAnimation((int)1);
			HX_STACK_LINE(22)
			{
				HX_STACK_LINE(22)
				int _g = (int)0;		HX_STACK_VAR(_g,"_g");
				Array< ::IEnemyState > _g1 = this->nextStates;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(22)
				while(((_g < _g1->length))){
					HX_STACK_LINE(22)
					::IEnemyState state = _g1->__get(_g);		HX_STACK_VAR(state,"state");
					HX_STACK_LINE(22)
					++(_g);
					HX_STACK_LINE(23)
					enemy->enterState(state);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(IdleEnemyState_obj,tick,(void))


IdleEnemyState_obj::IdleEnemyState_obj()
{
}

void IdleEnemyState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(IdleEnemyState);
	HX_MARK_MEMBER_NAME(nextStates,"nextStates");
	HX_MARK_END_CLASS();
}

void IdleEnemyState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(nextStates,"nextStates");
}

Dynamic IdleEnemyState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"hitWall") ) { return hitWall_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"nextStates") ) { return nextStates; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic IdleEnemyState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 10:
		if (HX_FIELD_EQ(inName,"nextStates") ) { nextStates=inValue.Cast< Array< ::IEnemyState > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void IdleEnemyState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("nextStates"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("hitWall"),
	HX_CSTRING("tick"),
	HX_CSTRING("nextStates"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IdleEnemyState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IdleEnemyState_obj::__mClass,"__mClass");
};

Class IdleEnemyState_obj::__mClass;

void IdleEnemyState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("IdleEnemyState"), hx::TCanCast< IdleEnemyState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IdleEnemyState_obj::__boot()
{
}

