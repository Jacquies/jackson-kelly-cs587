#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_Background
#include <Background.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void Background_obj::__construct(::native::geom::Point spawnPoint,::TileRenderer tileRenderer)
{
HX_STACK_PUSH("Background::new","Background.hx",11);
{
	HX_STACK_LINE(12)
	Float tempRand = ::Math_obj::random();		HX_STACK_VAR(tempRand,"tempRand");
	HX_STACK_LINE(13)
	int spriteNum;		HX_STACK_VAR(spriteNum,"spriteNum");
	HX_STACK_LINE(15)
	this->animation = ::Animation_obj::__new((int)53,(int)0,(int)30,spawnPoint->x,spawnPoint->y,((int)90 * ::Math_obj::floor((::Math_obj::random() * (int)4))),(int)0,tileRenderer,null());
}
;
	return null();
}

Background_obj::~Background_obj() { }

Dynamic Background_obj::__CreateEmpty() { return  new Background_obj; }
hx::ObjectPtr< Background_obj > Background_obj::__new(::native::geom::Point spawnPoint,::TileRenderer tileRenderer)
{  hx::ObjectPtr< Background_obj > result = new Background_obj();
	result->__construct(spawnPoint,tileRenderer);
	return result;}

Dynamic Background_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Background_obj > result = new Background_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

hx::Object *Background_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IRenderable_obj)) return operator ::IRenderable_obj *();
	return super::__ToInterface(inType);
}

Void Background_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("Background::render","Background.hx",18);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(18)
		this->animation->render(deltaTime,camera);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Background_obj,render,(void))


Background_obj::Background_obj()
{
}

void Background_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Background);
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_END_CLASS();
}

void Background_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(animation,"animation");
}

Dynamic Background_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Background_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::Animation >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Background_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("animation"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("render"),
	HX_CSTRING("animation"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Background_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Background_obj::__mClass,"__mClass");
};

Class Background_obj::__mClass;

void Background_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("Background"), hx::TCanCast< Background_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Background_obj::__boot()
{
}

