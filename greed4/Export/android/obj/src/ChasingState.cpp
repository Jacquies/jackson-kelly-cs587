#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_ChasingState
#include <ChasingState.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_IState
#include <IState.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void ChasingState_obj::__construct()
{
HX_STACK_PUSH("ChasingState::new","ChasingState.hx",8);
{
}
;
	return null();
}

ChasingState_obj::~ChasingState_obj() { }

Dynamic ChasingState_obj::__CreateEmpty() { return  new ChasingState_obj; }
hx::ObjectPtr< ChasingState_obj > ChasingState_obj::__new()
{  hx::ObjectPtr< ChasingState_obj > result = new ChasingState_obj();
	result->__construct();
	return result;}

Dynamic ChasingState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< ChasingState_obj > result = new ChasingState_obj();
	result->__construct();
	return result;}

hx::Object *ChasingState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IState_obj)) return operator ::IState_obj *();
	return super::__ToInterface(inType);
}

Void ChasingState_obj::tick( Float deltaTime,::Enemy enemy){
{
		HX_STACK_PUSH("ChasingState::tick","ChasingState.hx",12);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(14)
		if (((::Enemy_obj::player->getAnimation()->getPosition()->y > enemy->getAnimation()->getPosition()->y))){
			HX_STACK_LINE(14)
			enemy->getAnimation()->setRotation((::SceneManager_obj::usingFlash * (((int)180 - (Float(((int)180 * ::Math_obj::atan((Float(((::Enemy_obj::player->getAnimation()->getPosition()->x - enemy->getAnimation()->getPosition()->x))) / Float(((::Enemy_obj::player->getAnimation()->getPosition()->y - enemy->getAnimation()->getPosition()->y))))))) / Float(::Math_obj::PI))))));
		}
		else{
			HX_STACK_LINE(16)
			if (((::Enemy_obj::player->getAnimation()->getPosition()->y < enemy->getAnimation()->getPosition()->y))){
				HX_STACK_LINE(16)
				enemy->getAnimation()->setRotation((::SceneManager_obj::usingFlash * ((Float(((int)-180 * ::Math_obj::atan((Float(((::Enemy_obj::player->getAnimation()->getPosition()->x - enemy->getAnimation()->getPosition()->x))) / Float(((::Enemy_obj::player->getAnimation()->getPosition()->y - enemy->getAnimation()->getPosition()->y))))))) / Float(::Math_obj::PI)))));
			}
		}
		HX_STACK_LINE(21)
		enemy->getAnimation()->setX((enemy->getAnimation()->getPosition()->x + (Float((((int)2 * ::Math_obj::sin((Float(((::SceneManager_obj::usingFlash * enemy->getAnimation()->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime)) / Float((int)30))));
		HX_STACK_LINE(22)
		enemy->getAnimation()->setY((enemy->getAnimation()->getPosition()->y - (Float((((int)2 * ::Math_obj::cos((Float(((::SceneManager_obj::usingFlash * enemy->getAnimation()->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime)) / Float((int)30))));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(ChasingState_obj,tick,(void))


ChasingState_obj::ChasingState_obj()
{
}

void ChasingState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(ChasingState);
	HX_MARK_END_CLASS();
}

void ChasingState_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic ChasingState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic ChasingState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void ChasingState_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("tick"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ChasingState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ChasingState_obj::__mClass,"__mClass");
};

Class ChasingState_obj::__mClass;

void ChasingState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("ChasingState"), hx::TCanCast< ChasingState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void ChasingState_obj::__boot()
{
}

