#include <hxcpp.h>

#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif

HX_DEFINE_DYNAMIC_FUNC1(ITickable_obj,tick,)


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ITickable_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ITickable_obj::__mClass,"__mClass");
};

Class ITickable_obj::__mClass;

void ITickable_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("ITickable"), hx::TCanCast< ITickable_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void ITickable_obj::__boot()
{
}

