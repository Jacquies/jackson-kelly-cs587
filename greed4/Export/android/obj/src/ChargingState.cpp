#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_ChargingState
#include <ChargingState.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_IState
#include <IState.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void ChargingState_obj::__construct()
{
HX_STACK_PUSH("ChargingState::new","ChargingState.hx",11);
{
	HX_STACK_LINE(12)
	this->speed = (int)1;
	HX_STACK_LINE(13)
	this->rotation = (::Math_obj::random() * (int)360);
}
;
	return null();
}

ChargingState_obj::~ChargingState_obj() { }

Dynamic ChargingState_obj::__CreateEmpty() { return  new ChargingState_obj; }
hx::ObjectPtr< ChargingState_obj > ChargingState_obj::__new()
{  hx::ObjectPtr< ChargingState_obj > result = new ChargingState_obj();
	result->__construct();
	return result;}

Dynamic ChargingState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< ChargingState_obj > result = new ChargingState_obj();
	result->__construct();
	return result;}

hx::Object *ChargingState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IState_obj)) return operator ::IState_obj *();
	return super::__ToInterface(inType);
}

Void ChargingState_obj::tick( Float deltaTime,::Enemy enemy){
{
		HX_STACK_PUSH("ChargingState::tick","ChargingState.hx",16);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(17)
		enemy->getAnimation()->setRotation(this->rotation);
		HX_STACK_LINE(20)
		enemy->getAnimation()->setX((enemy->getAnimation()->getPosition()->x + (Float(((this->speed * ::Math_obj::sin((Float(((::SceneManager_obj::usingFlash * enemy->getAnimation()->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime)) / Float((int)30))));
		HX_STACK_LINE(21)
		enemy->getAnimation()->setY((enemy->getAnimation()->getPosition()->y - (Float(((this->speed * ::Math_obj::cos((Float(((::SceneManager_obj::usingFlash * enemy->getAnimation()->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime)) / Float((int)30))));
		HX_STACK_LINE(23)
		hx::MultEq(this->speed,1.025);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(ChargingState_obj,tick,(void))


ChargingState_obj::ChargingState_obj()
{
}

void ChargingState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(ChargingState);
	HX_MARK_MEMBER_NAME(rotation,"rotation");
	HX_MARK_MEMBER_NAME(speed,"speed");
	HX_MARK_END_CLASS();
}

void ChargingState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(rotation,"rotation");
	HX_VISIT_MEMBER_NAME(speed,"speed");
}

Dynamic ChargingState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"speed") ) { return speed; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"rotation") ) { return rotation; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic ChargingState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"speed") ) { speed=inValue.Cast< Float >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"rotation") ) { rotation=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void ChargingState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("rotation"));
	outFields->push(HX_CSTRING("speed"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("tick"),
	HX_CSTRING("rotation"),
	HX_CSTRING("speed"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ChargingState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ChargingState_obj::__mClass,"__mClass");
};

Class ChargingState_obj::__mClass;

void ChargingState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("ChargingState"), hx::TCanCast< ChargingState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void ChargingState_obj::__boot()
{
}

