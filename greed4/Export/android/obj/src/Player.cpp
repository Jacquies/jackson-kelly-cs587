#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_Wall
#include <Wall.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_geom_Rectangle
#include <native/geom/Rectangle.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif

Void Player_obj::__construct(::TileRenderer tileRenderer)
{
HX_STACK_PUSH("Player::new","Player.hx",20);
{
	HX_STACK_LINE(21)
	this->renderer = tileRenderer;
	HX_STACK_LINE(22)
	this->collider = ::Collider_obj::__new((int)0,(int)0,(int)20,(int)20,::Collider_obj::TYPE_PLAYER);
	HX_STACK_LINE(24)
	this->position = ::native::geom::Point_obj::__new((int)0,(int)0);
	HX_STACK_LINE(25)
	this->rotation = (int)0;
	HX_STACK_LINE(26)
	this->currentAnimationID = (int)0;
	HX_STACK_LINE(27)
	this->hitCountdown = (int)0;
	HX_STACK_LINE(29)
	this->animations = Array_obj< ::Animation >::__new();
	HX_STACK_LINE(30)
	this->animations->push(::Animation_obj::__new((int)62,(int)0,(int)30,(int)0,(int)0,(int)0,(int)1,tileRenderer,null()));
	HX_STACK_LINE(31)
	this->animations->push(::Animation_obj::__new((int)70,(int)3,(int)10,(int)0,(int)0,(int)0,(int)1,tileRenderer,false));
	HX_STACK_LINE(33)
	{
		HX_STACK_LINE(33)
		int _g1 = (int)1;		HX_STACK_VAR(_g1,"_g1");
		int _g = this->animations->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(33)
		while(((_g1 < _g))){
			HX_STACK_LINE(33)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(34)
			this->animations->__get(i)->removeFromRenderSet();
		}
	}
	HX_STACK_LINE(37)
	this->animation = this->animations->__get(this->currentAnimationID);
}
;
	return null();
}

Player_obj::~Player_obj() { }

Dynamic Player_obj::__CreateEmpty() { return  new Player_obj; }
hx::ObjectPtr< Player_obj > Player_obj::__new(::TileRenderer tileRenderer)
{  hx::ObjectPtr< Player_obj > result = new Player_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic Player_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Player_obj > result = new Player_obj();
	result->__construct(inArgs[0]);
	return result;}

hx::Object *Player_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::ITickable_obj)) return operator ::ITickable_obj *();
	if (inType==typeid( ::IRenderable_obj)) return operator ::IRenderable_obj *();
	if (inType==typeid( ::ICollidable_obj)) return operator ::ICollidable_obj *();
	return super::__ToInterface(inType);
}

Void Player_obj::collide( ::ICollidable otherCollidable){
{
		HX_STACK_PUSH("Player::collide","Player.hx",177);
		HX_STACK_THIS(this);
		HX_STACK_ARG(otherCollidable,"otherCollidable");
		HX_STACK_LINE(177)
		if (((otherCollidable->getCollider()->getType() == ::Collider_obj::TYPE_WALL))){
			HX_STACK_LINE(180)
			::Wall wall = hx::TCast< Wall >::cast(otherCollidable);		HX_STACK_VAR(wall,"wall");
			HX_STACK_LINE(182)
			if ((::SceneManager_obj::currentSceneManager->getMouseDown())){
				HX_STACK_LINE(182)
				switch( (int)(wall->getDirection())){
					case (int)0: {
						HX_STACK_LINE(184)
						hx::AddEq(this->position->x,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width + (int)1));
					}
					;break;
					case (int)1: {
						HX_STACK_LINE(186)
						hx::SubEq(this->position->y,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height + (int)1));
					}
					;break;
					case (int)2: {
						HX_STACK_LINE(188)
						hx::SubEq(this->position->x,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width + (int)1));
					}
					;break;
					case (int)3: {
						HX_STACK_LINE(190)
						hx::AddEq(this->position->y,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height + (int)1));
					}
					;break;
				}
			}
		}
		else{
			HX_STACK_LINE(194)
			if (((otherCollidable->getCollider()->getType() == ::Collider_obj::TYPE_PROJECTILE))){
				HX_STACK_LINE(194)
				this->decreaseHealth();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,collide,(void))

int Player_obj::getHitCountdown( ){
	HX_STACK_PUSH("Player::getHitCountdown","Player.hx",173);
	HX_STACK_THIS(this);
	HX_STACK_LINE(173)
	return this->hitCountdown;
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,getHitCountdown,return )

Void Player_obj::decreaseHealth( ){
{
		HX_STACK_PUSH("Player::decreaseHealth","Player.hx",156);
		HX_STACK_THIS(this);
		HX_STACK_LINE(156)
		if (((this->hitCountdown <= (int)0))){
			HX_STACK_LINE(159)
			this->hitCountdown = (int)30;
			HX_STACK_LINE(161)
			if (((::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) > (int)0))){
				HX_STACK_LINE(162)
				(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("health")))--;
				HX_STACK_LINE(163)
				::SceneManager_obj::currentSceneManager->updateHealthBar();
			}
			HX_STACK_LINE(166)
			if (((::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) <= (int)0))){
				HX_STACK_LINE(167)
				::SceneManager_obj::currentSceneManager->setIdle(true);
				HX_STACK_LINE(168)
				::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("newGame")) = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,decreaseHealth,(void))

Void Player_obj::hitWall( ::ICollidable wallCollidable){
{
		HX_STACK_PUSH("Player::hitWall","Player.hx",138);
		HX_STACK_THIS(this);
		HX_STACK_ARG(wallCollidable,"wallCollidable");
		HX_STACK_LINE(140)
		::Wall wall = hx::TCast< Wall >::cast(wallCollidable);		HX_STACK_VAR(wall,"wall");
		HX_STACK_LINE(142)
		if ((::SceneManager_obj::currentSceneManager->getMouseDown())){
			HX_STACK_LINE(142)
			switch( (int)(wall->getDirection())){
				case (int)0: {
					HX_STACK_LINE(144)
					hx::AddEq(this->position->x,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width + (int)1));
				}
				;break;
				case (int)1: {
					HX_STACK_LINE(146)
					hx::SubEq(this->position->y,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height + (int)1));
				}
				;break;
				case (int)2: {
					HX_STACK_LINE(148)
					hx::SubEq(this->position->x,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width + (int)1));
				}
				;break;
				case (int)3: {
					HX_STACK_LINE(150)
					hx::AddEq(this->position->y,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height + (int)1));
				}
				;break;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,hitWall,(void))

Void Player_obj::hit( ){
{
		HX_STACK_PUSH("Player::hit","Player.hx",122);
		HX_STACK_THIS(this);
		HX_STACK_LINE(122)
		if (((this->hitCountdown <= (int)0))){
			HX_STACK_LINE(124)
			this->hitCountdown = (int)30;
			HX_STACK_LINE(126)
			if (((::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) > (int)0))){
				HX_STACK_LINE(127)
				(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("health")))--;
				HX_STACK_LINE(128)
				::SceneManager_obj::currentSceneManager->updateHealthBar();
			}
			HX_STACK_LINE(131)
			if (((::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) <= (int)0))){
				HX_STACK_LINE(132)
				::SceneManager_obj::currentSceneManager->setIdle(true);
				HX_STACK_LINE(133)
				::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("newGame")) = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,hit,(void))

Void Player_obj::switchAnimation( int ID){
{
		HX_STACK_PUSH("Player::switchAnimation","Player.hx",110);
		HX_STACK_THIS(this);
		HX_STACK_ARG(ID,"ID");
		HX_STACK_LINE(110)
		if (((this->currentAnimationID != ID))){
			HX_STACK_LINE(112)
			this->animation->removeFromRenderSet();
			HX_STACK_LINE(113)
			this->animation = this->animations->__get(ID);
			HX_STACK_LINE(114)
			this->animation->setX(this->position->x);
			HX_STACK_LINE(115)
			this->animation->setY(this->position->y);
			HX_STACK_LINE(116)
			this->animation->setRotation(this->rotation);
			HX_STACK_LINE(117)
			this->animation->addToRenderSet();
			HX_STACK_LINE(118)
			this->currentAnimationID = ID;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,switchAnimation,(void))

Void Player_obj::tick( int deltaTime){
{
		HX_STACK_PUSH("Player::tick","Player.hx",76);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_LINE(77)
		if ((this->animation->isFinished())){
			HX_STACK_LINE(78)
			this->switchAnimation((int)0);
		}
		HX_STACK_LINE(80)
		if (((this->hitCountdown > (int)0))){
			HX_STACK_LINE(81)
			(this->hitCountdown)--;
		}
		HX_STACK_LINE(83)
		if ((::SceneManager_obj::currentSceneManager->getMouseDown())){
			HX_STACK_LINE(85)
			if (((::SceneManager_obj::currentSceneManager->getMousePosition()->y > (Float(::TileRenderer_obj::stageHeight) / Float((int)2))))){
				HX_STACK_LINE(85)
				this->rotation = (::SceneManager_obj::usingFlash * (((int)180 - (Float((((Float(((int)180 * ::Math_obj::atan((Float(((::SceneManager_obj::currentSceneManager->getMousePosition()->x - (Float(::TileRenderer_obj::stageWidth) / Float((int)2))))) / Float(((::SceneManager_obj::currentSceneManager->getMousePosition()->y - (Float(::TileRenderer_obj::stageHeight) / Float((int)2))))))))) / Float(::Math_obj::PI)) * deltaTime) * (int)3)) / Float((int)100)))));
			}
			else{
				HX_STACK_LINE(87)
				if (((::SceneManager_obj::currentSceneManager->getMousePosition()->y < (Float(::TileRenderer_obj::stageHeight) / Float((int)2))))){
					HX_STACK_LINE(87)
					this->rotation = (::SceneManager_obj::usingFlash * ((Float((((Float(((int)-180 * ::Math_obj::atan((Float(((::SceneManager_obj::currentSceneManager->getMousePosition()->x - (Float(::TileRenderer_obj::stageWidth) / Float((int)2))))) / Float(((::SceneManager_obj::currentSceneManager->getMousePosition()->y - (Float(::TileRenderer_obj::stageHeight) / Float((int)2))))))))) / Float(::Math_obj::PI)) * deltaTime) * (int)3)) / Float((int)100))));
				}
			}
			HX_STACK_LINE(91)
			this->animation->setRotation(this->rotation);
			HX_STACK_LINE(94)
			hx::AddEq(this->position->x,(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("speed"),true) * ::Math_obj::sin((Float(((::SceneManager_obj::usingFlash * this->rotation) * ::Math_obj::PI)) / Float((int)180)))));
			HX_STACK_LINE(95)
			hx::SubEq(this->position->y,(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("speed"),true) * ::Math_obj::cos((Float(((::SceneManager_obj::usingFlash * this->rotation) * ::Math_obj::PI)) / Float((int)180)))));
			HX_STACK_LINE(97)
			this->animation->setX(this->position->x);
			HX_STACK_LINE(98)
			this->animation->setY(this->position->y);
			HX_STACK_LINE(99)
			this->collider->setX(this->position->x);
			HX_STACK_LINE(100)
			this->collider->setY(this->position->y);
			HX_STACK_LINE(101)
			::SceneManager_obj::currentSceneManager->setCameraX(this->position->x);
			HX_STACK_LINE(102)
			::SceneManager_obj::currentSceneManager->setCameraY(this->position->y);
		}
		HX_STACK_LINE(105)
		if (((::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) <= (int)0))){
			HX_STACK_LINE(105)
			this->switchAnimation((int)1);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,tick,(void))

Void Player_obj::translateY( Float dy){
{
		HX_STACK_PUSH("Player::translateY","Player.hx",72);
		HX_STACK_THIS(this);
		HX_STACK_ARG(dy,"dy");
		HX_STACK_LINE(72)
		this->animation->setY((this->animation->getPosition()->y + dy));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,translateY,(void))

Void Player_obj::translateX( Float dx){
{
		HX_STACK_PUSH("Player::translateX","Player.hx",68);
		HX_STACK_THIS(this);
		HX_STACK_ARG(dx,"dx");
		HX_STACK_LINE(68)
		this->animation->setX((this->animation->getPosition()->x + dx));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,translateX,(void))

Void Player_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("Player::render","Player.hx",64);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(64)
		this->animation->render(deltaTime,camera);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Player_obj,render,(void))

Void Player_obj::setRotation( Float rot){
{
		HX_STACK_PUSH("Player::setRotation","Player.hx",60);
		HX_STACK_THIS(this);
		HX_STACK_ARG(rot,"rot");
		HX_STACK_LINE(60)
		this->rotation = rot;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,setRotation,(void))

Float Player_obj::getRotation( ){
	HX_STACK_PUSH("Player::getRotation","Player.hx",56);
	HX_STACK_THIS(this);
	HX_STACK_LINE(56)
	return this->rotation;
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,getRotation,return )

Void Player_obj::setY( Float y){
{
		HX_STACK_PUSH("Player::setY","Player.hx",52);
		HX_STACK_THIS(this);
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(52)
		this->position->y = y;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,setY,(void))

Void Player_obj::setX( Float x){
{
		HX_STACK_PUSH("Player::setX","Player.hx",48);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_LINE(48)
		this->position->x = x;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,setX,(void))

::native::geom::Point Player_obj::getPosition( ){
	HX_STACK_PUSH("Player::getPosition","Player.hx",44);
	HX_STACK_THIS(this);
	HX_STACK_LINE(44)
	return this->position;
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,getPosition,return )

::Collider Player_obj::getCollider( ){
	HX_STACK_PUSH("Player::getCollider","Player.hx",40);
	HX_STACK_THIS(this);
	HX_STACK_LINE(40)
	return this->collider;
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,getCollider,return )


Player_obj::Player_obj()
{
}

void Player_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Player);
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_MEMBER_NAME(animations,"animations");
	HX_MARK_MEMBER_NAME(hitCountdown,"hitCountdown");
	HX_MARK_MEMBER_NAME(currentAnimationID,"currentAnimationID");
	HX_MARK_MEMBER_NAME(rotation,"rotation");
	HX_MARK_MEMBER_NAME(position,"position");
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_MEMBER_NAME(collider,"collider");
	HX_MARK_END_CLASS();
}

void Player_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	HX_VISIT_MEMBER_NAME(animations,"animations");
	HX_VISIT_MEMBER_NAME(hitCountdown,"hitCountdown");
	HX_VISIT_MEMBER_NAME(currentAnimationID,"currentAnimationID");
	HX_VISIT_MEMBER_NAME(rotation,"rotation");
	HX_VISIT_MEMBER_NAME(position,"position");
	HX_VISIT_MEMBER_NAME(animation,"animation");
	HX_VISIT_MEMBER_NAME(collider,"collider");
}

Dynamic Player_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"hit") ) { return hit_dyn(); }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		if (HX_FIELD_EQ(inName,"setY") ) { return setY_dyn(); }
		if (HX_FIELD_EQ(inName,"setX") ) { return setX_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		if (HX_FIELD_EQ(inName,"hitWall") ) { return hitWall_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		if (HX_FIELD_EQ(inName,"rotation") ) { return rotation; }
		if (HX_FIELD_EQ(inName,"position") ) { return position; }
		if (HX_FIELD_EQ(inName,"collider") ) { return collider; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"translateY") ) { return translateY_dyn(); }
		if (HX_FIELD_EQ(inName,"translateX") ) { return translateX_dyn(); }
		if (HX_FIELD_EQ(inName,"animations") ) { return animations; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"setRotation") ) { return setRotation_dyn(); }
		if (HX_FIELD_EQ(inName,"getRotation") ) { return getRotation_dyn(); }
		if (HX_FIELD_EQ(inName,"getPosition") ) { return getPosition_dyn(); }
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"hitCountdown") ) { return hitCountdown; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"decreaseHealth") ) { return decreaseHealth_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"getHitCountdown") ) { return getHitCountdown_dyn(); }
		if (HX_FIELD_EQ(inName,"switchAnimation") ) { return switchAnimation_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"currentAnimationID") ) { return currentAnimationID; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Player_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::TileRenderer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"rotation") ) { rotation=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"position") ) { position=inValue.Cast< ::native::geom::Point >(); return inValue; }
		if (HX_FIELD_EQ(inName,"collider") ) { collider=inValue.Cast< ::Collider >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::Animation >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"animations") ) { animations=inValue.Cast< Array< ::Animation > >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"hitCountdown") ) { hitCountdown=inValue.Cast< int >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"currentAnimationID") ) { currentAnimationID=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Player_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("renderer"));
	outFields->push(HX_CSTRING("animations"));
	outFields->push(HX_CSTRING("hitCountdown"));
	outFields->push(HX_CSTRING("currentAnimationID"));
	outFields->push(HX_CSTRING("rotation"));
	outFields->push(HX_CSTRING("position"));
	outFields->push(HX_CSTRING("animation"));
	outFields->push(HX_CSTRING("collider"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	HX_CSTRING("getHitCountdown"),
	HX_CSTRING("decreaseHealth"),
	HX_CSTRING("hitWall"),
	HX_CSTRING("hit"),
	HX_CSTRING("switchAnimation"),
	HX_CSTRING("tick"),
	HX_CSTRING("translateY"),
	HX_CSTRING("translateX"),
	HX_CSTRING("render"),
	HX_CSTRING("setRotation"),
	HX_CSTRING("getRotation"),
	HX_CSTRING("setY"),
	HX_CSTRING("setX"),
	HX_CSTRING("getPosition"),
	HX_CSTRING("getCollider"),
	HX_CSTRING("renderer"),
	HX_CSTRING("animations"),
	HX_CSTRING("hitCountdown"),
	HX_CSTRING("currentAnimationID"),
	HX_CSTRING("rotation"),
	HX_CSTRING("position"),
	HX_CSTRING("animation"),
	HX_CSTRING("collider"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Player_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Player_obj::__mClass,"__mClass");
};

Class Player_obj::__mClass;

void Player_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("Player"), hx::TCanCast< Player_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Player_obj::__boot()
{
}

