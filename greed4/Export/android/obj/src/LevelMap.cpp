#include <hxcpp.h>

#ifndef INCLUDED_LevelMap
#include <LevelMap.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void LevelMap_obj::__construct(int difficulty)
{
HX_STACK_PUSH("LevelMap::new","LevelMap.hx",15);
{
	HX_STACK_LINE(17)
	this->mapRadius = (::Math_obj::floor(::Math_obj::sqrt(difficulty)) + (int)1);
	HX_STACK_LINE(18)
	this->map = Array_obj< Array< bool > >::__new();
	HX_STACK_LINE(19)
	Array< Array< bool > > mapWithIslands = Array_obj< Array< bool > >::__new();		HX_STACK_VAR(mapWithIslands,"mapWithIslands");
	HX_STACK_LINE(21)
	{
		HX_STACK_LINE(21)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		int _g = (((int)2 * this->mapRadius) + (int)1);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(21)
		while(((_g1 < _g))){
			HX_STACK_LINE(21)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(22)
			this->map[i] = Array_obj< bool >::__new();
			HX_STACK_LINE(23)
			mapWithIslands[i] = Array_obj< bool >::__new();
			HX_STACK_LINE(24)
			{
				HX_STACK_LINE(24)
				int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
				int _g2 = (((int)2 * this->mapRadius) + (int)1);		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(24)
				while(((_g3 < _g2))){
					HX_STACK_LINE(24)
					int j = (_g3)++;		HX_STACK_VAR(j,"j");
					HX_STACK_LINE(25)
					this->map->__get(i)[j] = false;
					HX_STACK_LINE(26)
					mapWithIslands->__get(i)[j] = false;
				}
			}
		}
	}
	HX_STACK_LINE(30)
	int distanceFromCenter;		HX_STACK_VAR(distanceFromCenter,"distanceFromCenter");
	HX_STACK_LINE(31)
	{
		HX_STACK_LINE(31)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		int _g = this->map->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(31)
		while(((_g1 < _g))){
			HX_STACK_LINE(31)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(32)
			{
				HX_STACK_LINE(32)
				int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
				int _g2 = this->map->__get(i)->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(32)
				while(((_g3 < _g2))){
					HX_STACK_LINE(32)
					int j = (_g3)++;		HX_STACK_VAR(j,"j");
					HX_STACK_LINE(33)
					distanceFromCenter = (::Math_obj::floor(::Math_obj::max(::Math_obj::abs((this->mapRadius - i)),::Math_obj::abs((this->mapRadius - j)))) - (int)1);
					HX_STACK_LINE(35)
					if (((::Math_obj::random() > (Float(distanceFromCenter) / Float(((this->mapRadius + 0.))))))){
						HX_STACK_LINE(35)
						mapWithIslands->__get(i)[j] = true;
					}
				}
			}
		}
	}
	HX_STACK_LINE(41)
	this->removeIslands(this->mapRadius,this->mapRadius,mapWithIslands);
}
;
	return null();
}

LevelMap_obj::~LevelMap_obj() { }

Dynamic LevelMap_obj::__CreateEmpty() { return  new LevelMap_obj; }
hx::ObjectPtr< LevelMap_obj > LevelMap_obj::__new(int difficulty)
{  hx::ObjectPtr< LevelMap_obj > result = new LevelMap_obj();
	result->__construct(difficulty);
	return result;}

Dynamic LevelMap_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< LevelMap_obj > result = new LevelMap_obj();
	result->__construct(inArgs[0]);
	return result;}

Void LevelMap_obj::printMap( ){
{
		HX_STACK_PUSH("LevelMap::printMap","LevelMap.hx",142);
		HX_STACK_THIS(this);
		HX_STACK_LINE(143)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		int _g = this->map->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(143)
		while(((_g1 < _g))){
			HX_STACK_LINE(143)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(144)
			::haxe::Log_obj::trace(this->map->__get(i),hx::SourceInfo(HX_CSTRING("LevelMap.hx"),144,HX_CSTRING("LevelMap"),HX_CSTRING("printMap")));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(LevelMap_obj,printMap,(void))

Void LevelMap_obj::removeIslands( int i,int j,Array< Array< bool > > tempMap){
{
		HX_STACK_PUSH("LevelMap::removeIslands","LevelMap.hx",126);
		HX_STACK_THIS(this);
		HX_STACK_ARG(i,"i");
		HX_STACK_ARG(j,"j");
		HX_STACK_ARG(tempMap,"tempMap");
		HX_STACK_LINE(127)
		this->map->__get(i)[j] = true;
		HX_STACK_LINE(128)
		if (((bool((bool((j < ((int)2 * this->mapRadius))) && bool(!(this->map->__get(i)->__get((j + (int)1)))))) && bool(tempMap->__get(i)->__get((j + (int)1)))))){
			HX_STACK_LINE(128)
			this->removeIslands(i,(j + (int)1),tempMap);
		}
		HX_STACK_LINE(131)
		if (((bool((bool((j > (int)0)) && bool(!(this->map->__get(i)->__get((j - (int)1)))))) && bool(tempMap->__get(i)->__get((j - (int)1)))))){
			HX_STACK_LINE(131)
			this->removeIslands(i,(j - (int)1),tempMap);
		}
		HX_STACK_LINE(134)
		if (((bool((bool((i < ((int)2 * this->mapRadius))) && bool(!(this->map->__get((i + (int)1))->__get(j))))) && bool(tempMap->__get((i + (int)1))->__get(j))))){
			HX_STACK_LINE(134)
			this->removeIslands((i + (int)1),j,tempMap);
		}
		HX_STACK_LINE(137)
		if (((bool((bool((i > (int)0)) && bool(!(this->map->__get((i - (int)1))->__get(j))))) && bool(tempMap->__get((i - (int)1))->__get(j))))){
			HX_STACK_LINE(137)
			this->removeIslands((i - (int)1),j,tempMap);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC3(LevelMap_obj,removeIslands,(void))

Array< ::native::geom::Point > LevelMap_obj::generateBackgroundPositions( ){
	HX_STACK_PUSH("LevelMap::generateBackgroundPositions","LevelMap.hx",112);
	HX_STACK_THIS(this);
	HX_STACK_LINE(113)
	Array< ::native::geom::Point > points = Array_obj< ::native::geom::Point >::__new();		HX_STACK_VAR(points,"points");
	HX_STACK_LINE(115)
	{
		HX_STACK_LINE(115)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		int _g = this->map->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(115)
		while(((_g1 < _g))){
			HX_STACK_LINE(115)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(116)
			{
				HX_STACK_LINE(116)
				int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
				int _g2 = this->map->__get(i)->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(116)
				while(((_g3 < _g2))){
					HX_STACK_LINE(116)
					int j = (_g3)++;		HX_STACK_VAR(j,"j");
					HX_STACK_LINE(117)
					if ((this->map->__get(i)->__get(j))){
						HX_STACK_LINE(117)
						points->push(::native::geom::Point_obj::__new((((j - this->mapRadius)) * (int)180),(((i - this->mapRadius)) * (int)180)));
					}
				}
			}
		}
	}
	HX_STACK_LINE(123)
	return points;
}


HX_DEFINE_DYNAMIC_FUNC0(LevelMap_obj,generateBackgroundPositions,return )

Array< ::native::geom::Point > LevelMap_obj::generateWallPositions( Array< int > wallDirections){
	HX_STACK_PUSH("LevelMap::generateWallPositions","LevelMap.hx",75);
	HX_STACK_THIS(this);
	HX_STACK_ARG(wallDirections,"wallDirections");
	HX_STACK_LINE(76)
	Array< ::native::geom::Point > points = Array_obj< ::native::geom::Point >::__new();		HX_STACK_VAR(points,"points");
	HX_STACK_LINE(77)
	wallDirections->splice((int)0,wallDirections->length);
	HX_STACK_LINE(79)
	{
		HX_STACK_LINE(79)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		int _g = this->map->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(79)
		while(((_g1 < _g))){
			HX_STACK_LINE(79)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(80)
			{
				HX_STACK_LINE(80)
				int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
				int _g2 = this->map->__get(i)->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(80)
				while(((_g3 < _g2))){
					HX_STACK_LINE(80)
					int j = (_g3)++;		HX_STACK_VAR(j,"j");
					HX_STACK_LINE(81)
					if ((this->map->__get(i)->__get(j))){
						HX_STACK_LINE(83)
						if (((bool((j == (int)0)) || bool(!(this->map->__get(i)->__get((j - (int)1))))))){
							HX_STACK_LINE(84)
							points->push(::native::geom::Point_obj::__new(((((j - this->mapRadius)) * (int)180) - (int)90),(((i - this->mapRadius)) * (int)180)));
							HX_STACK_LINE(85)
							wallDirections->push((int)0);
						}
						HX_STACK_LINE(89)
						if (((bool((j == (this->map->__get(i)->length - (int)1))) || bool(!(this->map->__get(i)->__get((j + (int)1))))))){
							HX_STACK_LINE(90)
							points->push(::native::geom::Point_obj::__new(((((j - this->mapRadius)) * (int)180) + (int)90),(((i - this->mapRadius)) * (int)180)));
							HX_STACK_LINE(91)
							wallDirections->push((int)2);
						}
						HX_STACK_LINE(95)
						if (((bool((i == (int)0)) || bool(!(this->map->__get((i - (int)1))->__get(j)))))){
							HX_STACK_LINE(96)
							points->push(::native::geom::Point_obj::__new((((j - this->mapRadius)) * (int)180),((((i - this->mapRadius)) * (int)180) - (int)90)));
							HX_STACK_LINE(97)
							wallDirections->push((int)3);
						}
						HX_STACK_LINE(101)
						if (((bool((i == (this->map->length - (int)1))) || bool(!(this->map->__get((i + (int)1))->__get(j)))))){
							HX_STACK_LINE(102)
							points->push(::native::geom::Point_obj::__new((((j - this->mapRadius)) * (int)180),((((i - this->mapRadius)) * (int)180) + (int)90)));
							HX_STACK_LINE(103)
							wallDirections->push((int)1);
						}
					}
				}
			}
		}
	}
	HX_STACK_LINE(109)
	return points;
}


HX_DEFINE_DYNAMIC_FUNC1(LevelMap_obj,generateWallPositions,return )

Array< ::native::geom::Point > LevelMap_obj::generateEnemiesOrCoins( int difficulty,int denseness){
	HX_STACK_PUSH("LevelMap::generateEnemiesOrCoins","LevelMap.hx",56);
	HX_STACK_THIS(this);
	HX_STACK_ARG(difficulty,"difficulty");
	HX_STACK_ARG(denseness,"denseness");
	HX_STACK_LINE(57)
	Array< ::native::geom::Point > points = Array_obj< ::native::geom::Point >::__new();		HX_STACK_VAR(points,"points");
	HX_STACK_LINE(59)
	int distanceFromCenter;		HX_STACK_VAR(distanceFromCenter,"distanceFromCenter");
	HX_STACK_LINE(60)
	{
		HX_STACK_LINE(60)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		int _g = this->map->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(60)
		while(((_g1 < _g))){
			HX_STACK_LINE(60)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(61)
			{
				HX_STACK_LINE(61)
				int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
				int _g2 = this->map->__get(i)->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(61)
				while(((_g3 < _g2))){
					HX_STACK_LINE(61)
					int j = (_g3)++;		HX_STACK_VAR(j,"j");
					HX_STACK_LINE(62)
					if ((this->map->__get(i)->__get(j))){
						HX_STACK_LINE(63)
						distanceFromCenter = ::Math_obj::floor(::Math_obj::max(::Math_obj::abs((this->mapRadius - i)),::Math_obj::abs((this->mapRadius - j))));
						HX_STACK_LINE(65)
						if (((::Math_obj::random() < ((Float(distanceFromCenter) / Float(((((((int)5 - denseness)) * this->mapRadius) + 0.)))) * ((Float(((int)2 * difficulty)) / Float(((difficulty + (int)1))))))))){
							HX_STACK_LINE(65)
							points->push(::native::geom::Point_obj::__new((((((j - this->mapRadius) + ::Math_obj::random())) * (int)180) - (int)90),(((((i - this->mapRadius) + ::Math_obj::random())) * (int)180) - (int)90)));
						}
					}
				}
			}
		}
	}
	HX_STACK_LINE(72)
	return points;
}


HX_DEFINE_DYNAMIC_FUNC2(LevelMap_obj,generateEnemiesOrCoins,return )

::native::geom::Point LevelMap_obj::generateLevelEnd( Dynamic difficulty){
	HX_STACK_PUSH("LevelMap::generateLevelEnd","LevelMap.hx",52);
	HX_STACK_THIS(this);
	HX_STACK_ARG(difficulty,"difficulty");
	HX_STACK_LINE(52)
	return ::native::geom::Point_obj::__new(((::Math_obj::random() * (int)540) - (int)270),((::Math_obj::random() * (int)540) - (int)270));
}


HX_DEFINE_DYNAMIC_FUNC1(LevelMap_obj,generateLevelEnd,return )

Array< ::native::geom::Point > LevelMap_obj::generateCoins( int difficulty){
	HX_STACK_PUSH("LevelMap::generateCoins","LevelMap.hx",48);
	HX_STACK_THIS(this);
	HX_STACK_ARG(difficulty,"difficulty");
	HX_STACK_LINE(48)
	return this->generateEnemiesOrCoins(difficulty,(int)4);
}


HX_DEFINE_DYNAMIC_FUNC1(LevelMap_obj,generateCoins,return )

Array< ::native::geom::Point > LevelMap_obj::generateEnemies( int difficulty){
	HX_STACK_PUSH("LevelMap::generateEnemies","LevelMap.hx",44);
	HX_STACK_THIS(this);
	HX_STACK_ARG(difficulty,"difficulty");
	HX_STACK_LINE(44)
	return this->generateEnemiesOrCoins(difficulty,(int)2);
}


HX_DEFINE_DYNAMIC_FUNC1(LevelMap_obj,generateEnemies,return )

int LevelMap_obj::mapScale;


LevelMap_obj::LevelMap_obj()
{
}

void LevelMap_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(LevelMap);
	HX_MARK_MEMBER_NAME(mapRadius,"mapRadius");
	HX_MARK_MEMBER_NAME(map,"map");
	HX_MARK_END_CLASS();
}

void LevelMap_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(mapRadius,"mapRadius");
	HX_VISIT_MEMBER_NAME(map,"map");
}

Dynamic LevelMap_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"map") ) { return map; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"mapScale") ) { return mapScale; }
		if (HX_FIELD_EQ(inName,"printMap") ) { return printMap_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"mapRadius") ) { return mapRadius; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"removeIslands") ) { return removeIslands_dyn(); }
		if (HX_FIELD_EQ(inName,"generateCoins") ) { return generateCoins_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"generateEnemies") ) { return generateEnemies_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"generateLevelEnd") ) { return generateLevelEnd_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"generateWallPositions") ) { return generateWallPositions_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"generateEnemiesOrCoins") ) { return generateEnemiesOrCoins_dyn(); }
		break;
	case 27:
		if (HX_FIELD_EQ(inName,"generateBackgroundPositions") ) { return generateBackgroundPositions_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic LevelMap_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"map") ) { map=inValue.Cast< Array< Array< bool > > >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"mapScale") ) { mapScale=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"mapRadius") ) { mapRadius=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void LevelMap_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("mapRadius"));
	outFields->push(HX_CSTRING("map"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("mapScale"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("printMap"),
	HX_CSTRING("removeIslands"),
	HX_CSTRING("generateBackgroundPositions"),
	HX_CSTRING("generateWallPositions"),
	HX_CSTRING("generateEnemiesOrCoins"),
	HX_CSTRING("generateLevelEnd"),
	HX_CSTRING("generateCoins"),
	HX_CSTRING("generateEnemies"),
	HX_CSTRING("mapRadius"),
	HX_CSTRING("map"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(LevelMap_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(LevelMap_obj::mapScale,"mapScale");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(LevelMap_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(LevelMap_obj::mapScale,"mapScale");
};

Class LevelMap_obj::__mClass;

void LevelMap_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("LevelMap"), hx::TCanCast< LevelMap_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void LevelMap_obj::__boot()
{
	mapScale= (int)180;
}

