#include <hxcpp.h>

#ifndef INCLUDED_ChargingEnemyState
#include <ChargingEnemyState.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IEnemyState
#include <IEnemyState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void ChargingEnemyState_obj::__construct()
{
HX_STACK_PUSH("ChargingEnemyState::new","ChargingEnemyState.hx",11);
{
	HX_STACK_LINE(12)
	this->speed = (int)1;
	HX_STACK_LINE(13)
	this->rotation = (int)-1;
}
;
	return null();
}

ChargingEnemyState_obj::~ChargingEnemyState_obj() { }

Dynamic ChargingEnemyState_obj::__CreateEmpty() { return  new ChargingEnemyState_obj; }
hx::ObjectPtr< ChargingEnemyState_obj > ChargingEnemyState_obj::__new()
{  hx::ObjectPtr< ChargingEnemyState_obj > result = new ChargingEnemyState_obj();
	result->__construct();
	return result;}

Dynamic ChargingEnemyState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< ChargingEnemyState_obj > result = new ChargingEnemyState_obj();
	result->__construct();
	return result;}

hx::Object *ChargingEnemyState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IEnemyState_obj)) return operator ::IEnemyState_obj *();
	return super::__ToInterface(inType);
}

Void ChargingEnemyState_obj::hitWall( ::ICollidable wallCollidable){
{
		HX_STACK_PUSH("ChargingEnemyState::hitWall","ChargingEnemyState.hx",34);
		HX_STACK_THIS(this);
		HX_STACK_ARG(wallCollidable,"wallCollidable");
		HX_STACK_LINE(35)
		this->speed = (int)1;
		HX_STACK_LINE(36)
		this->rotation = (::Math_obj::random() * (int)360);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(ChargingEnemyState_obj,hitWall,(void))

Void ChargingEnemyState_obj::tick( int deltaTime,::Enemy enemy){
{
		HX_STACK_PUSH("ChargingEnemyState::tick","ChargingEnemyState.hx",16);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(17)
		if (((this->rotation == (int)-1))){
			HX_STACK_LINE(17)
			this->rotation = enemy->getRotation();
		}
		else{
			HX_STACK_LINE(19)
			enemy->setRotation(this->rotation);
		}
		HX_STACK_LINE(24)
		enemy->setX((enemy->getPosition()->x + (Float((((this->speed * ::Math_obj::sin((Float(((::SceneManager_obj::usingFlash * enemy->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime) * (int)3)) / Float((int)100))));
		HX_STACK_LINE(25)
		enemy->setY((enemy->getPosition()->y - (Float((((this->speed * ::Math_obj::cos((Float(((::SceneManager_obj::usingFlash * enemy->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime) * (int)3)) / Float((int)100))));
		HX_STACK_LINE(27)
		if (((this->speed < (int)10))){
			HX_STACK_LINE(27)
			hx::MultEq(this->speed,1.075);
		}
		else{
			HX_STACK_LINE(29)
			this->speed = (int)10;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(ChargingEnemyState_obj,tick,(void))


ChargingEnemyState_obj::ChargingEnemyState_obj()
{
}

void ChargingEnemyState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(ChargingEnemyState);
	HX_MARK_MEMBER_NAME(rotation,"rotation");
	HX_MARK_MEMBER_NAME(speed,"speed");
	HX_MARK_END_CLASS();
}

void ChargingEnemyState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(rotation,"rotation");
	HX_VISIT_MEMBER_NAME(speed,"speed");
}

Dynamic ChargingEnemyState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"speed") ) { return speed; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"hitWall") ) { return hitWall_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"rotation") ) { return rotation; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic ChargingEnemyState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"speed") ) { speed=inValue.Cast< Float >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"rotation") ) { rotation=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void ChargingEnemyState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("rotation"));
	outFields->push(HX_CSTRING("speed"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("hitWall"),
	HX_CSTRING("tick"),
	HX_CSTRING("rotation"),
	HX_CSTRING("speed"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ChargingEnemyState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ChargingEnemyState_obj::__mClass,"__mClass");
};

Class ChargingEnemyState_obj::__mClass;

void ChargingEnemyState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("ChargingEnemyState"), hx::TCanCast< ChargingEnemyState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void ChargingEnemyState_obj::__boot()
{
}

