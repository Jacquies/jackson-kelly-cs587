#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void Animation_obj::__construct(int startingFrame,int length,int framesPerSecond,Float x,Float y,Float rot,int displayLayer,::TileRenderer tileRenderer,hx::Null< bool >  __o_loop)
{
HX_STACK_PUSH("Animation::new","Animation.hx",21);
bool loop = __o_loop.Default(true);
{
	HX_STACK_LINE(22)
	this->startFrame = startingFrame;
	HX_STACK_LINE(23)
	this->animationLength = length;
	HX_STACK_LINE(24)
	this->animationSpeed = (framesPerSecond + (int)1);
	HX_STACK_LINE(25)
	this->currentFrame = (int)0;
	HX_STACK_LINE(26)
	this->currentTime = (int)0;
	HX_STACK_LINE(27)
	this->position = ::native::geom::Point_obj::__new(x,y);
	HX_STACK_LINE(28)
	this->rotation = rot;
	HX_STACK_LINE(29)
	this->layer = displayLayer;
	HX_STACK_LINE(30)
	this->loops = (  (((length == (int)0))) ? bool(false) : bool(loop) );
	HX_STACK_LINE(31)
	this->rendering = false;
	HX_STACK_LINE(33)
	this->renderer = tileRenderer;
	HX_STACK_LINE(34)
	this->addToRenderSet();
}
;
	return null();
}

Animation_obj::~Animation_obj() { }

Dynamic Animation_obj::__CreateEmpty() { return  new Animation_obj; }
hx::ObjectPtr< Animation_obj > Animation_obj::__new(int startingFrame,int length,int framesPerSecond,Float x,Float y,Float rot,int displayLayer,::TileRenderer tileRenderer,hx::Null< bool >  __o_loop)
{  hx::ObjectPtr< Animation_obj > result = new Animation_obj();
	result->__construct(startingFrame,length,framesPerSecond,x,y,rot,displayLayer,tileRenderer,__o_loop);
	return result;}

Dynamic Animation_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Animation_obj > result = new Animation_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4],inArgs[5],inArgs[6],inArgs[7],inArgs[8]);
	return result;}

Void Animation_obj::restartAnimation( ){
{
		HX_STACK_PUSH("Animation::restartAnimation","Animation.hx",101);
		HX_STACK_THIS(this);
		HX_STACK_LINE(101)
		this->currentFrame = (int)0;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Animation_obj,restartAnimation,(void))

bool Animation_obj::isFinished( ){
	HX_STACK_PUSH("Animation::isFinished","Animation.hx",97);
	HX_STACK_THIS(this);
	HX_STACK_LINE(97)
	return (this->currentFrame == this->animationLength);
}


HX_DEFINE_DYNAMIC_FUNC0(Animation_obj,isFinished,return )

int Animation_obj::getCurrentFrame( ){
	HX_STACK_PUSH("Animation::getCurrentFrame","Animation.hx",93);
	HX_STACK_THIS(this);
	HX_STACK_LINE(93)
	return this->currentFrame;
}


HX_DEFINE_DYNAMIC_FUNC0(Animation_obj,getCurrentFrame,return )

::native::geom::Point Animation_obj::getPosition( ){
	HX_STACK_PUSH("Animation::getPosition","Animation.hx",89);
	HX_STACK_THIS(this);
	HX_STACK_LINE(89)
	return this->position;
}


HX_DEFINE_DYNAMIC_FUNC0(Animation_obj,getPosition,return )

Void Animation_obj::setY( Float y){
{
		HX_STACK_PUSH("Animation::setY","Animation.hx",85);
		HX_STACK_THIS(this);
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(85)
		this->position->y = y;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Animation_obj,setY,(void))

Void Animation_obj::setX( Float x){
{
		HX_STACK_PUSH("Animation::setX","Animation.hx",81);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_LINE(81)
		this->position->x = x;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Animation_obj,setX,(void))

Float Animation_obj::getRotation( ){
	HX_STACK_PUSH("Animation::getRotation","Animation.hx",77);
	HX_STACK_THIS(this);
	HX_STACK_LINE(77)
	return this->rotation;
}


HX_DEFINE_DYNAMIC_FUNC0(Animation_obj,getRotation,return )

Void Animation_obj::setRotation( Float rot){
{
		HX_STACK_PUSH("Animation::setRotation","Animation.hx",73);
		HX_STACK_THIS(this);
		HX_STACK_ARG(rot,"rot");
		HX_STACK_LINE(73)
		this->rotation = hx::Mod(rot,(int)360);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Animation_obj,setRotation,(void))

Void Animation_obj::addToRenderSet( ){
{
		HX_STACK_PUSH("Animation::addToRenderSet","Animation.hx",66);
		HX_STACK_THIS(this);
		HX_STACK_LINE(66)
		if ((!(this->rendering))){
			HX_STACK_LINE(68)
			this->ID = this->renderer->addTile((this->position->x * ::SceneManager_obj::scale),(this->position->y * ::SceneManager_obj::scale),this->startFrame,this->rotation,::SceneManager_obj::scale,this->layer);
			HX_STACK_LINE(69)
			this->rendering = true;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Animation_obj,addToRenderSet,(void))

Void Animation_obj::removeFromRenderSet( ){
{
		HX_STACK_PUSH("Animation::removeFromRenderSet","Animation.hx",59);
		HX_STACK_THIS(this);
		HX_STACK_LINE(59)
		if ((this->rendering)){
			HX_STACK_LINE(61)
			this->renderer->remove(this->ID,this->layer);
			HX_STACK_LINE(62)
			this->rendering = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Animation_obj,removeFromRenderSet,(void))

Void Animation_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("Animation::render","Animation.hx",43);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(45)
		hx::AddEq(this->currentTime,deltaTime);
		HX_STACK_LINE(46)
		if (((this->currentTime > (Float(1000.) / Float(this->animationSpeed))))){
			HX_STACK_LINE(47)
			hx::SubEq(this->currentTime,(Float(1000.) / Float(this->animationSpeed)));
			HX_STACK_LINE(48)
			(this->currentFrame)++;
			HX_STACK_LINE(49)
			if (((bool((this->currentFrame > this->animationLength)) && bool(this->loops)))){
				HX_STACK_LINE(49)
				this->currentFrame = (int)0;
			}
			else{
				HX_STACK_LINE(51)
				if (((this->currentFrame > this->animationLength))){
					HX_STACK_LINE(51)
					this->currentFrame = this->animationLength;
				}
			}
		}
		HX_STACK_LINE(56)
		this->renderer->updateObject(this->ID,((((this->position->x - camera->x)) * ::SceneManager_obj::scale) + (Float(::TileRenderer_obj::stageWidth) / Float((int)2))),((((this->position->y - camera->y)) * ::SceneManager_obj::scale) + (Float(::TileRenderer_obj::stageHeight) / Float((int)2))),(this->startFrame + this->currentFrame),this->rotation,::SceneManager_obj::scale,this->layer);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Animation_obj,render,(void))

Void Animation_obj::update( Float x,Float y,Float rot){
{
		HX_STACK_PUSH("Animation::update","Animation.hx",37);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_ARG(y,"y");
		HX_STACK_ARG(rot,"rot");
		HX_STACK_LINE(38)
		this->position->x = x;
		HX_STACK_LINE(39)
		this->position->y = y;
		HX_STACK_LINE(40)
		this->rotation = rot;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC3(Animation_obj,update,(void))


Animation_obj::Animation_obj()
{
}

void Animation_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Animation);
	HX_MARK_MEMBER_NAME(rendering,"rendering");
	HX_MARK_MEMBER_NAME(loops,"loops");
	HX_MARK_MEMBER_NAME(ID,"ID");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_MEMBER_NAME(layer,"layer");
	HX_MARK_MEMBER_NAME(rotation,"rotation");
	HX_MARK_MEMBER_NAME(position,"position");
	HX_MARK_MEMBER_NAME(currentTime,"currentTime");
	HX_MARK_MEMBER_NAME(currentFrame,"currentFrame");
	HX_MARK_MEMBER_NAME(animationSpeed,"animationSpeed");
	HX_MARK_MEMBER_NAME(animationLength,"animationLength");
	HX_MARK_MEMBER_NAME(startFrame,"startFrame");
	HX_MARK_END_CLASS();
}

void Animation_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(rendering,"rendering");
	HX_VISIT_MEMBER_NAME(loops,"loops");
	HX_VISIT_MEMBER_NAME(ID,"ID");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	HX_VISIT_MEMBER_NAME(layer,"layer");
	HX_VISIT_MEMBER_NAME(rotation,"rotation");
	HX_VISIT_MEMBER_NAME(position,"position");
	HX_VISIT_MEMBER_NAME(currentTime,"currentTime");
	HX_VISIT_MEMBER_NAME(currentFrame,"currentFrame");
	HX_VISIT_MEMBER_NAME(animationSpeed,"animationSpeed");
	HX_VISIT_MEMBER_NAME(animationLength,"animationLength");
	HX_VISIT_MEMBER_NAME(startFrame,"startFrame");
}

Dynamic Animation_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"ID") ) { return ID; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"setY") ) { return setY_dyn(); }
		if (HX_FIELD_EQ(inName,"setX") ) { return setX_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"loops") ) { return loops; }
		if (HX_FIELD_EQ(inName,"layer") ) { return layer; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		if (HX_FIELD_EQ(inName,"rotation") ) { return rotation; }
		if (HX_FIELD_EQ(inName,"position") ) { return position; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"rendering") ) { return rendering; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"isFinished") ) { return isFinished_dyn(); }
		if (HX_FIELD_EQ(inName,"startFrame") ) { return startFrame; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getPosition") ) { return getPosition_dyn(); }
		if (HX_FIELD_EQ(inName,"getRotation") ) { return getRotation_dyn(); }
		if (HX_FIELD_EQ(inName,"setRotation") ) { return setRotation_dyn(); }
		if (HX_FIELD_EQ(inName,"currentTime") ) { return currentTime; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"currentFrame") ) { return currentFrame; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"addToRenderSet") ) { return addToRenderSet_dyn(); }
		if (HX_FIELD_EQ(inName,"animationSpeed") ) { return animationSpeed; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"getCurrentFrame") ) { return getCurrentFrame_dyn(); }
		if (HX_FIELD_EQ(inName,"animationLength") ) { return animationLength; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"restartAnimation") ) { return restartAnimation_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"removeFromRenderSet") ) { return removeFromRenderSet_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Animation_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"ID") ) { ID=inValue.Cast< int >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"loops") ) { loops=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"layer") ) { layer=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::TileRenderer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"rotation") ) { rotation=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"position") ) { position=inValue.Cast< ::native::geom::Point >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"rendering") ) { rendering=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"startFrame") ) { startFrame=inValue.Cast< int >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"currentTime") ) { currentTime=inValue.Cast< Float >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"currentFrame") ) { currentFrame=inValue.Cast< int >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"animationSpeed") ) { animationSpeed=inValue.Cast< int >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"animationLength") ) { animationLength=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Animation_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("rendering"));
	outFields->push(HX_CSTRING("loops"));
	outFields->push(HX_CSTRING("ID"));
	outFields->push(HX_CSTRING("renderer"));
	outFields->push(HX_CSTRING("layer"));
	outFields->push(HX_CSTRING("rotation"));
	outFields->push(HX_CSTRING("position"));
	outFields->push(HX_CSTRING("currentTime"));
	outFields->push(HX_CSTRING("currentFrame"));
	outFields->push(HX_CSTRING("animationSpeed"));
	outFields->push(HX_CSTRING("animationLength"));
	outFields->push(HX_CSTRING("startFrame"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("restartAnimation"),
	HX_CSTRING("isFinished"),
	HX_CSTRING("getCurrentFrame"),
	HX_CSTRING("getPosition"),
	HX_CSTRING("setY"),
	HX_CSTRING("setX"),
	HX_CSTRING("getRotation"),
	HX_CSTRING("setRotation"),
	HX_CSTRING("addToRenderSet"),
	HX_CSTRING("removeFromRenderSet"),
	HX_CSTRING("render"),
	HX_CSTRING("update"),
	HX_CSTRING("rendering"),
	HX_CSTRING("loops"),
	HX_CSTRING("ID"),
	HX_CSTRING("renderer"),
	HX_CSTRING("layer"),
	HX_CSTRING("rotation"),
	HX_CSTRING("position"),
	HX_CSTRING("currentTime"),
	HX_CSTRING("currentFrame"),
	HX_CSTRING("animationSpeed"),
	HX_CSTRING("animationLength"),
	HX_CSTRING("startFrame"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Animation_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Animation_obj::__mClass,"__mClass");
};

Class Animation_obj::__mClass;

void Animation_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("Animation"), hx::TCanCast< Animation_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Animation_obj::__boot()
{
}

