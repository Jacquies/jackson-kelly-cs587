#include <hxcpp.h>

#ifndef INCLUDED_Background
#include <Background.h>
#endif
#ifndef INCLUDED_Button
#include <Button.h>
#endif
#ifndef INCLUDED_EndOfLevel
#include <EndOfLevel.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_EnemyFactory
#include <EnemyFactory.h>
#endif
#ifndef INCLUDED_GoldPiece
#include <GoldPiece.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_Level
#include <Level.h>
#endif
#ifndef INCLUDED_LevelMap
#include <LevelMap.h>
#endif
#ifndef INCLUDED_MainMenu
#include <MainMenu.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_UpgradeMenu
#include <UpgradeMenu.h>
#endif
#ifndef INCLUDED_Wall
#include <Wall.h>
#endif
#ifndef INCLUDED_native_Lib
#include <native/Lib.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_display_Stage
#include <native/display/Stage.h>
#endif
#ifndef INCLUDED_native_events_Event
#include <native/events/Event.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_MouseEvent
#include <native/events/MouseEvent.h>
#endif
#ifndef INCLUDED_native_events_TouchEvent
#include <native/events/TouchEvent.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
#ifndef INCLUDED_native_net_SharedObjectFlushStatus
#include <native/net/SharedObjectFlushStatus.h>
#endif

Void Level_obj::__construct(::TileRenderer tileRenderer)
{
HX_STACK_PUSH("Level::new","Level.hx",25);
{
	HX_STACK_LINE(26)
	super::__construct();
	HX_STACK_LINE(28)
	this->renderer = tileRenderer;
	HX_STACK_LINE(30)
	this->addEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null(),null(),null());
}
;
	return null();
}

Level_obj::~Level_obj() { }

Dynamic Level_obj::__CreateEmpty() { return  new Level_obj; }
hx::ObjectPtr< Level_obj > Level_obj::__new(::TileRenderer tileRenderer)
{  hx::ObjectPtr< Level_obj > result = new Level_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic Level_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Level_obj > result = new Level_obj();
	result->__construct(inArgs[0]);
	return result;}

Void Level_obj::dummyFunction( ){
{
		HX_STACK_PUSH("Level::dummyFunction","Level.hx",241);
		HX_STACK_THIS(this);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Level_obj,dummyFunction,(void))

Void Level_obj::cleanup( ){
{
		HX_STACK_PUSH("Level::cleanup","Level.hx",198);
		HX_STACK_THIS(this);
		HX_STACK_LINE(199)
		this->sceneManager->setLevelOver(true);
		HX_STACK_LINE(201)
		this->removeEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null());
		HX_STACK_LINE(202)
		this->removeEventListener(::native::events::Event_obj::ENTER_FRAME,this->onEnterFrame_dyn(),null());
		HX_STACK_LINE(207)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null());
		HX_STACK_LINE(208)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_END,this->onTouchEnd_dyn(),null());
		HX_STACK_LINE(209)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_MOVE,this->onTouchMove_dyn(),null());
		HX_STACK_LINE(212)
		this->sceneManager->clearCollisionSet();
		HX_STACK_LINE(213)
		this->sceneManager->clearRenderSet();
		HX_STACK_LINE(214)
		this->sceneManager->clearTickableSet();
		HX_STACK_LINE(216)
		this->renderer->clear();
		HX_STACK_LINE(218)
		this->sceneManager->getSharedObject()->flush(null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Level_obj,cleanup,(void))

Void Level_obj::returnToMainMenu( ){
{
		HX_STACK_PUSH("Level::returnToMainMenu","Level.hx",190);
		HX_STACK_THIS(this);
		HX_STACK_LINE(191)
		this->cleanup();
		HX_STACK_LINE(193)
		this->get_parent()->addChild(::MainMenu_obj::__new(this->renderer));
		HX_STACK_LINE(195)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Level_obj,returnToMainMenu,(void))

Void Level_obj::endLevel( ){
{
		HX_STACK_PUSH("Level::endLevel","Level.hx",182);
		HX_STACK_THIS(this);
		HX_STACK_LINE(183)
		this->cleanup();
		HX_STACK_LINE(185)
		this->get_parent()->addChild(::UpgradeMenu_obj::__new(this->renderer));
		HX_STACK_LINE(187)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Level_obj,endLevel,(void))

Void Level_obj::onTouchMove( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("Level::onTouchMove","Level.hx",177);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(177)
		this->sceneManager->setMousePosition(event->localX,event->localY);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onTouchMove,(void))

Void Level_obj::onTouchEnd( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("Level::onTouchEnd","Level.hx",173);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(173)
		this->sceneManager->setMouseDown(false);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onTouchEnd,(void))

Void Level_obj::onTouchBegin( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("Level::onTouchBegin","Level.hx",165);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(166)
		this->sceneManager->setMouseDown(true);
		HX_STACK_LINE(167)
		this->sceneManager->setMousePosition(this->get_mouseX(),this->get_mouseY());
		HX_STACK_LINE(168)
		if (((this->deathScreenCountdown <= (int)0))){
			HX_STACK_LINE(168)
			this->mainMenuButton->onClick(this->get_mouseX(),this->get_mouseY());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onTouchBegin,(void))

Void Level_obj::onEnterFrame( ::native::events::Event event){
{
		HX_STACK_PUSH("Level::onEnterFrame","Level.hx",131);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(132)
		if ((this->sceneManager->isIdle())){
			HX_STACK_LINE(132)
			(this->deathScreenCountdown)--;
		}
		else{
			HX_STACK_LINE(134)
			this->deathScreenCountdown = (int)40;
		}
		HX_STACK_LINE(137)
		if (((this->deathScreenCountdown == (int)0))){
			HX_STACK_LINE(138)
			::Button_obj::__new((Float(::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::TileRenderer_obj::stageHeight) / Float((int)2)),(int)61,::SceneManager_obj::scale,this->renderer,this->dummyFunction_dyn());
			HX_STACK_LINE(139)
			this->mainMenuButton = ::Button_obj::__new((Float(::TileRenderer_obj::stageWidth) / Float((int)2)),((Float(::TileRenderer_obj::stageHeight) / Float((int)2)) + (Float((::TileRenderer_obj::heights->__get((int)51) * ::SceneManager_obj::scale)) / Float((int)2))),(int)51,::SceneManager_obj::scale,this->renderer,this->returnToMainMenu_dyn());
		}
		HX_STACK_LINE(142)
		int deltaTime = (::native::Lib_obj::getTimer() - this->previousTickTime);		HX_STACK_VAR(deltaTime,"deltaTime");
		HX_STACK_LINE(143)
		this->previousTickTime = ::native::Lib_obj::getTimer();
		HX_STACK_LINE(149)
		this->sceneManager->tick(deltaTime);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onEnterFrame,(void))

Void Level_obj::onAddedToStage( ::native::events::Event event){
{
		HX_STACK_PUSH("Level::onAddedToStage","Level.hx",33);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(34)
		this->renderer->clear();
		HX_STACK_LINE(35)
		this->sceneManager = ::SceneManager_obj::__new(this->renderer);
		HX_STACK_LINE(36)
		this->sceneManager->initialize();
		HX_STACK_LINE(37)
		this->previousTickTime = ::native::Lib_obj::getTimer();
		HX_STACK_LINE(39)
		this->deathScreenCountdown = (int)45;
		HX_STACK_LINE(41)
		this->addEventListener(::native::events::Event_obj::ENTER_FRAME,this->onEnterFrame_dyn(),null(),null(),null());
		HX_STACK_LINE(46)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null(),null(),null());
		HX_STACK_LINE(47)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_END,this->onTouchEnd_dyn(),null(),null(),null());
		HX_STACK_LINE(48)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_MOVE,this->onTouchMove_dyn(),null(),null(),null());
		HX_STACK_LINE(59)
		this->renderer->addTile((Float(::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::TileRenderer_obj::stageHeight) / Float((int)2)),(int)57,(int)0,(Float(::Math_obj::max(::TileRenderer_obj::stageWidth,::TileRenderer_obj::stageHeight)) / Float(::TileRenderer_obj::widths->__get((int)57))),(int)0);
		HX_STACK_LINE(63)
		this->map = ::LevelMap_obj::__new(this->sceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true));
		HX_STACK_LINE(66)
		this->levelEnd = ::EndOfLevel_obj::__new(this->map->generateLevelEnd(this->sceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true)),this->renderer,hx::ObjectPtr<OBJ_>(this));
		HX_STACK_LINE(67)
		this->sceneManager->addToCollisionSet(this->levelEnd);
		HX_STACK_LINE(68)
		this->sceneManager->addToRenderSet(this->levelEnd);
		HX_STACK_LINE(71)
		Array< ::native::geom::Point > points = this->map->generateCoins(this->sceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true));		HX_STACK_VAR(points,"points");
		HX_STACK_LINE(72)
		this->coins = Array_obj< ::GoldPiece >::__new();
		HX_STACK_LINE(73)
		{
			HX_STACK_LINE(73)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = points->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(73)
			while(((_g1 < _g))){
				HX_STACK_LINE(73)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(74)
				this->coins->push(::GoldPiece_obj::__new(points->__get(i),this->renderer));
				HX_STACK_LINE(75)
				this->sceneManager->addToCollisionSet(this->coins->__get(i));
				HX_STACK_LINE(76)
				this->sceneManager->addToRenderSet(this->coins->__get(i));
			}
		}
		HX_STACK_LINE(80)
		points = this->map->generateEnemies(this->sceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true));
		HX_STACK_LINE(81)
		this->enemies = Array_obj< ::Enemy >::__new();
		HX_STACK_LINE(82)
		{
			HX_STACK_LINE(82)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = points->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(82)
			while(((_g1 < _g))){
				HX_STACK_LINE(82)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(83)
				if (((::Math_obj::random() < .5))){
					HX_STACK_LINE(83)
					this->enemies->push(::EnemyFactory_obj::getEnemy(points->__get(i),::Enemy_obj::TYPE_CHASING_ENEMY,this->renderer));
				}
				else{
					HX_STACK_LINE(85)
					this->enemies->push(::EnemyFactory_obj::getEnemy(points->__get(i),::Enemy_obj::TYPE_CHARGING_ENEMY,this->renderer));
				}
				HX_STACK_LINE(96)
				this->sceneManager->addToCollisionSet(this->enemies->__get(i));
				HX_STACK_LINE(97)
				this->sceneManager->addToRenderSet(this->enemies->__get(i));
				HX_STACK_LINE(98)
				this->sceneManager->addToTickableSet(this->enemies->__get(i));
			}
		}
		HX_STACK_LINE(102)
		points = this->map->generateBackgroundPositions();
		HX_STACK_LINE(103)
		this->backgrounds = Array_obj< ::Background >::__new();
		HX_STACK_LINE(104)
		{
			HX_STACK_LINE(104)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = points->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(104)
			while(((_g1 < _g))){
				HX_STACK_LINE(104)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(105)
				this->backgrounds->push(::Background_obj::__new(points->__get(i),this->renderer));
				HX_STACK_LINE(106)
				this->sceneManager->addToRenderSet(this->backgrounds->__get(i));
			}
		}
		HX_STACK_LINE(110)
		Array< int > directions = Array_obj< int >::__new();		HX_STACK_VAR(directions,"directions");
		HX_STACK_LINE(111)
		points = this->map->generateWallPositions(directions);
		HX_STACK_LINE(112)
		this->walls = Array_obj< ::Wall >::__new();
		HX_STACK_LINE(113)
		{
			HX_STACK_LINE(113)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = points->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(113)
			while(((_g1 < _g))){
				HX_STACK_LINE(113)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(114)
				this->walls->push(::Wall_obj::__new(points->__get(i),directions->__get(i),this->renderer));
				HX_STACK_LINE(115)
				this->sceneManager->addToCollisionSet(this->walls->__get(i));
				HX_STACK_LINE(117)
				this->sceneManager->addToRenderSet(this->walls->__get(i));
			}
		}
		HX_STACK_LINE(122)
		this->player = ::Player_obj::__new(this->renderer);
		HX_STACK_LINE(123)
		this->sceneManager->addToCollisionSet(this->player);
		HX_STACK_LINE(124)
		this->sceneManager->addToRenderSet(this->player);
		HX_STACK_LINE(125)
		this->sceneManager->addToTickableSet(this->player);
		HX_STACK_LINE(126)
		::Enemy_obj::player = this->player;
		HX_STACK_LINE(128)
		(this->sceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("difficulty")))++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onAddedToStage,(void))

Void Level_obj::initializeLocalData( ::native::net::SharedObject localData){
{
		HX_STACK_PUSH("Level::initializeLocalData","Level.hx",221);
		HX_STACK_ARG(localData,"localData");
		HX_STACK_LINE(222)
		localData->data->__FieldRef(HX_CSTRING("initialized")) = true;
		HX_STACK_LINE(223)
		localData->data->__FieldRef(HX_CSTRING("mostGold")) = (int)0;
		HX_STACK_LINE(224)
		::Level_obj::newGame(localData);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Level_obj,initializeLocalData,(void))

Void Level_obj::newGame( ::native::net::SharedObject localData){
{
		HX_STACK_PUSH("Level::newGame","Level.hx",227);
		HX_STACK_ARG(localData,"localData");
		HX_STACK_LINE(228)
		localData->data->__FieldRef(HX_CSTRING("gold")) = (int)10000;
		HX_STACK_LINE(229)
		localData->data->__FieldRef(HX_CSTRING("totalGold")) = (int)0;
		HX_STACK_LINE(230)
		localData->data->__FieldRef(HX_CSTRING("difficulty")) = (int)1;
		HX_STACK_LINE(231)
		localData->data->__FieldRef(HX_CSTRING("newGame")) = false;
		HX_STACK_LINE(233)
		localData->data->__FieldRef(HX_CSTRING("health")) = (int)1;
		HX_STACK_LINE(234)
		localData->data->__FieldRef(HX_CSTRING("speed")) = (int)4;
		HX_STACK_LINE(235)
		localData->data->__FieldRef(HX_CSTRING("collectRadius")) = (int)10;
		HX_STACK_LINE(236)
		localData->data->__FieldRef(HX_CSTRING("aggroRadius")) = (int)150;
		HX_STACK_LINE(238)
		localData->flush(null());
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Level_obj,newGame,(void))


Level_obj::Level_obj()
{
}

void Level_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Level);
	HX_MARK_MEMBER_NAME(previousTickTime,"previousTickTime");
	HX_MARK_MEMBER_NAME(mainMenuButton,"mainMenuButton");
	HX_MARK_MEMBER_NAME(deathScreenCountdown,"deathScreenCountdown");
	HX_MARK_MEMBER_NAME(backgrounds,"backgrounds");
	HX_MARK_MEMBER_NAME(walls,"walls");
	HX_MARK_MEMBER_NAME(levelEnd,"levelEnd");
	HX_MARK_MEMBER_NAME(coins,"coins");
	HX_MARK_MEMBER_NAME(enemies,"enemies");
	HX_MARK_MEMBER_NAME(player,"player");
	HX_MARK_MEMBER_NAME(map,"map");
	HX_MARK_MEMBER_NAME(sceneManager,"sceneManager");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	super::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Level_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(previousTickTime,"previousTickTime");
	HX_VISIT_MEMBER_NAME(mainMenuButton,"mainMenuButton");
	HX_VISIT_MEMBER_NAME(deathScreenCountdown,"deathScreenCountdown");
	HX_VISIT_MEMBER_NAME(backgrounds,"backgrounds");
	HX_VISIT_MEMBER_NAME(walls,"walls");
	HX_VISIT_MEMBER_NAME(levelEnd,"levelEnd");
	HX_VISIT_MEMBER_NAME(coins,"coins");
	HX_VISIT_MEMBER_NAME(enemies,"enemies");
	HX_VISIT_MEMBER_NAME(player,"player");
	HX_VISIT_MEMBER_NAME(map,"map");
	HX_VISIT_MEMBER_NAME(sceneManager,"sceneManager");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	super::__Visit(HX_VISIT_ARG);
}

Dynamic Level_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"map") ) { return map; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"walls") ) { return walls; }
		if (HX_FIELD_EQ(inName,"coins") ) { return coins; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { return player; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"newGame") ) { return newGame_dyn(); }
		if (HX_FIELD_EQ(inName,"cleanup") ) { return cleanup_dyn(); }
		if (HX_FIELD_EQ(inName,"enemies") ) { return enemies; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"endLevel") ) { return endLevel_dyn(); }
		if (HX_FIELD_EQ(inName,"levelEnd") ) { return levelEnd; }
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"onTouchEnd") ) { return onTouchEnd_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"onTouchMove") ) { return onTouchMove_dyn(); }
		if (HX_FIELD_EQ(inName,"backgrounds") ) { return backgrounds; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"onTouchBegin") ) { return onTouchBegin_dyn(); }
		if (HX_FIELD_EQ(inName,"onEnterFrame") ) { return onEnterFrame_dyn(); }
		if (HX_FIELD_EQ(inName,"sceneManager") ) { return sceneManager; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"dummyFunction") ) { return dummyFunction_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"onAddedToStage") ) { return onAddedToStage_dyn(); }
		if (HX_FIELD_EQ(inName,"mainMenuButton") ) { return mainMenuButton; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"returnToMainMenu") ) { return returnToMainMenu_dyn(); }
		if (HX_FIELD_EQ(inName,"previousTickTime") ) { return previousTickTime; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"initializeLocalData") ) { return initializeLocalData_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"deathScreenCountdown") ) { return deathScreenCountdown; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Level_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"map") ) { map=inValue.Cast< ::LevelMap >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"walls") ) { walls=inValue.Cast< Array< ::Wall > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"coins") ) { coins=inValue.Cast< Array< ::GoldPiece > >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { player=inValue.Cast< ::Player >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"enemies") ) { enemies=inValue.Cast< Array< ::Enemy > >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"levelEnd") ) { levelEnd=inValue.Cast< ::EndOfLevel >(); return inValue; }
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::TileRenderer >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"backgrounds") ) { backgrounds=inValue.Cast< Array< ::Background > >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"sceneManager") ) { sceneManager=inValue.Cast< ::SceneManager >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"mainMenuButton") ) { mainMenuButton=inValue.Cast< ::Button >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"previousTickTime") ) { previousTickTime=inValue.Cast< int >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"deathScreenCountdown") ) { deathScreenCountdown=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Level_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("previousTickTime"));
	outFields->push(HX_CSTRING("mainMenuButton"));
	outFields->push(HX_CSTRING("deathScreenCountdown"));
	outFields->push(HX_CSTRING("backgrounds"));
	outFields->push(HX_CSTRING("walls"));
	outFields->push(HX_CSTRING("levelEnd"));
	outFields->push(HX_CSTRING("coins"));
	outFields->push(HX_CSTRING("enemies"));
	outFields->push(HX_CSTRING("player"));
	outFields->push(HX_CSTRING("map"));
	outFields->push(HX_CSTRING("sceneManager"));
	outFields->push(HX_CSTRING("renderer"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("initializeLocalData"),
	HX_CSTRING("newGame"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("dummyFunction"),
	HX_CSTRING("cleanup"),
	HX_CSTRING("returnToMainMenu"),
	HX_CSTRING("endLevel"),
	HX_CSTRING("onTouchMove"),
	HX_CSTRING("onTouchEnd"),
	HX_CSTRING("onTouchBegin"),
	HX_CSTRING("onEnterFrame"),
	HX_CSTRING("onAddedToStage"),
	HX_CSTRING("previousTickTime"),
	HX_CSTRING("mainMenuButton"),
	HX_CSTRING("deathScreenCountdown"),
	HX_CSTRING("backgrounds"),
	HX_CSTRING("walls"),
	HX_CSTRING("levelEnd"),
	HX_CSTRING("coins"),
	HX_CSTRING("enemies"),
	HX_CSTRING("player"),
	HX_CSTRING("map"),
	HX_CSTRING("sceneManager"),
	HX_CSTRING("renderer"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Level_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Level_obj::__mClass,"__mClass");
};

Class Level_obj::__mClass;

void Level_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("Level"), hx::TCanCast< Level_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Level_obj::__boot()
{
}

