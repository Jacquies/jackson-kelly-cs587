#include <hxcpp.h>

#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IColliderState
#include <IColliderState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_KillableColliderState
#include <KillableColliderState.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_Projectile
#include <Projectile.h>
#endif

Void KillableColliderState_obj::__construct()
{
HX_STACK_PUSH("KillableColliderState::new","KillableColliderState.hx",8);
{
}
;
	return null();
}

KillableColliderState_obj::~KillableColliderState_obj() { }

Dynamic KillableColliderState_obj::__CreateEmpty() { return  new KillableColliderState_obj; }
hx::ObjectPtr< KillableColliderState_obj > KillableColliderState_obj::__new()
{  hx::ObjectPtr< KillableColliderState_obj > result = new KillableColliderState_obj();
	result->__construct();
	return result;}

Dynamic KillableColliderState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< KillableColliderState_obj > result = new KillableColliderState_obj();
	result->__construct();
	return result;}

hx::Object *KillableColliderState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IColliderState_obj)) return operator ::IColliderState_obj *();
	return super::__ToInterface(inType);
}

Void KillableColliderState_obj::collide( ::ICollidable collidingObject,::ICollidable otherCollidingObject){
{
		HX_STACK_PUSH("KillableColliderState::collide","KillableColliderState.hx",10);
		HX_STACK_THIS(this);
		HX_STACK_ARG(collidingObject,"collidingObject");
		HX_STACK_ARG(otherCollidingObject,"otherCollidingObject");
		HX_STACK_LINE(10)
		if (((bool((otherCollidingObject->getCollider()->getType() == ::Collider_obj::TYPE_ENEMY)) || bool((bool((otherCollidingObject->getCollider()->getType() == ::Collider_obj::TYPE_PROJECTILE)) && bool((hx::TCast< Projectile >::cast(otherCollidingObject))->isActivated())))))){
			HX_STACK_LINE(11)
			(hx::TCast< Player >::cast(collidingObject))->hit();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(KillableColliderState_obj,collide,(void))


KillableColliderState_obj::KillableColliderState_obj()
{
}

void KillableColliderState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(KillableColliderState);
	HX_MARK_END_CLASS();
}

void KillableColliderState_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic KillableColliderState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic KillableColliderState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void KillableColliderState_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(KillableColliderState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(KillableColliderState_obj::__mClass,"__mClass");
};

Class KillableColliderState_obj::__mClass;

void KillableColliderState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("KillableColliderState"), hx::TCanCast< KillableColliderState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void KillableColliderState_obj::__boot()
{
}

