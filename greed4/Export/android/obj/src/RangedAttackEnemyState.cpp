#include <hxcpp.h>

#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IEnemyState
#include <IEnemyState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_RangedAttackEnemyState
#include <RangedAttackEnemyState.h>
#endif

Void RangedAttackEnemyState_obj::__construct()
{
HX_STACK_PUSH("RangedAttackEnemyState::new","RangedAttackEnemyState.hx",10);
{
	HX_STACK_LINE(10)
	this->fireCooldown = (int)35;
}
;
	return null();
}

RangedAttackEnemyState_obj::~RangedAttackEnemyState_obj() { }

Dynamic RangedAttackEnemyState_obj::__CreateEmpty() { return  new RangedAttackEnemyState_obj; }
hx::ObjectPtr< RangedAttackEnemyState_obj > RangedAttackEnemyState_obj::__new()
{  hx::ObjectPtr< RangedAttackEnemyState_obj > result = new RangedAttackEnemyState_obj();
	result->__construct();
	return result;}

Dynamic RangedAttackEnemyState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< RangedAttackEnemyState_obj > result = new RangedAttackEnemyState_obj();
	result->__construct();
	return result;}

hx::Object *RangedAttackEnemyState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IEnemyState_obj)) return operator ::IEnemyState_obj *();
	return super::__ToInterface(inType);
}

Void RangedAttackEnemyState_obj::hitWall( ::ICollidable wallCollidable){
{
		HX_STACK_PUSH("RangedAttackEnemyState::hitWall","RangedAttackEnemyState.hx",26);
		HX_STACK_THIS(this);
		HX_STACK_ARG(wallCollidable,"wallCollidable");
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(RangedAttackEnemyState_obj,hitWall,(void))

Void RangedAttackEnemyState_obj::tick( int deltaTime,::Enemy enemy){
{
		HX_STACK_PUSH("RangedAttackEnemyState::tick","RangedAttackEnemyState.hx",14);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(15)
		(this->fireCooldown)--;
		HX_STACK_LINE(17)
		if (((this->fireCooldown <= (int)0))){
			HX_STACK_LINE(17)
			this->fireCooldown = (int)35;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(RangedAttackEnemyState_obj,tick,(void))


RangedAttackEnemyState_obj::RangedAttackEnemyState_obj()
{
}

void RangedAttackEnemyState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(RangedAttackEnemyState);
	HX_MARK_MEMBER_NAME(fireCooldown,"fireCooldown");
	HX_MARK_END_CLASS();
}

void RangedAttackEnemyState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(fireCooldown,"fireCooldown");
}

Dynamic RangedAttackEnemyState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"hitWall") ) { return hitWall_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"fireCooldown") ) { return fireCooldown; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic RangedAttackEnemyState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 12:
		if (HX_FIELD_EQ(inName,"fireCooldown") ) { fireCooldown=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void RangedAttackEnemyState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("fireCooldown"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("hitWall"),
	HX_CSTRING("tick"),
	HX_CSTRING("fireCooldown"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(RangedAttackEnemyState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(RangedAttackEnemyState_obj::__mClass,"__mClass");
};

Class RangedAttackEnemyState_obj::__mClass;

void RangedAttackEnemyState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("RangedAttackEnemyState"), hx::TCanCast< RangedAttackEnemyState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void RangedAttackEnemyState_obj::__boot()
{
}

