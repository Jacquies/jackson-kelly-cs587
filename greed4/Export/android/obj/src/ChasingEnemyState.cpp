#include <hxcpp.h>

#ifndef INCLUDED_ChasingEnemyState
#include <ChasingEnemyState.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IEnemyState
#include <IEnemyState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void ChasingEnemyState_obj::__construct(int difficulty)
{
HX_STACK_PUSH("ChasingEnemyState::new","ChasingEnemyState.hx",10);
{
	HX_STACK_LINE(10)
	this->speed = ((int)4 - (Float((int)3) / Float(::Math_obj::sqrt(difficulty))));
}
;
	return null();
}

ChasingEnemyState_obj::~ChasingEnemyState_obj() { }

Dynamic ChasingEnemyState_obj::__CreateEmpty() { return  new ChasingEnemyState_obj; }
hx::ObjectPtr< ChasingEnemyState_obj > ChasingEnemyState_obj::__new(int difficulty)
{  hx::ObjectPtr< ChasingEnemyState_obj > result = new ChasingEnemyState_obj();
	result->__construct(difficulty);
	return result;}

Dynamic ChasingEnemyState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< ChasingEnemyState_obj > result = new ChasingEnemyState_obj();
	result->__construct(inArgs[0]);
	return result;}

hx::Object *ChasingEnemyState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IEnemyState_obj)) return operator ::IEnemyState_obj *();
	return super::__ToInterface(inType);
}

Void ChasingEnemyState_obj::hitWall( ::ICollidable wallCollidable){
{
		HX_STACK_PUSH("ChasingEnemyState::hitWall","ChasingEnemyState.hx",27);
		HX_STACK_THIS(this);
		HX_STACK_ARG(wallCollidable,"wallCollidable");
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(ChasingEnemyState_obj,hitWall,(void))

Void ChasingEnemyState_obj::tick( int deltaTime,::Enemy enemy){
{
		HX_STACK_PUSH("ChasingEnemyState::tick","ChasingEnemyState.hx",14);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(16)
		if (((::Enemy_obj::player->getPosition()->y > enemy->getPosition()->y))){
			HX_STACK_LINE(16)
			enemy->setRotation((::SceneManager_obj::usingFlash * (((int)180 - (Float(((int)180 * ::Math_obj::atan((Float(((::Enemy_obj::player->getPosition()->x - enemy->getPosition()->x))) / Float(((::Enemy_obj::player->getPosition()->y - enemy->getPosition()->y))))))) / Float(::Math_obj::PI))))));
		}
		else{
			HX_STACK_LINE(18)
			if (((::Enemy_obj::player->getPosition()->y < enemy->getPosition()->y))){
				HX_STACK_LINE(18)
				enemy->setRotation((::SceneManager_obj::usingFlash * ((Float(((int)-180 * ::Math_obj::atan((Float(((::Enemy_obj::player->getPosition()->x - enemy->getPosition()->x))) / Float(((::Enemy_obj::player->getPosition()->y - enemy->getPosition()->y))))))) / Float(::Math_obj::PI)))));
			}
		}
		HX_STACK_LINE(23)
		enemy->setX((enemy->getPosition()->x + (Float((((this->speed * ::Math_obj::sin((Float(((::SceneManager_obj::usingFlash * enemy->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime) * (int)3)) / Float((int)100))));
		HX_STACK_LINE(24)
		enemy->setY((enemy->getPosition()->y - (Float((((this->speed * ::Math_obj::cos((Float(((::SceneManager_obj::usingFlash * enemy->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime) * (int)3)) / Float((int)100))));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(ChasingEnemyState_obj,tick,(void))


ChasingEnemyState_obj::ChasingEnemyState_obj()
{
}

void ChasingEnemyState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(ChasingEnemyState);
	HX_MARK_MEMBER_NAME(speed,"speed");
	HX_MARK_END_CLASS();
}

void ChasingEnemyState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(speed,"speed");
}

Dynamic ChasingEnemyState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"speed") ) { return speed; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"hitWall") ) { return hitWall_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic ChasingEnemyState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"speed") ) { speed=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void ChasingEnemyState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("speed"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("hitWall"),
	HX_CSTRING("tick"),
	HX_CSTRING("speed"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ChasingEnemyState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ChasingEnemyState_obj::__mClass,"__mClass");
};

Class ChasingEnemyState_obj::__mClass;

void ChasingEnemyState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("ChasingEnemyState"), hx::TCanCast< ChasingEnemyState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void ChasingEnemyState_obj::__boot()
{
}

