#include <hxcpp.h>

#ifndef INCLUDED_CollectableColliderState
#include <CollectableColliderState.h>
#endif
#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_ColliderFactory
#include <ColliderFactory.h>
#endif
#ifndef INCLUDED_IColliderState
#include <IColliderState.h>
#endif
#ifndef INCLUDED_KillableColliderState
#include <KillableColliderState.h>
#endif
#ifndef INCLUDED_LevelEndColliderState
#include <LevelEndColliderState.h>
#endif
#ifndef INCLUDED_MovableColliderState
#include <MovableColliderState.h>
#endif

Void ColliderFactory_obj::__construct()
{
	return null();
}

ColliderFactory_obj::~ColliderFactory_obj() { }

Dynamic ColliderFactory_obj::__CreateEmpty() { return  new ColliderFactory_obj; }
hx::ObjectPtr< ColliderFactory_obj > ColliderFactory_obj::__new()
{  hx::ObjectPtr< ColliderFactory_obj > result = new ColliderFactory_obj();
	result->__construct();
	return result;}

Dynamic ColliderFactory_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< ColliderFactory_obj > result = new ColliderFactory_obj();
	result->__construct();
	return result;}

::Collider ColliderFactory_obj::getCollider( Float x,Float y,Float width,Float height,int type){
	HX_STACK_PUSH("ColliderFactory::getCollider","ColliderFactory.hx",8);
	HX_STACK_ARG(x,"x");
	HX_STACK_ARG(y,"y");
	HX_STACK_ARG(width,"width");
	HX_STACK_ARG(height,"height");
	HX_STACK_ARG(type,"type");
	HX_STACK_LINE(9)
	Array< ::IColliderState > states = Array_obj< ::IColliderState >::__new();		HX_STACK_VAR(states,"states");
	HX_STACK_LINE(11)
	int _switch_1 = (type);
	if (  ( _switch_1==::Collider_obj::TYPE_PLAYER)){
		HX_STACK_LINE(13)
		states->push(::MovableColliderState_obj::__new());
		HX_STACK_LINE(14)
		states->push(::KillableColliderState_obj::__new());
	}
	else if (  ( _switch_1==::Collider_obj::TYPE_ENEMY)){
		HX_STACK_LINE(15)
		states->push(::MovableColliderState_obj::__new());
	}
	else if (  ( _switch_1==::Collider_obj::TYPE_COIN)){
		HX_STACK_LINE(17)
		states->push(::CollectableColliderState_obj::__new());
	}
	else if (  ( _switch_1==::Collider_obj::TYPE_END)){
		HX_STACK_LINE(19)
		states->push(::LevelEndColliderState_obj::__new());
	}
	else if (  ( _switch_1==::Collider_obj::TYPE_PROJECTILE)){
		HX_STACK_LINE(21)
		states->push(::MovableColliderState_obj::__new());
	}
	HX_STACK_LINE(25)
	return ::Collider_obj::__new(x,y,width,height,type,states);
}


STATIC_HX_DEFINE_DYNAMIC_FUNC5(ColliderFactory_obj,getCollider,return )


ColliderFactory_obj::ColliderFactory_obj()
{
}

void ColliderFactory_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(ColliderFactory);
	HX_MARK_END_CLASS();
}

void ColliderFactory_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic ColliderFactory_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 11:
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic ColliderFactory_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void ColliderFactory_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("getCollider"),
	String(null()) };

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ColliderFactory_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ColliderFactory_obj::__mClass,"__mClass");
};

Class ColliderFactory_obj::__mClass;

void ColliderFactory_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("ColliderFactory"), hx::TCanCast< ColliderFactory_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void ColliderFactory_obj::__boot()
{
}

