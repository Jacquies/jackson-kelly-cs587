#include <hxcpp.h>

#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_IState
#include <IState.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif

HX_DEFINE_DYNAMIC_FUNC2(IState_obj,tick,)


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IState_obj::__mClass,"__mClass");
};

Class IState_obj::__mClass;

void IState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("IState"), hx::TCanCast< IState_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IState_obj::__boot()
{
}

