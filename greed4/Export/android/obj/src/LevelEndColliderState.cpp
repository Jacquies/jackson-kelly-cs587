#include <hxcpp.h>

#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_EndOfLevel
#include <EndOfLevel.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IColliderState
#include <IColliderState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_LevelEndColliderState
#include <LevelEndColliderState.h>
#endif

Void LevelEndColliderState_obj::__construct()
{
HX_STACK_PUSH("LevelEndColliderState::new","LevelEndColliderState.hx",8);
{
}
;
	return null();
}

LevelEndColliderState_obj::~LevelEndColliderState_obj() { }

Dynamic LevelEndColliderState_obj::__CreateEmpty() { return  new LevelEndColliderState_obj; }
hx::ObjectPtr< LevelEndColliderState_obj > LevelEndColliderState_obj::__new()
{  hx::ObjectPtr< LevelEndColliderState_obj > result = new LevelEndColliderState_obj();
	result->__construct();
	return result;}

Dynamic LevelEndColliderState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< LevelEndColliderState_obj > result = new LevelEndColliderState_obj();
	result->__construct();
	return result;}

hx::Object *LevelEndColliderState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IColliderState_obj)) return operator ::IColliderState_obj *();
	return super::__ToInterface(inType);
}

Void LevelEndColliderState_obj::collide( ::ICollidable collidingObject,::ICollidable otherCollidingObject){
{
		HX_STACK_PUSH("LevelEndColliderState::collide","LevelEndColliderState.hx",10);
		HX_STACK_THIS(this);
		HX_STACK_ARG(collidingObject,"collidingObject");
		HX_STACK_ARG(otherCollidingObject,"otherCollidingObject");
		HX_STACK_LINE(10)
		if (((otherCollidingObject->getCollider()->getType() == ::Collider_obj::TYPE_PLAYER))){
			HX_STACK_LINE(11)
			(hx::TCast< EndOfLevel >::cast(collidingObject))->endLevel();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(LevelEndColliderState_obj,collide,(void))


LevelEndColliderState_obj::LevelEndColliderState_obj()
{
}

void LevelEndColliderState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(LevelEndColliderState);
	HX_MARK_END_CLASS();
}

void LevelEndColliderState_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic LevelEndColliderState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic LevelEndColliderState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void LevelEndColliderState_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(LevelEndColliderState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(LevelEndColliderState_obj::__mClass,"__mClass");
};

Class LevelEndColliderState_obj::__mClass;

void LevelEndColliderState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("LevelEndColliderState"), hx::TCanCast< LevelEndColliderState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void LevelEndColliderState_obj::__boot()
{
}

