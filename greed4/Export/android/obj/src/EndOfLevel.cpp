#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_EndOfLevel
#include <EndOfLevel.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_Level
#include <Level.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void EndOfLevel_obj::__construct(::native::geom::Point spawnPoint,::TileRenderer tileRenderer,::Level parentLevel)
{
HX_STACK_PUSH("EndOfLevel::new","EndOfLevel.hx",12);
{
	HX_STACK_LINE(13)
	this->collider = ::Collider_obj::__new(spawnPoint->x,spawnPoint->y,(int)20,(int)20,::Collider_obj::TYPE_END);
	HX_STACK_LINE(14)
	this->animation = ::Animation_obj::__new((int)32,(int)0,(int)30,spawnPoint->x,spawnPoint->y,(int)0,(int)1,tileRenderer,null());
	HX_STACK_LINE(15)
	this->parent = parentLevel;
}
;
	return null();
}

EndOfLevel_obj::~EndOfLevel_obj() { }

Dynamic EndOfLevel_obj::__CreateEmpty() { return  new EndOfLevel_obj; }
hx::ObjectPtr< EndOfLevel_obj > EndOfLevel_obj::__new(::native::geom::Point spawnPoint,::TileRenderer tileRenderer,::Level parentLevel)
{  hx::ObjectPtr< EndOfLevel_obj > result = new EndOfLevel_obj();
	result->__construct(spawnPoint,tileRenderer,parentLevel);
	return result;}

Dynamic EndOfLevel_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< EndOfLevel_obj > result = new EndOfLevel_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

hx::Object *EndOfLevel_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IRenderable_obj)) return operator ::IRenderable_obj *();
	if (inType==typeid( ::ICollidable_obj)) return operator ::ICollidable_obj *();
	return super::__ToInterface(inType);
}

Void EndOfLevel_obj::collide( ::ICollidable otherCollidable){
{
		HX_STACK_PUSH("EndOfLevel::collide","EndOfLevel.hx",35);
		HX_STACK_THIS(this);
		HX_STACK_ARG(otherCollidable,"otherCollidable");
		HX_STACK_LINE(35)
		if (((otherCollidable->getCollider()->getType() == ::Collider_obj::TYPE_PLAYER))){
			HX_STACK_LINE(36)
			this->parent->endLevel();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(EndOfLevel_obj,collide,(void))

Void EndOfLevel_obj::endLevel( ){
{
		HX_STACK_PUSH("EndOfLevel::endLevel","EndOfLevel.hx",30);
		HX_STACK_THIS(this);
		HX_STACK_LINE(30)
		this->parent->endLevel();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(EndOfLevel_obj,endLevel,(void))

Void EndOfLevel_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("EndOfLevel::render","EndOfLevel.hx",26);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(26)
		this->animation->render(deltaTime,camera);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(EndOfLevel_obj,render,(void))

::Collider EndOfLevel_obj::getCollider( ){
	HX_STACK_PUSH("EndOfLevel::getCollider","EndOfLevel.hx",18);
	HX_STACK_THIS(this);
	HX_STACK_LINE(18)
	return this->collider;
}


HX_DEFINE_DYNAMIC_FUNC0(EndOfLevel_obj,getCollider,return )


EndOfLevel_obj::EndOfLevel_obj()
{
}

void EndOfLevel_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(EndOfLevel);
	HX_MARK_MEMBER_NAME(parent,"parent");
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_MEMBER_NAME(collider,"collider");
	HX_MARK_END_CLASS();
}

void EndOfLevel_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(parent,"parent");
	HX_VISIT_MEMBER_NAME(animation,"animation");
	HX_VISIT_MEMBER_NAME(collider,"collider");
}

Dynamic EndOfLevel_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		if (HX_FIELD_EQ(inName,"parent") ) { return parent; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"endLevel") ) { return endLevel_dyn(); }
		if (HX_FIELD_EQ(inName,"collider") ) { return collider; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic EndOfLevel_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"parent") ) { parent=inValue.Cast< ::Level >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"collider") ) { collider=inValue.Cast< ::Collider >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::Animation >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void EndOfLevel_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("parent"));
	outFields->push(HX_CSTRING("animation"));
	outFields->push(HX_CSTRING("collider"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	HX_CSTRING("endLevel"),
	HX_CSTRING("render"),
	HX_CSTRING("getCollider"),
	HX_CSTRING("parent"),
	HX_CSTRING("animation"),
	HX_CSTRING("collider"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(EndOfLevel_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(EndOfLevel_obj::__mClass,"__mClass");
};

Class EndOfLevel_obj::__mClass;

void EndOfLevel_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("EndOfLevel"), hx::TCanCast< EndOfLevel_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void EndOfLevel_obj::__boot()
{
}

