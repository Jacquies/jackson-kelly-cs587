#include <hxcpp.h>

#ifndef INCLUDED_Button
#include <Button.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void Button_obj::__construct(Float xPos,Float yPos,int buttonSpriteID,Float scale,::TileRenderer tileRenderer,Dynamic func)
{
HX_STACK_PUSH("Button::new","Button.hx",14);
{
	HX_STACK_LINE(15)
	this->position = ::native::geom::Point_obj::__new(xPos,yPos);
	HX_STACK_LINE(21)
	this->width = (::TileRenderer_obj::widths->__get(buttonSpriteID) * scale);
	HX_STACK_LINE(22)
	this->height = (::TileRenderer_obj::heights->__get(buttonSpriteID) * scale);
	HX_STACK_LINE(24)
	this->renderer = tileRenderer;
	HX_STACK_LINE(27)
	this->onPressFunction = func;
	HX_STACK_LINE(29)
	tileRenderer->addTile(xPos,yPos,buttonSpriteID,(int)0,scale,(int)2);
}
;
	return null();
}

Button_obj::~Button_obj() { }

Dynamic Button_obj::__CreateEmpty() { return  new Button_obj; }
hx::ObjectPtr< Button_obj > Button_obj::__new(Float xPos,Float yPos,int buttonSpriteID,Float scale,::TileRenderer tileRenderer,Dynamic func)
{  hx::ObjectPtr< Button_obj > result = new Button_obj();
	result->__construct(xPos,yPos,buttonSpriteID,scale,tileRenderer,func);
	return result;}

Dynamic Button_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Button_obj > result = new Button_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4],inArgs[5]);
	return result;}

Void Button_obj::onClick( Float clickedX,Float clickedY){
{
		HX_STACK_PUSH("Button::onClick","Button.hx",32);
		HX_STACK_THIS(this);
		HX_STACK_ARG(clickedX,"clickedX");
		HX_STACK_ARG(clickedY,"clickedY");
		HX_STACK_LINE(32)
		if (((bool((::Math_obj::abs((this->position->x - clickedX)) < (Float(this->width) / Float((int)2)))) && bool((::Math_obj::abs((this->position->y - clickedY)) < (Float(this->height) / Float((int)2))))))){
			HX_STACK_LINE(34)
			this->onPressFunction();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Button_obj,onClick,(void))


Button_obj::Button_obj()
{
}

void Button_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Button);
	HX_MARK_MEMBER_NAME(onPressFunction,"onPressFunction");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_MEMBER_NAME(height,"height");
	HX_MARK_MEMBER_NAME(width,"width");
	HX_MARK_MEMBER_NAME(position,"position");
	HX_MARK_END_CLASS();
}

void Button_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(onPressFunction,"onPressFunction");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	HX_VISIT_MEMBER_NAME(height,"height");
	HX_VISIT_MEMBER_NAME(width,"width");
	HX_VISIT_MEMBER_NAME(position,"position");
}

Dynamic Button_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"width") ) { return width; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"height") ) { return height; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"onClick") ) { return onClick_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		if (HX_FIELD_EQ(inName,"position") ) { return position; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"onPressFunction") ) { return onPressFunction; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Button_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"width") ) { width=inValue.Cast< Float >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"height") ) { height=inValue.Cast< Float >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::TileRenderer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"position") ) { position=inValue.Cast< ::native::geom::Point >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"onPressFunction") ) { onPressFunction=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Button_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("renderer"));
	outFields->push(HX_CSTRING("height"));
	outFields->push(HX_CSTRING("width"));
	outFields->push(HX_CSTRING("position"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("onClick"),
	HX_CSTRING("onPressFunction"),
	HX_CSTRING("renderer"),
	HX_CSTRING("height"),
	HX_CSTRING("width"),
	HX_CSTRING("position"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Button_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Button_obj::__mClass,"__mClass");
};

Class Button_obj::__mClass;

void Button_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("Button"), hx::TCanCast< Button_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Button_obj::__boot()
{
}

