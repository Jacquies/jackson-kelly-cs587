#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IEnemyState
#include <IEnemyState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_IdleEnemyState
#include <IdleEnemyState.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_Wall
#include <Wall.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_geom_Rectangle
#include <native/geom/Rectangle.h>
#endif

Void Enemy_obj::__construct(::native::geom::Point spawnPoint,Array< ::IEnemyState > states,Array< ::Animation > enemyAnimations,::TileRenderer tileRenderer)
{
HX_STACK_PUSH("Enemy::new","Enemy.hx",22);
{
	HX_STACK_LINE(23)
	this->collider = ::Collider_obj::__new(spawnPoint->x,spawnPoint->y,(int)20,(int)20,::Collider_obj::TYPE_ENEMY);
	HX_STACK_LINE(25)
	this->currentAnimationID = (int)0;
	HX_STACK_LINE(26)
	this->position = spawnPoint;
	HX_STACK_LINE(27)
	this->rotation = (::Math_obj::random() * (int)360);
	HX_STACK_LINE(28)
	this->animations = enemyAnimations;
	HX_STACK_LINE(29)
	this->animation = this->animations->__get((int)0);
	HX_STACK_LINE(30)
	{
		HX_STACK_LINE(30)
		int _g1 = (int)1;		HX_STACK_VAR(_g1,"_g1");
		int _g = this->animations->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(30)
		while(((_g1 < _g))){
			HX_STACK_LINE(30)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(31)
			this->animations->__get(i)->removeFromRenderSet();
		}
	}
	HX_STACK_LINE(35)
	this->enemyStates = Array_obj< ::IEnemyState >::__new();
	HX_STACK_LINE(36)
	this->enemyStates->push(::IdleEnemyState_obj::__new(states));
}
;
	return null();
}

Enemy_obj::~Enemy_obj() { }

Dynamic Enemy_obj::__CreateEmpty() { return  new Enemy_obj; }
hx::ObjectPtr< Enemy_obj > Enemy_obj::__new(::native::geom::Point spawnPoint,Array< ::IEnemyState > states,Array< ::Animation > enemyAnimations,::TileRenderer tileRenderer)
{  hx::ObjectPtr< Enemy_obj > result = new Enemy_obj();
	result->__construct(spawnPoint,states,enemyAnimations,tileRenderer);
	return result;}

Dynamic Enemy_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Enemy_obj > result = new Enemy_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3]);
	return result;}

hx::Object *Enemy_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::ITickable_obj)) return operator ::ITickable_obj *();
	if (inType==typeid( ::IRenderable_obj)) return operator ::IRenderable_obj *();
	if (inType==typeid( ::ICollidable_obj)) return operator ::ICollidable_obj *();
	return super::__ToInterface(inType);
}

Void Enemy_obj::collide( ::ICollidable otherCollidable){
{
		HX_STACK_PUSH("Enemy::collide","Enemy.hx",107);
		HX_STACK_THIS(this);
		HX_STACK_ARG(otherCollidable,"otherCollidable");
		HX_STACK_LINE(107)
		if (((otherCollidable->getCollider()->getType() == ::Collider_obj::TYPE_ENEMY))){
			HX_STACK_LINE(110)
			::native::geom::Point collisionVector = ::native::geom::Point_obj::__new(this->collider->getBoundingBox()->intersection(otherCollidable->getCollider()->getBoundingBox())->x,this->collider->getBoundingBox()->intersection(otherCollidable->getCollider()->getBoundingBox())->y);		HX_STACK_VAR(collisionVector,"collisionVector");
			HX_STACK_LINE(112)
			if (((collisionVector->x < collisionVector->y))){
				HX_STACK_LINE(112)
				if (((this->position->x < (hx::TCast< Enemy >::cast(otherCollidable))->getPosition()->x))){
					HX_STACK_LINE(113)
					(this->position->x)--;
				}
				else{
					HX_STACK_LINE(115)
					(this->position->x)++;
				}
			}
			else{
				HX_STACK_LINE(118)
				if (((this->position->y < (hx::TCast< Enemy >::cast(otherCollidable))->getPosition()->y))){
					HX_STACK_LINE(119)
					(this->position->y)--;
				}
				else{
					HX_STACK_LINE(121)
					(this->position->y)++;
				}
			}
		}
		else{
			HX_STACK_LINE(125)
			if (((otherCollidable->getCollider()->getType() == ::Collider_obj::TYPE_WALL))){
				HX_STACK_LINE(127)
				::Wall wall = hx::TCast< Wall >::cast(otherCollidable);		HX_STACK_VAR(wall,"wall");
				HX_STACK_LINE(129)
				switch( (int)(wall->getDirection())){
					case (int)0: {
						HX_STACK_LINE(130)
						hx::AddEq(this->position->x,wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width);
					}
					;break;
					case (int)1: {
						HX_STACK_LINE(132)
						hx::SubEq(this->position->y,wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height);
					}
					;break;
					case (int)2: {
						HX_STACK_LINE(134)
						hx::SubEq(this->position->x,wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width);
					}
					;break;
					case (int)3: {
						HX_STACK_LINE(136)
						hx::AddEq(this->position->y,wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height);
					}
					;break;
				}
				HX_STACK_LINE(140)
				{
					HX_STACK_LINE(140)
					int _g = (int)0;		HX_STACK_VAR(_g,"_g");
					Array< ::IEnemyState > _g1 = this->enemyStates;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(140)
					while(((_g < _g1->length))){
						HX_STACK_LINE(140)
						::IEnemyState state = _g1->__get(_g);		HX_STACK_VAR(state,"state");
						HX_STACK_LINE(140)
						++(_g);
						HX_STACK_LINE(141)
						state->hitWall(otherCollidable);
					}
				}
			}
			else{
				HX_STACK_LINE(143)
				if (((otherCollidable->getCollider()->getType() == ::Collider_obj::TYPE_PLAYER))){
					HX_STACK_LINE(143)
					if (((::Enemy_obj::player->getHitCountdown() <= (int)0))){
						HX_STACK_LINE(145)
						this->switchAnimation((int)2);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Enemy_obj,collide,(void))

Void Enemy_obj::switchAnimation( int ID){
{
		HX_STACK_PUSH("Enemy::switchAnimation","Enemy.hx",94);
		HX_STACK_THIS(this);
		HX_STACK_ARG(ID,"ID");
		HX_STACK_LINE(94)
		if (((this->currentAnimationID != ID))){
			HX_STACK_LINE(96)
			this->animation->restartAnimation();
			HX_STACK_LINE(97)
			this->animation->removeFromRenderSet();
			HX_STACK_LINE(98)
			this->animation = this->animations->__get(ID);
			HX_STACK_LINE(99)
			this->animation->setX(this->position->x);
			HX_STACK_LINE(100)
			this->animation->setY(this->position->y);
			HX_STACK_LINE(101)
			this->animation->setRotation(this->rotation);
			HX_STACK_LINE(102)
			this->animation->addToRenderSet();
			HX_STACK_LINE(103)
			this->currentAnimationID = ID;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Enemy_obj,switchAnimation,(void))

Void Enemy_obj::exitState( ::IEnemyState state){
{
		HX_STACK_PUSH("Enemy::exitState","Enemy.hx",90);
		HX_STACK_THIS(this);
		HX_STACK_ARG(state,"state");
		HX_STACK_LINE(90)
		this->enemyStates->remove(state);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Enemy_obj,exitState,(void))

Void Enemy_obj::enterState( ::IEnemyState state){
{
		HX_STACK_PUSH("Enemy::enterState","Enemy.hx",86);
		HX_STACK_THIS(this);
		HX_STACK_ARG(state,"state");
		HX_STACK_LINE(86)
		this->enemyStates->push(state);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Enemy_obj,enterState,(void))

Void Enemy_obj::tick( int deltaTime){
{
		HX_STACK_PUSH("Enemy::tick","Enemy.hx",67);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_LINE(68)
		if (((bool((this->currentAnimationID == (int)2)) && bool(this->animation->isFinished())))){
			HX_STACK_LINE(69)
			this->switchAnimation((int)1);
			HX_STACK_LINE(70)
			::Enemy_obj::player->decreaseHealth();
		}
		HX_STACK_LINE(73)
		if (((this->currentAnimationID != (int)2))){
			HX_STACK_LINE(74)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			Array< ::IEnemyState > _g1 = this->enemyStates;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(74)
			while(((_g < _g1->length))){
				HX_STACK_LINE(74)
				::IEnemyState state = _g1->__get(_g);		HX_STACK_VAR(state,"state");
				HX_STACK_LINE(74)
				++(_g);
				HX_STACK_LINE(75)
				state->tick(deltaTime,hx::ObjectPtr<OBJ_>(this));
			}
		}
		HX_STACK_LINE(79)
		this->animation->setX(this->position->x);
		HX_STACK_LINE(80)
		this->animation->setY(this->position->y);
		HX_STACK_LINE(81)
		this->animation->setRotation((this->rotation - (int)90));
		HX_STACK_LINE(82)
		this->collider->setX(this->position->x);
		HX_STACK_LINE(83)
		this->collider->setY(this->position->y);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Enemy_obj,tick,(void))

Void Enemy_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("Enemy::render","Enemy.hx",63);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(63)
		this->animation->render(deltaTime,camera);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Enemy_obj,render,(void))

::Collider Enemy_obj::getCollider( ){
	HX_STACK_PUSH("Enemy::getCollider","Enemy.hx",59);
	HX_STACK_THIS(this);
	HX_STACK_LINE(59)
	return this->collider;
}


HX_DEFINE_DYNAMIC_FUNC0(Enemy_obj,getCollider,return )

Void Enemy_obj::setRotation( Float rot){
{
		HX_STACK_PUSH("Enemy::setRotation","Enemy.hx",55);
		HX_STACK_THIS(this);
		HX_STACK_ARG(rot,"rot");
		HX_STACK_LINE(55)
		this->rotation = rot;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Enemy_obj,setRotation,(void))

Float Enemy_obj::getRotation( ){
	HX_STACK_PUSH("Enemy::getRotation","Enemy.hx",51);
	HX_STACK_THIS(this);
	HX_STACK_LINE(51)
	return this->rotation;
}


HX_DEFINE_DYNAMIC_FUNC0(Enemy_obj,getRotation,return )

Void Enemy_obj::setY( Float y){
{
		HX_STACK_PUSH("Enemy::setY","Enemy.hx",47);
		HX_STACK_THIS(this);
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(47)
		this->position->y = y;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Enemy_obj,setY,(void))

Void Enemy_obj::setX( Float x){
{
		HX_STACK_PUSH("Enemy::setX","Enemy.hx",43);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_LINE(43)
		this->position->x = x;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Enemy_obj,setX,(void))

::native::geom::Point Enemy_obj::getPosition( ){
	HX_STACK_PUSH("Enemy::getPosition","Enemy.hx",39);
	HX_STACK_THIS(this);
	HX_STACK_LINE(39)
	return this->position;
}


HX_DEFINE_DYNAMIC_FUNC0(Enemy_obj,getPosition,return )

int Enemy_obj::TYPE_CHASING_ENEMY;

int Enemy_obj::TYPE_RANGED_ENEMY;

int Enemy_obj::TYPE_CHARGING_ENEMY;

::Player Enemy_obj::player;


Enemy_obj::Enemy_obj()
{
}

void Enemy_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Enemy);
	HX_MARK_MEMBER_NAME(animations,"animations");
	HX_MARK_MEMBER_NAME(currentAnimationID,"currentAnimationID");
	HX_MARK_MEMBER_NAME(rotation,"rotation");
	HX_MARK_MEMBER_NAME(position,"position");
	HX_MARK_MEMBER_NAME(enemyStates,"enemyStates");
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_MEMBER_NAME(collider,"collider");
	HX_MARK_END_CLASS();
}

void Enemy_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(animations,"animations");
	HX_VISIT_MEMBER_NAME(currentAnimationID,"currentAnimationID");
	HX_VISIT_MEMBER_NAME(rotation,"rotation");
	HX_VISIT_MEMBER_NAME(position,"position");
	HX_VISIT_MEMBER_NAME(enemyStates,"enemyStates");
	HX_VISIT_MEMBER_NAME(animation,"animation");
	HX_VISIT_MEMBER_NAME(collider,"collider");
}

Dynamic Enemy_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		if (HX_FIELD_EQ(inName,"setY") ) { return setY_dyn(); }
		if (HX_FIELD_EQ(inName,"setX") ) { return setX_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { return player; }
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"rotation") ) { return rotation; }
		if (HX_FIELD_EQ(inName,"position") ) { return position; }
		if (HX_FIELD_EQ(inName,"collider") ) { return collider; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"exitState") ) { return exitState_dyn(); }
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"enterState") ) { return enterState_dyn(); }
		if (HX_FIELD_EQ(inName,"animations") ) { return animations; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
		if (HX_FIELD_EQ(inName,"setRotation") ) { return setRotation_dyn(); }
		if (HX_FIELD_EQ(inName,"getRotation") ) { return getRotation_dyn(); }
		if (HX_FIELD_EQ(inName,"getPosition") ) { return getPosition_dyn(); }
		if (HX_FIELD_EQ(inName,"enemyStates") ) { return enemyStates; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"switchAnimation") ) { return switchAnimation_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"TYPE_RANGED_ENEMY") ) { return TYPE_RANGED_ENEMY; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"TYPE_CHASING_ENEMY") ) { return TYPE_CHASING_ENEMY; }
		if (HX_FIELD_EQ(inName,"currentAnimationID") ) { return currentAnimationID; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"TYPE_CHARGING_ENEMY") ) { return TYPE_CHARGING_ENEMY; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Enemy_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { player=inValue.Cast< ::Player >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"rotation") ) { rotation=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"position") ) { position=inValue.Cast< ::native::geom::Point >(); return inValue; }
		if (HX_FIELD_EQ(inName,"collider") ) { collider=inValue.Cast< ::Collider >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::Animation >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"animations") ) { animations=inValue.Cast< Array< ::Animation > >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"enemyStates") ) { enemyStates=inValue.Cast< Array< ::IEnemyState > >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"TYPE_RANGED_ENEMY") ) { TYPE_RANGED_ENEMY=inValue.Cast< int >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"TYPE_CHASING_ENEMY") ) { TYPE_CHASING_ENEMY=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"currentAnimationID") ) { currentAnimationID=inValue.Cast< int >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"TYPE_CHARGING_ENEMY") ) { TYPE_CHARGING_ENEMY=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Enemy_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("animations"));
	outFields->push(HX_CSTRING("currentAnimationID"));
	outFields->push(HX_CSTRING("rotation"));
	outFields->push(HX_CSTRING("position"));
	outFields->push(HX_CSTRING("enemyStates"));
	outFields->push(HX_CSTRING("animation"));
	outFields->push(HX_CSTRING("collider"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("TYPE_CHASING_ENEMY"),
	HX_CSTRING("TYPE_RANGED_ENEMY"),
	HX_CSTRING("TYPE_CHARGING_ENEMY"),
	HX_CSTRING("player"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	HX_CSTRING("switchAnimation"),
	HX_CSTRING("exitState"),
	HX_CSTRING("enterState"),
	HX_CSTRING("tick"),
	HX_CSTRING("render"),
	HX_CSTRING("getCollider"),
	HX_CSTRING("setRotation"),
	HX_CSTRING("getRotation"),
	HX_CSTRING("setY"),
	HX_CSTRING("setX"),
	HX_CSTRING("getPosition"),
	HX_CSTRING("animations"),
	HX_CSTRING("currentAnimationID"),
	HX_CSTRING("rotation"),
	HX_CSTRING("position"),
	HX_CSTRING("enemyStates"),
	HX_CSTRING("animation"),
	HX_CSTRING("collider"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Enemy_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(Enemy_obj::TYPE_CHASING_ENEMY,"TYPE_CHASING_ENEMY");
	HX_MARK_MEMBER_NAME(Enemy_obj::TYPE_RANGED_ENEMY,"TYPE_RANGED_ENEMY");
	HX_MARK_MEMBER_NAME(Enemy_obj::TYPE_CHARGING_ENEMY,"TYPE_CHARGING_ENEMY");
	HX_MARK_MEMBER_NAME(Enemy_obj::player,"player");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Enemy_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(Enemy_obj::TYPE_CHASING_ENEMY,"TYPE_CHASING_ENEMY");
	HX_VISIT_MEMBER_NAME(Enemy_obj::TYPE_RANGED_ENEMY,"TYPE_RANGED_ENEMY");
	HX_VISIT_MEMBER_NAME(Enemy_obj::TYPE_CHARGING_ENEMY,"TYPE_CHARGING_ENEMY");
	HX_VISIT_MEMBER_NAME(Enemy_obj::player,"player");
};

Class Enemy_obj::__mClass;

void Enemy_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("Enemy"), hx::TCanCast< Enemy_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Enemy_obj::__boot()
{
	TYPE_CHASING_ENEMY= (int)0;
	TYPE_RANGED_ENEMY= (int)1;
	TYPE_CHARGING_ENEMY= (int)2;
}

