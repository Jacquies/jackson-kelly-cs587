#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_ChargingEnemyState
#include <ChargingEnemyState.h>
#endif
#ifndef INCLUDED_ChasingEnemyState
#include <ChasingEnemyState.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_EnemyFactory
#include <EnemyFactory.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IEnemyState
#include <IEnemyState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_RangedAttackEnemyState
#include <RangedAttackEnemyState.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_StrafingEnemyState
#include <StrafingEnemyState.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif

Void EnemyFactory_obj::__construct()
{
	return null();
}

EnemyFactory_obj::~EnemyFactory_obj() { }

Dynamic EnemyFactory_obj::__CreateEmpty() { return  new EnemyFactory_obj; }
hx::ObjectPtr< EnemyFactory_obj > EnemyFactory_obj::__new()
{  hx::ObjectPtr< EnemyFactory_obj > result = new EnemyFactory_obj();
	result->__construct();
	return result;}

Dynamic EnemyFactory_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< EnemyFactory_obj > result = new EnemyFactory_obj();
	result->__construct();
	return result;}

::Enemy EnemyFactory_obj::getEnemy( ::native::geom::Point position,int type,::TileRenderer tileRenderer){
	HX_STACK_PUSH("EnemyFactory::getEnemy","EnemyFactory.hx",9);
	HX_STACK_ARG(position,"position");
	HX_STACK_ARG(type,"type");
	HX_STACK_ARG(tileRenderer,"tileRenderer");
	HX_STACK_LINE(10)
	Array< ::IEnemyState > states = Array_obj< ::IEnemyState >::__new();		HX_STACK_VAR(states,"states");
	HX_STACK_LINE(11)
	Array< ::Animation > animations = Array_obj< ::Animation >::__new();		HX_STACK_VAR(animations,"animations");
	HX_STACK_LINE(13)
	int _switch_1 = (type);
	if (  ( _switch_1==::Enemy_obj::TYPE_CHASING_ENEMY)){
		HX_STACK_LINE(15)
		states->push(::ChasingEnemyState_obj::__new(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true)));
		HX_STACK_LINE(16)
		animations->push(::Animation_obj::__new((int)93,(int)0,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(17)
		animations->push(::Animation_obj::__new((int)93,(int)1,(int)15,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(18)
		animations->push(::Animation_obj::__new((int)94,(int)9,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,false));
	}
	else if (  ( _switch_1==::Enemy_obj::TYPE_CHARGING_ENEMY)){
		HX_STACK_LINE(20)
		states->push(::ChargingEnemyState_obj::__new());
		HX_STACK_LINE(21)
		animations->push(::Animation_obj::__new((int)104,(int)0,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(22)
		animations->push(::Animation_obj::__new((int)104,(int)1,(int)15,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(23)
		animations->push(::Animation_obj::__new((int)105,(int)9,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,false));
	}
	else if (  ( _switch_1==::Enemy_obj::TYPE_RANGED_ENEMY)){
		HX_STACK_LINE(25)
		states->push(::StrafingEnemyState_obj::__new());
		HX_STACK_LINE(26)
		states->push(::RangedAttackEnemyState_obj::__new());
		HX_STACK_LINE(27)
		animations->push(::Animation_obj::__new((int)115,(int)0,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(28)
		animations->push(::Animation_obj::__new((int)115,(int)1,(int)15,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(29)
		animations->push(::Animation_obj::__new((int)116,(int)9,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,false));
	}
	HX_STACK_LINE(32)
	return ::Enemy_obj::__new(position,states,animations,tileRenderer);
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(EnemyFactory_obj,getEnemy,return )


EnemyFactory_obj::EnemyFactory_obj()
{
}

void EnemyFactory_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(EnemyFactory);
	HX_MARK_END_CLASS();
}

void EnemyFactory_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic EnemyFactory_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"getEnemy") ) { return getEnemy_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic EnemyFactory_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void EnemyFactory_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("getEnemy"),
	String(null()) };

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(EnemyFactory_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(EnemyFactory_obj::__mClass,"__mClass");
};

Class EnemyFactory_obj::__mClass;

void EnemyFactory_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("EnemyFactory"), hx::TCanCast< EnemyFactory_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void EnemyFactory_obj::__boot()
{
}

