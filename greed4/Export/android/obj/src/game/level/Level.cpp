#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
#ifndef INCLUDED_engine_managers_SceneManager
#include <engine/managers/SceneManager.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_menuItems_Button
#include <engine/menuItems/Button.h>
#endif
#ifndef INCLUDED_engine_menuItems_MainMenu
#include <engine/menuItems/MainMenu.h>
#endif
#ifndef INCLUDED_engine_menuItems_UpgradeMenu
#include <engine/menuItems/UpgradeMenu.h>
#endif
#ifndef INCLUDED_engine_objects_Background
#include <engine/objects/Background.h>
#endif
#ifndef INCLUDED_game_level_Level
#include <game/level/Level.h>
#endif
#ifndef INCLUDED_game_level_LevelMap
#include <game/level/LevelMap.h>
#endif
#ifndef INCLUDED_game_objects_EndOfLevel
#include <game/objects/EndOfLevel.h>
#endif
#ifndef INCLUDED_game_objects_GoldPiece
#include <game/objects/GoldPiece.h>
#endif
#ifndef INCLUDED_game_objects_Player
#include <game/objects/Player.h>
#endif
#ifndef INCLUDED_game_objects_Wall
#include <game/objects/Wall.h>
#endif
#ifndef INCLUDED_game_objects_enemy_Enemy
#include <game/objects/enemy/Enemy.h>
#endif
#ifndef INCLUDED_game_objects_enemy_EnemyFactory
#include <game/objects/enemy/EnemyFactory.h>
#endif
#ifndef INCLUDED_native_Lib
#include <native/Lib.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_display_Stage
#include <native/display/Stage.h>
#endif
#ifndef INCLUDED_native_events_Event
#include <native/events/Event.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_MouseEvent
#include <native/events/MouseEvent.h>
#endif
#ifndef INCLUDED_native_events_TouchEvent
#include <native/events/TouchEvent.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
#ifndef INCLUDED_native_net_SharedObjectFlushStatus
#include <native/net/SharedObjectFlushStatus.h>
#endif
namespace game{
namespace level{

Void Level_obj::__construct(::engine::managers::TileRenderer tileRenderer)
{
HX_STACK_PUSH("Level::new","game/level/Level.hx",37);
{
	HX_STACK_LINE(38)
	super::__construct();
	HX_STACK_LINE(40)
	this->renderer = tileRenderer;
	HX_STACK_LINE(42)
	this->addEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null(),null(),null());
}
;
	return null();
}

Level_obj::~Level_obj() { }

Dynamic Level_obj::__CreateEmpty() { return  new Level_obj; }
hx::ObjectPtr< Level_obj > Level_obj::__new(::engine::managers::TileRenderer tileRenderer)
{  hx::ObjectPtr< Level_obj > result = new Level_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic Level_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Level_obj > result = new Level_obj();
	result->__construct(inArgs[0]);
	return result;}

Void Level_obj::dummyFunction( ){
{
		HX_STACK_PUSH("Level::dummyFunction","game/level/Level.hx",253);
		HX_STACK_THIS(this);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Level_obj,dummyFunction,(void))

Void Level_obj::cleanup( ){
{
		HX_STACK_PUSH("Level::cleanup","game/level/Level.hx",210);
		HX_STACK_THIS(this);
		HX_STACK_LINE(211)
		this->sceneManager->setLevelOver(true);
		HX_STACK_LINE(213)
		this->removeEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null());
		HX_STACK_LINE(214)
		this->removeEventListener(::native::events::Event_obj::ENTER_FRAME,this->onEnterFrame_dyn(),null());
		HX_STACK_LINE(219)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null());
		HX_STACK_LINE(220)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_END,this->onTouchEnd_dyn(),null());
		HX_STACK_LINE(221)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_MOVE,this->onTouchMove_dyn(),null());
		HX_STACK_LINE(224)
		this->sceneManager->clearCollisionSet();
		HX_STACK_LINE(225)
		this->sceneManager->clearRenderSet();
		HX_STACK_LINE(226)
		this->sceneManager->clearTickableSet();
		HX_STACK_LINE(228)
		this->renderer->clear();
		HX_STACK_LINE(230)
		this->sceneManager->getSharedObject()->flush(null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Level_obj,cleanup,(void))

Void Level_obj::returnToMainMenu( ){
{
		HX_STACK_PUSH("Level::returnToMainMenu","game/level/Level.hx",202);
		HX_STACK_THIS(this);
		HX_STACK_LINE(203)
		this->cleanup();
		HX_STACK_LINE(205)
		this->get_parent()->addChild(::engine::menuItems::MainMenu_obj::__new(this->renderer));
		HX_STACK_LINE(207)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Level_obj,returnToMainMenu,(void))

Void Level_obj::endLevel( ){
{
		HX_STACK_PUSH("Level::endLevel","game/level/Level.hx",194);
		HX_STACK_THIS(this);
		HX_STACK_LINE(195)
		this->cleanup();
		HX_STACK_LINE(197)
		this->get_parent()->addChild(::engine::menuItems::UpgradeMenu_obj::__new(this->renderer));
		HX_STACK_LINE(199)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Level_obj,endLevel,(void))

Void Level_obj::onTouchMove( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("Level::onTouchMove","game/level/Level.hx",189);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(189)
		this->sceneManager->setMousePosition(event->localX,event->localY);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onTouchMove,(void))

Void Level_obj::onTouchEnd( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("Level::onTouchEnd","game/level/Level.hx",185);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(185)
		this->sceneManager->setMouseDown(false);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onTouchEnd,(void))

Void Level_obj::onTouchBegin( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("Level::onTouchBegin","game/level/Level.hx",177);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(178)
		this->sceneManager->setMouseDown(true);
		HX_STACK_LINE(179)
		this->sceneManager->setMousePosition(this->get_mouseX(),this->get_mouseY());
		HX_STACK_LINE(180)
		if (((this->deathScreenCountdown <= (int)0))){
			HX_STACK_LINE(180)
			this->mainMenuButton->onClick(this->get_mouseX(),this->get_mouseY());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onTouchBegin,(void))

Void Level_obj::onEnterFrame( ::native::events::Event event){
{
		HX_STACK_PUSH("Level::onEnterFrame","game/level/Level.hx",143);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(144)
		if ((this->sceneManager->isIdle())){
			HX_STACK_LINE(144)
			(this->deathScreenCountdown)--;
		}
		else{
			HX_STACK_LINE(146)
			this->deathScreenCountdown = (int)40;
		}
		HX_STACK_LINE(149)
		if (((this->deathScreenCountdown == (int)0))){
			HX_STACK_LINE(150)
			::engine::menuItems::Button_obj::__new((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)2)),(int)61,::engine::managers::SceneManager_obj::scale,this->renderer,this->dummyFunction_dyn());
			HX_STACK_LINE(151)
			this->mainMenuButton = ::engine::menuItems::Button_obj::__new((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),((Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)2)) + (Float((::engine::managers::TileRenderer_obj::heights->__get((int)51) * ::engine::managers::SceneManager_obj::scale)) / Float((int)2))),(int)51,::engine::managers::SceneManager_obj::scale,this->renderer,this->returnToMainMenu_dyn());
		}
		HX_STACK_LINE(154)
		int deltaTime = (::native::Lib_obj::getTimer() - this->previousTickTime);		HX_STACK_VAR(deltaTime,"deltaTime");
		HX_STACK_LINE(155)
		this->previousTickTime = ::native::Lib_obj::getTimer();
		HX_STACK_LINE(161)
		this->sceneManager->tick(deltaTime);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onEnterFrame,(void))

Void Level_obj::onAddedToStage( ::native::events::Event event){
{
		HX_STACK_PUSH("Level::onAddedToStage","game/level/Level.hx",45);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(46)
		this->renderer->clear();
		HX_STACK_LINE(47)
		this->sceneManager = ::engine::managers::SceneManager_obj::__new(this->renderer);
		HX_STACK_LINE(48)
		this->sceneManager->initialize();
		HX_STACK_LINE(49)
		this->previousTickTime = ::native::Lib_obj::getTimer();
		HX_STACK_LINE(51)
		this->deathScreenCountdown = (int)45;
		HX_STACK_LINE(53)
		this->addEventListener(::native::events::Event_obj::ENTER_FRAME,this->onEnterFrame_dyn(),null(),null(),null());
		HX_STACK_LINE(58)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null(),null(),null());
		HX_STACK_LINE(59)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_END,this->onTouchEnd_dyn(),null(),null(),null());
		HX_STACK_LINE(60)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_MOVE,this->onTouchMove_dyn(),null(),null(),null());
		HX_STACK_LINE(71)
		this->renderer->addTile((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)2)),(int)57,(int)0,(Float(::Math_obj::max(::engine::managers::TileRenderer_obj::stageWidth,::engine::managers::TileRenderer_obj::stageHeight)) / Float(::engine::managers::TileRenderer_obj::widths->__get((int)57))),(int)0);
		HX_STACK_LINE(75)
		this->map = ::game::level::LevelMap_obj::__new(this->sceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true));
		HX_STACK_LINE(78)
		this->levelEnd = ::game::objects::EndOfLevel_obj::__new(this->map->generateLevelEnd(this->sceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true)),this->renderer,hx::ObjectPtr<OBJ_>(this));
		HX_STACK_LINE(79)
		this->sceneManager->addToCollisionSet(this->levelEnd);
		HX_STACK_LINE(80)
		this->sceneManager->addToRenderSet(this->levelEnd);
		HX_STACK_LINE(83)
		Array< ::native::geom::Point > points = this->map->generateCoins(this->sceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true));		HX_STACK_VAR(points,"points");
		HX_STACK_LINE(84)
		this->coins = Array_obj< ::game::objects::GoldPiece >::__new();
		HX_STACK_LINE(85)
		{
			HX_STACK_LINE(85)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = points->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(85)
			while(((_g1 < _g))){
				HX_STACK_LINE(85)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(86)
				this->coins->push(::game::objects::GoldPiece_obj::__new(points->__get(i),this->renderer));
				HX_STACK_LINE(87)
				this->sceneManager->addToCollisionSet(this->coins->__get(i));
				HX_STACK_LINE(88)
				this->sceneManager->addToRenderSet(this->coins->__get(i));
			}
		}
		HX_STACK_LINE(92)
		points = this->map->generateEnemies(this->sceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true));
		HX_STACK_LINE(93)
		this->enemies = Array_obj< ::game::objects::enemy::Enemy >::__new();
		HX_STACK_LINE(94)
		{
			HX_STACK_LINE(94)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = points->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(94)
			while(((_g1 < _g))){
				HX_STACK_LINE(94)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(95)
				if (((::Math_obj::random() < .5))){
					HX_STACK_LINE(95)
					this->enemies->push(::game::objects::enemy::EnemyFactory_obj::getEnemy(points->__get(i),::game::objects::enemy::Enemy_obj::TYPE_CHASING_ENEMY,this->renderer));
				}
				else{
					HX_STACK_LINE(97)
					this->enemies->push(::game::objects::enemy::EnemyFactory_obj::getEnemy(points->__get(i),::game::objects::enemy::Enemy_obj::TYPE_CHARGING_ENEMY,this->renderer));
				}
				HX_STACK_LINE(108)
				this->sceneManager->addToCollisionSet(this->enemies->__get(i));
				HX_STACK_LINE(109)
				this->sceneManager->addToRenderSet(this->enemies->__get(i));
				HX_STACK_LINE(110)
				this->sceneManager->addToTickableSet(this->enemies->__get(i));
			}
		}
		HX_STACK_LINE(114)
		points = this->map->generateBackgroundPositions();
		HX_STACK_LINE(115)
		this->backgrounds = Array_obj< ::engine::objects::Background >::__new();
		HX_STACK_LINE(116)
		{
			HX_STACK_LINE(116)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = points->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(116)
			while(((_g1 < _g))){
				HX_STACK_LINE(116)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(117)
				this->backgrounds->push(::engine::objects::Background_obj::__new(points->__get(i),this->renderer));
				HX_STACK_LINE(118)
				this->sceneManager->addToRenderSet(this->backgrounds->__get(i));
			}
		}
		HX_STACK_LINE(122)
		Array< int > directions = Array_obj< int >::__new();		HX_STACK_VAR(directions,"directions");
		HX_STACK_LINE(123)
		points = this->map->generateWallPositions(directions);
		HX_STACK_LINE(124)
		this->walls = Array_obj< ::game::objects::Wall >::__new();
		HX_STACK_LINE(125)
		{
			HX_STACK_LINE(125)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = points->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(125)
			while(((_g1 < _g))){
				HX_STACK_LINE(125)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(126)
				this->walls->push(::game::objects::Wall_obj::__new(points->__get(i),directions->__get(i),this->renderer));
				HX_STACK_LINE(127)
				this->sceneManager->addToCollisionSet(this->walls->__get(i));
				HX_STACK_LINE(129)
				this->sceneManager->addToRenderSet(this->walls->__get(i));
			}
		}
		HX_STACK_LINE(134)
		this->player = ::game::objects::Player_obj::__new(this->renderer);
		HX_STACK_LINE(135)
		this->sceneManager->addToCollisionSet(this->player);
		HX_STACK_LINE(136)
		this->sceneManager->addToRenderSet(this->player);
		HX_STACK_LINE(137)
		this->sceneManager->addToTickableSet(this->player);
		HX_STACK_LINE(138)
		::game::objects::enemy::Enemy_obj::player = this->player;
		HX_STACK_LINE(140)
		(this->sceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("difficulty")))++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Level_obj,onAddedToStage,(void))

Void Level_obj::initializeLocalData( ::native::net::SharedObject localData){
{
		HX_STACK_PUSH("Level::initializeLocalData","game/level/Level.hx",233);
		HX_STACK_ARG(localData,"localData");
		HX_STACK_LINE(234)
		localData->data->__FieldRef(HX_CSTRING("initialized")) = true;
		HX_STACK_LINE(235)
		localData->data->__FieldRef(HX_CSTRING("mostGold")) = (int)0;
		HX_STACK_LINE(236)
		::game::level::Level_obj::newGame(localData);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Level_obj,initializeLocalData,(void))

Void Level_obj::newGame( ::native::net::SharedObject localData){
{
		HX_STACK_PUSH("Level::newGame","game/level/Level.hx",239);
		HX_STACK_ARG(localData,"localData");
		HX_STACK_LINE(240)
		localData->data->__FieldRef(HX_CSTRING("gold")) = (int)0;
		HX_STACK_LINE(241)
		localData->data->__FieldRef(HX_CSTRING("totalGold")) = (int)0;
		HX_STACK_LINE(242)
		localData->data->__FieldRef(HX_CSTRING("difficulty")) = (int)1;
		HX_STACK_LINE(243)
		localData->data->__FieldRef(HX_CSTRING("newGame")) = false;
		HX_STACK_LINE(245)
		localData->data->__FieldRef(HX_CSTRING("health")) = (int)1;
		HX_STACK_LINE(246)
		localData->data->__FieldRef(HX_CSTRING("speed")) = (int)4;
		HX_STACK_LINE(247)
		localData->data->__FieldRef(HX_CSTRING("collectRadius")) = (int)10;
		HX_STACK_LINE(248)
		localData->data->__FieldRef(HX_CSTRING("aggroRadius")) = (int)150;
		HX_STACK_LINE(250)
		localData->flush(null());
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Level_obj,newGame,(void))


Level_obj::Level_obj()
{
}

void Level_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Level);
	HX_MARK_MEMBER_NAME(previousTickTime,"previousTickTime");
	HX_MARK_MEMBER_NAME(mainMenuButton,"mainMenuButton");
	HX_MARK_MEMBER_NAME(deathScreenCountdown,"deathScreenCountdown");
	HX_MARK_MEMBER_NAME(backgrounds,"backgrounds");
	HX_MARK_MEMBER_NAME(walls,"walls");
	HX_MARK_MEMBER_NAME(levelEnd,"levelEnd");
	HX_MARK_MEMBER_NAME(coins,"coins");
	HX_MARK_MEMBER_NAME(enemies,"enemies");
	HX_MARK_MEMBER_NAME(player,"player");
	HX_MARK_MEMBER_NAME(map,"map");
	HX_MARK_MEMBER_NAME(sceneManager,"sceneManager");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	super::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Level_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(previousTickTime,"previousTickTime");
	HX_VISIT_MEMBER_NAME(mainMenuButton,"mainMenuButton");
	HX_VISIT_MEMBER_NAME(deathScreenCountdown,"deathScreenCountdown");
	HX_VISIT_MEMBER_NAME(backgrounds,"backgrounds");
	HX_VISIT_MEMBER_NAME(walls,"walls");
	HX_VISIT_MEMBER_NAME(levelEnd,"levelEnd");
	HX_VISIT_MEMBER_NAME(coins,"coins");
	HX_VISIT_MEMBER_NAME(enemies,"enemies");
	HX_VISIT_MEMBER_NAME(player,"player");
	HX_VISIT_MEMBER_NAME(map,"map");
	HX_VISIT_MEMBER_NAME(sceneManager,"sceneManager");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	super::__Visit(HX_VISIT_ARG);
}

Dynamic Level_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"map") ) { return map; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"walls") ) { return walls; }
		if (HX_FIELD_EQ(inName,"coins") ) { return coins; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { return player; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"newGame") ) { return newGame_dyn(); }
		if (HX_FIELD_EQ(inName,"cleanup") ) { return cleanup_dyn(); }
		if (HX_FIELD_EQ(inName,"enemies") ) { return enemies; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"endLevel") ) { return endLevel_dyn(); }
		if (HX_FIELD_EQ(inName,"levelEnd") ) { return levelEnd; }
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"onTouchEnd") ) { return onTouchEnd_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"onTouchMove") ) { return onTouchMove_dyn(); }
		if (HX_FIELD_EQ(inName,"backgrounds") ) { return backgrounds; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"onTouchBegin") ) { return onTouchBegin_dyn(); }
		if (HX_FIELD_EQ(inName,"onEnterFrame") ) { return onEnterFrame_dyn(); }
		if (HX_FIELD_EQ(inName,"sceneManager") ) { return sceneManager; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"dummyFunction") ) { return dummyFunction_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"onAddedToStage") ) { return onAddedToStage_dyn(); }
		if (HX_FIELD_EQ(inName,"mainMenuButton") ) { return mainMenuButton; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"returnToMainMenu") ) { return returnToMainMenu_dyn(); }
		if (HX_FIELD_EQ(inName,"previousTickTime") ) { return previousTickTime; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"initializeLocalData") ) { return initializeLocalData_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"deathScreenCountdown") ) { return deathScreenCountdown; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Level_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"map") ) { map=inValue.Cast< ::game::level::LevelMap >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"walls") ) { walls=inValue.Cast< Array< ::game::objects::Wall > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"coins") ) { coins=inValue.Cast< Array< ::game::objects::GoldPiece > >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { player=inValue.Cast< ::game::objects::Player >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"enemies") ) { enemies=inValue.Cast< Array< ::game::objects::enemy::Enemy > >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"levelEnd") ) { levelEnd=inValue.Cast< ::game::objects::EndOfLevel >(); return inValue; }
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::engine::managers::TileRenderer >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"backgrounds") ) { backgrounds=inValue.Cast< Array< ::engine::objects::Background > >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"sceneManager") ) { sceneManager=inValue.Cast< ::engine::managers::SceneManager >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"mainMenuButton") ) { mainMenuButton=inValue.Cast< ::engine::menuItems::Button >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"previousTickTime") ) { previousTickTime=inValue.Cast< int >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"deathScreenCountdown") ) { deathScreenCountdown=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Level_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("previousTickTime"));
	outFields->push(HX_CSTRING("mainMenuButton"));
	outFields->push(HX_CSTRING("deathScreenCountdown"));
	outFields->push(HX_CSTRING("backgrounds"));
	outFields->push(HX_CSTRING("walls"));
	outFields->push(HX_CSTRING("levelEnd"));
	outFields->push(HX_CSTRING("coins"));
	outFields->push(HX_CSTRING("enemies"));
	outFields->push(HX_CSTRING("player"));
	outFields->push(HX_CSTRING("map"));
	outFields->push(HX_CSTRING("sceneManager"));
	outFields->push(HX_CSTRING("renderer"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("initializeLocalData"),
	HX_CSTRING("newGame"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("dummyFunction"),
	HX_CSTRING("cleanup"),
	HX_CSTRING("returnToMainMenu"),
	HX_CSTRING("endLevel"),
	HX_CSTRING("onTouchMove"),
	HX_CSTRING("onTouchEnd"),
	HX_CSTRING("onTouchBegin"),
	HX_CSTRING("onEnterFrame"),
	HX_CSTRING("onAddedToStage"),
	HX_CSTRING("previousTickTime"),
	HX_CSTRING("mainMenuButton"),
	HX_CSTRING("deathScreenCountdown"),
	HX_CSTRING("backgrounds"),
	HX_CSTRING("walls"),
	HX_CSTRING("levelEnd"),
	HX_CSTRING("coins"),
	HX_CSTRING("enemies"),
	HX_CSTRING("player"),
	HX_CSTRING("map"),
	HX_CSTRING("sceneManager"),
	HX_CSTRING("renderer"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Level_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Level_obj::__mClass,"__mClass");
};

Class Level_obj::__mClass;

void Level_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.level.Level"), hx::TCanCast< Level_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Level_obj::__boot()
{
}

} // end namespace game
} // end namespace level
