#include <hxcpp.h>

#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
#ifndef INCLUDED_engine_managers_SceneManager
#include <engine/managers/SceneManager.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_objects_Animation
#include <engine/objects/Animation.h>
#endif
#ifndef INCLUDED_game_objects_enemy_Enemy
#include <game/objects/enemy/Enemy.h>
#endif
#ifndef INCLUDED_game_objects_enemy_EnemyFactory
#include <game/objects/enemy/EnemyFactory.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_ChargingEnemyState
#include <game/objects/enemy/ai/ChargingEnemyState.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_ChasingEnemyState
#include <game/objects/enemy/ai/ChasingEnemyState.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_IEnemyState
#include <game/objects/enemy/ai/IEnemyState.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_RangedAttackEnemyState
#include <game/objects/enemy/ai/RangedAttackEnemyState.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_StrafingEnemyState
#include <game/objects/enemy/ai/StrafingEnemyState.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
namespace game{
namespace objects{
namespace enemy{

Void EnemyFactory_obj::__construct()
{
	return null();
}

EnemyFactory_obj::~EnemyFactory_obj() { }

Dynamic EnemyFactory_obj::__CreateEmpty() { return  new EnemyFactory_obj; }
hx::ObjectPtr< EnemyFactory_obj > EnemyFactory_obj::__new()
{  hx::ObjectPtr< EnemyFactory_obj > result = new EnemyFactory_obj();
	result->__construct();
	return result;}

Dynamic EnemyFactory_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< EnemyFactory_obj > result = new EnemyFactory_obj();
	result->__construct();
	return result;}

::game::objects::enemy::Enemy EnemyFactory_obj::getEnemy( ::native::geom::Point position,int type,::engine::managers::TileRenderer tileRenderer){
	HX_STACK_PUSH("EnemyFactory::getEnemy","game/objects/enemy/EnemyFactory.hx",17);
	HX_STACK_ARG(position,"position");
	HX_STACK_ARG(type,"type");
	HX_STACK_ARG(tileRenderer,"tileRenderer");
	HX_STACK_LINE(18)
	Array< ::game::objects::enemy::ai::IEnemyState > states = Array_obj< ::game::objects::enemy::ai::IEnemyState >::__new();		HX_STACK_VAR(states,"states");
	HX_STACK_LINE(19)
	Array< ::engine::objects::Animation > animations = Array_obj< ::engine::objects::Animation >::__new();		HX_STACK_VAR(animations,"animations");
	HX_STACK_LINE(21)
	int _switch_1 = (type);
	if (  ( _switch_1==::game::objects::enemy::Enemy_obj::TYPE_CHASING_ENEMY)){
		HX_STACK_LINE(23)
		states->push(::game::objects::enemy::ai::ChasingEnemyState_obj::__new(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("difficulty"),true)));
		HX_STACK_LINE(24)
		animations->push(::engine::objects::Animation_obj::__new((int)93,(int)0,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(25)
		animations->push(::engine::objects::Animation_obj::__new((int)93,(int)1,(int)15,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(26)
		animations->push(::engine::objects::Animation_obj::__new((int)94,(int)9,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,false));
	}
	else if (  ( _switch_1==::game::objects::enemy::Enemy_obj::TYPE_CHARGING_ENEMY)){
		HX_STACK_LINE(28)
		states->push(::game::objects::enemy::ai::ChargingEnemyState_obj::__new());
		HX_STACK_LINE(29)
		animations->push(::engine::objects::Animation_obj::__new((int)104,(int)0,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(30)
		animations->push(::engine::objects::Animation_obj::__new((int)104,(int)1,(int)15,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(31)
		animations->push(::engine::objects::Animation_obj::__new((int)105,(int)9,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,false));
	}
	else if (  ( _switch_1==::game::objects::enemy::Enemy_obj::TYPE_RANGED_ENEMY)){
		HX_STACK_LINE(33)
		states->push(::game::objects::enemy::ai::StrafingEnemyState_obj::__new());
		HX_STACK_LINE(34)
		states->push(::game::objects::enemy::ai::RangedAttackEnemyState_obj::__new());
		HX_STACK_LINE(35)
		animations->push(::engine::objects::Animation_obj::__new((int)115,(int)0,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(36)
		animations->push(::engine::objects::Animation_obj::__new((int)115,(int)1,(int)15,position->x,position->y,(int)0,(int)1,tileRenderer,null()));
		HX_STACK_LINE(37)
		animations->push(::engine::objects::Animation_obj::__new((int)116,(int)9,(int)30,position->x,position->y,(int)0,(int)1,tileRenderer,false));
	}
	HX_STACK_LINE(40)
	return ::game::objects::enemy::Enemy_obj::__new(position,states,animations,tileRenderer);
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(EnemyFactory_obj,getEnemy,return )


EnemyFactory_obj::EnemyFactory_obj()
{
}

void EnemyFactory_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(EnemyFactory);
	HX_MARK_END_CLASS();
}

void EnemyFactory_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic EnemyFactory_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"getEnemy") ) { return getEnemy_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic EnemyFactory_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void EnemyFactory_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("getEnemy"),
	String(null()) };

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(EnemyFactory_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(EnemyFactory_obj::__mClass,"__mClass");
};

Class EnemyFactory_obj::__mClass;

void EnemyFactory_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.enemy.EnemyFactory"), hx::TCanCast< EnemyFactory_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void EnemyFactory_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
} // end namespace enemy
