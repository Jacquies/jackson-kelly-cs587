#include <hxcpp.h>

#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
#ifndef INCLUDED_game_objects_enemy_Enemy
#include <game/objects/enemy/Enemy.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_IEnemyState
#include <game/objects/enemy/ai/IEnemyState.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_RangedAttackEnemyState
#include <game/objects/enemy/ai/RangedAttackEnemyState.h>
#endif
namespace game{
namespace objects{
namespace enemy{
namespace ai{

Void RangedAttackEnemyState_obj::__construct()
{
HX_STACK_PUSH("RangedAttackEnemyState::new","game/objects/enemy/ai/RangedAttackEnemyState.hx",13);
{
	HX_STACK_LINE(13)
	this->fireCooldown = (int)35;
}
;
	return null();
}

RangedAttackEnemyState_obj::~RangedAttackEnemyState_obj() { }

Dynamic RangedAttackEnemyState_obj::__CreateEmpty() { return  new RangedAttackEnemyState_obj; }
hx::ObjectPtr< RangedAttackEnemyState_obj > RangedAttackEnemyState_obj::__new()
{  hx::ObjectPtr< RangedAttackEnemyState_obj > result = new RangedAttackEnemyState_obj();
	result->__construct();
	return result;}

Dynamic RangedAttackEnemyState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< RangedAttackEnemyState_obj > result = new RangedAttackEnemyState_obj();
	result->__construct();
	return result;}

hx::Object *RangedAttackEnemyState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::game::objects::enemy::ai::IEnemyState_obj)) return operator ::game::objects::enemy::ai::IEnemyState_obj *();
	return super::__ToInterface(inType);
}

Void RangedAttackEnemyState_obj::hitWall( ::engine::interfaces::ICollidable wallCollidable){
{
		HX_STACK_PUSH("RangedAttackEnemyState::hitWall","game/objects/enemy/ai/RangedAttackEnemyState.hx",29);
		HX_STACK_THIS(this);
		HX_STACK_ARG(wallCollidable,"wallCollidable");
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(RangedAttackEnemyState_obj,hitWall,(void))

Void RangedAttackEnemyState_obj::tick( int deltaTime,::game::objects::enemy::Enemy enemy){
{
		HX_STACK_PUSH("RangedAttackEnemyState::tick","game/objects/enemy/ai/RangedAttackEnemyState.hx",17);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(18)
		(this->fireCooldown)--;
		HX_STACK_LINE(20)
		if (((this->fireCooldown <= (int)0))){
			HX_STACK_LINE(20)
			this->fireCooldown = (int)35;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(RangedAttackEnemyState_obj,tick,(void))


RangedAttackEnemyState_obj::RangedAttackEnemyState_obj()
{
}

void RangedAttackEnemyState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(RangedAttackEnemyState);
	HX_MARK_MEMBER_NAME(fireCooldown,"fireCooldown");
	HX_MARK_END_CLASS();
}

void RangedAttackEnemyState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(fireCooldown,"fireCooldown");
}

Dynamic RangedAttackEnemyState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"hitWall") ) { return hitWall_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"fireCooldown") ) { return fireCooldown; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic RangedAttackEnemyState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 12:
		if (HX_FIELD_EQ(inName,"fireCooldown") ) { fireCooldown=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void RangedAttackEnemyState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("fireCooldown"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("hitWall"),
	HX_CSTRING("tick"),
	HX_CSTRING("fireCooldown"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(RangedAttackEnemyState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(RangedAttackEnemyState_obj::__mClass,"__mClass");
};

Class RangedAttackEnemyState_obj::__mClass;

void RangedAttackEnemyState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.enemy.ai.RangedAttackEnemyState"), hx::TCanCast< RangedAttackEnemyState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void RangedAttackEnemyState_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
} // end namespace enemy
} // end namespace ai
