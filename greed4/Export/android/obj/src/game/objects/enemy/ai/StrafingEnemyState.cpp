#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
#ifndef INCLUDED_engine_managers_SceneManager
#include <engine/managers/SceneManager.h>
#endif
#ifndef INCLUDED_game_objects_Player
#include <game/objects/Player.h>
#endif
#ifndef INCLUDED_game_objects_enemy_Enemy
#include <game/objects/enemy/Enemy.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_IEnemyState
#include <game/objects/enemy/ai/IEnemyState.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_StrafingEnemyState
#include <game/objects/enemy/ai/StrafingEnemyState.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
namespace game{
namespace objects{
namespace enemy{
namespace ai{

Void StrafingEnemyState_obj::__construct()
{
HX_STACK_PUSH("StrafingEnemyState::new","game/objects/enemy/ai/StrafingEnemyState.hx",14);
{
	HX_STACK_LINE(15)
	this->strafeDirection = ::Math_obj::floor((::Math_obj::random() * (int)2));
	HX_STACK_LINE(16)
	if (((this->strafeDirection == (int)0))){
		HX_STACK_LINE(17)
		this->strafeDirection = (int)-1;
	}
	HX_STACK_LINE(18)
	this->strafeAngle = (int)75;
}
;
	return null();
}

StrafingEnemyState_obj::~StrafingEnemyState_obj() { }

Dynamic StrafingEnemyState_obj::__CreateEmpty() { return  new StrafingEnemyState_obj; }
hx::ObjectPtr< StrafingEnemyState_obj > StrafingEnemyState_obj::__new()
{  hx::ObjectPtr< StrafingEnemyState_obj > result = new StrafingEnemyState_obj();
	result->__construct();
	return result;}

Dynamic StrafingEnemyState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< StrafingEnemyState_obj > result = new StrafingEnemyState_obj();
	result->__construct();
	return result;}

hx::Object *StrafingEnemyState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::game::objects::enemy::ai::IEnemyState_obj)) return operator ::game::objects::enemy::ai::IEnemyState_obj *();
	return super::__ToInterface(inType);
}

Void StrafingEnemyState_obj::hitWall( ::engine::interfaces::ICollidable wallCollidable){
{
		HX_STACK_PUSH("StrafingEnemyState::hitWall","game/objects/enemy/ai/StrafingEnemyState.hx",51);
		HX_STACK_THIS(this);
		HX_STACK_ARG(wallCollidable,"wallCollidable");
		HX_STACK_LINE(51)
		hx::MultEq(this->strafeDirection,(int)-1);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(StrafingEnemyState_obj,hitWall,(void))

Void StrafingEnemyState_obj::tick( int deltaTime,::game::objects::enemy::Enemy enemy){
{
		HX_STACK_PUSH("StrafingEnemyState::tick","game/objects/enemy/ai/StrafingEnemyState.hx",21);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(22)
		Float distanceSquared = ::engine::managers::SceneManager_obj::squareDistance(enemy->getPosition(),::game::objects::enemy::Enemy_obj::player->getPosition());		HX_STACK_VAR(distanceSquared,"distanceSquared");
		HX_STACK_LINE(24)
		if (((distanceSquared > (int)90000))){
			HX_STACK_LINE(24)
			this->strafeAngle = (int)15;
		}
		else{
			HX_STACK_LINE(26)
			this->strafeAngle = (int)75;
		}
		HX_STACK_LINE(31)
		if (((::game::objects::enemy::Enemy_obj::player->getPosition()->y > enemy->getPosition()->y))){
			HX_STACK_LINE(31)
			enemy->setRotation((::engine::managers::SceneManager_obj::usingFlash * (((int)180 - (Float(((int)180 * ::Math_obj::atan((Float(((::game::objects::enemy::Enemy_obj::player->getPosition()->x - enemy->getPosition()->x))) / Float(((::game::objects::enemy::Enemy_obj::player->getPosition()->y - enemy->getPosition()->y))))))) / Float(::Math_obj::PI))))));
		}
		else{
			HX_STACK_LINE(33)
			if (((::game::objects::enemy::Enemy_obj::player->getPosition()->y < enemy->getPosition()->y))){
				HX_STACK_LINE(33)
				enemy->setRotation((::engine::managers::SceneManager_obj::usingFlash * ((Float(((int)-180 * ::Math_obj::atan((Float(((::game::objects::enemy::Enemy_obj::player->getPosition()->x - enemy->getPosition()->x))) / Float(((::game::objects::enemy::Enemy_obj::player->getPosition()->y - enemy->getPosition()->y))))))) / Float(::Math_obj::PI)))));
			}
		}
		HX_STACK_LINE(37)
		enemy->setRotation((enemy->getRotation() + (this->strafeDirection * this->strafeAngle)));
		HX_STACK_LINE(40)
		enemy->setX((enemy->getPosition()->x + (Float(((((int)2 * ::Math_obj::sin((Float(((::engine::managers::SceneManager_obj::usingFlash * enemy->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime) * (int)3)) / Float((int)100))));
		HX_STACK_LINE(41)
		enemy->setY((enemy->getPosition()->y - (Float(((((int)2 * ::Math_obj::cos((Float(((::engine::managers::SceneManager_obj::usingFlash * enemy->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime) * (int)3)) / Float((int)100))));
		HX_STACK_LINE(44)
		if (((::game::objects::enemy::Enemy_obj::player->getPosition()->y > enemy->getPosition()->y))){
			HX_STACK_LINE(44)
			enemy->setRotation((::engine::managers::SceneManager_obj::usingFlash * (((int)180 - (Float(((int)180 * ::Math_obj::atan((Float(((::game::objects::enemy::Enemy_obj::player->getPosition()->x - enemy->getPosition()->x))) / Float(((::game::objects::enemy::Enemy_obj::player->getPosition()->y - enemy->getPosition()->y))))))) / Float(::Math_obj::PI))))));
		}
		else{
			HX_STACK_LINE(46)
			if (((::game::objects::enemy::Enemy_obj::player->getPosition()->y < enemy->getPosition()->y))){
				HX_STACK_LINE(46)
				enemy->setRotation((::engine::managers::SceneManager_obj::usingFlash * ((Float(((int)-180 * ::Math_obj::atan((Float(((::game::objects::enemy::Enemy_obj::player->getPosition()->x - enemy->getPosition()->x))) / Float(((::game::objects::enemy::Enemy_obj::player->getPosition()->y - enemy->getPosition()->y))))))) / Float(::Math_obj::PI)))));
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(StrafingEnemyState_obj,tick,(void))


StrafingEnemyState_obj::StrafingEnemyState_obj()
{
}

void StrafingEnemyState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(StrafingEnemyState);
	HX_MARK_MEMBER_NAME(strafeAngle,"strafeAngle");
	HX_MARK_MEMBER_NAME(strafeDirection,"strafeDirection");
	HX_MARK_END_CLASS();
}

void StrafingEnemyState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(strafeAngle,"strafeAngle");
	HX_VISIT_MEMBER_NAME(strafeDirection,"strafeDirection");
}

Dynamic StrafingEnemyState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"hitWall") ) { return hitWall_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"strafeAngle") ) { return strafeAngle; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"strafeDirection") ) { return strafeDirection; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic StrafingEnemyState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 11:
		if (HX_FIELD_EQ(inName,"strafeAngle") ) { strafeAngle=inValue.Cast< Float >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"strafeDirection") ) { strafeDirection=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void StrafingEnemyState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("strafeAngle"));
	outFields->push(HX_CSTRING("strafeDirection"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("hitWall"),
	HX_CSTRING("tick"),
	HX_CSTRING("strafeAngle"),
	HX_CSTRING("strafeDirection"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(StrafingEnemyState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(StrafingEnemyState_obj::__mClass,"__mClass");
};

Class StrafingEnemyState_obj::__mClass;

void StrafingEnemyState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.enemy.ai.StrafingEnemyState"), hx::TCanCast< StrafingEnemyState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void StrafingEnemyState_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
} // end namespace enemy
} // end namespace ai
