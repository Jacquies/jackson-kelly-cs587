#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
#ifndef INCLUDED_engine_managers_SceneManager
#include <engine/managers/SceneManager.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_objects_Animation
#include <engine/objects/Animation.h>
#endif
#ifndef INCLUDED_engine_objects_Collider
#include <engine/objects/Collider.h>
#endif
#ifndef INCLUDED_game_objects_enemy_Projectile
#include <game/objects/enemy/Projectile.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
namespace game{
namespace objects{
namespace enemy{

Void Projectile_obj::__construct(Float x,Float y,::engine::managers::TileRenderer tileRenderer,int ID)
{
HX_STACK_PUSH("Projectile::new","game/objects/enemy/Projectile.hx",24);
{
	HX_STACK_LINE(25)
	this->poolID = ID;
	HX_STACK_LINE(27)
	this->position = ::native::geom::Point_obj::__new(x,y);
	HX_STACK_LINE(29)
	this->collider = ::engine::objects::Collider_obj::__new(x,y,(int)10,(int)10,::engine::objects::Collider_obj::TYPE_PROJECTILE);
	HX_STACK_LINE(30)
	this->animation = ::engine::objects::Animation_obj::__new((int)33,(int)0,(int)30,x,y,(int)0,(int)1,tileRenderer,null());
	HX_STACK_LINE(32)
	this->animation->removeFromRenderSet();
	HX_STACK_LINE(34)
	this->renderer = tileRenderer;
}
;
	return null();
}

Projectile_obj::~Projectile_obj() { }

Dynamic Projectile_obj::__CreateEmpty() { return  new Projectile_obj; }
hx::ObjectPtr< Projectile_obj > Projectile_obj::__new(Float x,Float y,::engine::managers::TileRenderer tileRenderer,int ID)
{  hx::ObjectPtr< Projectile_obj > result = new Projectile_obj();
	result->__construct(x,y,tileRenderer,ID);
	return result;}

Dynamic Projectile_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Projectile_obj > result = new Projectile_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3]);
	return result;}

hx::Object *Projectile_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::engine::interfaces::ITickable_obj)) return operator ::engine::interfaces::ITickable_obj *();
	if (inType==typeid( ::engine::interfaces::IRenderable_obj)) return operator ::engine::interfaces::IRenderable_obj *();
	if (inType==typeid( ::engine::interfaces::ICollidable_obj)) return operator ::engine::interfaces::ICollidable_obj *();
	return super::__ToInterface(inType);
}

Void Projectile_obj::collide( ::engine::interfaces::ICollidable otherCollidable){
{
		HX_STACK_PUSH("Projectile::collide","game/objects/enemy/Projectile.hx",76);
		HX_STACK_THIS(this);
		HX_STACK_ARG(otherCollidable,"otherCollidable");
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Projectile_obj,collide,(void))

Void Projectile_obj::tick( int deltaTime){
{
		HX_STACK_PUSH("Projectile::tick","game/objects/enemy/Projectile.hx",66);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_LINE(68)
		hx::AddEq(this->position->x,((int)7 * ::Math_obj::sin((Float(((::engine::managers::SceneManager_obj::usingFlash * this->rotation) * ::Math_obj::PI)) / Float((int)180)))));
		HX_STACK_LINE(69)
		hx::SubEq(this->position->y,((int)7 * ::Math_obj::cos((Float(((::engine::managers::SceneManager_obj::usingFlash * this->rotation) * ::Math_obj::PI)) / Float((int)180)))));
		HX_STACK_LINE(70)
		this->animation->setX(this->position->x);
		HX_STACK_LINE(71)
		this->animation->setY(this->position->y);
		HX_STACK_LINE(72)
		this->collider->setX(this->position->x);
		HX_STACK_LINE(73)
		this->collider->setY(this->position->y);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Projectile_obj,tick,(void))

Void Projectile_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("Projectile::render","game/objects/enemy/Projectile.hx",62);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(62)
		this->animation->render(deltaTime,camera);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Projectile_obj,render,(void))

::engine::objects::Collider Projectile_obj::getCollider( ){
	HX_STACK_PUSH("Projectile::getCollider","game/objects/enemy/Projectile.hx",58);
	HX_STACK_THIS(this);
	HX_STACK_LINE(58)
	return this->collider;
}


HX_DEFINE_DYNAMIC_FUNC0(Projectile_obj,getCollider,return )

Void Projectile_obj::setRotation( Float rot){
{
		HX_STACK_PUSH("Projectile::setRotation","game/objects/enemy/Projectile.hx",53);
		HX_STACK_THIS(this);
		HX_STACK_ARG(rot,"rot");
		HX_STACK_LINE(54)
		this->rotation = rot;
		HX_STACK_LINE(55)
		this->animation->setRotation(rot);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Projectile_obj,setRotation,(void))

Void Projectile_obj::setY( Float y){
{
		HX_STACK_PUSH("Projectile::setY","game/objects/enemy/Projectile.hx",49);
		HX_STACK_THIS(this);
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(49)
		this->position->y = y;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Projectile_obj,setY,(void))

Void Projectile_obj::setX( Float x){
{
		HX_STACK_PUSH("Projectile::setX","game/objects/enemy/Projectile.hx",45);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_LINE(45)
		this->position->x = x;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Projectile_obj,setX,(void))

Void Projectile_obj::deinitialize( ){
{
		HX_STACK_PUSH("Projectile::deinitialize","game/objects/enemy/Projectile.hx",41);
		HX_STACK_THIS(this);
		HX_STACK_LINE(41)
		this->animation->removeFromRenderSet();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Projectile_obj,deinitialize,(void))

Void Projectile_obj::initialize( ){
{
		HX_STACK_PUSH("Projectile::initialize","game/objects/enemy/Projectile.hx",37);
		HX_STACK_THIS(this);
		HX_STACK_LINE(37)
		this->animation->addToRenderSet();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Projectile_obj,initialize,(void))


Projectile_obj::Projectile_obj()
{
}

void Projectile_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Projectile);
	HX_MARK_MEMBER_NAME(poolID,"poolID");
	HX_MARK_MEMBER_NAME(rotation,"rotation");
	HX_MARK_MEMBER_NAME(position,"position");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_MEMBER_NAME(collider,"collider");
	HX_MARK_END_CLASS();
}

void Projectile_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(poolID,"poolID");
	HX_VISIT_MEMBER_NAME(rotation,"rotation");
	HX_VISIT_MEMBER_NAME(position,"position");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	HX_VISIT_MEMBER_NAME(animation,"animation");
	HX_VISIT_MEMBER_NAME(collider,"collider");
}

Dynamic Projectile_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		if (HX_FIELD_EQ(inName,"setY") ) { return setY_dyn(); }
		if (HX_FIELD_EQ(inName,"setX") ) { return setX_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		if (HX_FIELD_EQ(inName,"poolID") ) { return poolID; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"rotation") ) { return rotation; }
		if (HX_FIELD_EQ(inName,"position") ) { return position; }
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		if (HX_FIELD_EQ(inName,"collider") ) { return collider; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"initialize") ) { return initialize_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
		if (HX_FIELD_EQ(inName,"setRotation") ) { return setRotation_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"deinitialize") ) { return deinitialize_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Projectile_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"poolID") ) { poolID=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"rotation") ) { rotation=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"position") ) { position=inValue.Cast< ::native::geom::Point >(); return inValue; }
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::engine::managers::TileRenderer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"collider") ) { collider=inValue.Cast< ::engine::objects::Collider >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::engine::objects::Animation >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Projectile_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("poolID"));
	outFields->push(HX_CSTRING("rotation"));
	outFields->push(HX_CSTRING("position"));
	outFields->push(HX_CSTRING("renderer"));
	outFields->push(HX_CSTRING("animation"));
	outFields->push(HX_CSTRING("collider"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	HX_CSTRING("tick"),
	HX_CSTRING("render"),
	HX_CSTRING("getCollider"),
	HX_CSTRING("setRotation"),
	HX_CSTRING("setY"),
	HX_CSTRING("setX"),
	HX_CSTRING("deinitialize"),
	HX_CSTRING("initialize"),
	HX_CSTRING("poolID"),
	HX_CSTRING("rotation"),
	HX_CSTRING("position"),
	HX_CSTRING("renderer"),
	HX_CSTRING("animation"),
	HX_CSTRING("collider"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Projectile_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Projectile_obj::__mClass,"__mClass");
};

Class Projectile_obj::__mClass;

void Projectile_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.enemy.Projectile"), hx::TCanCast< Projectile_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Projectile_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
} // end namespace enemy
