#include <hxcpp.h>

#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
#ifndef INCLUDED_game_objects_enemy_Enemy
#include <game/objects/enemy/Enemy.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_IEnemyState
#include <game/objects/enemy/ai/IEnemyState.h>
#endif
namespace game{
namespace objects{
namespace enemy{
namespace ai{

HX_DEFINE_DYNAMIC_FUNC1(IEnemyState_obj,hitWall,)

HX_DEFINE_DYNAMIC_FUNC2(IEnemyState_obj,tick,)


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IEnemyState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IEnemyState_obj::__mClass,"__mClass");
};

Class IEnemyState_obj::__mClass;

void IEnemyState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.enemy.ai.IEnemyState"), hx::TCanCast< IEnemyState_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IEnemyState_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
} // end namespace enemy
} // end namespace ai
