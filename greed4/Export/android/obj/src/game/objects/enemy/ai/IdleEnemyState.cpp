#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
#ifndef INCLUDED_engine_managers_SceneManager
#include <engine/managers/SceneManager.h>
#endif
#ifndef INCLUDED_game_objects_Player
#include <game/objects/Player.h>
#endif
#ifndef INCLUDED_game_objects_enemy_Enemy
#include <game/objects/enemy/Enemy.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_IEnemyState
#include <game/objects/enemy/ai/IEnemyState.h>
#endif
#ifndef INCLUDED_game_objects_enemy_ai_IdleEnemyState
#include <game/objects/enemy/ai/IdleEnemyState.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
namespace game{
namespace objects{
namespace enemy{
namespace ai{

Void IdleEnemyState_obj::__construct(Array< ::game::objects::enemy::ai::IEnemyState > states)
{
HX_STACK_PUSH("IdleEnemyState::new","game/objects/enemy/ai/IdleEnemyState.hx",13);
{
	HX_STACK_LINE(13)
	this->nextStates = states;
}
;
	return null();
}

IdleEnemyState_obj::~IdleEnemyState_obj() { }

Dynamic IdleEnemyState_obj::__CreateEmpty() { return  new IdleEnemyState_obj; }
hx::ObjectPtr< IdleEnemyState_obj > IdleEnemyState_obj::__new(Array< ::game::objects::enemy::ai::IEnemyState > states)
{  hx::ObjectPtr< IdleEnemyState_obj > result = new IdleEnemyState_obj();
	result->__construct(states);
	return result;}

Dynamic IdleEnemyState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< IdleEnemyState_obj > result = new IdleEnemyState_obj();
	result->__construct(inArgs[0]);
	return result;}

hx::Object *IdleEnemyState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::game::objects::enemy::ai::IEnemyState_obj)) return operator ::game::objects::enemy::ai::IEnemyState_obj *();
	return super::__ToInterface(inType);
}

Void IdleEnemyState_obj::hitWall( ::engine::interfaces::ICollidable wallCollidable){
{
		HX_STACK_PUSH("IdleEnemyState::hitWall","game/objects/enemy/ai/IdleEnemyState.hx",31);
		HX_STACK_THIS(this);
		HX_STACK_ARG(wallCollidable,"wallCollidable");
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(IdleEnemyState_obj,hitWall,(void))

Void IdleEnemyState_obj::tick( int _tmp_deltaTime,::game::objects::enemy::Enemy enemy){
{
		HX_STACK_PUSH("IdleEnemyState::tick","game/objects/enemy/ai/IdleEnemyState.hx",17);
		HX_STACK_THIS(this);
		HX_STACK_ARG(_tmp_deltaTime,"_tmp_deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(17)
		Float deltaTime = _tmp_deltaTime;		HX_STACK_VAR(deltaTime,"deltaTime");
		HX_STACK_LINE(17)
		if (((bool((::engine::managers::SceneManager_obj::squareDistance(enemy->getPosition(),::game::objects::enemy::Enemy_obj::player->getPosition()) < ::Math_obj::pow(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("aggroRadius"),true),(int)2))) && bool(!(::engine::managers::SceneManager_obj::currentSceneManager->isIdle()))))){
			HX_STACK_LINE(21)
			enemy->exitState(hx::ObjectPtr<OBJ_>(this));
			HX_STACK_LINE(23)
			enemy->switchAnimation((int)1);
			HX_STACK_LINE(25)
			{
				HX_STACK_LINE(25)
				int _g = (int)0;		HX_STACK_VAR(_g,"_g");
				Array< ::game::objects::enemy::ai::IEnemyState > _g1 = this->nextStates;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(25)
				while(((_g < _g1->length))){
					HX_STACK_LINE(25)
					::game::objects::enemy::ai::IEnemyState state = _g1->__get(_g);		HX_STACK_VAR(state,"state");
					HX_STACK_LINE(25)
					++(_g);
					HX_STACK_LINE(26)
					enemy->enterState(state);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(IdleEnemyState_obj,tick,(void))


IdleEnemyState_obj::IdleEnemyState_obj()
{
}

void IdleEnemyState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(IdleEnemyState);
	HX_MARK_MEMBER_NAME(nextStates,"nextStates");
	HX_MARK_END_CLASS();
}

void IdleEnemyState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(nextStates,"nextStates");
}

Dynamic IdleEnemyState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"hitWall") ) { return hitWall_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"nextStates") ) { return nextStates; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic IdleEnemyState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 10:
		if (HX_FIELD_EQ(inName,"nextStates") ) { nextStates=inValue.Cast< Array< ::game::objects::enemy::ai::IEnemyState > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void IdleEnemyState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("nextStates"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("hitWall"),
	HX_CSTRING("tick"),
	HX_CSTRING("nextStates"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IdleEnemyState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IdleEnemyState_obj::__mClass,"__mClass");
};

Class IdleEnemyState_obj::__mClass;

void IdleEnemyState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.enemy.ai.IdleEnemyState"), hx::TCanCast< IdleEnemyState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IdleEnemyState_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
} // end namespace enemy
} // end namespace ai
