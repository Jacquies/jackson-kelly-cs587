#include <hxcpp.h>

#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_objects_Animation
#include <engine/objects/Animation.h>
#endif
#ifndef INCLUDED_engine_objects_Collider
#include <engine/objects/Collider.h>
#endif
#ifndef INCLUDED_game_objects_Wall
#include <game/objects/Wall.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
namespace game{
namespace objects{

Void Wall_obj::__construct(::native::geom::Point spawnPoint,int facingDirection,::engine::managers::TileRenderer tileRenderer)
{
HX_STACK_PUSH("Wall::new","game/objects/Wall.hx",18);
{
	HX_STACK_LINE(19)
	this->direction = facingDirection;
	HX_STACK_LINE(21)
	if (((hx::Mod(this->direction,(int)2) == (int)0))){
		HX_STACK_LINE(21)
		this->collider = ::engine::objects::Collider_obj::__new(spawnPoint->x,spawnPoint->y,(int)16,(int)160,::engine::objects::Collider_obj::TYPE_WALL);
	}
	else{
		HX_STACK_LINE(23)
		this->collider = ::engine::objects::Collider_obj::__new(spawnPoint->x,spawnPoint->y,(int)160,(int)16,::engine::objects::Collider_obj::TYPE_WALL);
	}
	HX_STACK_LINE(27)
	this->animation = ::engine::objects::Animation_obj::__new((int)56,(int)0,(int)30,spawnPoint->x,spawnPoint->y,((int)90 * this->direction),(int)0,tileRenderer,null());
}
;
	return null();
}

Wall_obj::~Wall_obj() { }

Dynamic Wall_obj::__CreateEmpty() { return  new Wall_obj; }
hx::ObjectPtr< Wall_obj > Wall_obj::__new(::native::geom::Point spawnPoint,int facingDirection,::engine::managers::TileRenderer tileRenderer)
{  hx::ObjectPtr< Wall_obj > result = new Wall_obj();
	result->__construct(spawnPoint,facingDirection,tileRenderer);
	return result;}

Dynamic Wall_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Wall_obj > result = new Wall_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

hx::Object *Wall_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::engine::interfaces::ICollidable_obj)) return operator ::engine::interfaces::ICollidable_obj *();
	if (inType==typeid( ::engine::interfaces::IRenderable_obj)) return operator ::engine::interfaces::IRenderable_obj *();
	return super::__ToInterface(inType);
}

Void Wall_obj::collide( ::engine::interfaces::ICollidable otherCollidable){
{
		HX_STACK_PUSH("Wall::collide","game/objects/Wall.hx",47);
		HX_STACK_THIS(this);
		HX_STACK_ARG(otherCollidable,"otherCollidable");
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Wall_obj,collide,(void))

int Wall_obj::getDirection( ){
	HX_STACK_PUSH("Wall::getDirection","game/objects/Wall.hx",43);
	HX_STACK_THIS(this);
	HX_STACK_LINE(43)
	return this->direction;
}


HX_DEFINE_DYNAMIC_FUNC0(Wall_obj,getDirection,return )

Void Wall_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("Wall::render","game/objects/Wall.hx",39);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(39)
		this->animation->render(deltaTime,camera);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Wall_obj,render,(void))

::engine::objects::Collider Wall_obj::getCollider( ){
	HX_STACK_PUSH("Wall::getCollider","game/objects/Wall.hx",31);
	HX_STACK_THIS(this);
	HX_STACK_LINE(31)
	return this->collider;
}


HX_DEFINE_DYNAMIC_FUNC0(Wall_obj,getCollider,return )


Wall_obj::Wall_obj()
{
}

void Wall_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Wall);
	HX_MARK_MEMBER_NAME(direction,"direction");
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_MEMBER_NAME(collider,"collider");
	HX_MARK_END_CLASS();
}

void Wall_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(direction,"direction");
	HX_VISIT_MEMBER_NAME(animation,"animation");
	HX_VISIT_MEMBER_NAME(collider,"collider");
}

Dynamic Wall_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"collider") ) { return collider; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"direction") ) { return direction; }
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"getDirection") ) { return getDirection_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Wall_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"collider") ) { collider=inValue.Cast< ::engine::objects::Collider >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"direction") ) { direction=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::engine::objects::Animation >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Wall_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("direction"));
	outFields->push(HX_CSTRING("animation"));
	outFields->push(HX_CSTRING("collider"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	HX_CSTRING("getDirection"),
	HX_CSTRING("render"),
	HX_CSTRING("getCollider"),
	HX_CSTRING("direction"),
	HX_CSTRING("animation"),
	HX_CSTRING("collider"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Wall_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Wall_obj::__mClass,"__mClass");
};

Class Wall_obj::__mClass;

void Wall_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.Wall"), hx::TCanCast< Wall_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Wall_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
