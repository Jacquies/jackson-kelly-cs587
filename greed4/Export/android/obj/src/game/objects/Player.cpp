#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
#ifndef INCLUDED_engine_managers_SceneManager
#include <engine/managers/SceneManager.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_objects_Animation
#include <engine/objects/Animation.h>
#endif
#ifndef INCLUDED_engine_objects_Collider
#include <engine/objects/Collider.h>
#endif
#ifndef INCLUDED_game_objects_Player
#include <game/objects/Player.h>
#endif
#ifndef INCLUDED_game_objects_Wall
#include <game/objects/Wall.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_geom_Rectangle
#include <native/geom/Rectangle.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
namespace game{
namespace objects{

Void Player_obj::__construct(::engine::managers::TileRenderer tileRenderer)
{
HX_STACK_PUSH("Player::new","game/objects/Player.hx",27);
{
	HX_STACK_LINE(28)
	this->renderer = tileRenderer;
	HX_STACK_LINE(29)
	this->collider = ::engine::objects::Collider_obj::__new((int)0,(int)0,(int)20,(int)20,::engine::objects::Collider_obj::TYPE_PLAYER);
	HX_STACK_LINE(31)
	this->position = ::native::geom::Point_obj::__new((int)0,(int)0);
	HX_STACK_LINE(32)
	this->rotation = (int)0;
	HX_STACK_LINE(33)
	this->currentAnimationID = (int)0;
	HX_STACK_LINE(34)
	this->hitCountdown = (int)0;
	HX_STACK_LINE(36)
	this->animations = Array_obj< ::engine::objects::Animation >::__new();
	HX_STACK_LINE(37)
	this->animations->push(::engine::objects::Animation_obj::__new((int)62,(int)0,(int)30,(int)0,(int)0,(int)0,(int)1,tileRenderer,null()));
	HX_STACK_LINE(38)
	this->animations->push(::engine::objects::Animation_obj::__new((int)70,(int)3,(int)10,(int)0,(int)0,(int)0,(int)1,tileRenderer,false));
	HX_STACK_LINE(40)
	{
		HX_STACK_LINE(40)
		int _g1 = (int)1;		HX_STACK_VAR(_g1,"_g1");
		int _g = this->animations->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(40)
		while(((_g1 < _g))){
			HX_STACK_LINE(40)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(41)
			this->animations->__get(i)->removeFromRenderSet();
		}
	}
	HX_STACK_LINE(44)
	this->animation = this->animations->__get(this->currentAnimationID);
}
;
	return null();
}

Player_obj::~Player_obj() { }

Dynamic Player_obj::__CreateEmpty() { return  new Player_obj; }
hx::ObjectPtr< Player_obj > Player_obj::__new(::engine::managers::TileRenderer tileRenderer)
{  hx::ObjectPtr< Player_obj > result = new Player_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic Player_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Player_obj > result = new Player_obj();
	result->__construct(inArgs[0]);
	return result;}

hx::Object *Player_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::engine::interfaces::ITickable_obj)) return operator ::engine::interfaces::ITickable_obj *();
	if (inType==typeid( ::engine::interfaces::IRenderable_obj)) return operator ::engine::interfaces::IRenderable_obj *();
	if (inType==typeid( ::engine::interfaces::ICollidable_obj)) return operator ::engine::interfaces::ICollidable_obj *();
	return super::__ToInterface(inType);
}

Void Player_obj::collide( ::engine::interfaces::ICollidable otherCollidable){
{
		HX_STACK_PUSH("Player::collide","game/objects/Player.hx",184);
		HX_STACK_THIS(this);
		HX_STACK_ARG(otherCollidable,"otherCollidable");
		HX_STACK_LINE(184)
		if (((otherCollidable->getCollider()->getType() == ::engine::objects::Collider_obj::TYPE_WALL))){
			HX_STACK_LINE(187)
			::game::objects::Wall wall = hx::TCast< game::objects::Wall >::cast(otherCollidable);		HX_STACK_VAR(wall,"wall");
			HX_STACK_LINE(189)
			if ((::engine::managers::SceneManager_obj::currentSceneManager->getMouseDown())){
				HX_STACK_LINE(189)
				switch( (int)(wall->getDirection())){
					case (int)0: {
						HX_STACK_LINE(191)
						hx::AddEq(this->position->x,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width + (int)1));
					}
					;break;
					case (int)1: {
						HX_STACK_LINE(193)
						hx::SubEq(this->position->y,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height + (int)1));
					}
					;break;
					case (int)2: {
						HX_STACK_LINE(195)
						hx::SubEq(this->position->x,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width + (int)1));
					}
					;break;
					case (int)3: {
						HX_STACK_LINE(197)
						hx::AddEq(this->position->y,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height + (int)1));
					}
					;break;
				}
			}
		}
		else{
			HX_STACK_LINE(201)
			if (((otherCollidable->getCollider()->getType() == ::engine::objects::Collider_obj::TYPE_PROJECTILE))){
				HX_STACK_LINE(201)
				this->decreaseHealth();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,collide,(void))

int Player_obj::getHitCountdown( ){
	HX_STACK_PUSH("Player::getHitCountdown","game/objects/Player.hx",180);
	HX_STACK_THIS(this);
	HX_STACK_LINE(180)
	return this->hitCountdown;
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,getHitCountdown,return )

Void Player_obj::decreaseHealth( ){
{
		HX_STACK_PUSH("Player::decreaseHealth","game/objects/Player.hx",163);
		HX_STACK_THIS(this);
		HX_STACK_LINE(163)
		if (((this->hitCountdown <= (int)0))){
			HX_STACK_LINE(166)
			this->hitCountdown = (int)30;
			HX_STACK_LINE(168)
			if (((::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) > (int)0))){
				HX_STACK_LINE(169)
				(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("health")))--;
				HX_STACK_LINE(170)
				::engine::managers::SceneManager_obj::currentSceneManager->updateHealthBar();
			}
			HX_STACK_LINE(173)
			if (((::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) <= (int)0))){
				HX_STACK_LINE(174)
				::engine::managers::SceneManager_obj::currentSceneManager->setIdle(true);
				HX_STACK_LINE(175)
				::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("newGame")) = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,decreaseHealth,(void))

Void Player_obj::hitWall( ::engine::interfaces::ICollidable wallCollidable){
{
		HX_STACK_PUSH("Player::hitWall","game/objects/Player.hx",145);
		HX_STACK_THIS(this);
		HX_STACK_ARG(wallCollidable,"wallCollidable");
		HX_STACK_LINE(147)
		::game::objects::Wall wall = hx::TCast< game::objects::Wall >::cast(wallCollidable);		HX_STACK_VAR(wall,"wall");
		HX_STACK_LINE(149)
		if ((::engine::managers::SceneManager_obj::currentSceneManager->getMouseDown())){
			HX_STACK_LINE(149)
			switch( (int)(wall->getDirection())){
				case (int)0: {
					HX_STACK_LINE(151)
					hx::AddEq(this->position->x,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width + (int)1));
				}
				;break;
				case (int)1: {
					HX_STACK_LINE(153)
					hx::SubEq(this->position->y,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height + (int)1));
				}
				;break;
				case (int)2: {
					HX_STACK_LINE(155)
					hx::SubEq(this->position->x,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->width + (int)1));
				}
				;break;
				case (int)3: {
					HX_STACK_LINE(157)
					hx::AddEq(this->position->y,(wall->getCollider()->getBoundingBox()->intersection(this->collider->getBoundingBox())->height + (int)1));
				}
				;break;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,hitWall,(void))

Void Player_obj::hit( ){
{
		HX_STACK_PUSH("Player::hit","game/objects/Player.hx",129);
		HX_STACK_THIS(this);
		HX_STACK_LINE(129)
		if (((this->hitCountdown <= (int)0))){
			HX_STACK_LINE(131)
			this->hitCountdown = (int)30;
			HX_STACK_LINE(133)
			if (((::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) > (int)0))){
				HX_STACK_LINE(134)
				(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("health")))--;
				HX_STACK_LINE(135)
				::engine::managers::SceneManager_obj::currentSceneManager->updateHealthBar();
			}
			HX_STACK_LINE(138)
			if (((::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) <= (int)0))){
				HX_STACK_LINE(139)
				::engine::managers::SceneManager_obj::currentSceneManager->setIdle(true);
				HX_STACK_LINE(140)
				::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("newGame")) = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,hit,(void))

Void Player_obj::switchAnimation( int ID){
{
		HX_STACK_PUSH("Player::switchAnimation","game/objects/Player.hx",117);
		HX_STACK_THIS(this);
		HX_STACK_ARG(ID,"ID");
		HX_STACK_LINE(117)
		if (((this->currentAnimationID != ID))){
			HX_STACK_LINE(119)
			this->animation->removeFromRenderSet();
			HX_STACK_LINE(120)
			this->animation = this->animations->__get(ID);
			HX_STACK_LINE(121)
			this->animation->setX(this->position->x);
			HX_STACK_LINE(122)
			this->animation->setY(this->position->y);
			HX_STACK_LINE(123)
			this->animation->setRotation(this->rotation);
			HX_STACK_LINE(124)
			this->animation->addToRenderSet();
			HX_STACK_LINE(125)
			this->currentAnimationID = ID;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,switchAnimation,(void))

Void Player_obj::tick( int deltaTime){
{
		HX_STACK_PUSH("Player::tick","game/objects/Player.hx",83);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_LINE(84)
		if ((this->animation->isFinished())){
			HX_STACK_LINE(85)
			this->switchAnimation((int)0);
		}
		HX_STACK_LINE(87)
		if (((this->hitCountdown > (int)0))){
			HX_STACK_LINE(88)
			(this->hitCountdown)--;
		}
		HX_STACK_LINE(90)
		if ((::engine::managers::SceneManager_obj::currentSceneManager->getMouseDown())){
			HX_STACK_LINE(92)
			if (((::engine::managers::SceneManager_obj::currentSceneManager->getMousePosition()->y > (Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)2))))){
				HX_STACK_LINE(92)
				this->rotation = (::engine::managers::SceneManager_obj::usingFlash * (((int)180 - (Float((((Float(((int)180 * ::Math_obj::atan((Float(((::engine::managers::SceneManager_obj::currentSceneManager->getMousePosition()->x - (Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2))))) / Float(((::engine::managers::SceneManager_obj::currentSceneManager->getMousePosition()->y - (Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)2))))))))) / Float(::Math_obj::PI)) * deltaTime) * (int)3)) / Float((int)100)))));
			}
			else{
				HX_STACK_LINE(94)
				if (((::engine::managers::SceneManager_obj::currentSceneManager->getMousePosition()->y < (Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)2))))){
					HX_STACK_LINE(94)
					this->rotation = (::engine::managers::SceneManager_obj::usingFlash * ((Float((((Float(((int)-180 * ::Math_obj::atan((Float(((::engine::managers::SceneManager_obj::currentSceneManager->getMousePosition()->x - (Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2))))) / Float(((::engine::managers::SceneManager_obj::currentSceneManager->getMousePosition()->y - (Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)2))))))))) / Float(::Math_obj::PI)) * deltaTime) * (int)3)) / Float((int)100))));
				}
			}
			HX_STACK_LINE(98)
			this->animation->setRotation(this->rotation);
			HX_STACK_LINE(101)
			hx::AddEq(this->position->x,(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("speed"),true) * ::Math_obj::sin((Float(((::engine::managers::SceneManager_obj::usingFlash * this->rotation) * ::Math_obj::PI)) / Float((int)180)))));
			HX_STACK_LINE(102)
			hx::SubEq(this->position->y,(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("speed"),true) * ::Math_obj::cos((Float(((::engine::managers::SceneManager_obj::usingFlash * this->rotation) * ::Math_obj::PI)) / Float((int)180)))));
			HX_STACK_LINE(104)
			this->animation->setX(this->position->x);
			HX_STACK_LINE(105)
			this->animation->setY(this->position->y);
			HX_STACK_LINE(106)
			this->collider->setX(this->position->x);
			HX_STACK_LINE(107)
			this->collider->setY(this->position->y);
			HX_STACK_LINE(108)
			::engine::managers::SceneManager_obj::currentSceneManager->setCameraX(this->position->x);
			HX_STACK_LINE(109)
			::engine::managers::SceneManager_obj::currentSceneManager->setCameraY(this->position->y);
		}
		HX_STACK_LINE(112)
		if (((::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("health"),true) <= (int)0))){
			HX_STACK_LINE(112)
			this->switchAnimation((int)1);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,tick,(void))

Void Player_obj::translateY( Float dy){
{
		HX_STACK_PUSH("Player::translateY","game/objects/Player.hx",79);
		HX_STACK_THIS(this);
		HX_STACK_ARG(dy,"dy");
		HX_STACK_LINE(79)
		this->animation->setY((this->animation->getPosition()->y + dy));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,translateY,(void))

Void Player_obj::translateX( Float dx){
{
		HX_STACK_PUSH("Player::translateX","game/objects/Player.hx",75);
		HX_STACK_THIS(this);
		HX_STACK_ARG(dx,"dx");
		HX_STACK_LINE(75)
		this->animation->setX((this->animation->getPosition()->x + dx));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,translateX,(void))

Void Player_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("Player::render","game/objects/Player.hx",71);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(71)
		this->animation->render(deltaTime,camera);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Player_obj,render,(void))

Void Player_obj::setRotation( Float rot){
{
		HX_STACK_PUSH("Player::setRotation","game/objects/Player.hx",67);
		HX_STACK_THIS(this);
		HX_STACK_ARG(rot,"rot");
		HX_STACK_LINE(67)
		this->rotation = rot;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,setRotation,(void))

Float Player_obj::getRotation( ){
	HX_STACK_PUSH("Player::getRotation","game/objects/Player.hx",63);
	HX_STACK_THIS(this);
	HX_STACK_LINE(63)
	return this->rotation;
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,getRotation,return )

Void Player_obj::setY( Float y){
{
		HX_STACK_PUSH("Player::setY","game/objects/Player.hx",59);
		HX_STACK_THIS(this);
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(59)
		this->position->y = y;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,setY,(void))

Void Player_obj::setX( Float x){
{
		HX_STACK_PUSH("Player::setX","game/objects/Player.hx",55);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_LINE(55)
		this->position->x = x;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Player_obj,setX,(void))

::native::geom::Point Player_obj::getPosition( ){
	HX_STACK_PUSH("Player::getPosition","game/objects/Player.hx",51);
	HX_STACK_THIS(this);
	HX_STACK_LINE(51)
	return this->position;
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,getPosition,return )

::engine::objects::Collider Player_obj::getCollider( ){
	HX_STACK_PUSH("Player::getCollider","game/objects/Player.hx",47);
	HX_STACK_THIS(this);
	HX_STACK_LINE(47)
	return this->collider;
}


HX_DEFINE_DYNAMIC_FUNC0(Player_obj,getCollider,return )


Player_obj::Player_obj()
{
}

void Player_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Player);
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_MEMBER_NAME(animations,"animations");
	HX_MARK_MEMBER_NAME(hitCountdown,"hitCountdown");
	HX_MARK_MEMBER_NAME(currentAnimationID,"currentAnimationID");
	HX_MARK_MEMBER_NAME(rotation,"rotation");
	HX_MARK_MEMBER_NAME(position,"position");
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_MEMBER_NAME(collider,"collider");
	HX_MARK_END_CLASS();
}

void Player_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	HX_VISIT_MEMBER_NAME(animations,"animations");
	HX_VISIT_MEMBER_NAME(hitCountdown,"hitCountdown");
	HX_VISIT_MEMBER_NAME(currentAnimationID,"currentAnimationID");
	HX_VISIT_MEMBER_NAME(rotation,"rotation");
	HX_VISIT_MEMBER_NAME(position,"position");
	HX_VISIT_MEMBER_NAME(animation,"animation");
	HX_VISIT_MEMBER_NAME(collider,"collider");
}

Dynamic Player_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"hit") ) { return hit_dyn(); }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		if (HX_FIELD_EQ(inName,"setY") ) { return setY_dyn(); }
		if (HX_FIELD_EQ(inName,"setX") ) { return setX_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		if (HX_FIELD_EQ(inName,"hitWall") ) { return hitWall_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		if (HX_FIELD_EQ(inName,"rotation") ) { return rotation; }
		if (HX_FIELD_EQ(inName,"position") ) { return position; }
		if (HX_FIELD_EQ(inName,"collider") ) { return collider; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"translateY") ) { return translateY_dyn(); }
		if (HX_FIELD_EQ(inName,"translateX") ) { return translateX_dyn(); }
		if (HX_FIELD_EQ(inName,"animations") ) { return animations; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"setRotation") ) { return setRotation_dyn(); }
		if (HX_FIELD_EQ(inName,"getRotation") ) { return getRotation_dyn(); }
		if (HX_FIELD_EQ(inName,"getPosition") ) { return getPosition_dyn(); }
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"hitCountdown") ) { return hitCountdown; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"decreaseHealth") ) { return decreaseHealth_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"getHitCountdown") ) { return getHitCountdown_dyn(); }
		if (HX_FIELD_EQ(inName,"switchAnimation") ) { return switchAnimation_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"currentAnimationID") ) { return currentAnimationID; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Player_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::engine::managers::TileRenderer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"rotation") ) { rotation=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"position") ) { position=inValue.Cast< ::native::geom::Point >(); return inValue; }
		if (HX_FIELD_EQ(inName,"collider") ) { collider=inValue.Cast< ::engine::objects::Collider >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::engine::objects::Animation >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"animations") ) { animations=inValue.Cast< Array< ::engine::objects::Animation > >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"hitCountdown") ) { hitCountdown=inValue.Cast< int >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"currentAnimationID") ) { currentAnimationID=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Player_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("renderer"));
	outFields->push(HX_CSTRING("animations"));
	outFields->push(HX_CSTRING("hitCountdown"));
	outFields->push(HX_CSTRING("currentAnimationID"));
	outFields->push(HX_CSTRING("rotation"));
	outFields->push(HX_CSTRING("position"));
	outFields->push(HX_CSTRING("animation"));
	outFields->push(HX_CSTRING("collider"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	HX_CSTRING("getHitCountdown"),
	HX_CSTRING("decreaseHealth"),
	HX_CSTRING("hitWall"),
	HX_CSTRING("hit"),
	HX_CSTRING("switchAnimation"),
	HX_CSTRING("tick"),
	HX_CSTRING("translateY"),
	HX_CSTRING("translateX"),
	HX_CSTRING("render"),
	HX_CSTRING("setRotation"),
	HX_CSTRING("getRotation"),
	HX_CSTRING("setY"),
	HX_CSTRING("setX"),
	HX_CSTRING("getPosition"),
	HX_CSTRING("getCollider"),
	HX_CSTRING("renderer"),
	HX_CSTRING("animations"),
	HX_CSTRING("hitCountdown"),
	HX_CSTRING("currentAnimationID"),
	HX_CSTRING("rotation"),
	HX_CSTRING("position"),
	HX_CSTRING("animation"),
	HX_CSTRING("collider"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Player_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Player_obj::__mClass,"__mClass");
};

Class Player_obj::__mClass;

void Player_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.Player"), hx::TCanCast< Player_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Player_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
