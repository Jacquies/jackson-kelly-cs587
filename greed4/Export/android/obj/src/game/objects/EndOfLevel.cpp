#include <hxcpp.h>

#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_objects_Animation
#include <engine/objects/Animation.h>
#endif
#ifndef INCLUDED_engine_objects_Collider
#include <engine/objects/Collider.h>
#endif
#ifndef INCLUDED_game_level_Level
#include <game/level/Level.h>
#endif
#ifndef INCLUDED_game_objects_EndOfLevel
#include <game/objects/EndOfLevel.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
namespace game{
namespace objects{

Void EndOfLevel_obj::__construct(::native::geom::Point spawnPoint,::engine::managers::TileRenderer tileRenderer,::game::level::Level parentLevel)
{
HX_STACK_PUSH("EndOfLevel::new","game/objects/EndOfLevel.hx",18);
{
	HX_STACK_LINE(19)
	this->collider = ::engine::objects::Collider_obj::__new(spawnPoint->x,spawnPoint->y,(int)20,(int)20,::engine::objects::Collider_obj::TYPE_END);
	HX_STACK_LINE(20)
	this->animation = ::engine::objects::Animation_obj::__new((int)32,(int)0,(int)30,spawnPoint->x,spawnPoint->y,(int)0,(int)1,tileRenderer,null());
	HX_STACK_LINE(21)
	this->parent = parentLevel;
}
;
	return null();
}

EndOfLevel_obj::~EndOfLevel_obj() { }

Dynamic EndOfLevel_obj::__CreateEmpty() { return  new EndOfLevel_obj; }
hx::ObjectPtr< EndOfLevel_obj > EndOfLevel_obj::__new(::native::geom::Point spawnPoint,::engine::managers::TileRenderer tileRenderer,::game::level::Level parentLevel)
{  hx::ObjectPtr< EndOfLevel_obj > result = new EndOfLevel_obj();
	result->__construct(spawnPoint,tileRenderer,parentLevel);
	return result;}

Dynamic EndOfLevel_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< EndOfLevel_obj > result = new EndOfLevel_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

hx::Object *EndOfLevel_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::engine::interfaces::IRenderable_obj)) return operator ::engine::interfaces::IRenderable_obj *();
	if (inType==typeid( ::engine::interfaces::ICollidable_obj)) return operator ::engine::interfaces::ICollidable_obj *();
	return super::__ToInterface(inType);
}

Void EndOfLevel_obj::collide( ::engine::interfaces::ICollidable otherCollidable){
{
		HX_STACK_PUSH("EndOfLevel::collide","game/objects/EndOfLevel.hx",41);
		HX_STACK_THIS(this);
		HX_STACK_ARG(otherCollidable,"otherCollidable");
		HX_STACK_LINE(41)
		if (((otherCollidable->getCollider()->getType() == ::engine::objects::Collider_obj::TYPE_PLAYER))){
			HX_STACK_LINE(42)
			this->parent->endLevel();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(EndOfLevel_obj,collide,(void))

Void EndOfLevel_obj::endLevel( ){
{
		HX_STACK_PUSH("EndOfLevel::endLevel","game/objects/EndOfLevel.hx",36);
		HX_STACK_THIS(this);
		HX_STACK_LINE(36)
		this->parent->endLevel();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(EndOfLevel_obj,endLevel,(void))

Void EndOfLevel_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("EndOfLevel::render","game/objects/EndOfLevel.hx",32);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(32)
		this->animation->render(deltaTime,camera);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(EndOfLevel_obj,render,(void))

::engine::objects::Collider EndOfLevel_obj::getCollider( ){
	HX_STACK_PUSH("EndOfLevel::getCollider","game/objects/EndOfLevel.hx",24);
	HX_STACK_THIS(this);
	HX_STACK_LINE(24)
	return this->collider;
}


HX_DEFINE_DYNAMIC_FUNC0(EndOfLevel_obj,getCollider,return )


EndOfLevel_obj::EndOfLevel_obj()
{
}

void EndOfLevel_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(EndOfLevel);
	HX_MARK_MEMBER_NAME(parent,"parent");
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_MEMBER_NAME(collider,"collider");
	HX_MARK_END_CLASS();
}

void EndOfLevel_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(parent,"parent");
	HX_VISIT_MEMBER_NAME(animation,"animation");
	HX_VISIT_MEMBER_NAME(collider,"collider");
}

Dynamic EndOfLevel_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		if (HX_FIELD_EQ(inName,"parent") ) { return parent; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"endLevel") ) { return endLevel_dyn(); }
		if (HX_FIELD_EQ(inName,"collider") ) { return collider; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic EndOfLevel_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"parent") ) { parent=inValue.Cast< ::game::level::Level >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"collider") ) { collider=inValue.Cast< ::engine::objects::Collider >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::engine::objects::Animation >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void EndOfLevel_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("parent"));
	outFields->push(HX_CSTRING("animation"));
	outFields->push(HX_CSTRING("collider"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	HX_CSTRING("endLevel"),
	HX_CSTRING("render"),
	HX_CSTRING("getCollider"),
	HX_CSTRING("parent"),
	HX_CSTRING("animation"),
	HX_CSTRING("collider"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(EndOfLevel_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(EndOfLevel_obj::__mClass,"__mClass");
};

Class EndOfLevel_obj::__mClass;

void EndOfLevel_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.EndOfLevel"), hx::TCanCast< EndOfLevel_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void EndOfLevel_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
