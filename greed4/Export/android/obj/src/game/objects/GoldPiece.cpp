#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_managers_SceneManager
#include <engine/managers/SceneManager.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_objects_Animation
#include <engine/objects/Animation.h>
#endif
#ifndef INCLUDED_engine_objects_Collider
#include <engine/objects/Collider.h>
#endif
#ifndef INCLUDED_game_objects_GoldPiece
#include <game/objects/GoldPiece.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
namespace game{
namespace objects{

Void GoldPiece_obj::__construct(::native::geom::Point spawnPoint,::engine::managers::TileRenderer tileRenderer)
{
HX_STACK_PUSH("GoldPiece::new","game/objects/GoldPiece.hx",21);
{
	HX_STACK_LINE(22)
	if (((::Math_obj::random() > .9))){
		HX_STACK_LINE(23)
		this->value = (int)25;
		HX_STACK_LINE(24)
		this->animation = ::engine::objects::Animation_obj::__new((int)34,(int)0,(int)30,spawnPoint->x,spawnPoint->y,(int)0,(int)1,tileRenderer,null());
	}
	else{
		HX_STACK_LINE(26)
		this->value = (int)5;
		HX_STACK_LINE(27)
		this->animation = ::engine::objects::Animation_obj::__new((int)74,(int)13,(int)20,spawnPoint->x,spawnPoint->y,(int)0,(int)1,tileRenderer,null());
	}
	HX_STACK_LINE(30)
	this->collider = ::engine::objects::Collider_obj::__new(spawnPoint->x,spawnPoint->y,(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("collectRadius"),true) * (int)2),(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("collectRadius"),true) * (int)2),::engine::objects::Collider_obj::TYPE_COIN);
	HX_STACK_LINE(31)
	this->collected = false;
	HX_STACK_LINE(32)
	this->renderer = tileRenderer;
}
;
	return null();
}

GoldPiece_obj::~GoldPiece_obj() { }

Dynamic GoldPiece_obj::__CreateEmpty() { return  new GoldPiece_obj; }
hx::ObjectPtr< GoldPiece_obj > GoldPiece_obj::__new(::native::geom::Point spawnPoint,::engine::managers::TileRenderer tileRenderer)
{  hx::ObjectPtr< GoldPiece_obj > result = new GoldPiece_obj();
	result->__construct(spawnPoint,tileRenderer);
	return result;}

Dynamic GoldPiece_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< GoldPiece_obj > result = new GoldPiece_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

hx::Object *GoldPiece_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::engine::interfaces::IRenderable_obj)) return operator ::engine::interfaces::IRenderable_obj *();
	if (inType==typeid( ::engine::interfaces::ICollidable_obj)) return operator ::engine::interfaces::ICollidable_obj *();
	return super::__ToInterface(inType);
}

Void GoldPiece_obj::collide( ::engine::interfaces::ICollidable otherCollidable){
{
		HX_STACK_PUSH("GoldPiece::collide","game/objects/GoldPiece.hx",49);
		HX_STACK_THIS(this);
		HX_STACK_ARG(otherCollidable,"otherCollidable");
		HX_STACK_LINE(49)
		if (((otherCollidable->getCollider()->getType() == ::engine::objects::Collider_obj::TYPE_PLAYER))){
			HX_STACK_LINE(50)
			if ((!(this->collected))){
				HX_STACK_LINE(53)
				hx::AddEq(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("gold")),this->value);
				HX_STACK_LINE(54)
				hx::AddEq(::engine::managers::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("totalGold")),this->value);
				HX_STACK_LINE(55)
				this->collected = true;
				HX_STACK_LINE(62)
				::native::geom::Point position = this->animation->getPosition();		HX_STACK_VAR(position,"position");
				HX_STACK_LINE(63)
				this->animation->removeFromRenderSet();
				HX_STACK_LINE(64)
				if (((this->value == (int)5))){
					HX_STACK_LINE(64)
					this->animation = ::engine::objects::Animation_obj::__new((int)88,(int)4,(int)30,position->x,position->y,(int)0,(int)1,this->renderer,false);
				}
				else{
					HX_STACK_LINE(66)
					::engine::managers::SceneManager_obj::currentSceneManager->removeFromRenderSet(hx::ObjectPtr<OBJ_>(this));
				}
				HX_STACK_LINE(69)
				::engine::managers::SceneManager_obj::currentSceneManager->updateScoreboard();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(GoldPiece_obj,collide,(void))

Void GoldPiece_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("GoldPiece::render","game/objects/GoldPiece.hx",39);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(39)
		if (((bool(this->collected) && bool(this->animation->isFinished())))){
			HX_STACK_LINE(42)
			::engine::managers::SceneManager_obj::currentSceneManager->removeFromRenderSet(hx::ObjectPtr<OBJ_>(this));
			HX_STACK_LINE(43)
			this->animation->removeFromRenderSet();
		}
		else{
			HX_STACK_LINE(44)
			this->animation->render(deltaTime,camera);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(GoldPiece_obj,render,(void))

::engine::objects::Collider GoldPiece_obj::getCollider( ){
	HX_STACK_PUSH("GoldPiece::getCollider","game/objects/GoldPiece.hx",35);
	HX_STACK_THIS(this);
	HX_STACK_LINE(35)
	return this->collider;
}


HX_DEFINE_DYNAMIC_FUNC0(GoldPiece_obj,getCollider,return )


GoldPiece_obj::GoldPiece_obj()
{
}

void GoldPiece_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(GoldPiece);
	HX_MARK_MEMBER_NAME(value,"value");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_MEMBER_NAME(collected,"collected");
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_MEMBER_NAME(collider,"collider");
	HX_MARK_END_CLASS();
}

void GoldPiece_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(value,"value");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	HX_VISIT_MEMBER_NAME(collected,"collected");
	HX_VISIT_MEMBER_NAME(animation,"animation");
	HX_VISIT_MEMBER_NAME(collider,"collider");
}

Dynamic GoldPiece_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"value") ) { return value; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		if (HX_FIELD_EQ(inName,"collider") ) { return collider; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"collected") ) { return collected; }
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic GoldPiece_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"value") ) { value=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::engine::managers::TileRenderer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"collider") ) { collider=inValue.Cast< ::engine::objects::Collider >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"collected") ) { collected=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::engine::objects::Animation >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void GoldPiece_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("value"));
	outFields->push(HX_CSTRING("renderer"));
	outFields->push(HX_CSTRING("collected"));
	outFields->push(HX_CSTRING("animation"));
	outFields->push(HX_CSTRING("collider"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	HX_CSTRING("render"),
	HX_CSTRING("getCollider"),
	HX_CSTRING("value"),
	HX_CSTRING("renderer"),
	HX_CSTRING("collected"),
	HX_CSTRING("animation"),
	HX_CSTRING("collider"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(GoldPiece_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(GoldPiece_obj::__mClass,"__mClass");
};

Class GoldPiece_obj::__mClass;

void GoldPiece_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("game.objects.GoldPiece"), hx::TCanCast< GoldPiece_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void GoldPiece_obj::__boot()
{
}

} // end namespace game
} // end namespace objects
