#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

HX_DEFINE_DYNAMIC_FUNC2(IRenderable_obj,render,)


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IRenderable_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IRenderable_obj::__mClass,"__mClass");
};

Class IRenderable_obj::__mClass;

void IRenderable_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("IRenderable"), hx::TCanCast< IRenderable_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IRenderable_obj::__boot()
{
}

