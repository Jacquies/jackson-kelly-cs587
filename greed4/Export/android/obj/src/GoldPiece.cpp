#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_GoldPiece
#include <GoldPiece.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif

Void GoldPiece_obj::__construct(::native::geom::Point spawnPoint,::TileRenderer tileRenderer)
{
HX_STACK_PUSH("GoldPiece::new","GoldPiece.hx",15);
{
	HX_STACK_LINE(16)
	if (((::Math_obj::random() > .9))){
		HX_STACK_LINE(17)
		this->value = (int)25;
		HX_STACK_LINE(18)
		this->animation = ::Animation_obj::__new((int)34,(int)0,(int)30,spawnPoint->x,spawnPoint->y,(int)0,(int)1,tileRenderer,null());
	}
	else{
		HX_STACK_LINE(20)
		this->value = (int)5;
		HX_STACK_LINE(21)
		this->animation = ::Animation_obj::__new((int)74,(int)13,(int)20,spawnPoint->x,spawnPoint->y,(int)0,(int)1,tileRenderer,null());
	}
	HX_STACK_LINE(24)
	this->collider = ::Collider_obj::__new(spawnPoint->x,spawnPoint->y,(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("collectRadius"),true) * (int)2),(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("collectRadius"),true) * (int)2),::Collider_obj::TYPE_COIN);
	HX_STACK_LINE(25)
	this->collected = false;
	HX_STACK_LINE(26)
	this->renderer = tileRenderer;
}
;
	return null();
}

GoldPiece_obj::~GoldPiece_obj() { }

Dynamic GoldPiece_obj::__CreateEmpty() { return  new GoldPiece_obj; }
hx::ObjectPtr< GoldPiece_obj > GoldPiece_obj::__new(::native::geom::Point spawnPoint,::TileRenderer tileRenderer)
{  hx::ObjectPtr< GoldPiece_obj > result = new GoldPiece_obj();
	result->__construct(spawnPoint,tileRenderer);
	return result;}

Dynamic GoldPiece_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< GoldPiece_obj > result = new GoldPiece_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

hx::Object *GoldPiece_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IRenderable_obj)) return operator ::IRenderable_obj *();
	if (inType==typeid( ::ICollidable_obj)) return operator ::ICollidable_obj *();
	return super::__ToInterface(inType);
}

Void GoldPiece_obj::collide( ::ICollidable otherCollidable){
{
		HX_STACK_PUSH("GoldPiece::collide","GoldPiece.hx",43);
		HX_STACK_THIS(this);
		HX_STACK_ARG(otherCollidable,"otherCollidable");
		HX_STACK_LINE(43)
		if (((otherCollidable->getCollider()->getType() == ::Collider_obj::TYPE_PLAYER))){
			HX_STACK_LINE(44)
			if ((!(this->collected))){
				HX_STACK_LINE(47)
				hx::AddEq(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("gold")),this->value);
				HX_STACK_LINE(48)
				hx::AddEq(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__FieldRef(HX_CSTRING("totalGold")),this->value);
				HX_STACK_LINE(49)
				this->collected = true;
				HX_STACK_LINE(56)
				::native::geom::Point position = this->animation->getPosition();		HX_STACK_VAR(position,"position");
				HX_STACK_LINE(57)
				this->animation->removeFromRenderSet();
				HX_STACK_LINE(58)
				if (((this->value == (int)5))){
					HX_STACK_LINE(58)
					this->animation = ::Animation_obj::__new((int)88,(int)4,(int)30,position->x,position->y,(int)0,(int)1,this->renderer,false);
				}
				else{
					HX_STACK_LINE(60)
					::SceneManager_obj::currentSceneManager->removeFromRenderSet(hx::ObjectPtr<OBJ_>(this));
				}
				HX_STACK_LINE(63)
				::SceneManager_obj::currentSceneManager->updateScoreboard();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(GoldPiece_obj,collide,(void))

Void GoldPiece_obj::render( int deltaTime,::native::geom::Point camera){
{
		HX_STACK_PUSH("GoldPiece::render","GoldPiece.hx",33);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(camera,"camera");
		HX_STACK_LINE(33)
		if (((bool(this->collected) && bool(this->animation->isFinished())))){
			HX_STACK_LINE(36)
			::SceneManager_obj::currentSceneManager->removeFromRenderSet(hx::ObjectPtr<OBJ_>(this));
			HX_STACK_LINE(37)
			this->animation->removeFromRenderSet();
		}
		else{
			HX_STACK_LINE(38)
			this->animation->render(deltaTime,camera);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(GoldPiece_obj,render,(void))

::Collider GoldPiece_obj::getCollider( ){
	HX_STACK_PUSH("GoldPiece::getCollider","GoldPiece.hx",29);
	HX_STACK_THIS(this);
	HX_STACK_LINE(29)
	return this->collider;
}


HX_DEFINE_DYNAMIC_FUNC0(GoldPiece_obj,getCollider,return )


GoldPiece_obj::GoldPiece_obj()
{
}

void GoldPiece_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(GoldPiece);
	HX_MARK_MEMBER_NAME(value,"value");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_MEMBER_NAME(collected,"collected");
	HX_MARK_MEMBER_NAME(animation,"animation");
	HX_MARK_MEMBER_NAME(collider,"collider");
	HX_MARK_END_CLASS();
}

void GoldPiece_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(value,"value");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	HX_VISIT_MEMBER_NAME(collected,"collected");
	HX_VISIT_MEMBER_NAME(animation,"animation");
	HX_VISIT_MEMBER_NAME(collider,"collider");
}

Dynamic GoldPiece_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"value") ) { return value; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"render") ) { return render_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		if (HX_FIELD_EQ(inName,"collider") ) { return collider; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"collected") ) { return collected; }
		if (HX_FIELD_EQ(inName,"animation") ) { return animation; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getCollider") ) { return getCollider_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic GoldPiece_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"value") ) { value=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::TileRenderer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"collider") ) { collider=inValue.Cast< ::Collider >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"collected") ) { collected=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"animation") ) { animation=inValue.Cast< ::Animation >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void GoldPiece_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("value"));
	outFields->push(HX_CSTRING("renderer"));
	outFields->push(HX_CSTRING("collected"));
	outFields->push(HX_CSTRING("animation"));
	outFields->push(HX_CSTRING("collider"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	HX_CSTRING("render"),
	HX_CSTRING("getCollider"),
	HX_CSTRING("value"),
	HX_CSTRING("renderer"),
	HX_CSTRING("collected"),
	HX_CSTRING("animation"),
	HX_CSTRING("collider"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(GoldPiece_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(GoldPiece_obj::__mClass,"__mClass");
};

Class GoldPiece_obj::__mClass;

void GoldPiece_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("GoldPiece"), hx::TCanCast< GoldPiece_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void GoldPiece_obj::__boot()
{
}

