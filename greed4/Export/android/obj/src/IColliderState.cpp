#include <hxcpp.h>

#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IColliderState
#include <IColliderState.h>
#endif

HX_DEFINE_DYNAMIC_FUNC2(IColliderState_obj,collide,)


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IColliderState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IColliderState_obj::__mClass,"__mClass");
};

Class IColliderState_obj::__mClass;

void IColliderState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("IColliderState"), hx::TCanCast< IColliderState_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IColliderState_obj::__boot()
{
}

