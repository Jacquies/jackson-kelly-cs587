#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_ChargingState
#include <ChargingState.h>
#endif
#ifndef INCLUDED_ChasingState
#include <ChasingState.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_IState
#include <IState.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_IdleState
#include <IdleState.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_StrafingState
#include <StrafingState.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif

Void IdleState_obj::__construct(int type)
{
HX_STACK_PUSH("IdleState::new","IdleState.hx",10);
{
	HX_STACK_LINE(11)
	this->nextStates = Array_obj< ::IState >::__new();
	HX_STACK_LINE(12)
	switch( (int)(type)){
		case (int)0: {
			HX_STACK_LINE(13)
			this->nextStates->push(::ChasingState_obj::__new());
		}
		;break;
		case (int)1: {
			HX_STACK_LINE(15)
			this->nextStates->push(::StrafingState_obj::__new());
		}
		;break;
		case (int)2: {
			HX_STACK_LINE(18)
			this->nextStates->push(::ChargingState_obj::__new());
		}
		;break;
	}
}
;
	return null();
}

IdleState_obj::~IdleState_obj() { }

Dynamic IdleState_obj::__CreateEmpty() { return  new IdleState_obj; }
hx::ObjectPtr< IdleState_obj > IdleState_obj::__new(int type)
{  hx::ObjectPtr< IdleState_obj > result = new IdleState_obj();
	result->__construct(type);
	return result;}

Dynamic IdleState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< IdleState_obj > result = new IdleState_obj();
	result->__construct(inArgs[0]);
	return result;}

hx::Object *IdleState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IState_obj)) return operator ::IState_obj *();
	return super::__ToInterface(inType);
}

Void IdleState_obj::tick( Float deltaTime,::Enemy enemy){
{
		HX_STACK_PUSH("IdleState::tick","IdleState.hx",23);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(23)
		if (((::SceneManager_obj::squareDistance(enemy->getAnimation()->getPosition(),::Enemy_obj::player->getAnimation()->getPosition()) < ::Math_obj::pow(::SceneManager_obj::currentSceneManager->getSharedObject()->data->__Field(HX_CSTRING("aggroRadius"),true),(int)2)))){
			HX_STACK_LINE(26)
			enemy->exitState(hx::ObjectPtr<OBJ_>(this));
			HX_STACK_LINE(28)
			{
				HX_STACK_LINE(28)
				int _g = (int)0;		HX_STACK_VAR(_g,"_g");
				Array< ::IState > _g1 = this->nextStates;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(28)
				while(((_g < _g1->length))){
					HX_STACK_LINE(28)
					::IState state = _g1->__get(_g);		HX_STACK_VAR(state,"state");
					HX_STACK_LINE(28)
					++(_g);
					HX_STACK_LINE(29)
					enemy->enterState(state);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(IdleState_obj,tick,(void))


IdleState_obj::IdleState_obj()
{
}

void IdleState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(IdleState);
	HX_MARK_MEMBER_NAME(nextStates,"nextStates");
	HX_MARK_END_CLASS();
}

void IdleState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(nextStates,"nextStates");
}

Dynamic IdleState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"nextStates") ) { return nextStates; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic IdleState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 10:
		if (HX_FIELD_EQ(inName,"nextStates") ) { nextStates=inValue.Cast< Array< ::IState > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void IdleState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("nextStates"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("tick"),
	HX_CSTRING("nextStates"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IdleState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IdleState_obj::__mClass,"__mClass");
};

Class IdleState_obj::__mClass;

void IdleState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("IdleState"), hx::TCanCast< IdleState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IdleState_obj::__boot()
{
}

