#include <hxcpp.h>

#ifndef INCLUDED_Animation
#include <Animation.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_IState
#include <IState.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_StrafingState
#include <StrafingState.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif

Void StrafingState_obj::__construct()
{
HX_STACK_PUSH("StrafingState::new","StrafingState.hx",8);
{
}
;
	return null();
}

StrafingState_obj::~StrafingState_obj() { }

Dynamic StrafingState_obj::__CreateEmpty() { return  new StrafingState_obj; }
hx::ObjectPtr< StrafingState_obj > StrafingState_obj::__new()
{  hx::ObjectPtr< StrafingState_obj > result = new StrafingState_obj();
	result->__construct();
	return result;}

Dynamic StrafingState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< StrafingState_obj > result = new StrafingState_obj();
	result->__construct();
	return result;}

hx::Object *StrafingState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IState_obj)) return operator ::IState_obj *();
	return super::__ToInterface(inType);
}

Void StrafingState_obj::tick( Float deltaTime,::Enemy enemy){
{
		HX_STACK_PUSH("StrafingState::tick","StrafingState.hx",12);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_ARG(enemy,"enemy");
		HX_STACK_LINE(14)
		if (((::Enemy_obj::player->getAnimation()->getPosition()->y > enemy->getAnimation()->getPosition()->y))){
			HX_STACK_LINE(14)
			enemy->getAnimation()->setRotation((::SceneManager_obj::usingFlash * (((int)180 - (Float(((int)180 * ::Math_obj::atan((Float(((::Enemy_obj::player->getAnimation()->getPosition()->x - enemy->getAnimation()->getPosition()->x))) / Float(((::Enemy_obj::player->getAnimation()->getPosition()->y - enemy->getAnimation()->getPosition()->y))))))) / Float(::Math_obj::PI))))));
		}
		else{
			HX_STACK_LINE(16)
			if (((::Enemy_obj::player->getAnimation()->getPosition()->y < enemy->getAnimation()->getPosition()->y))){
				HX_STACK_LINE(16)
				enemy->getAnimation()->setRotation((::SceneManager_obj::usingFlash * ((Float(((int)-180 * ::Math_obj::atan((Float(((::Enemy_obj::player->getAnimation()->getPosition()->x - enemy->getAnimation()->getPosition()->x))) / Float(((::Enemy_obj::player->getAnimation()->getPosition()->y - enemy->getAnimation()->getPosition()->y))))))) / Float(::Math_obj::PI)))));
			}
		}
		HX_STACK_LINE(20)
		enemy->getAnimation()->setRotation((enemy->getAnimation()->getRotation() - (int)75));
		HX_STACK_LINE(23)
		enemy->getAnimation()->setX((enemy->getAnimation()->getPosition()->x + (Float((((int)2 * ::Math_obj::sin((Float(((::SceneManager_obj::usingFlash * enemy->getAnimation()->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime)) / Float((int)30))));
		HX_STACK_LINE(24)
		enemy->getAnimation()->setY((enemy->getAnimation()->getPosition()->y - (Float((((int)2 * ::Math_obj::cos((Float(((::SceneManager_obj::usingFlash * enemy->getAnimation()->getRotation()) * ::Math_obj::PI)) / Float((int)180)))) * deltaTime)) / Float((int)30))));
		HX_STACK_LINE(27)
		if (((::Enemy_obj::player->getAnimation()->getPosition()->y > enemy->getAnimation()->getPosition()->y))){
			HX_STACK_LINE(27)
			enemy->getAnimation()->setRotation((::SceneManager_obj::usingFlash * (((int)180 - (Float(((int)180 * ::Math_obj::atan((Float(((::Enemy_obj::player->getAnimation()->getPosition()->x - enemy->getAnimation()->getPosition()->x))) / Float(((::Enemy_obj::player->getAnimation()->getPosition()->y - enemy->getAnimation()->getPosition()->y))))))) / Float(::Math_obj::PI))))));
		}
		else{
			HX_STACK_LINE(29)
			if (((::Enemy_obj::player->getAnimation()->getPosition()->y < enemy->getAnimation()->getPosition()->y))){
				HX_STACK_LINE(29)
				enemy->getAnimation()->setRotation((::SceneManager_obj::usingFlash * ((Float(((int)-180 * ::Math_obj::atan((Float(((::Enemy_obj::player->getAnimation()->getPosition()->x - enemy->getAnimation()->getPosition()->x))) / Float(((::Enemy_obj::player->getAnimation()->getPosition()->y - enemy->getAnimation()->getPosition()->y))))))) / Float(::Math_obj::PI)))));
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(StrafingState_obj,tick,(void))


StrafingState_obj::StrafingState_obj()
{
}

void StrafingState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(StrafingState);
	HX_MARK_END_CLASS();
}

void StrafingState_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic StrafingState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic StrafingState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void StrafingState_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("tick"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(StrafingState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(StrafingState_obj::__mClass,"__mClass");
};

Class StrafingState_obj::__mClass;

void StrafingState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("StrafingState"), hx::TCanCast< StrafingState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void StrafingState_obj::__boot()
{
}

