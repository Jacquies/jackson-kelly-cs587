#include <hxcpp.h>

#ifndef INCLUDED_Greed3
#include <Greed3.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_menuItems_MainMenu
#include <engine/menuItems/MainMenu.h>
#endif
#ifndef INCLUDED_native_display_BitmapData
#include <native/display/BitmapData.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_Graphics
#include <native/display/Graphics.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_MovieClip
#include <native/display/MovieClip.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_display_Stage
#include <native/display/Stage.h>
#endif
#ifndef INCLUDED_native_display_Tilesheet
#include <native/display/Tilesheet.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_geom_Rectangle
#include <native/geom/Rectangle.h>
#endif
#ifndef INCLUDED_nme_Lib
#include <nme/Lib.h>
#endif
#ifndef INCLUDED_nme_installer_Assets
#include <nme/installer/Assets.h>
#endif

Void Greed3_obj::__construct()
{
HX_STACK_PUSH("Greed3::new","Greed3.hx",38);
{
	HX_STACK_LINE(39)
	super::__construct();
	HX_STACK_LINE(45)
	this->tilesheet = ::native::display::Tilesheet_obj::__new(::nme::installer::Assets_obj::getBitmapData(HX_CSTRING("assets/sheet.png"),null()));
	HX_STACK_LINE(47)
	this->tilesheet->addTileRect(::native::geom::Rectangle_obj::__new((int)4,(int)0,(int)8,(int)16),null());
	HX_STACK_LINE(48)
	this->tilesheet->addTileRect(::native::geom::Rectangle_obj::__new((int)20,(int)0,(int)8,(int)16),null());
	HX_STACK_LINE(50)
	Array< Float > tileData = Array_obj< Float >::__new().Add(20.).Add((int)10).Add((int)0).Add((int)10).Add((Float(::Math_obj::PI) / Float((int)4))).Add((int)100).Add((int)100).Add((int)0).Add((int)10).Add((Float(-(::Math_obj::PI)) / Float((int)3)));		HX_STACK_VAR(tileData,"tileData");
	HX_STACK_LINE(53)
	this->tilesheet->drawTiles(this->get_graphics(),tileData,false,(int)3);
}
;
	return null();
}

Greed3_obj::~Greed3_obj() { }

Dynamic Greed3_obj::__CreateEmpty() { return  new Greed3_obj; }
hx::ObjectPtr< Greed3_obj > Greed3_obj::__new()
{  hx::ObjectPtr< Greed3_obj > result = new Greed3_obj();
	result->__construct();
	return result;}

Dynamic Greed3_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Greed3_obj > result = new Greed3_obj();
	result->__construct();
	return result;}

Void Greed3_obj::main( ){
{
		HX_STACK_PUSH("Greed3::main","Greed3.hx",27);
		HX_STACK_LINE(31)
		::engine::managers::TileRenderer renderer = ::engine::managers::TileRenderer_obj::__new(::nme::Lib_obj::get_current()->get_graphics(),::nme::Lib_obj::get_current()->get_stage());		HX_STACK_VAR(renderer,"renderer");
		HX_STACK_LINE(33)
		::nme::Lib_obj::get_current()->get_stage()->addChild(::engine::menuItems::MainMenu_obj::__new(renderer));
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(Greed3_obj,main,(void))


Greed3_obj::Greed3_obj()
{
}

void Greed3_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Greed3);
	HX_MARK_MEMBER_NAME(tileData,"tileData");
	HX_MARK_MEMBER_NAME(tilesheet,"tilesheet");
	super::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Greed3_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(tileData,"tileData");
	HX_VISIT_MEMBER_NAME(tilesheet,"tilesheet");
	super::__Visit(HX_VISIT_ARG);
}

Dynamic Greed3_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"main") ) { return main_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"tileData") ) { return tileData; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tilesheet") ) { return tilesheet; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Greed3_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"tileData") ) { tileData=inValue.Cast< Array< Float > >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tilesheet") ) { tilesheet=inValue.Cast< ::native::display::Tilesheet >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Greed3_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("tileData"));
	outFields->push(HX_CSTRING("tilesheet"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("main"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("tileData"),
	HX_CSTRING("tilesheet"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Greed3_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Greed3_obj::__mClass,"__mClass");
};

Class Greed3_obj::__mClass;

void Greed3_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("Greed3"), hx::TCanCast< Greed3_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Greed3_obj::__boot()
{
}

