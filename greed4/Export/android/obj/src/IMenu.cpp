#include <hxcpp.h>

#ifndef INCLUDED_IMenu
#include <IMenu.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif

HX_DEFINE_DYNAMIC_FUNC0(IMenu_obj,getSharedObject,return )

HX_DEFINE_DYNAMIC_FUNC0(IMenu_obj,options,)

HX_DEFINE_DYNAMIC_FUNC0(IMenu_obj,back,)

HX_DEFINE_DYNAMIC_FUNC0(IMenu_obj,forward,)

HX_DEFINE_DYNAMIC_FUNC0(IMenu_obj,refresh,)


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IMenu_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IMenu_obj::__mClass,"__mClass");
};

Class IMenu_obj::__mClass;

void IMenu_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("IMenu"), hx::TCanCast< IMenu_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IMenu_obj::__boot()
{
}

