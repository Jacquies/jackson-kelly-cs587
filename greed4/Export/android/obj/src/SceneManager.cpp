#include <hxcpp.h>

#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_HealthBar
#include <HealthBar.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_Level
#include <Level.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_SceneManager
#include <SceneManager.h>
#endif
#ifndef INCLUDED_Scoreboard
#include <Scoreboard.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif

Void SceneManager_obj::__construct(::TileRenderer tileRenderer)
{
HX_STACK_PUSH("SceneManager::new","SceneManager.hx",27);
{
	HX_STACK_LINE(28)
	this->renderer = tileRenderer;
	HX_STACK_LINE(29)
	::SceneManager_obj::currentSceneManager = hx::ObjectPtr<OBJ_>(this);
	HX_STACK_LINE(30)
	::SceneManager_obj::usingFlash = (int)-1;
}
;
	return null();
}

SceneManager_obj::~SceneManager_obj() { }

Dynamic SceneManager_obj::__CreateEmpty() { return  new SceneManager_obj; }
hx::ObjectPtr< SceneManager_obj > SceneManager_obj::__new(::TileRenderer tileRenderer)
{  hx::ObjectPtr< SceneManager_obj > result = new SceneManager_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic SceneManager_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< SceneManager_obj > result = new SceneManager_obj();
	result->__construct(inArgs[0]);
	return result;}

Void SceneManager_obj::setLevelOver( bool isLevelOver){
{
		HX_STACK_PUSH("SceneManager::setLevelOver","SceneManager.hx",229);
		HX_STACK_THIS(this);
		HX_STACK_ARG(isLevelOver,"isLevelOver");
		HX_STACK_LINE(229)
		this->levelOver = isLevelOver;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setLevelOver,(void))

Void SceneManager_obj::setIdle( bool idleState){
{
		HX_STACK_PUSH("SceneManager::setIdle","SceneManager.hx",225);
		HX_STACK_THIS(this);
		HX_STACK_ARG(idleState,"idleState");
		HX_STACK_LINE(225)
		this->idle = idleState;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setIdle,(void))

bool SceneManager_obj::isIdle( ){
	HX_STACK_PUSH("SceneManager::isIdle","SceneManager.hx",221);
	HX_STACK_THIS(this);
	HX_STACK_LINE(221)
	return this->idle;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,isIdle,return )

::native::net::SharedObject SceneManager_obj::getSharedObject( ){
	HX_STACK_PUSH("SceneManager::getSharedObject","SceneManager.hx",217);
	HX_STACK_THIS(this);
	HX_STACK_LINE(217)
	return this->localData;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,getSharedObject,return )

Void SceneManager_obj::clearTickableSet( ){
{
		HX_STACK_PUSH("SceneManager::clearTickableSet","SceneManager.hx",213);
		HX_STACK_THIS(this);
		HX_STACK_LINE(213)
		this->tickableSet->slice((int)0,this->tickableSet->length);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,clearTickableSet,(void))

Void SceneManager_obj::removeFromTickableSet( ::ITickable tickable){
{
		HX_STACK_PUSH("SceneManager::removeFromTickableSet","SceneManager.hx",209);
		HX_STACK_THIS(this);
		HX_STACK_ARG(tickable,"tickable");
		HX_STACK_LINE(209)
		this->tickableSet->remove(tickable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,removeFromTickableSet,(void))

Void SceneManager_obj::addToTickableSet( ::ITickable tickable){
{
		HX_STACK_PUSH("SceneManager::addToTickableSet","SceneManager.hx",205);
		HX_STACK_THIS(this);
		HX_STACK_ARG(tickable,"tickable");
		HX_STACK_LINE(205)
		this->tickableSet->push(tickable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,addToTickableSet,(void))

Void SceneManager_obj::clearRenderSet( ){
{
		HX_STACK_PUSH("SceneManager::clearRenderSet","SceneManager.hx",201);
		HX_STACK_THIS(this);
		HX_STACK_LINE(201)
		this->renderSet->slice((int)0,this->renderSet->length);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,clearRenderSet,(void))

Void SceneManager_obj::removeFromRenderSet( ::IRenderable renderable){
{
		HX_STACK_PUSH("SceneManager::removeFromRenderSet","SceneManager.hx",197);
		HX_STACK_THIS(this);
		HX_STACK_ARG(renderable,"renderable");
		HX_STACK_LINE(197)
		this->renderSet->remove(renderable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,removeFromRenderSet,(void))

Void SceneManager_obj::addToRenderSet( ::IRenderable renderable){
{
		HX_STACK_PUSH("SceneManager::addToRenderSet","SceneManager.hx",193);
		HX_STACK_THIS(this);
		HX_STACK_ARG(renderable,"renderable");
		HX_STACK_LINE(193)
		this->renderSet->push(renderable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,addToRenderSet,(void))

Void SceneManager_obj::clearCollisionSet( ){
{
		HX_STACK_PUSH("SceneManager::clearCollisionSet","SceneManager.hx",189);
		HX_STACK_THIS(this);
		HX_STACK_LINE(189)
		this->collisionSet->slice((int)0,this->collisionSet->length);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,clearCollisionSet,(void))

Void SceneManager_obj::removeFromCollisionSet( ::ICollidable collidable){
{
		HX_STACK_PUSH("SceneManager::removeFromCollisionSet","SceneManager.hx",185);
		HX_STACK_THIS(this);
		HX_STACK_ARG(collidable,"collidable");
		HX_STACK_LINE(185)
		this->collisionSet->remove(collidable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,removeFromCollisionSet,(void))

Void SceneManager_obj::addToCollisionSet( ::ICollidable collidable){
{
		HX_STACK_PUSH("SceneManager::addToCollisionSet","SceneManager.hx",181);
		HX_STACK_THIS(this);
		HX_STACK_ARG(collidable,"collidable");
		HX_STACK_LINE(181)
		this->collisionSet->push(collidable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,addToCollisionSet,(void))

Void SceneManager_obj::setCameraY( Float y){
{
		HX_STACK_PUSH("SceneManager::setCameraY","SceneManager.hx",177);
		HX_STACK_THIS(this);
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(177)
		this->camera->y = y;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setCameraY,(void))

Void SceneManager_obj::setCameraX( Float x){
{
		HX_STACK_PUSH("SceneManager::setCameraX","SceneManager.hx",173);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_LINE(173)
		this->camera->x = x;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setCameraX,(void))

::native::geom::Point SceneManager_obj::getCameraPosition( ){
	HX_STACK_PUSH("SceneManager::getCameraPosition","SceneManager.hx",169);
	HX_STACK_THIS(this);
	HX_STACK_LINE(169)
	return this->camera;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,getCameraPosition,return )

::native::geom::Point SceneManager_obj::getMousePosition( ){
	HX_STACK_PUSH("SceneManager::getMousePosition","SceneManager.hx",165);
	HX_STACK_THIS(this);
	HX_STACK_LINE(165)
	return this->mousePosition;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,getMousePosition,return )

Void SceneManager_obj::setMousePosition( Float x,Float y){
{
		HX_STACK_PUSH("SceneManager::setMousePosition","SceneManager.hx",160);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(161)
		this->mousePosition->x = x;
		HX_STACK_LINE(162)
		this->mousePosition->y = y;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(SceneManager_obj,setMousePosition,(void))

bool SceneManager_obj::getMouseDown( ){
	HX_STACK_PUSH("SceneManager::getMouseDown","SceneManager.hx",156);
	HX_STACK_THIS(this);
	HX_STACK_LINE(156)
	return this->mouseDown;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,getMouseDown,return )

Void SceneManager_obj::setMouseDown( bool isMouseDown){
{
		HX_STACK_PUSH("SceneManager::setMouseDown","SceneManager.hx",152);
		HX_STACK_THIS(this);
		HX_STACK_ARG(isMouseDown,"isMouseDown");
		HX_STACK_LINE(152)
		this->mouseDown = isMouseDown;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setMouseDown,(void))

Void SceneManager_obj::updateHealthBar( ){
{
		HX_STACK_PUSH("SceneManager::updateHealthBar","SceneManager.hx",148);
		HX_STACK_THIS(this);
		HX_STACK_LINE(148)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,updateHealthBar,(void))

Void SceneManager_obj::updateScoreboard( ){
{
		HX_STACK_PUSH("SceneManager::updateScoreboard","SceneManager.hx",144);
		HX_STACK_THIS(this);
		HX_STACK_LINE(144)
		this->scoreboard->update(this->localData->data->__Field(HX_CSTRING("gold"),true));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,updateScoreboard,(void))

Void SceneManager_obj::tick( int deltaTime){
{
		HX_STACK_PUSH("SceneManager::tick","SceneManager.hx",90);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_LINE(91)
		if ((this->levelOver)){
			HX_STACK_LINE(92)
			return null();
		}
		HX_STACK_LINE(94)
		if ((!(this->idle))){
			HX_STACK_LINE(95)
			{
				HX_STACK_LINE(95)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				int _g = this->collisionSet->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(95)
				while(((_g1 < _g))){
					HX_STACK_LINE(95)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(96)
					{
						HX_STACK_LINE(96)
						int _g3 = (i + (int)1);		HX_STACK_VAR(_g3,"_g3");
						int _g2 = this->collisionSet->length;		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(96)
						while(((_g3 < _g2))){
							HX_STACK_LINE(96)
							int j = (_g3)++;		HX_STACK_VAR(j,"j");
							HX_STACK_LINE(97)
							::SceneManager_obj::collide(this->collisionSet->__get(i),this->collisionSet->__get(j));
						}
					}
				}
			}
			HX_STACK_LINE(101)
			{
				HX_STACK_LINE(101)
				int _g = (int)0;		HX_STACK_VAR(_g,"_g");
				Array< ::ITickable > _g1 = this->tickableSet;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(101)
				while(((_g < _g1->length))){
					HX_STACK_LINE(101)
					::ITickable tickable = _g1->__get(_g);		HX_STACK_VAR(tickable,"tickable");
					HX_STACK_LINE(101)
					++(_g);
					HX_STACK_LINE(102)
					tickable->tick(deltaTime);
				}
			}
		}
		HX_STACK_LINE(106)
		{
			HX_STACK_LINE(106)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			Array< ::IRenderable > _g1 = this->renderSet;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(106)
			while(((_g < _g1->length))){
				HX_STACK_LINE(106)
				::IRenderable renderable = _g1->__get(_g);		HX_STACK_VAR(renderable,"renderable");
				HX_STACK_LINE(106)
				++(_g);
				HX_STACK_LINE(107)
				renderable->render(deltaTime,this->camera);
			}
		}
		HX_STACK_LINE(110)
		this->renderer->draw();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,tick,(void))

Void SceneManager_obj::initialize( ){
{
		HX_STACK_PUSH("SceneManager::initialize","SceneManager.hx",36);
		HX_STACK_THIS(this);
		HX_STACK_LINE(40)
		if (((bool((::TileRenderer_obj::stageHeight > (int)350)) && bool((::TileRenderer_obj::stageWidth > (int)350))))){
			HX_STACK_LINE(41)
			::SceneManager_obj::scale = (int)1;
			HX_STACK_LINE(42)
			if (((::TileRenderer_obj::DPI > (int)200))){
				HX_STACK_LINE(43)
				::SceneManager_obj::scale = (Float(::TileRenderer_obj::DPI) / Float((int)200));
			}
		}
		else{
			HX_STACK_LINE(44)
			::SceneManager_obj::scale = (Float(::Math_obj::min(::TileRenderer_obj::stageHeight,::TileRenderer_obj::stageWidth)) / Float((int)350));
		}
		HX_STACK_LINE(49)
		this->idle = true;
		HX_STACK_LINE(50)
		this->mouseDown = false;
		HX_STACK_LINE(51)
		this->camera = ::native::geom::Point_obj::__new((int)0,(int)0);
		HX_STACK_LINE(52)
		this->mousePosition = ::native::geom::Point_obj::__new((int)0,(int)0);
		HX_STACK_LINE(53)
		this->levelOver = false;
		HX_STACK_LINE(56)
		this->collisionSet = Array_obj< ::ICollidable >::__new();
		HX_STACK_LINE(57)
		this->renderSet = Array_obj< ::IRenderable >::__new();
		HX_STACK_LINE(58)
		this->tickableSet = Array_obj< ::ITickable >::__new();
		HX_STACK_LINE(61)
		this->localData = ::native::net::SharedObject_obj::getLocal(HX_CSTRING("greed"),null(),null());
		HX_STACK_LINE(62)
		if ((!(this->localData->data->__Field(HX_CSTRING("initialized"),true)))){
			HX_STACK_LINE(62)
			::Level_obj::initializeLocalData(this->localData);
		}
		else{
			HX_STACK_LINE(64)
			if ((this->localData->data->__Field(HX_CSTRING("newGame"),true))){
				HX_STACK_LINE(64)
				::Level_obj::newGame(this->localData);
			}
		}
		HX_STACK_LINE(69)
		this->scoreboard = ::Scoreboard_obj::__new(this->renderer);
		HX_STACK_LINE(70)
		this->scoreboard->update(this->localData->data->__Field(HX_CSTRING("gold"),true));
		HX_STACK_LINE(73)
		this->healthBar = ::HealthBar_obj::__new(this->renderer);
		HX_STACK_LINE(74)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
		HX_STACK_LINE(87)
		this->idle = false;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,initialize,(void))

Float SceneManager_obj::scale;

int SceneManager_obj::usingFlash;

::SceneManager SceneManager_obj::currentSceneManager;

Void SceneManager_obj::collide( ::ICollidable collidable1,::ICollidable collidable2){
{
		HX_STACK_PUSH("SceneManager::collide","SceneManager.hx",233);
		HX_STACK_ARG(collidable1,"collidable1");
		HX_STACK_ARG(collidable2,"collidable2");
		HX_STACK_LINE(233)
		if ((collidable1->getCollider()->collides(collidable2->getCollider()))){
			HX_STACK_LINE(235)
			collidable1->collide(collidable2);
			HX_STACK_LINE(236)
			collidable2->collide(collidable1);
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(SceneManager_obj,collide,(void))

Float SceneManager_obj::distance( ::native::geom::Point point1,::native::geom::Point point2){
	HX_STACK_PUSH("SceneManager::distance","SceneManager.hx",240);
	HX_STACK_ARG(point1,"point1");
	HX_STACK_ARG(point2,"point2");
	HX_STACK_LINE(240)
	return ::Math_obj::sqrt(((((point1->x - point2->x)) * ((point1->x - point2->x))) + (((point1->y - point2->y)) * ((point1->y - point2->y)))));
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(SceneManager_obj,distance,return )

Float SceneManager_obj::squareDistance( ::native::geom::Point point1,::native::geom::Point point2){
	HX_STACK_PUSH("SceneManager::squareDistance","SceneManager.hx",244);
	HX_STACK_ARG(point1,"point1");
	HX_STACK_ARG(point2,"point2");
	HX_STACK_LINE(244)
	return ((((point1->x - point2->x)) * ((point1->x - point2->x))) + (((point1->y - point2->y)) * ((point1->y - point2->y))));
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(SceneManager_obj,squareDistance,return )


SceneManager_obj::SceneManager_obj()
{
}

void SceneManager_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(SceneManager);
	HX_MARK_MEMBER_NAME(levelOver,"levelOver");
	HX_MARK_MEMBER_NAME(healthBar,"healthBar");
	HX_MARK_MEMBER_NAME(scoreboard,"scoreboard");
	HX_MARK_MEMBER_NAME(idle,"idle");
	HX_MARK_MEMBER_NAME(localData,"localData");
	HX_MARK_MEMBER_NAME(mousePosition,"mousePosition");
	HX_MARK_MEMBER_NAME(camera,"camera");
	HX_MARK_MEMBER_NAME(mouseDown,"mouseDown");
	HX_MARK_MEMBER_NAME(tickableSet,"tickableSet");
	HX_MARK_MEMBER_NAME(renderSet,"renderSet");
	HX_MARK_MEMBER_NAME(collisionSet,"collisionSet");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_END_CLASS();
}

void SceneManager_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(levelOver,"levelOver");
	HX_VISIT_MEMBER_NAME(healthBar,"healthBar");
	HX_VISIT_MEMBER_NAME(scoreboard,"scoreboard");
	HX_VISIT_MEMBER_NAME(idle,"idle");
	HX_VISIT_MEMBER_NAME(localData,"localData");
	HX_VISIT_MEMBER_NAME(mousePosition,"mousePosition");
	HX_VISIT_MEMBER_NAME(camera,"camera");
	HX_VISIT_MEMBER_NAME(mouseDown,"mouseDown");
	HX_VISIT_MEMBER_NAME(tickableSet,"tickableSet");
	HX_VISIT_MEMBER_NAME(renderSet,"renderSet");
	HX_VISIT_MEMBER_NAME(collisionSet,"collisionSet");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
}

Dynamic SceneManager_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		if (HX_FIELD_EQ(inName,"idle") ) { return idle; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { return scale; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"isIdle") ) { return isIdle_dyn(); }
		if (HX_FIELD_EQ(inName,"camera") ) { return camera; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		if (HX_FIELD_EQ(inName,"setIdle") ) { return setIdle_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"distance") ) { return distance_dyn(); }
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"levelOver") ) { return levelOver; }
		if (HX_FIELD_EQ(inName,"healthBar") ) { return healthBar; }
		if (HX_FIELD_EQ(inName,"localData") ) { return localData; }
		if (HX_FIELD_EQ(inName,"mouseDown") ) { return mouseDown; }
		if (HX_FIELD_EQ(inName,"renderSet") ) { return renderSet; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"usingFlash") ) { return usingFlash; }
		if (HX_FIELD_EQ(inName,"setCameraY") ) { return setCameraY_dyn(); }
		if (HX_FIELD_EQ(inName,"setCameraX") ) { return setCameraX_dyn(); }
		if (HX_FIELD_EQ(inName,"initialize") ) { return initialize_dyn(); }
		if (HX_FIELD_EQ(inName,"scoreboard") ) { return scoreboard; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"tickableSet") ) { return tickableSet; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"setLevelOver") ) { return setLevelOver_dyn(); }
		if (HX_FIELD_EQ(inName,"getMouseDown") ) { return getMouseDown_dyn(); }
		if (HX_FIELD_EQ(inName,"setMouseDown") ) { return setMouseDown_dyn(); }
		if (HX_FIELD_EQ(inName,"collisionSet") ) { return collisionSet; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"mousePosition") ) { return mousePosition; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"squareDistance") ) { return squareDistance_dyn(); }
		if (HX_FIELD_EQ(inName,"clearRenderSet") ) { return clearRenderSet_dyn(); }
		if (HX_FIELD_EQ(inName,"addToRenderSet") ) { return addToRenderSet_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"getSharedObject") ) { return getSharedObject_dyn(); }
		if (HX_FIELD_EQ(inName,"updateHealthBar") ) { return updateHealthBar_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"clearTickableSet") ) { return clearTickableSet_dyn(); }
		if (HX_FIELD_EQ(inName,"addToTickableSet") ) { return addToTickableSet_dyn(); }
		if (HX_FIELD_EQ(inName,"getMousePosition") ) { return getMousePosition_dyn(); }
		if (HX_FIELD_EQ(inName,"setMousePosition") ) { return setMousePosition_dyn(); }
		if (HX_FIELD_EQ(inName,"updateScoreboard") ) { return updateScoreboard_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"clearCollisionSet") ) { return clearCollisionSet_dyn(); }
		if (HX_FIELD_EQ(inName,"addToCollisionSet") ) { return addToCollisionSet_dyn(); }
		if (HX_FIELD_EQ(inName,"getCameraPosition") ) { return getCameraPosition_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"currentSceneManager") ) { return currentSceneManager; }
		if (HX_FIELD_EQ(inName,"removeFromRenderSet") ) { return removeFromRenderSet_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"removeFromTickableSet") ) { return removeFromTickableSet_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"removeFromCollisionSet") ) { return removeFromCollisionSet_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic SceneManager_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"idle") ) { idle=inValue.Cast< bool >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { scale=inValue.Cast< Float >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"camera") ) { camera=inValue.Cast< ::native::geom::Point >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::TileRenderer >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"levelOver") ) { levelOver=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"healthBar") ) { healthBar=inValue.Cast< ::HealthBar >(); return inValue; }
		if (HX_FIELD_EQ(inName,"localData") ) { localData=inValue.Cast< ::native::net::SharedObject >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouseDown") ) { mouseDown=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"renderSet") ) { renderSet=inValue.Cast< Array< ::IRenderable > >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"usingFlash") ) { usingFlash=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"scoreboard") ) { scoreboard=inValue.Cast< ::Scoreboard >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"tickableSet") ) { tickableSet=inValue.Cast< Array< ::ITickable > >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"collisionSet") ) { collisionSet=inValue.Cast< Array< ::ICollidable > >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"mousePosition") ) { mousePosition=inValue.Cast< ::native::geom::Point >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"currentSceneManager") ) { currentSceneManager=inValue.Cast< ::SceneManager >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void SceneManager_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("levelOver"));
	outFields->push(HX_CSTRING("healthBar"));
	outFields->push(HX_CSTRING("scoreboard"));
	outFields->push(HX_CSTRING("idle"));
	outFields->push(HX_CSTRING("localData"));
	outFields->push(HX_CSTRING("mousePosition"));
	outFields->push(HX_CSTRING("camera"));
	outFields->push(HX_CSTRING("mouseDown"));
	outFields->push(HX_CSTRING("tickableSet"));
	outFields->push(HX_CSTRING("renderSet"));
	outFields->push(HX_CSTRING("collisionSet"));
	outFields->push(HX_CSTRING("renderer"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("scale"),
	HX_CSTRING("usingFlash"),
	HX_CSTRING("currentSceneManager"),
	HX_CSTRING("collide"),
	HX_CSTRING("distance"),
	HX_CSTRING("squareDistance"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("setLevelOver"),
	HX_CSTRING("setIdle"),
	HX_CSTRING("isIdle"),
	HX_CSTRING("getSharedObject"),
	HX_CSTRING("clearTickableSet"),
	HX_CSTRING("removeFromTickableSet"),
	HX_CSTRING("addToTickableSet"),
	HX_CSTRING("clearRenderSet"),
	HX_CSTRING("removeFromRenderSet"),
	HX_CSTRING("addToRenderSet"),
	HX_CSTRING("clearCollisionSet"),
	HX_CSTRING("removeFromCollisionSet"),
	HX_CSTRING("addToCollisionSet"),
	HX_CSTRING("setCameraY"),
	HX_CSTRING("setCameraX"),
	HX_CSTRING("getCameraPosition"),
	HX_CSTRING("getMousePosition"),
	HX_CSTRING("setMousePosition"),
	HX_CSTRING("getMouseDown"),
	HX_CSTRING("setMouseDown"),
	HX_CSTRING("updateHealthBar"),
	HX_CSTRING("updateScoreboard"),
	HX_CSTRING("tick"),
	HX_CSTRING("initialize"),
	HX_CSTRING("levelOver"),
	HX_CSTRING("healthBar"),
	HX_CSTRING("scoreboard"),
	HX_CSTRING("idle"),
	HX_CSTRING("localData"),
	HX_CSTRING("mousePosition"),
	HX_CSTRING("camera"),
	HX_CSTRING("mouseDown"),
	HX_CSTRING("tickableSet"),
	HX_CSTRING("renderSet"),
	HX_CSTRING("collisionSet"),
	HX_CSTRING("renderer"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(SceneManager_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(SceneManager_obj::scale,"scale");
	HX_MARK_MEMBER_NAME(SceneManager_obj::usingFlash,"usingFlash");
	HX_MARK_MEMBER_NAME(SceneManager_obj::currentSceneManager,"currentSceneManager");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(SceneManager_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(SceneManager_obj::scale,"scale");
	HX_VISIT_MEMBER_NAME(SceneManager_obj::usingFlash,"usingFlash");
	HX_VISIT_MEMBER_NAME(SceneManager_obj::currentSceneManager,"currentSceneManager");
};

Class SceneManager_obj::__mClass;

void SceneManager_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("SceneManager"), hx::TCanCast< SceneManager_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void SceneManager_obj::__boot()
{
}

