#include <hxcpp.h>

#ifndef INCLUDED_Button
#include <Button.h>
#endif
#ifndef INCLUDED_HealthBar
#include <HealthBar.h>
#endif
#ifndef INCLUDED_Level
#include <Level.h>
#endif
#ifndef INCLUDED_MainMenu
#include <MainMenu.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Scoreboard
#include <Scoreboard.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_UpgradeMenu
#include <UpgradeMenu.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_display_Stage
#include <native/display/Stage.h>
#endif
#ifndef INCLUDED_native_events_Event
#include <native/events/Event.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_MouseEvent
#include <native/events/MouseEvent.h>
#endif
#ifndef INCLUDED_native_events_TouchEvent
#include <native/events/TouchEvent.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
#ifndef INCLUDED_native_net_SharedObjectFlushStatus
#include <native/net/SharedObjectFlushStatus.h>
#endif

Void UpgradeMenu_obj::__construct(::TileRenderer tileRenderer)
{
HX_STACK_PUSH("UpgradeMenu::new","UpgradeMenu.hx",21);
{
	HX_STACK_LINE(22)
	super::__construct();
	HX_STACK_LINE(24)
	this->renderer = tileRenderer;
	HX_STACK_LINE(26)
	this->addEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null(),null(),null());
}
;
	return null();
}

UpgradeMenu_obj::~UpgradeMenu_obj() { }

Dynamic UpgradeMenu_obj::__CreateEmpty() { return  new UpgradeMenu_obj; }
hx::ObjectPtr< UpgradeMenu_obj > UpgradeMenu_obj::__new(::TileRenderer tileRenderer)
{  hx::ObjectPtr< UpgradeMenu_obj > result = new UpgradeMenu_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic UpgradeMenu_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< UpgradeMenu_obj > result = new UpgradeMenu_obj();
	result->__construct(inArgs[0]);
	return result;}

Void UpgradeMenu_obj::dummyFunction( ){
{
		HX_STACK_PUSH("UpgradeMenu::dummyFunction","UpgradeMenu.hx",237);
		HX_STACK_THIS(this);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,dummyFunction,(void))

Void UpgradeMenu_obj::onTouchBegin( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("UpgradeMenu::onTouchBegin","UpgradeMenu.hx",231);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(232)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		Array< ::Button > _g1 = this->buttons;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(232)
		while(((_g < _g1->length))){
			HX_STACK_LINE(232)
			::Button button = _g1->__get(_g);		HX_STACK_VAR(button,"button");
			HX_STACK_LINE(232)
			++(_g);
			HX_STACK_LINE(233)
			button->onClick(this->get_mouseX(),this->get_mouseY());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(UpgradeMenu_obj,onTouchBegin,(void))

Void UpgradeMenu_obj::nextLevel( ){
{
		HX_STACK_PUSH("UpgradeMenu::nextLevel","UpgradeMenu.hx",211);
		HX_STACK_THIS(this);
		HX_STACK_LINE(212)
		this->buttons->splice((int)0,this->buttons->length);
		HX_STACK_LINE(213)
		this->get_parent()->addChild(::Level_obj::__new(this->renderer));
		HX_STACK_LINE(215)
		this->removeEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null());
		HX_STACK_LINE(219)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null());
		HX_STACK_LINE(222)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,nextLevel,(void))

Void UpgradeMenu_obj::saveAndExit( ){
{
		HX_STACK_PUSH("UpgradeMenu::saveAndExit","UpgradeMenu.hx",195);
		HX_STACK_THIS(this);
		HX_STACK_LINE(196)
		this->localData->flush(null());
		HX_STACK_LINE(198)
		this->buttons->splice((int)0,this->buttons->length);
		HX_STACK_LINE(199)
		this->get_parent()->addChild(::MainMenu_obj::__new(this->renderer));
		HX_STACK_LINE(201)
		this->removeEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null());
		HX_STACK_LINE(205)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null());
		HX_STACK_LINE(208)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,saveAndExit,(void))

Void UpgradeMenu_obj::upgradeAggroRadius( ){
{
		HX_STACK_PUSH("UpgradeMenu::upgradeAggroRadius","UpgradeMenu.hx",188);
		HX_STACK_THIS(this);
		HX_STACK_LINE(189)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("gold")),::Math_obj::floor((Float(((int)50 * -(((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) - (int)165))))) / Float((int)15))));
		HX_STACK_LINE(190)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("aggroRadius")),(int)15);
		HX_STACK_LINE(191)
		this->localData->flush(null());
		HX_STACK_LINE(192)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,upgradeAggroRadius,(void))

Void UpgradeMenu_obj::upgradeCollectRadius( ){
{
		HX_STACK_PUSH("UpgradeMenu::upgradeCollectRadius","UpgradeMenu.hx",181);
		HX_STACK_THIS(this);
		HX_STACK_LINE(182)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("gold")),::Math_obj::floor(((Float(((int)50 * ((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) - (int)10)))) / Float((int)4)) + (int)50)));
		HX_STACK_LINE(183)
		hx::AddEq(this->localData->data->__FieldRef(HX_CSTRING("collectRadius")),(int)4);
		HX_STACK_LINE(184)
		this->localData->flush(null());
		HX_STACK_LINE(185)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,upgradeCollectRadius,(void))

Void UpgradeMenu_obj::upgradeSpeed( ){
{
		HX_STACK_PUSH("UpgradeMenu::upgradeSpeed","UpgradeMenu.hx",174);
		HX_STACK_THIS(this);
		HX_STACK_LINE(175)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("gold")),::Math_obj::floor(((int)100 * ((this->localData->data->__Field(HX_CSTRING("speed"),true) - (int)3)))));
		HX_STACK_LINE(176)
		(this->localData->data->__FieldRef(HX_CSTRING("speed")))++;
		HX_STACK_LINE(177)
		this->localData->flush(null());
		HX_STACK_LINE(178)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,upgradeSpeed,(void))

Void UpgradeMenu_obj::upgradeHealth( ){
{
		HX_STACK_PUSH("UpgradeMenu::upgradeHealth","UpgradeMenu.hx",166);
		HX_STACK_THIS(this);
		HX_STACK_LINE(167)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("gold")),::Math_obj::floor(((int)100 * this->localData->data->__Field(HX_CSTRING("health"),true))));
		HX_STACK_LINE(168)
		(this->localData->data->__FieldRef(HX_CSTRING("health")))++;
		HX_STACK_LINE(169)
		this->localData->flush(null());
		HX_STACK_LINE(170)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
		HX_STACK_LINE(171)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,upgradeHealth,(void))

Void UpgradeMenu_obj::refresh( ){
{
		HX_STACK_PUSH("UpgradeMenu::refresh","UpgradeMenu.hx",54);
		HX_STACK_THIS(this);
		HX_STACK_LINE(56)
		this->buttons = Array_obj< ::Button >::__new();
		HX_STACK_LINE(57)
		this->renderer->clear();
		HX_STACK_LINE(58)
		int cost;		HX_STACK_VAR(cost,"cost");
		HX_STACK_LINE(68)
		this->scale = ::Math_obj::max((Float(::TileRenderer_obj::stageWidth) / Float(::TileRenderer_obj::widths->__get((int)58))),(Float(::TileRenderer_obj::stageHeight) / Float(::TileRenderer_obj::heights->__get((int)58))));
		HX_STACK_LINE(69)
		this->buttons->push(::Button_obj::__new((Float(::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::TileRenderer_obj::stageHeight) / Float((int)2)),(int)58,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(73)
		this->scale = ::Math_obj::min((Float(::TileRenderer_obj::stageWidth) / Float(::TileRenderer_obj::widths->__get((int)30))),(Float(::TileRenderer_obj::stageHeight) / Float((((int)5 * ::TileRenderer_obj::heights->__get((int)30))))));
		HX_STACK_LINE(74)
		this->buttons->push(::Button_obj::__new((Float(::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::TileRenderer_obj::stageHeight) / Float((int)10)),(int)30,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(76)
		this->scale = (Float(::Math_obj::min((Float(::TileRenderer_obj::stageWidth) / Float(::TileRenderer_obj::widths->__get((int)15))),(Float(::TileRenderer_obj::stageHeight) / Float(::TileRenderer_obj::heights->__get((int)15))))) / Float(10.));
		HX_STACK_LINE(79)
		this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)3)) / Float((int)20)),(Float((::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)17,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(80)
		{
			HX_STACK_LINE(80)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = this->localData->data->__Field(HX_CSTRING("health"),true);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(80)
			while(((_g1 < _g))){
				HX_STACK_LINE(80)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(81)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)18,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(83)
		{
			HX_STACK_LINE(83)
			int _g = this->localData->data->__Field(HX_CSTRING("health"),true);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(83)
			while(((_g < (int)3))){
				HX_STACK_LINE(83)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(84)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)19,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(86)
		if (((this->localData->data->__Field(HX_CSTRING("health"),true) < (int)3))){
			HX_STACK_LINE(87)
			cost = ::Math_obj::floor(((int)100 * this->localData->data->__Field(HX_CSTRING("health"),true)));
			HX_STACK_LINE(88)
			this->scoreboard->renderNumber((Float((::TileRenderer_obj::stageWidth * (int)13)) / Float((int)20)),((Float((::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)) - (Float(::TileRenderer_obj::widths->__get((int)15)) / Float((int)4))),this->scale,cost);
			HX_STACK_LINE(89)
			if (((this->localData->data->__Field(HX_CSTRING("gold"),true) >= cost))){
				HX_STACK_LINE(89)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)15,this->scale,this->renderer,this->upgradeHealth_dyn()));
			}
			else{
				HX_STACK_LINE(91)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)16,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(97)
		this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)3)) / Float((int)20)),(Float((::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)20,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(98)
		{
			HX_STACK_LINE(98)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = ::Math_obj::floor((this->localData->data->__Field(HX_CSTRING("speed"),true) - (int)4));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(98)
			while(((_g1 < _g))){
				HX_STACK_LINE(98)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(99)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)21,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(101)
		{
			HX_STACK_LINE(101)
			int _g = ::Math_obj::floor((this->localData->data->__Field(HX_CSTRING("speed"),true) - (int)4));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(101)
			while(((_g < (int)3))){
				HX_STACK_LINE(101)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(102)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)22,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(104)
		if (((this->localData->data->__Field(HX_CSTRING("speed"),true) < (int)7))){
			HX_STACK_LINE(105)
			cost = ::Math_obj::floor(((int)100 * ((this->localData->data->__Field(HX_CSTRING("speed"),true) - (int)3))));
			HX_STACK_LINE(106)
			this->scoreboard->renderNumber((Float((::TileRenderer_obj::stageWidth * (int)13)) / Float((int)20)),(Float((::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),this->scale,cost);
			HX_STACK_LINE(107)
			if (((this->localData->data->__Field(HX_CSTRING("gold"),true) >= cost))){
				HX_STACK_LINE(107)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)15,this->scale,this->renderer,this->upgradeSpeed_dyn()));
			}
			else{
				HX_STACK_LINE(109)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)16,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(115)
		this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)3)) / Float((int)20)),(Float((::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)23,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(116)
		{
			HX_STACK_LINE(116)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = ::Math_obj::floor((Float(((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) - (int)10))) / Float((int)4)));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(116)
			while(((_g1 < _g))){
				HX_STACK_LINE(116)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(117)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)24,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(119)
		{
			HX_STACK_LINE(119)
			int _g = ::Math_obj::floor((Float(((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) - (int)10))) / Float((int)4)));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(119)
			while(((_g < (int)3))){
				HX_STACK_LINE(119)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(120)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)25,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(122)
		if (((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) < (int)19))){
			HX_STACK_LINE(123)
			cost = ::Math_obj::floor(((Float(((int)50 * ((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) - (int)10)))) / Float((int)4)) + (int)50));
			HX_STACK_LINE(124)
			this->scoreboard->renderNumber((Float((::TileRenderer_obj::stageWidth * (int)13)) / Float((int)20)),(Float((::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),this->scale,cost);
			HX_STACK_LINE(125)
			if (((this->localData->data->__Field(HX_CSTRING("gold"),true) >= cost))){
				HX_STACK_LINE(125)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)15,this->scale,this->renderer,this->upgradeCollectRadius_dyn()));
			}
			else{
				HX_STACK_LINE(127)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)16,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(133)
		this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)3)) / Float((int)20)),(Float((::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)26,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(134)
		{
			HX_STACK_LINE(134)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = ::Math_obj::floor((Float(-(((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) - (int)150)))) / Float((int)15)));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(134)
			while(((_g1 < _g))){
				HX_STACK_LINE(134)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(135)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)27,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(137)
		{
			HX_STACK_LINE(137)
			int _g = ::Math_obj::floor((Float(-(((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) - (int)150)))) / Float((int)15)));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(137)
			while(((_g < (int)3))){
				HX_STACK_LINE(137)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(138)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)28,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(140)
		if (((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) > (int)105))){
			HX_STACK_LINE(141)
			cost = ::Math_obj::floor((Float(((int)50 * -(((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) - (int)165))))) / Float((int)15)));
			HX_STACK_LINE(142)
			this->scoreboard->renderNumber((Float((::TileRenderer_obj::stageWidth * (int)13)) / Float((int)20)),(Float((::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),this->scale,cost);
			HX_STACK_LINE(143)
			if (((this->localData->data->__Field(HX_CSTRING("gold"),true) >= cost))){
				HX_STACK_LINE(143)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)15,this->scale,this->renderer,this->upgradeAggroRadius_dyn()));
			}
			else{
				HX_STACK_LINE(145)
				this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)16,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(150)
		this->scale = ::Math_obj::min((Float(::TileRenderer_obj::stageHeight) / Float((((int)5 * ::TileRenderer_obj::heights->__get((int)14))))),(Float(::TileRenderer_obj::stageWidth) / Float((((int)2 * ::TileRenderer_obj::widths->__get((int)52))))));
		HX_STACK_LINE(152)
		this->buttons->push(::Button_obj::__new((Float((::TileRenderer_obj::widths->__get((int)52) * this->scale)) / Float((int)2)),(Float((::TileRenderer_obj::stageHeight * (int)9)) / Float((int)10)),(int)52,this->scale,this->renderer,this->saveAndExit_dyn()));
		HX_STACK_LINE(153)
		this->buttons->push(::Button_obj::__new((::TileRenderer_obj::stageWidth - (Float((::TileRenderer_obj::widths->__get((int)14) * this->scale)) / Float((int)2))),(Float((::TileRenderer_obj::stageHeight * (int)9)) / Float((int)10)),(int)14,this->scale,this->renderer,this->nextLevel_dyn()));
		HX_STACK_LINE(156)
		this->scoreboard = ::Scoreboard_obj::__new(this->renderer);
		HX_STACK_LINE(157)
		this->scoreboard->update(this->localData->data->__Field(HX_CSTRING("gold"),true));
		HX_STACK_LINE(160)
		this->healthBar = ::HealthBar_obj::__new(this->renderer);
		HX_STACK_LINE(161)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
		HX_STACK_LINE(163)
		this->renderer->draw();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,refresh,(void))

Void UpgradeMenu_obj::onAddedToStage( ::native::events::Event event){
{
		HX_STACK_PUSH("UpgradeMenu::onAddedToStage","UpgradeMenu.hx",29);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(33)
		this->localData = ::native::net::SharedObject_obj::getLocal(HX_CSTRING("greed"),null(),null());
		HX_STACK_LINE(38)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null(),null(),null());
		HX_STACK_LINE(41)
		this->scale = (int)1;
		HX_STACK_LINE(44)
		this->scoreboard = ::Scoreboard_obj::__new(this->renderer);
		HX_STACK_LINE(45)
		this->scoreboard->update(this->localData->data->__Field(HX_CSTRING("gold"),true));
		HX_STACK_LINE(48)
		this->healthBar = ::HealthBar_obj::__new(this->renderer);
		HX_STACK_LINE(49)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
		HX_STACK_LINE(51)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(UpgradeMenu_obj,onAddedToStage,(void))


UpgradeMenu_obj::UpgradeMenu_obj()
{
}

void UpgradeMenu_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(UpgradeMenu);
	HX_MARK_MEMBER_NAME(scale,"scale");
	HX_MARK_MEMBER_NAME(healthBar,"healthBar");
	HX_MARK_MEMBER_NAME(scoreboard,"scoreboard");
	HX_MARK_MEMBER_NAME(localData,"localData");
	HX_MARK_MEMBER_NAME(buttons,"buttons");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	super::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void UpgradeMenu_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(scale,"scale");
	HX_VISIT_MEMBER_NAME(healthBar,"healthBar");
	HX_VISIT_MEMBER_NAME(scoreboard,"scoreboard");
	HX_VISIT_MEMBER_NAME(localData,"localData");
	HX_VISIT_MEMBER_NAME(buttons,"buttons");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	super::__Visit(HX_VISIT_ARG);
}

Dynamic UpgradeMenu_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { return scale; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"refresh") ) { return refresh_dyn(); }
		if (HX_FIELD_EQ(inName,"buttons") ) { return buttons; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"nextLevel") ) { return nextLevel_dyn(); }
		if (HX_FIELD_EQ(inName,"healthBar") ) { return healthBar; }
		if (HX_FIELD_EQ(inName,"localData") ) { return localData; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"scoreboard") ) { return scoreboard; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"saveAndExit") ) { return saveAndExit_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"onTouchBegin") ) { return onTouchBegin_dyn(); }
		if (HX_FIELD_EQ(inName,"upgradeSpeed") ) { return upgradeSpeed_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"dummyFunction") ) { return dummyFunction_dyn(); }
		if (HX_FIELD_EQ(inName,"upgradeHealth") ) { return upgradeHealth_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"onAddedToStage") ) { return onAddedToStage_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"upgradeAggroRadius") ) { return upgradeAggroRadius_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"upgradeCollectRadius") ) { return upgradeCollectRadius_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic UpgradeMenu_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { scale=inValue.Cast< Float >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"buttons") ) { buttons=inValue.Cast< Array< ::Button > >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::TileRenderer >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"healthBar") ) { healthBar=inValue.Cast< ::HealthBar >(); return inValue; }
		if (HX_FIELD_EQ(inName,"localData") ) { localData=inValue.Cast< ::native::net::SharedObject >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"scoreboard") ) { scoreboard=inValue.Cast< ::Scoreboard >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void UpgradeMenu_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("scale"));
	outFields->push(HX_CSTRING("healthBar"));
	outFields->push(HX_CSTRING("scoreboard"));
	outFields->push(HX_CSTRING("localData"));
	outFields->push(HX_CSTRING("buttons"));
	outFields->push(HX_CSTRING("renderer"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("dummyFunction"),
	HX_CSTRING("onTouchBegin"),
	HX_CSTRING("nextLevel"),
	HX_CSTRING("saveAndExit"),
	HX_CSTRING("upgradeAggroRadius"),
	HX_CSTRING("upgradeCollectRadius"),
	HX_CSTRING("upgradeSpeed"),
	HX_CSTRING("upgradeHealth"),
	HX_CSTRING("refresh"),
	HX_CSTRING("onAddedToStage"),
	HX_CSTRING("scale"),
	HX_CSTRING("healthBar"),
	HX_CSTRING("scoreboard"),
	HX_CSTRING("localData"),
	HX_CSTRING("buttons"),
	HX_CSTRING("renderer"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(UpgradeMenu_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(UpgradeMenu_obj::__mClass,"__mClass");
};

Class UpgradeMenu_obj::__mClass;

void UpgradeMenu_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("UpgradeMenu"), hx::TCanCast< UpgradeMenu_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void UpgradeMenu_obj::__boot()
{
}

