#include <hxcpp.h>

#ifndef INCLUDED_engine_objects_Collider
#include <engine/objects/Collider.h>
#endif
#ifndef INCLUDED_native_geom_Rectangle
#include <native/geom/Rectangle.h>
#endif
namespace engine{
namespace objects{

Void Collider_obj::__construct(Float x,Float y,Float width,Float height,int colliderType)
{
HX_STACK_PUSH("Collider::new","engine/objects/Collider.hx",21);
{
	HX_STACK_LINE(22)
	this->boundingBox = ::native::geom::Rectangle_obj::__new((x - (Float(width) / Float((int)2))),(y - (Float(height) / Float((int)2))),width,height);
	HX_STACK_LINE(24)
	this->type = colliderType;
}
;
	return null();
}

Collider_obj::~Collider_obj() { }

Dynamic Collider_obj::__CreateEmpty() { return  new Collider_obj; }
hx::ObjectPtr< Collider_obj > Collider_obj::__new(Float x,Float y,Float width,Float height,int colliderType)
{  hx::ObjectPtr< Collider_obj > result = new Collider_obj();
	result->__construct(x,y,width,height,colliderType);
	return result;}

Dynamic Collider_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Collider_obj > result = new Collider_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4]);
	return result;}

bool Collider_obj::collides( ::engine::objects::Collider otherCollider){
	HX_STACK_PUSH("Collider::collides","engine/objects/Collider.hx",43);
	HX_STACK_THIS(this);
	HX_STACK_ARG(otherCollider,"otherCollider");
	HX_STACK_LINE(43)
	return this->boundingBox->intersects(otherCollider->getBoundingBox());
}


HX_DEFINE_DYNAMIC_FUNC1(Collider_obj,collides,return )

Void Collider_obj::setY( Float y){
{
		HX_STACK_PUSH("Collider::setY","engine/objects/Collider.hx",39);
		HX_STACK_THIS(this);
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(39)
		this->boundingBox->y = (y - (Float(this->boundingBox->height) / Float((int)2)));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Collider_obj,setY,(void))

Void Collider_obj::setX( Float x){
{
		HX_STACK_PUSH("Collider::setX","engine/objects/Collider.hx",35);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_LINE(35)
		this->boundingBox->x = (x - (Float(this->boundingBox->width) / Float((int)2)));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Collider_obj,setX,(void))

int Collider_obj::getType( ){
	HX_STACK_PUSH("Collider::getType","engine/objects/Collider.hx",31);
	HX_STACK_THIS(this);
	HX_STACK_LINE(31)
	return this->type;
}


HX_DEFINE_DYNAMIC_FUNC0(Collider_obj,getType,return )

::native::geom::Rectangle Collider_obj::getBoundingBox( ){
	HX_STACK_PUSH("Collider::getBoundingBox","engine/objects/Collider.hx",27);
	HX_STACK_THIS(this);
	HX_STACK_LINE(27)
	return this->boundingBox;
}


HX_DEFINE_DYNAMIC_FUNC0(Collider_obj,getBoundingBox,return )

int Collider_obj::TYPE_PLAYER;

int Collider_obj::TYPE_ENEMY;

int Collider_obj::TYPE_COIN;

int Collider_obj::TYPE_WALL;

int Collider_obj::TYPE_END;

int Collider_obj::TYPE_PROJECTILE;


Collider_obj::Collider_obj()
{
}

void Collider_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Collider);
	HX_MARK_MEMBER_NAME(type,"type");
	HX_MARK_MEMBER_NAME(boundingBox,"boundingBox");
	HX_MARK_END_CLASS();
}

void Collider_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(type,"type");
	HX_VISIT_MEMBER_NAME(boundingBox,"boundingBox");
}

Dynamic Collider_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"setY") ) { return setY_dyn(); }
		if (HX_FIELD_EQ(inName,"setX") ) { return setX_dyn(); }
		if (HX_FIELD_EQ(inName,"type") ) { return type; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"getType") ) { return getType_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"TYPE_END") ) { return TYPE_END; }
		if (HX_FIELD_EQ(inName,"collides") ) { return collides_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"TYPE_COIN") ) { return TYPE_COIN; }
		if (HX_FIELD_EQ(inName,"TYPE_WALL") ) { return TYPE_WALL; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"TYPE_ENEMY") ) { return TYPE_ENEMY; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"TYPE_PLAYER") ) { return TYPE_PLAYER; }
		if (HX_FIELD_EQ(inName,"boundingBox") ) { return boundingBox; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"getBoundingBox") ) { return getBoundingBox_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"TYPE_PROJECTILE") ) { return TYPE_PROJECTILE; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Collider_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"type") ) { type=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"TYPE_END") ) { TYPE_END=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"TYPE_COIN") ) { TYPE_COIN=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"TYPE_WALL") ) { TYPE_WALL=inValue.Cast< int >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"TYPE_ENEMY") ) { TYPE_ENEMY=inValue.Cast< int >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"TYPE_PLAYER") ) { TYPE_PLAYER=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"boundingBox") ) { boundingBox=inValue.Cast< ::native::geom::Rectangle >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"TYPE_PROJECTILE") ) { TYPE_PROJECTILE=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Collider_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("type"));
	outFields->push(HX_CSTRING("boundingBox"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("TYPE_PLAYER"),
	HX_CSTRING("TYPE_ENEMY"),
	HX_CSTRING("TYPE_COIN"),
	HX_CSTRING("TYPE_WALL"),
	HX_CSTRING("TYPE_END"),
	HX_CSTRING("TYPE_PROJECTILE"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collides"),
	HX_CSTRING("setY"),
	HX_CSTRING("setX"),
	HX_CSTRING("getType"),
	HX_CSTRING("getBoundingBox"),
	HX_CSTRING("type"),
	HX_CSTRING("boundingBox"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Collider_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(Collider_obj::TYPE_PLAYER,"TYPE_PLAYER");
	HX_MARK_MEMBER_NAME(Collider_obj::TYPE_ENEMY,"TYPE_ENEMY");
	HX_MARK_MEMBER_NAME(Collider_obj::TYPE_COIN,"TYPE_COIN");
	HX_MARK_MEMBER_NAME(Collider_obj::TYPE_WALL,"TYPE_WALL");
	HX_MARK_MEMBER_NAME(Collider_obj::TYPE_END,"TYPE_END");
	HX_MARK_MEMBER_NAME(Collider_obj::TYPE_PROJECTILE,"TYPE_PROJECTILE");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Collider_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(Collider_obj::TYPE_PLAYER,"TYPE_PLAYER");
	HX_VISIT_MEMBER_NAME(Collider_obj::TYPE_ENEMY,"TYPE_ENEMY");
	HX_VISIT_MEMBER_NAME(Collider_obj::TYPE_COIN,"TYPE_COIN");
	HX_VISIT_MEMBER_NAME(Collider_obj::TYPE_WALL,"TYPE_WALL");
	HX_VISIT_MEMBER_NAME(Collider_obj::TYPE_END,"TYPE_END");
	HX_VISIT_MEMBER_NAME(Collider_obj::TYPE_PROJECTILE,"TYPE_PROJECTILE");
};

Class Collider_obj::__mClass;

void Collider_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.objects.Collider"), hx::TCanCast< Collider_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Collider_obj::__boot()
{
	TYPE_PLAYER= (int)0;
	TYPE_ENEMY= (int)1;
	TYPE_COIN= (int)2;
	TYPE_WALL= (int)3;
	TYPE_END= (int)4;
	TYPE_PROJECTILE= (int)5;
}

} // end namespace engine
} // end namespace objects
