#include <hxcpp.h>

#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_objects_HealthBar
#include <engine/objects/HealthBar.h>
#endif
namespace engine{
namespace objects{

Void HealthBar_obj::__construct(::engine::managers::TileRenderer tileRenderer)
{
HX_STACK_PUSH("HealthBar::new","engine/objects/HealthBar.hx",14);
{
	HX_STACK_LINE(15)
	this->renderer = tileRenderer;
	HX_STACK_LINE(17)
	this->spriteIDs = Array_obj< int >::__new();
}
;
	return null();
}

HealthBar_obj::~HealthBar_obj() { }

Dynamic HealthBar_obj::__CreateEmpty() { return  new HealthBar_obj; }
hx::ObjectPtr< HealthBar_obj > HealthBar_obj::__new(::engine::managers::TileRenderer tileRenderer)
{  hx::ObjectPtr< HealthBar_obj > result = new HealthBar_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic HealthBar_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< HealthBar_obj > result = new HealthBar_obj();
	result->__construct(inArgs[0]);
	return result;}

Void HealthBar_obj::update( int health){
{
		HX_STACK_PUSH("HealthBar::update","engine/objects/HealthBar.hx",20);
		HX_STACK_THIS(this);
		HX_STACK_ARG(health,"health");
		HX_STACK_LINE(21)
		int diff = (health - this->spriteIDs->length);		HX_STACK_VAR(diff,"diff");
		HX_STACK_LINE(23)
		if (((diff > (int)0))){
			HX_STACK_LINE(24)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(24)
			while(((_g < diff))){
				HX_STACK_LINE(24)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(25)
				this->spriteIDs->push(this->renderer->addTile((int)0,(int)0,(int)0,(int)0,(int)1,(int)2));
			}
		}
		else{
			HX_STACK_LINE(27)
			if (((diff < (int)0))){
				HX_STACK_LINE(28)
				{
					HX_STACK_LINE(28)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					int _g = -(diff);		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(28)
					while(((_g1 < _g))){
						HX_STACK_LINE(28)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(29)
						this->renderer->remove(this->spriteIDs->__get(i),(int)2);
					}
				}
				HX_STACK_LINE(31)
				this->spriteIDs->splice((int)0,-(diff));
			}
		}
		HX_STACK_LINE(35)
		{
			HX_STACK_LINE(35)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = this->spriteIDs->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(35)
			while(((_g1 < _g))){
				HX_STACK_LINE(35)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(36)
				this->renderer->updateObject(this->spriteIDs->__get(i),((int)40 * ((i + .5))),(int)20,(int)11,(int)0,(int)1,(int)2);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(HealthBar_obj,update,(void))


HealthBar_obj::HealthBar_obj()
{
}

void HealthBar_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(HealthBar);
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_MEMBER_NAME(spriteIDs,"spriteIDs");
	HX_MARK_END_CLASS();
}

void HealthBar_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	HX_VISIT_MEMBER_NAME(spriteIDs,"spriteIDs");
}

Dynamic HealthBar_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"spriteIDs") ) { return spriteIDs; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic HealthBar_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::engine::managers::TileRenderer >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"spriteIDs") ) { spriteIDs=inValue.Cast< Array< int > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void HealthBar_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("renderer"));
	outFields->push(HX_CSTRING("spriteIDs"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("update"),
	HX_CSTRING("renderer"),
	HX_CSTRING("spriteIDs"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(HealthBar_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(HealthBar_obj::__mClass,"__mClass");
};

Class HealthBar_obj::__mClass;

void HealthBar_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.objects.HealthBar"), hx::TCanCast< HealthBar_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void HealthBar_obj::__boot()
{
}

} // end namespace engine
} // end namespace objects
