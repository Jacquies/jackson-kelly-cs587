#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_objects_Scoreboard
#include <engine/objects/Scoreboard.h>
#endif
namespace engine{
namespace objects{

Void Scoreboard_obj::__construct(::engine::managers::TileRenderer tileRenderer)
{
HX_STACK_PUSH("Scoreboard::new","engine/objects/Scoreboard.hx",14);
{
	HX_STACK_LINE(15)
	this->renderer = tileRenderer;
	HX_STACK_LINE(17)
	this->spriteIDs = Array_obj< int >::__new();
}
;
	return null();
}

Scoreboard_obj::~Scoreboard_obj() { }

Dynamic Scoreboard_obj::__CreateEmpty() { return  new Scoreboard_obj; }
hx::ObjectPtr< Scoreboard_obj > Scoreboard_obj::__new(::engine::managers::TileRenderer tileRenderer)
{  hx::ObjectPtr< Scoreboard_obj > result = new Scoreboard_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic Scoreboard_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Scoreboard_obj > result = new Scoreboard_obj();
	result->__construct(inArgs[0]);
	return result;}

Void Scoreboard_obj::renderNumber( Float x,Float y,Float scale,int num){
{
		HX_STACK_PUSH("Scoreboard::renderNumber","engine/objects/Scoreboard.hx",45);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_ARG(y,"y");
		HX_STACK_ARG(scale,"scale");
		HX_STACK_ARG(num,"num");
		HX_STACK_LINE(46)
		int numDigits = ::Math_obj::floor(((Float(::Math_obj::log(num)) / Float(::Math_obj::log((int)10))) + (int)1));		HX_STACK_VAR(numDigits,"numDigits");
		HX_STACK_LINE(47)
		{
			HX_STACK_LINE(47)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(47)
			while(((_g < numDigits))){
				HX_STACK_LINE(47)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(48)
				this->renderer->addTile((x + ((int)28 * ((i + .5)))),(y + (int)16),::Math_obj::floor(hx::Mod((Float(num) / Float(::Math_obj::pow((int)10,((numDigits - i) - (int)1)))),(int)10)),(int)0,scale,(int)2);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC4(Scoreboard_obj,renderNumber,(void))

Void Scoreboard_obj::update( Float gold){
{
		HX_STACK_PUSH("Scoreboard::update","engine/objects/Scoreboard.hx",20);
		HX_STACK_THIS(this);
		HX_STACK_ARG(gold,"gold");
		HX_STACK_LINE(21)
		int diff = (::Math_obj::floor(((Float(::Math_obj::log(gold)) / Float(::Math_obj::log((int)10))) + (int)2)) - this->spriteIDs->length);		HX_STACK_VAR(diff,"diff");
		HX_STACK_LINE(24)
		if (((gold == (int)0))){
			HX_STACK_LINE(25)
			diff = ((int)2 - this->spriteIDs->length);
		}
		HX_STACK_LINE(27)
		if (((diff > (int)0))){
			HX_STACK_LINE(28)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(28)
			while(((_g < diff))){
				HX_STACK_LINE(28)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(29)
				this->spriteIDs->push(this->renderer->addTile((int)0,(int)0,(int)0,(int)0,(int)1,(int)2));
			}
		}
		else{
			HX_STACK_LINE(31)
			if (((diff < (int)0))){
				HX_STACK_LINE(32)
				{
					HX_STACK_LINE(32)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					int _g = -(diff);		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(32)
					while(((_g1 < _g))){
						HX_STACK_LINE(32)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(33)
						this->renderer->remove(this->spriteIDs->__get(i),(int)2);
					}
				}
				HX_STACK_LINE(35)
				this->spriteIDs->splice((int)0,-(diff));
			}
		}
		HX_STACK_LINE(39)
		{
			HX_STACK_LINE(39)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = (this->spriteIDs->length - (int)1);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(39)
			while(((_g1 < _g))){
				HX_STACK_LINE(39)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(40)
				this->renderer->updateObject(this->spriteIDs->__get(i),(::engine::managers::TileRenderer_obj::stageWidth - ((int)28 * ((i + .5)))),(int)20,::Math_obj::floor(hx::Mod((Float(gold) / Float(::Math_obj::pow((int)10,i))),(int)10)),(int)0,(int)1,(int)2);
			}
		}
		HX_STACK_LINE(42)
		this->renderer->updateObject(this->spriteIDs->__get((this->spriteIDs->length - (int)1)),(::engine::managers::TileRenderer_obj::stageWidth - ((int)28 * ((this->spriteIDs->length - .5)))),(int)20,(int)10,(int)0,(int)1,(int)2);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Scoreboard_obj,update,(void))


Scoreboard_obj::Scoreboard_obj()
{
}

void Scoreboard_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Scoreboard);
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_MEMBER_NAME(spriteIDs,"spriteIDs");
	HX_MARK_END_CLASS();
}

void Scoreboard_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	HX_VISIT_MEMBER_NAME(spriteIDs,"spriteIDs");
}

Dynamic Scoreboard_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"spriteIDs") ) { return spriteIDs; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"renderNumber") ) { return renderNumber_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Scoreboard_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::engine::managers::TileRenderer >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"spriteIDs") ) { spriteIDs=inValue.Cast< Array< int > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Scoreboard_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("renderer"));
	outFields->push(HX_CSTRING("spriteIDs"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("renderNumber"),
	HX_CSTRING("update"),
	HX_CSTRING("renderer"),
	HX_CSTRING("spriteIDs"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Scoreboard_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Scoreboard_obj::__mClass,"__mClass");
};

Class Scoreboard_obj::__mClass;

void Scoreboard_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.objects.Scoreboard"), hx::TCanCast< Scoreboard_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void Scoreboard_obj::__boot()
{
}

} // end namespace engine
} // end namespace objects
