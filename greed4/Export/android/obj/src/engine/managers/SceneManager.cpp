#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
#ifndef INCLUDED_engine_managers_SceneManager
#include <engine/managers/SceneManager.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_objects_Collider
#include <engine/objects/Collider.h>
#endif
#ifndef INCLUDED_engine_objects_HealthBar
#include <engine/objects/HealthBar.h>
#endif
#ifndef INCLUDED_engine_objects_Scoreboard
#include <engine/objects/Scoreboard.h>
#endif
#ifndef INCLUDED_game_level_Level
#include <game/level/Level.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
namespace engine{
namespace managers{

Void SceneManager_obj::__construct(::engine::managers::TileRenderer tileRenderer)
{
HX_STACK_PUSH("SceneManager::new","engine/managers/SceneManager.hx",34);
{
	HX_STACK_LINE(35)
	this->renderer = tileRenderer;
	HX_STACK_LINE(36)
	::engine::managers::SceneManager_obj::currentSceneManager = hx::ObjectPtr<OBJ_>(this);
	HX_STACK_LINE(37)
	::engine::managers::SceneManager_obj::usingFlash = (int)-1;
}
;
	return null();
}

SceneManager_obj::~SceneManager_obj() { }

Dynamic SceneManager_obj::__CreateEmpty() { return  new SceneManager_obj; }
hx::ObjectPtr< SceneManager_obj > SceneManager_obj::__new(::engine::managers::TileRenderer tileRenderer)
{  hx::ObjectPtr< SceneManager_obj > result = new SceneManager_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic SceneManager_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< SceneManager_obj > result = new SceneManager_obj();
	result->__construct(inArgs[0]);
	return result;}

Void SceneManager_obj::setLevelOver( bool isLevelOver){
{
		HX_STACK_PUSH("SceneManager::setLevelOver","engine/managers/SceneManager.hx",236);
		HX_STACK_THIS(this);
		HX_STACK_ARG(isLevelOver,"isLevelOver");
		HX_STACK_LINE(236)
		this->levelOver = isLevelOver;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setLevelOver,(void))

Void SceneManager_obj::setIdle( bool idleState){
{
		HX_STACK_PUSH("SceneManager::setIdle","engine/managers/SceneManager.hx",232);
		HX_STACK_THIS(this);
		HX_STACK_ARG(idleState,"idleState");
		HX_STACK_LINE(232)
		this->idle = idleState;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setIdle,(void))

bool SceneManager_obj::isIdle( ){
	HX_STACK_PUSH("SceneManager::isIdle","engine/managers/SceneManager.hx",228);
	HX_STACK_THIS(this);
	HX_STACK_LINE(228)
	return this->idle;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,isIdle,return )

::native::net::SharedObject SceneManager_obj::getSharedObject( ){
	HX_STACK_PUSH("SceneManager::getSharedObject","engine/managers/SceneManager.hx",224);
	HX_STACK_THIS(this);
	HX_STACK_LINE(224)
	return this->localData;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,getSharedObject,return )

Void SceneManager_obj::clearTickableSet( ){
{
		HX_STACK_PUSH("SceneManager::clearTickableSet","engine/managers/SceneManager.hx",220);
		HX_STACK_THIS(this);
		HX_STACK_LINE(220)
		this->tickableSet->slice((int)0,this->tickableSet->length);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,clearTickableSet,(void))

Void SceneManager_obj::removeFromTickableSet( ::engine::interfaces::ITickable tickable){
{
		HX_STACK_PUSH("SceneManager::removeFromTickableSet","engine/managers/SceneManager.hx",216);
		HX_STACK_THIS(this);
		HX_STACK_ARG(tickable,"tickable");
		HX_STACK_LINE(216)
		this->tickableSet->remove(tickable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,removeFromTickableSet,(void))

Void SceneManager_obj::addToTickableSet( ::engine::interfaces::ITickable tickable){
{
		HX_STACK_PUSH("SceneManager::addToTickableSet","engine/managers/SceneManager.hx",212);
		HX_STACK_THIS(this);
		HX_STACK_ARG(tickable,"tickable");
		HX_STACK_LINE(212)
		this->tickableSet->push(tickable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,addToTickableSet,(void))

Void SceneManager_obj::clearRenderSet( ){
{
		HX_STACK_PUSH("SceneManager::clearRenderSet","engine/managers/SceneManager.hx",208);
		HX_STACK_THIS(this);
		HX_STACK_LINE(208)
		this->renderSet->slice((int)0,this->renderSet->length);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,clearRenderSet,(void))

Void SceneManager_obj::removeFromRenderSet( ::engine::interfaces::IRenderable renderable){
{
		HX_STACK_PUSH("SceneManager::removeFromRenderSet","engine/managers/SceneManager.hx",204);
		HX_STACK_THIS(this);
		HX_STACK_ARG(renderable,"renderable");
		HX_STACK_LINE(204)
		this->renderSet->remove(renderable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,removeFromRenderSet,(void))

Void SceneManager_obj::addToRenderSet( ::engine::interfaces::IRenderable renderable){
{
		HX_STACK_PUSH("SceneManager::addToRenderSet","engine/managers/SceneManager.hx",200);
		HX_STACK_THIS(this);
		HX_STACK_ARG(renderable,"renderable");
		HX_STACK_LINE(200)
		this->renderSet->push(renderable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,addToRenderSet,(void))

Void SceneManager_obj::clearCollisionSet( ){
{
		HX_STACK_PUSH("SceneManager::clearCollisionSet","engine/managers/SceneManager.hx",196);
		HX_STACK_THIS(this);
		HX_STACK_LINE(196)
		this->collisionSet->slice((int)0,this->collisionSet->length);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,clearCollisionSet,(void))

Void SceneManager_obj::removeFromCollisionSet( ::engine::interfaces::ICollidable collidable){
{
		HX_STACK_PUSH("SceneManager::removeFromCollisionSet","engine/managers/SceneManager.hx",192);
		HX_STACK_THIS(this);
		HX_STACK_ARG(collidable,"collidable");
		HX_STACK_LINE(192)
		this->collisionSet->remove(collidable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,removeFromCollisionSet,(void))

Void SceneManager_obj::addToCollisionSet( ::engine::interfaces::ICollidable collidable){
{
		HX_STACK_PUSH("SceneManager::addToCollisionSet","engine/managers/SceneManager.hx",188);
		HX_STACK_THIS(this);
		HX_STACK_ARG(collidable,"collidable");
		HX_STACK_LINE(188)
		this->collisionSet->push(collidable);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,addToCollisionSet,(void))

Void SceneManager_obj::setCameraY( Float y){
{
		HX_STACK_PUSH("SceneManager::setCameraY","engine/managers/SceneManager.hx",184);
		HX_STACK_THIS(this);
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(184)
		this->camera->y = y;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setCameraY,(void))

Void SceneManager_obj::setCameraX( Float x){
{
		HX_STACK_PUSH("SceneManager::setCameraX","engine/managers/SceneManager.hx",180);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_LINE(180)
		this->camera->x = x;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setCameraX,(void))

::native::geom::Point SceneManager_obj::getCameraPosition( ){
	HX_STACK_PUSH("SceneManager::getCameraPosition","engine/managers/SceneManager.hx",176);
	HX_STACK_THIS(this);
	HX_STACK_LINE(176)
	return this->camera;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,getCameraPosition,return )

::native::geom::Point SceneManager_obj::getMousePosition( ){
	HX_STACK_PUSH("SceneManager::getMousePosition","engine/managers/SceneManager.hx",172);
	HX_STACK_THIS(this);
	HX_STACK_LINE(172)
	return this->mousePosition;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,getMousePosition,return )

Void SceneManager_obj::setMousePosition( Float x,Float y){
{
		HX_STACK_PUSH("SceneManager::setMousePosition","engine/managers/SceneManager.hx",167);
		HX_STACK_THIS(this);
		HX_STACK_ARG(x,"x");
		HX_STACK_ARG(y,"y");
		HX_STACK_LINE(168)
		this->mousePosition->x = x;
		HX_STACK_LINE(169)
		this->mousePosition->y = y;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(SceneManager_obj,setMousePosition,(void))

bool SceneManager_obj::getMouseDown( ){
	HX_STACK_PUSH("SceneManager::getMouseDown","engine/managers/SceneManager.hx",163);
	HX_STACK_THIS(this);
	HX_STACK_LINE(163)
	return this->mouseDown;
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,getMouseDown,return )

Void SceneManager_obj::setMouseDown( bool isMouseDown){
{
		HX_STACK_PUSH("SceneManager::setMouseDown","engine/managers/SceneManager.hx",159);
		HX_STACK_THIS(this);
		HX_STACK_ARG(isMouseDown,"isMouseDown");
		HX_STACK_LINE(159)
		this->mouseDown = isMouseDown;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,setMouseDown,(void))

Void SceneManager_obj::updateHealthBar( ){
{
		HX_STACK_PUSH("SceneManager::updateHealthBar","engine/managers/SceneManager.hx",155);
		HX_STACK_THIS(this);
		HX_STACK_LINE(155)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,updateHealthBar,(void))

Void SceneManager_obj::updateScoreboard( ){
{
		HX_STACK_PUSH("SceneManager::updateScoreboard","engine/managers/SceneManager.hx",151);
		HX_STACK_THIS(this);
		HX_STACK_LINE(151)
		this->scoreboard->update(this->localData->data->__Field(HX_CSTRING("gold"),true));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,updateScoreboard,(void))

Void SceneManager_obj::tick( int deltaTime){
{
		HX_STACK_PUSH("SceneManager::tick","engine/managers/SceneManager.hx",97);
		HX_STACK_THIS(this);
		HX_STACK_ARG(deltaTime,"deltaTime");
		HX_STACK_LINE(98)
		if ((this->levelOver)){
			HX_STACK_LINE(99)
			return null();
		}
		HX_STACK_LINE(101)
		if ((!(this->idle))){
			HX_STACK_LINE(102)
			{
				HX_STACK_LINE(102)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				int _g = this->collisionSet->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(102)
				while(((_g1 < _g))){
					HX_STACK_LINE(102)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(103)
					{
						HX_STACK_LINE(103)
						int _g3 = (i + (int)1);		HX_STACK_VAR(_g3,"_g3");
						int _g2 = this->collisionSet->length;		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(103)
						while(((_g3 < _g2))){
							HX_STACK_LINE(103)
							int j = (_g3)++;		HX_STACK_VAR(j,"j");
							HX_STACK_LINE(104)
							::engine::managers::SceneManager_obj::collide(this->collisionSet->__get(i),this->collisionSet->__get(j));
						}
					}
				}
			}
			HX_STACK_LINE(108)
			{
				HX_STACK_LINE(108)
				int _g = (int)0;		HX_STACK_VAR(_g,"_g");
				Array< ::engine::interfaces::ITickable > _g1 = this->tickableSet;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(108)
				while(((_g < _g1->length))){
					HX_STACK_LINE(108)
					::engine::interfaces::ITickable tickable = _g1->__get(_g);		HX_STACK_VAR(tickable,"tickable");
					HX_STACK_LINE(108)
					++(_g);
					HX_STACK_LINE(109)
					tickable->tick(deltaTime);
				}
			}
		}
		HX_STACK_LINE(113)
		{
			HX_STACK_LINE(113)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			Array< ::engine::interfaces::IRenderable > _g1 = this->renderSet;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(113)
			while(((_g < _g1->length))){
				HX_STACK_LINE(113)
				::engine::interfaces::IRenderable renderable = _g1->__get(_g);		HX_STACK_VAR(renderable,"renderable");
				HX_STACK_LINE(113)
				++(_g);
				HX_STACK_LINE(114)
				renderable->render(deltaTime,this->camera);
			}
		}
		HX_STACK_LINE(117)
		this->renderer->draw();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SceneManager_obj,tick,(void))

Void SceneManager_obj::initialize( ){
{
		HX_STACK_PUSH("SceneManager::initialize","engine/managers/SceneManager.hx",43);
		HX_STACK_THIS(this);
		HX_STACK_LINE(47)
		if (((bool((::engine::managers::TileRenderer_obj::stageHeight > (int)350)) && bool((::engine::managers::TileRenderer_obj::stageWidth > (int)350))))){
			HX_STACK_LINE(48)
			::engine::managers::SceneManager_obj::scale = (int)1;
			HX_STACK_LINE(49)
			if (((::engine::managers::TileRenderer_obj::DPI > (int)200))){
				HX_STACK_LINE(50)
				::engine::managers::SceneManager_obj::scale = (Float(::engine::managers::TileRenderer_obj::DPI) / Float((int)200));
			}
		}
		else{
			HX_STACK_LINE(51)
			::engine::managers::SceneManager_obj::scale = (Float(::Math_obj::min(::engine::managers::TileRenderer_obj::stageHeight,::engine::managers::TileRenderer_obj::stageWidth)) / Float((int)350));
		}
		HX_STACK_LINE(56)
		this->idle = true;
		HX_STACK_LINE(57)
		this->mouseDown = false;
		HX_STACK_LINE(58)
		this->camera = ::native::geom::Point_obj::__new((int)0,(int)0);
		HX_STACK_LINE(59)
		this->mousePosition = ::native::geom::Point_obj::__new((int)0,(int)0);
		HX_STACK_LINE(60)
		this->levelOver = false;
		HX_STACK_LINE(63)
		this->collisionSet = Array_obj< ::engine::interfaces::ICollidable >::__new();
		HX_STACK_LINE(64)
		this->renderSet = Array_obj< ::engine::interfaces::IRenderable >::__new();
		HX_STACK_LINE(65)
		this->tickableSet = Array_obj< ::engine::interfaces::ITickable >::__new();
		HX_STACK_LINE(68)
		this->localData = ::native::net::SharedObject_obj::getLocal(HX_CSTRING("greed"),null(),null());
		HX_STACK_LINE(69)
		if ((!(this->localData->data->__Field(HX_CSTRING("initialized"),true)))){
			HX_STACK_LINE(69)
			::game::level::Level_obj::initializeLocalData(this->localData);
		}
		else{
			HX_STACK_LINE(71)
			if ((this->localData->data->__Field(HX_CSTRING("newGame"),true))){
				HX_STACK_LINE(71)
				::game::level::Level_obj::newGame(this->localData);
			}
		}
		HX_STACK_LINE(76)
		this->scoreboard = ::engine::objects::Scoreboard_obj::__new(this->renderer);
		HX_STACK_LINE(77)
		this->scoreboard->update(this->localData->data->__Field(HX_CSTRING("gold"),true));
		HX_STACK_LINE(80)
		this->healthBar = ::engine::objects::HealthBar_obj::__new(this->renderer);
		HX_STACK_LINE(81)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
		HX_STACK_LINE(94)
		this->idle = false;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SceneManager_obj,initialize,(void))

Float SceneManager_obj::scale;

int SceneManager_obj::usingFlash;

::engine::managers::SceneManager SceneManager_obj::currentSceneManager;

Void SceneManager_obj::collide( ::engine::interfaces::ICollidable collidable1,::engine::interfaces::ICollidable collidable2){
{
		HX_STACK_PUSH("SceneManager::collide","engine/managers/SceneManager.hx",240);
		HX_STACK_ARG(collidable1,"collidable1");
		HX_STACK_ARG(collidable2,"collidable2");
		HX_STACK_LINE(240)
		if ((collidable1->getCollider()->collides(collidable2->getCollider()))){
			HX_STACK_LINE(242)
			collidable1->collide(collidable2);
			HX_STACK_LINE(243)
			collidable2->collide(collidable1);
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(SceneManager_obj,collide,(void))

Float SceneManager_obj::distance( ::native::geom::Point point1,::native::geom::Point point2){
	HX_STACK_PUSH("SceneManager::distance","engine/managers/SceneManager.hx",247);
	HX_STACK_ARG(point1,"point1");
	HX_STACK_ARG(point2,"point2");
	HX_STACK_LINE(247)
	return ::Math_obj::sqrt(((((point1->x - point2->x)) * ((point1->x - point2->x))) + (((point1->y - point2->y)) * ((point1->y - point2->y)))));
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(SceneManager_obj,distance,return )

Float SceneManager_obj::squareDistance( ::native::geom::Point point1,::native::geom::Point point2){
	HX_STACK_PUSH("SceneManager::squareDistance","engine/managers/SceneManager.hx",251);
	HX_STACK_ARG(point1,"point1");
	HX_STACK_ARG(point2,"point2");
	HX_STACK_LINE(251)
	return ((((point1->x - point2->x)) * ((point1->x - point2->x))) + (((point1->y - point2->y)) * ((point1->y - point2->y))));
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(SceneManager_obj,squareDistance,return )


SceneManager_obj::SceneManager_obj()
{
}

void SceneManager_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(SceneManager);
	HX_MARK_MEMBER_NAME(levelOver,"levelOver");
	HX_MARK_MEMBER_NAME(healthBar,"healthBar");
	HX_MARK_MEMBER_NAME(scoreboard,"scoreboard");
	HX_MARK_MEMBER_NAME(idle,"idle");
	HX_MARK_MEMBER_NAME(localData,"localData");
	HX_MARK_MEMBER_NAME(mousePosition,"mousePosition");
	HX_MARK_MEMBER_NAME(camera,"camera");
	HX_MARK_MEMBER_NAME(mouseDown,"mouseDown");
	HX_MARK_MEMBER_NAME(tickableSet,"tickableSet");
	HX_MARK_MEMBER_NAME(renderSet,"renderSet");
	HX_MARK_MEMBER_NAME(collisionSet,"collisionSet");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	HX_MARK_END_CLASS();
}

void SceneManager_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(levelOver,"levelOver");
	HX_VISIT_MEMBER_NAME(healthBar,"healthBar");
	HX_VISIT_MEMBER_NAME(scoreboard,"scoreboard");
	HX_VISIT_MEMBER_NAME(idle,"idle");
	HX_VISIT_MEMBER_NAME(localData,"localData");
	HX_VISIT_MEMBER_NAME(mousePosition,"mousePosition");
	HX_VISIT_MEMBER_NAME(camera,"camera");
	HX_VISIT_MEMBER_NAME(mouseDown,"mouseDown");
	HX_VISIT_MEMBER_NAME(tickableSet,"tickableSet");
	HX_VISIT_MEMBER_NAME(renderSet,"renderSet");
	HX_VISIT_MEMBER_NAME(collisionSet,"collisionSet");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
}

Dynamic SceneManager_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tick") ) { return tick_dyn(); }
		if (HX_FIELD_EQ(inName,"idle") ) { return idle; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { return scale; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"isIdle") ) { return isIdle_dyn(); }
		if (HX_FIELD_EQ(inName,"camera") ) { return camera; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
		if (HX_FIELD_EQ(inName,"setIdle") ) { return setIdle_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"distance") ) { return distance_dyn(); }
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"levelOver") ) { return levelOver; }
		if (HX_FIELD_EQ(inName,"healthBar") ) { return healthBar; }
		if (HX_FIELD_EQ(inName,"localData") ) { return localData; }
		if (HX_FIELD_EQ(inName,"mouseDown") ) { return mouseDown; }
		if (HX_FIELD_EQ(inName,"renderSet") ) { return renderSet; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"usingFlash") ) { return usingFlash; }
		if (HX_FIELD_EQ(inName,"setCameraY") ) { return setCameraY_dyn(); }
		if (HX_FIELD_EQ(inName,"setCameraX") ) { return setCameraX_dyn(); }
		if (HX_FIELD_EQ(inName,"initialize") ) { return initialize_dyn(); }
		if (HX_FIELD_EQ(inName,"scoreboard") ) { return scoreboard; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"tickableSet") ) { return tickableSet; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"setLevelOver") ) { return setLevelOver_dyn(); }
		if (HX_FIELD_EQ(inName,"getMouseDown") ) { return getMouseDown_dyn(); }
		if (HX_FIELD_EQ(inName,"setMouseDown") ) { return setMouseDown_dyn(); }
		if (HX_FIELD_EQ(inName,"collisionSet") ) { return collisionSet; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"mousePosition") ) { return mousePosition; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"squareDistance") ) { return squareDistance_dyn(); }
		if (HX_FIELD_EQ(inName,"clearRenderSet") ) { return clearRenderSet_dyn(); }
		if (HX_FIELD_EQ(inName,"addToRenderSet") ) { return addToRenderSet_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"getSharedObject") ) { return getSharedObject_dyn(); }
		if (HX_FIELD_EQ(inName,"updateHealthBar") ) { return updateHealthBar_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"clearTickableSet") ) { return clearTickableSet_dyn(); }
		if (HX_FIELD_EQ(inName,"addToTickableSet") ) { return addToTickableSet_dyn(); }
		if (HX_FIELD_EQ(inName,"getMousePosition") ) { return getMousePosition_dyn(); }
		if (HX_FIELD_EQ(inName,"setMousePosition") ) { return setMousePosition_dyn(); }
		if (HX_FIELD_EQ(inName,"updateScoreboard") ) { return updateScoreboard_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"clearCollisionSet") ) { return clearCollisionSet_dyn(); }
		if (HX_FIELD_EQ(inName,"addToCollisionSet") ) { return addToCollisionSet_dyn(); }
		if (HX_FIELD_EQ(inName,"getCameraPosition") ) { return getCameraPosition_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"currentSceneManager") ) { return currentSceneManager; }
		if (HX_FIELD_EQ(inName,"removeFromRenderSet") ) { return removeFromRenderSet_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"removeFromTickableSet") ) { return removeFromTickableSet_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"removeFromCollisionSet") ) { return removeFromCollisionSet_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic SceneManager_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"idle") ) { idle=inValue.Cast< bool >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { scale=inValue.Cast< Float >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"camera") ) { camera=inValue.Cast< ::native::geom::Point >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::engine::managers::TileRenderer >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"levelOver") ) { levelOver=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"healthBar") ) { healthBar=inValue.Cast< ::engine::objects::HealthBar >(); return inValue; }
		if (HX_FIELD_EQ(inName,"localData") ) { localData=inValue.Cast< ::native::net::SharedObject >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouseDown") ) { mouseDown=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"renderSet") ) { renderSet=inValue.Cast< Array< ::engine::interfaces::IRenderable > >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"usingFlash") ) { usingFlash=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"scoreboard") ) { scoreboard=inValue.Cast< ::engine::objects::Scoreboard >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"tickableSet") ) { tickableSet=inValue.Cast< Array< ::engine::interfaces::ITickable > >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"collisionSet") ) { collisionSet=inValue.Cast< Array< ::engine::interfaces::ICollidable > >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"mousePosition") ) { mousePosition=inValue.Cast< ::native::geom::Point >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"currentSceneManager") ) { currentSceneManager=inValue.Cast< ::engine::managers::SceneManager >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void SceneManager_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("levelOver"));
	outFields->push(HX_CSTRING("healthBar"));
	outFields->push(HX_CSTRING("scoreboard"));
	outFields->push(HX_CSTRING("idle"));
	outFields->push(HX_CSTRING("localData"));
	outFields->push(HX_CSTRING("mousePosition"));
	outFields->push(HX_CSTRING("camera"));
	outFields->push(HX_CSTRING("mouseDown"));
	outFields->push(HX_CSTRING("tickableSet"));
	outFields->push(HX_CSTRING("renderSet"));
	outFields->push(HX_CSTRING("collisionSet"));
	outFields->push(HX_CSTRING("renderer"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("scale"),
	HX_CSTRING("usingFlash"),
	HX_CSTRING("currentSceneManager"),
	HX_CSTRING("collide"),
	HX_CSTRING("distance"),
	HX_CSTRING("squareDistance"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("setLevelOver"),
	HX_CSTRING("setIdle"),
	HX_CSTRING("isIdle"),
	HX_CSTRING("getSharedObject"),
	HX_CSTRING("clearTickableSet"),
	HX_CSTRING("removeFromTickableSet"),
	HX_CSTRING("addToTickableSet"),
	HX_CSTRING("clearRenderSet"),
	HX_CSTRING("removeFromRenderSet"),
	HX_CSTRING("addToRenderSet"),
	HX_CSTRING("clearCollisionSet"),
	HX_CSTRING("removeFromCollisionSet"),
	HX_CSTRING("addToCollisionSet"),
	HX_CSTRING("setCameraY"),
	HX_CSTRING("setCameraX"),
	HX_CSTRING("getCameraPosition"),
	HX_CSTRING("getMousePosition"),
	HX_CSTRING("setMousePosition"),
	HX_CSTRING("getMouseDown"),
	HX_CSTRING("setMouseDown"),
	HX_CSTRING("updateHealthBar"),
	HX_CSTRING("updateScoreboard"),
	HX_CSTRING("tick"),
	HX_CSTRING("initialize"),
	HX_CSTRING("levelOver"),
	HX_CSTRING("healthBar"),
	HX_CSTRING("scoreboard"),
	HX_CSTRING("idle"),
	HX_CSTRING("localData"),
	HX_CSTRING("mousePosition"),
	HX_CSTRING("camera"),
	HX_CSTRING("mouseDown"),
	HX_CSTRING("tickableSet"),
	HX_CSTRING("renderSet"),
	HX_CSTRING("collisionSet"),
	HX_CSTRING("renderer"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(SceneManager_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(SceneManager_obj::scale,"scale");
	HX_MARK_MEMBER_NAME(SceneManager_obj::usingFlash,"usingFlash");
	HX_MARK_MEMBER_NAME(SceneManager_obj::currentSceneManager,"currentSceneManager");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(SceneManager_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(SceneManager_obj::scale,"scale");
	HX_VISIT_MEMBER_NAME(SceneManager_obj::usingFlash,"usingFlash");
	HX_VISIT_MEMBER_NAME(SceneManager_obj::currentSceneManager,"currentSceneManager");
};

Class SceneManager_obj::__mClass;

void SceneManager_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.managers.SceneManager"), hx::TCanCast< SceneManager_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void SceneManager_obj::__boot()
{
}

} // end namespace engine
} // end namespace managers
