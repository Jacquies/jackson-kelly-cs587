#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_Xml
#include <Xml.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_native_display_BitmapData
#include <native/display/BitmapData.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_Graphics
#include <native/display/Graphics.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Stage
#include <native/display/Stage.h>
#endif
#ifndef INCLUDED_native_display_Tilesheet
#include <native/display/Tilesheet.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_geom_Rectangle
#include <native/geom/Rectangle.h>
#endif
#ifndef INCLUDED_native_system_Capabilities
#include <native/system/Capabilities.h>
#endif
#ifndef INCLUDED_nme_installer_Assets
#include <nme/installer/Assets.h>
#endif
namespace engine{
namespace managers{

Void TileRenderer_obj::__construct(::native::display::Graphics inGraphics,::native::display::Stage displayStage)
{
HX_STACK_PUSH("TileRenderer::new","engine/managers/TileRenderer.hx",31);
{
	HX_STACK_LINE(32)
	this->tilesheet = ::native::display::Tilesheet_obj::__new(::nme::installer::Assets_obj::getBitmapData(HX_CSTRING("assets/sheet.png"),null()));
	HX_STACK_LINE(34)
	this->tileData = Array_obj< Array< Float > >::__new();
	HX_STACK_LINE(35)
	this->freeIndices = Array_obj< Array< int > >::__new();
	HX_STACK_LINE(37)
	{
		HX_STACK_LINE(37)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(37)
		while(((_g < (int)3))){
			HX_STACK_LINE(37)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(38)
			this->tileData->push(Array_obj< Float >::__new());
			HX_STACK_LINE(39)
			this->freeIndices->push(Array_obj< int >::__new());
		}
	}
	HX_STACK_LINE(42)
	this->numTiles = (int)0;
	HX_STACK_LINE(43)
	this->graphics = inGraphics;
	HX_STACK_LINE(45)
	::engine::managers::TileRenderer_obj::stageWidth = displayStage->get_stageWidth();
	HX_STACK_LINE(46)
	::engine::managers::TileRenderer_obj::stageHeight = displayStage->get_stageHeight();
	HX_STACK_LINE(48)
	::engine::managers::TileRenderer_obj::widths = Array_obj< Float >::__new();
	HX_STACK_LINE(49)
	::engine::managers::TileRenderer_obj::heights = Array_obj< Float >::__new();
	HX_STACK_LINE(51)
	::engine::managers::TileRenderer_obj::DPI = ::native::system::Capabilities_obj::get_screenDPI();
	HX_STACK_LINE(53)
	this->loadAllTiles();
}
;
	return null();
}

TileRenderer_obj::~TileRenderer_obj() { }

Dynamic TileRenderer_obj::__CreateEmpty() { return  new TileRenderer_obj; }
hx::ObjectPtr< TileRenderer_obj > TileRenderer_obj::__new(::native::display::Graphics inGraphics,::native::display::Stage displayStage)
{  hx::ObjectPtr< TileRenderer_obj > result = new TileRenderer_obj();
	result->__construct(inGraphics,displayStage);
	return result;}

Dynamic TileRenderer_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< TileRenderer_obj > result = new TileRenderer_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

Void TileRenderer_obj::loadAllTiles( ){
{
		HX_STACK_PUSH("TileRenderer::loadAllTiles","engine/managers/TileRenderer.hx",145);
		HX_STACK_THIS(this);
		HX_STACK_LINE(146)
		::Xml xml = ::Xml_obj::parse(::nme::installer::Assets_obj::getText(HX_CSTRING("assets/sheet.xml")));		HX_STACK_VAR(xml,"xml");
		HX_STACK_LINE(147)
		xml = xml->firstChild();
		HX_STACK_LINE(148)
		Dynamic it = xml->elements();		HX_STACK_VAR(it,"it");
		HX_STACK_LINE(150)
		while((it->__Field(HX_CSTRING("hasNext"),true)())){
			HX_STACK_LINE(151)
			xml = it->__Field(HX_CSTRING("next"),true)();
			HX_STACK_LINE(152)
			this->loadTile(::Std_obj::parseFloat(xml->get(HX_CSTRING("x"))),::Std_obj::parseFloat(xml->get(HX_CSTRING("y"))),::Std_obj::parseFloat(xml->get(HX_CSTRING("width"))),::Std_obj::parseFloat(xml->get(HX_CSTRING("height"))),(Float(::Std_obj::parseFloat(xml->get(HX_CSTRING("width")))) / Float((int)2)),(Float(::Std_obj::parseFloat(xml->get(HX_CSTRING("height")))) / Float((int)2)));
			HX_STACK_LINE(153)
			::engine::managers::TileRenderer_obj::widths->push(::Std_obj::parseFloat(xml->get(HX_CSTRING("width"))));
			HX_STACK_LINE(154)
			::engine::managers::TileRenderer_obj::heights->push(::Std_obj::parseFloat(xml->get(HX_CSTRING("height"))));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(TileRenderer_obj,loadAllTiles,(void))

Void TileRenderer_obj::clear( ){
{
		HX_STACK_PUSH("TileRenderer::clear","engine/managers/TileRenderer.hx",135);
		HX_STACK_THIS(this);
		HX_STACK_LINE(136)
		this->graphics->clear();
		HX_STACK_LINE(138)
		{
			HX_STACK_LINE(138)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(138)
			while(((_g < (int)3))){
				HX_STACK_LINE(138)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(139)
				this->tileData->__get(i)->splice((int)0,this->tileData->__get(i)->length);
				HX_STACK_LINE(140)
				this->freeIndices->__get(i)->splice((int)0,this->freeIndices->__get(i)->length);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(TileRenderer_obj,clear,(void))

Void TileRenderer_obj::draw( ){
{
		HX_STACK_PUSH("TileRenderer::draw","engine/managers/TileRenderer.hx",123);
		HX_STACK_THIS(this);
		HX_STACK_LINE(124)
		this->graphics->clear();
		HX_STACK_LINE(126)
		{
			HX_STACK_LINE(126)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(126)
			while(((_g < (int)3))){
				HX_STACK_LINE(126)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(130)
				this->tilesheet->drawTiles(this->graphics,this->tileData->__get(i),false,(int)3);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(TileRenderer_obj,draw,(void))

Void TileRenderer_obj::updateObject( int ID,Float x,Float y,int SpriteID,Float rotation,Float scale,int layer){
{
		HX_STACK_PUSH("TileRenderer::updateObject","engine/managers/TileRenderer.hx",107);
		HX_STACK_THIS(this);
		HX_STACK_ARG(ID,"ID");
		HX_STACK_ARG(x,"x");
		HX_STACK_ARG(y,"y");
		HX_STACK_ARG(SpriteID,"SpriteID");
		HX_STACK_ARG(rotation,"rotation");
		HX_STACK_ARG(scale,"scale");
		HX_STACK_ARG(layer,"layer");
		HX_STACK_LINE(111)
		int index = (this->freeIndices->__get(layer)->__get(ID) * (int)5);		HX_STACK_VAR(index,"index");
		HX_STACK_LINE(113)
		this->tileData->__get(layer)[index] = x;
		HX_STACK_LINE(114)
		this->tileData->__get(layer)[(index + (int)1)] = y;
		HX_STACK_LINE(115)
		this->tileData->__get(layer)[(index + (int)2)] = SpriteID;
		HX_STACK_LINE(118)
		this->tileData->__get(layer)[(index + (int)3)] = scale;
		HX_STACK_LINE(119)
		this->tileData->__get(layer)[(index + (int)4)] = (Float((rotation * ::Math_obj::PI)) / Float((int)180));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC7(TileRenderer_obj,updateObject,(void))

Void TileRenderer_obj::remove( int ID,int layer){
{
		HX_STACK_PUSH("TileRenderer::remove","engine/managers/TileRenderer.hx",91);
		HX_STACK_THIS(this);
		HX_STACK_ARG(ID,"ID");
		HX_STACK_ARG(layer,"layer");
		HX_STACK_LINE(95)
		this->tileData->__get(layer)->splice(((int)4 * this->freeIndices->__get(layer)->__get(ID)),(int)5);
		HX_STACK_LINE(98)
		{
			HX_STACK_LINE(98)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = this->freeIndices->__get(layer)->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(98)
			while(((_g1 < _g))){
				HX_STACK_LINE(98)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(99)
				if (((this->freeIndices->__get(layer)->__get(i) > this->freeIndices->__get(layer)->__get(ID)))){
					HX_STACK_LINE(99)
					(this->freeIndices->__get(layer)[i])--;
				}
			}
		}
		HX_STACK_LINE(104)
		this->freeIndices->__get(layer)[ID] = (int)-1;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(TileRenderer_obj,remove,(void))

int TileRenderer_obj::addTile( Float x,Float y,int SpriteID,Float rot,Float scale,int layer){
	HX_STACK_PUSH("TileRenderer::addTile","engine/managers/TileRenderer.hx",62);
	HX_STACK_THIS(this);
	HX_STACK_ARG(x,"x");
	HX_STACK_ARG(y,"y");
	HX_STACK_ARG(SpriteID,"SpriteID");
	HX_STACK_ARG(rot,"rot");
	HX_STACK_ARG(scale,"scale");
	HX_STACK_ARG(layer,"layer");
	HX_STACK_LINE(63)
	this->tileData->__get(layer)->push(x);
	HX_STACK_LINE(64)
	this->tileData->__get(layer)->push(y);
	HX_STACK_LINE(65)
	this->tileData->__get(layer)->push(SpriteID);
	HX_STACK_LINE(68)
	this->tileData->__get(layer)->push(scale);
	HX_STACK_LINE(69)
	this->tileData->__get(layer)->push((Float((rot * ::Math_obj::PI)) / Float((int)180)));
	HX_STACK_LINE(72)
	{
		HX_STACK_LINE(72)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		int _g = this->freeIndices->__get(layer)->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(72)
		while(((_g1 < _g))){
			HX_STACK_LINE(72)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(73)
			if (((this->freeIndices->__get(layer)->__get(i) == (int)-1))){
				HX_STACK_LINE(77)
				this->freeIndices->__get(layer)[i] = (::Math_obj::floor((Float(this->tileData->__get(layer)->length) / Float((int)5))) - (int)1);
				HX_STACK_LINE(79)
				return i;
			}
		}
	}
	HX_STACK_LINE(86)
	this->freeIndices->__get(layer)->push((::Math_obj::floor((Float(this->tileData->__get(layer)->length) / Float((int)5))) - (int)1));
	HX_STACK_LINE(88)
	return (this->freeIndices->__get(layer)->length - (int)1);
}


HX_DEFINE_DYNAMIC_FUNC6(TileRenderer_obj,addTile,return )

int TileRenderer_obj::loadTile( Float x,Float y,Float width,Float height,Float centerX,Float centerY){
	HX_STACK_PUSH("TileRenderer::loadTile","engine/managers/TileRenderer.hx",56);
	HX_STACK_THIS(this);
	HX_STACK_ARG(x,"x");
	HX_STACK_ARG(y,"y");
	HX_STACK_ARG(width,"width");
	HX_STACK_ARG(height,"height");
	HX_STACK_ARG(centerX,"centerX");
	HX_STACK_ARG(centerY,"centerY");
	HX_STACK_LINE(57)
	this->tilesheet->addTileRect(::native::geom::Rectangle_obj::__new(x,y,width,height),::native::geom::Point_obj::__new(centerX,centerY));
	HX_STACK_LINE(58)
	(this->numTiles)++;
	HX_STACK_LINE(59)
	return (this->numTiles - (int)1);
}


HX_DEFINE_DYNAMIC_FUNC6(TileRenderer_obj,loadTile,return )

Float TileRenderer_obj::stageWidth;

Float TileRenderer_obj::stageHeight;

Array< Float > TileRenderer_obj::widths;

Array< Float > TileRenderer_obj::heights;

Float TileRenderer_obj::DPI;


TileRenderer_obj::TileRenderer_obj()
{
}

void TileRenderer_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(TileRenderer);
	HX_MARK_MEMBER_NAME(freeIndices,"freeIndices");
	HX_MARK_MEMBER_NAME(tileData,"tileData");
	HX_MARK_MEMBER_NAME(graphics,"graphics");
	HX_MARK_MEMBER_NAME(numTiles,"numTiles");
	HX_MARK_MEMBER_NAME(tilesheet,"tilesheet");
	HX_MARK_END_CLASS();
}

void TileRenderer_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(freeIndices,"freeIndices");
	HX_VISIT_MEMBER_NAME(tileData,"tileData");
	HX_VISIT_MEMBER_NAME(graphics,"graphics");
	HX_VISIT_MEMBER_NAME(numTiles,"numTiles");
	HX_VISIT_MEMBER_NAME(tilesheet,"tilesheet");
}

Dynamic TileRenderer_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"DPI") ) { return DPI; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"draw") ) { return draw_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"clear") ) { return clear_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"widths") ) { return widths; }
		if (HX_FIELD_EQ(inName,"remove") ) { return remove_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"heights") ) { return heights; }
		if (HX_FIELD_EQ(inName,"addTile") ) { return addTile_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"loadTile") ) { return loadTile_dyn(); }
		if (HX_FIELD_EQ(inName,"tileData") ) { return tileData; }
		if (HX_FIELD_EQ(inName,"graphics") ) { return graphics; }
		if (HX_FIELD_EQ(inName,"numTiles") ) { return numTiles; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tilesheet") ) { return tilesheet; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"stageWidth") ) { return stageWidth; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"stageHeight") ) { return stageHeight; }
		if (HX_FIELD_EQ(inName,"freeIndices") ) { return freeIndices; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"loadAllTiles") ) { return loadAllTiles_dyn(); }
		if (HX_FIELD_EQ(inName,"updateObject") ) { return updateObject_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic TileRenderer_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"DPI") ) { DPI=inValue.Cast< Float >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"widths") ) { widths=inValue.Cast< Array< Float > >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"heights") ) { heights=inValue.Cast< Array< Float > >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"tileData") ) { tileData=inValue.Cast< Array< Array< Float > > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"graphics") ) { graphics=inValue.Cast< ::native::display::Graphics >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numTiles") ) { numTiles=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tilesheet") ) { tilesheet=inValue.Cast< ::native::display::Tilesheet >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"stageWidth") ) { stageWidth=inValue.Cast< Float >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"stageHeight") ) { stageHeight=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"freeIndices") ) { freeIndices=inValue.Cast< Array< Array< int > > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void TileRenderer_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("freeIndices"));
	outFields->push(HX_CSTRING("tileData"));
	outFields->push(HX_CSTRING("graphics"));
	outFields->push(HX_CSTRING("numTiles"));
	outFields->push(HX_CSTRING("tilesheet"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("stageWidth"),
	HX_CSTRING("stageHeight"),
	HX_CSTRING("widths"),
	HX_CSTRING("heights"),
	HX_CSTRING("DPI"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("loadAllTiles"),
	HX_CSTRING("clear"),
	HX_CSTRING("draw"),
	HX_CSTRING("updateObject"),
	HX_CSTRING("remove"),
	HX_CSTRING("addTile"),
	HX_CSTRING("loadTile"),
	HX_CSTRING("freeIndices"),
	HX_CSTRING("tileData"),
	HX_CSTRING("graphics"),
	HX_CSTRING("numTiles"),
	HX_CSTRING("tilesheet"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(TileRenderer_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(TileRenderer_obj::stageWidth,"stageWidth");
	HX_MARK_MEMBER_NAME(TileRenderer_obj::stageHeight,"stageHeight");
	HX_MARK_MEMBER_NAME(TileRenderer_obj::widths,"widths");
	HX_MARK_MEMBER_NAME(TileRenderer_obj::heights,"heights");
	HX_MARK_MEMBER_NAME(TileRenderer_obj::DPI,"DPI");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(TileRenderer_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(TileRenderer_obj::stageWidth,"stageWidth");
	HX_VISIT_MEMBER_NAME(TileRenderer_obj::stageHeight,"stageHeight");
	HX_VISIT_MEMBER_NAME(TileRenderer_obj::widths,"widths");
	HX_VISIT_MEMBER_NAME(TileRenderer_obj::heights,"heights");
	HX_VISIT_MEMBER_NAME(TileRenderer_obj::DPI,"DPI");
};

Class TileRenderer_obj::__mClass;

void TileRenderer_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.managers.TileRenderer"), hx::TCanCast< TileRenderer_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void TileRenderer_obj::__boot()
{
}

} // end namespace engine
} // end namespace managers
