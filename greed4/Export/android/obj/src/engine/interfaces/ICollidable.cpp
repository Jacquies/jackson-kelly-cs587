#include <hxcpp.h>

#ifndef INCLUDED_engine_interfaces_ICollidable
#include <engine/interfaces/ICollidable.h>
#endif
#ifndef INCLUDED_engine_objects_Collider
#include <engine/objects/Collider.h>
#endif
namespace engine{
namespace interfaces{

HX_DEFINE_DYNAMIC_FUNC1(ICollidable_obj,collide,)

HX_DEFINE_DYNAMIC_FUNC0(ICollidable_obj,getCollider,return )


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ICollidable_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ICollidable_obj::__mClass,"__mClass");
};

Class ICollidable_obj::__mClass;

void ICollidable_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.interfaces.ICollidable"), hx::TCanCast< ICollidable_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void ICollidable_obj::__boot()
{
}

} // end namespace engine
} // end namespace interfaces
