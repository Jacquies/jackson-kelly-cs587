#include <hxcpp.h>

#ifndef INCLUDED_engine_interfaces_ITickable
#include <engine/interfaces/ITickable.h>
#endif
namespace engine{
namespace interfaces{

HX_DEFINE_DYNAMIC_FUNC1(ITickable_obj,tick,)


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ITickable_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ITickable_obj::__mClass,"__mClass");
};

Class ITickable_obj::__mClass;

void ITickable_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.interfaces.ITickable"), hx::TCanCast< ITickable_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void ITickable_obj::__boot()
{
}

} // end namespace engine
} // end namespace interfaces
