#include <hxcpp.h>

#ifndef INCLUDED_engine_interfaces_IRenderable
#include <engine/interfaces/IRenderable.h>
#endif
#ifndef INCLUDED_engine_objects_Animation
#include <engine/objects/Animation.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
namespace engine{
namespace interfaces{

HX_DEFINE_DYNAMIC_FUNC2(IRenderable_obj,render,)


static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(IRenderable_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(IRenderable_obj::__mClass,"__mClass");
};

Class IRenderable_obj::__mClass;

void IRenderable_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.interfaces.IRenderable"), hx::TCanCast< IRenderable_obj> ,0,0,
	0, 0,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void IRenderable_obj::__boot()
{
}

} // end namespace engine
} // end namespace interfaces
