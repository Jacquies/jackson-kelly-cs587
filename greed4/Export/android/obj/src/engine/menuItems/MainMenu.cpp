#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_menuItems_Button
#include <engine/menuItems/Button.h>
#endif
#ifndef INCLUDED_engine_menuItems_MainMenu
#include <engine/menuItems/MainMenu.h>
#endif
#ifndef INCLUDED_game_level_Level
#include <game/level/Level.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_display_Stage
#include <native/display/Stage.h>
#endif
#ifndef INCLUDED_native_events_Event
#include <native/events/Event.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_MouseEvent
#include <native/events/MouseEvent.h>
#endif
#ifndef INCLUDED_native_events_TouchEvent
#include <native/events/TouchEvent.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
#ifndef INCLUDED_native_net_SharedObjectFlushStatus
#include <native/net/SharedObjectFlushStatus.h>
#endif
#ifndef INCLUDED_native_system_System
#include <native/system/System.h>
#endif
namespace engine{
namespace menuItems{

Void MainMenu_obj::__construct(::engine::managers::TileRenderer tileRenderer)
{
HX_STACK_PUSH("MainMenu::new","engine/menuItems/MainMenu.hx",25);
{
	HX_STACK_LINE(26)
	super::__construct();
	HX_STACK_LINE(28)
	this->renderer = tileRenderer;
	HX_STACK_LINE(30)
	this->addEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null(),null(),null());
}
;
	return null();
}

MainMenu_obj::~MainMenu_obj() { }

Dynamic MainMenu_obj::__CreateEmpty() { return  new MainMenu_obj; }
hx::ObjectPtr< MainMenu_obj > MainMenu_obj::__new(::engine::managers::TileRenderer tileRenderer)
{  hx::ObjectPtr< MainMenu_obj > result = new MainMenu_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic MainMenu_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< MainMenu_obj > result = new MainMenu_obj();
	result->__construct(inArgs[0]);
	return result;}

Void MainMenu_obj::dummyFunction( ){
{
		HX_STACK_PUSH("MainMenu::dummyFunction","engine/menuItems/MainMenu.hx",150);
		HX_STACK_THIS(this);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MainMenu_obj,dummyFunction,(void))

Void MainMenu_obj::onTouchEnd( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("MainMenu::onTouchEnd","engine/menuItems/MainMenu.hx",145);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(145)
		this->clickable = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(MainMenu_obj,onTouchEnd,(void))

Void MainMenu_obj::onTouchBegin( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("MainMenu::onTouchBegin","engine/menuItems/MainMenu.hx",137);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(137)
		if ((this->clickable)){
			HX_STACK_LINE(139)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			Array< ::engine::menuItems::Button > _g1 = this->buttons;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(139)
			while(((_g < _g1->length))){
				HX_STACK_LINE(139)
				::engine::menuItems::Button button = _g1->__get(_g);		HX_STACK_VAR(button,"button");
				HX_STACK_LINE(139)
				++(_g);
				HX_STACK_LINE(140)
				button->onClick(this->get_mouseX(),this->get_mouseY());
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(MainMenu_obj,onTouchBegin,(void))

Void MainMenu_obj::exit( ){
{
		HX_STACK_PUSH("MainMenu::exit","engine/menuItems/MainMenu.hx",102);
		HX_STACK_THIS(this);
		HX_STACK_LINE(103)
		this->buttons->splice((int)0,this->buttons->length);
		HX_STACK_LINE(105)
		this->removeEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null());
		HX_STACK_LINE(110)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null());
		HX_STACK_LINE(111)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_END,this->onTouchEnd_dyn(),null());
		HX_STACK_LINE(114)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
		HX_STACK_LINE(120)
		::native::system::System_obj::exit((int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MainMenu_obj,exit,(void))

Void MainMenu_obj::newGame( ){
{
		HX_STACK_PUSH("MainMenu::newGame","engine/menuItems/MainMenu.hx",94);
		HX_STACK_THIS(this);
		HX_STACK_LINE(95)
		::native::net::SharedObject localData;		HX_STACK_VAR(localData,"localData");
		HX_STACK_LINE(96)
		localData = ::native::net::SharedObject_obj::getLocal(HX_CSTRING("greed"),null(),null());
		HX_STACK_LINE(97)
		localData->data->__FieldRef(HX_CSTRING("newGame")) = true;
		HX_STACK_LINE(98)
		localData->flush(null());
		HX_STACK_LINE(99)
		this->resumeGame();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MainMenu_obj,newGame,(void))

Void MainMenu_obj::resumeGame( ){
{
		HX_STACK_PUSH("MainMenu::resumeGame","engine/menuItems/MainMenu.hx",80);
		HX_STACK_THIS(this);
		HX_STACK_LINE(81)
		this->buttons->splice((int)0,this->buttons->length);
		HX_STACK_LINE(82)
		this->get_parent()->addChild(::game::level::Level_obj::__new(this->renderer));
		HX_STACK_LINE(84)
		this->removeEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null());
		HX_STACK_LINE(88)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null());
		HX_STACK_LINE(91)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MainMenu_obj,resumeGame,(void))

Void MainMenu_obj::refresh( ){
{
		HX_STACK_PUSH("MainMenu::refresh","engine/menuItems/MainMenu.hx",52);
		HX_STACK_THIS(this);
		HX_STACK_LINE(54)
		this->buttons = Array_obj< ::engine::menuItems::Button >::__new();
		HX_STACK_LINE(55)
		this->renderer->clear();
		HX_STACK_LINE(65)
		this->scale = ::Math_obj::max((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float(::engine::managers::TileRenderer_obj::widths->__get((int)58))),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float(::engine::managers::TileRenderer_obj::heights->__get((int)58))));
		HX_STACK_LINE(66)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)2)),(int)58,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(69)
		this->scale = ::Math_obj::min((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float(::engine::managers::TileRenderer_obj::widths->__get((int)29))),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)2)) / Float((((int)5 * ::engine::managers::TileRenderer_obj::heights->__get((int)29))))));
		HX_STACK_LINE(70)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)5)),(int)29,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(72)
		this->scale = ::Math_obj::min((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((((int)2 * ::engine::managers::TileRenderer_obj::widths->__get((int)47))))),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((((int)2 * ::engine::managers::TileRenderer_obj::heights->__get((int)47))))));
		HX_STACK_LINE(73)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)3)) / Float((int)6)),(int)47,this->scale,this->renderer,this->resumeGame_dyn()));
		HX_STACK_LINE(74)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)4)) / Float((int)6)),(int)48,this->scale,this->renderer,this->newGame_dyn()));
		HX_STACK_LINE(75)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)5)) / Float((int)6)),(int)31,this->scale,this->renderer,this->exit_dyn()));
		HX_STACK_LINE(77)
		this->renderer->draw();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MainMenu_obj,refresh,(void))

Void MainMenu_obj::onAddedToStage( ::native::events::Event event){
{
		HX_STACK_PUSH("MainMenu::onAddedToStage","engine/menuItems/MainMenu.hx",33);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(34)
		if ((::engine::menuItems::MainMenu_obj::initialized)){
			HX_STACK_LINE(34)
			this->clickable = false;
		}
		else{
			HX_STACK_LINE(37)
			this->clickable = true;
			HX_STACK_LINE(38)
			::engine::menuItems::MainMenu_obj::initialized = true;
		}
		HX_STACK_LINE(45)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null(),null(),null());
		HX_STACK_LINE(46)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_END,this->onTouchEnd_dyn(),null(),null(),null());
		HX_STACK_LINE(49)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(MainMenu_obj,onAddedToStage,(void))

bool MainMenu_obj::initialized;


MainMenu_obj::MainMenu_obj()
{
}

void MainMenu_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(MainMenu);
	HX_MARK_MEMBER_NAME(clickable,"clickable");
	HX_MARK_MEMBER_NAME(scale,"scale");
	HX_MARK_MEMBER_NAME(buttons,"buttons");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	super::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void MainMenu_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(clickable,"clickable");
	HX_VISIT_MEMBER_NAME(scale,"scale");
	HX_VISIT_MEMBER_NAME(buttons,"buttons");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	super::__Visit(HX_VISIT_ARG);
}

Dynamic MainMenu_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"exit") ) { return exit_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { return scale; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"newGame") ) { return newGame_dyn(); }
		if (HX_FIELD_EQ(inName,"refresh") ) { return refresh_dyn(); }
		if (HX_FIELD_EQ(inName,"buttons") ) { return buttons; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"clickable") ) { return clickable; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"onTouchEnd") ) { return onTouchEnd_dyn(); }
		if (HX_FIELD_EQ(inName,"resumeGame") ) { return resumeGame_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"initialized") ) { return initialized; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"onTouchBegin") ) { return onTouchBegin_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"dummyFunction") ) { return dummyFunction_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"onAddedToStage") ) { return onAddedToStage_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic MainMenu_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { scale=inValue.Cast< Float >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"buttons") ) { buttons=inValue.Cast< Array< ::engine::menuItems::Button > >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::engine::managers::TileRenderer >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"clickable") ) { clickable=inValue.Cast< bool >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"initialized") ) { initialized=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void MainMenu_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("clickable"));
	outFields->push(HX_CSTRING("scale"));
	outFields->push(HX_CSTRING("buttons"));
	outFields->push(HX_CSTRING("renderer"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("initialized"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("dummyFunction"),
	HX_CSTRING("onTouchEnd"),
	HX_CSTRING("onTouchBegin"),
	HX_CSTRING("exit"),
	HX_CSTRING("newGame"),
	HX_CSTRING("resumeGame"),
	HX_CSTRING("refresh"),
	HX_CSTRING("onAddedToStage"),
	HX_CSTRING("clickable"),
	HX_CSTRING("scale"),
	HX_CSTRING("buttons"),
	HX_CSTRING("renderer"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(MainMenu_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(MainMenu_obj::initialized,"initialized");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(MainMenu_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(MainMenu_obj::initialized,"initialized");
};

Class MainMenu_obj::__mClass;

void MainMenu_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.menuItems.MainMenu"), hx::TCanCast< MainMenu_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void MainMenu_obj::__boot()
{
}

} // end namespace engine
} // end namespace menuItems
