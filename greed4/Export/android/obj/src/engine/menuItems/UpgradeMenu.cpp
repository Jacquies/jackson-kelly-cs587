#include <hxcpp.h>

#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_engine_managers_TileRenderer
#include <engine/managers/TileRenderer.h>
#endif
#ifndef INCLUDED_engine_menuItems_Button
#include <engine/menuItems/Button.h>
#endif
#ifndef INCLUDED_engine_menuItems_MainMenu
#include <engine/menuItems/MainMenu.h>
#endif
#ifndef INCLUDED_engine_menuItems_UpgradeMenu
#include <engine/menuItems/UpgradeMenu.h>
#endif
#ifndef INCLUDED_engine_objects_HealthBar
#include <engine/objects/HealthBar.h>
#endif
#ifndef INCLUDED_engine_objects_Scoreboard
#include <engine/objects/Scoreboard.h>
#endif
#ifndef INCLUDED_game_level_Level
#include <game/level/Level.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_display_Stage
#include <native/display/Stage.h>
#endif
#ifndef INCLUDED_native_events_Event
#include <native/events/Event.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_MouseEvent
#include <native/events/MouseEvent.h>
#endif
#ifndef INCLUDED_native_events_TouchEvent
#include <native/events/TouchEvent.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif
#ifndef INCLUDED_native_net_SharedObjectFlushStatus
#include <native/net/SharedObjectFlushStatus.h>
#endif
namespace engine{
namespace menuItems{

Void UpgradeMenu_obj::__construct(::engine::managers::TileRenderer tileRenderer)
{
HX_STACK_PUSH("UpgradeMenu::new","engine/menuItems/UpgradeMenu.hx",25);
{
	HX_STACK_LINE(26)
	super::__construct();
	HX_STACK_LINE(28)
	this->renderer = tileRenderer;
	HX_STACK_LINE(30)
	this->addEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null(),null(),null());
}
;
	return null();
}

UpgradeMenu_obj::~UpgradeMenu_obj() { }

Dynamic UpgradeMenu_obj::__CreateEmpty() { return  new UpgradeMenu_obj; }
hx::ObjectPtr< UpgradeMenu_obj > UpgradeMenu_obj::__new(::engine::managers::TileRenderer tileRenderer)
{  hx::ObjectPtr< UpgradeMenu_obj > result = new UpgradeMenu_obj();
	result->__construct(tileRenderer);
	return result;}

Dynamic UpgradeMenu_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< UpgradeMenu_obj > result = new UpgradeMenu_obj();
	result->__construct(inArgs[0]);
	return result;}

Void UpgradeMenu_obj::dummyFunction( ){
{
		HX_STACK_PUSH("UpgradeMenu::dummyFunction","engine/menuItems/UpgradeMenu.hx",238);
		HX_STACK_THIS(this);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,dummyFunction,(void))

Void UpgradeMenu_obj::onTouchBegin( ::native::events::TouchEvent event){
{
		HX_STACK_PUSH("UpgradeMenu::onTouchBegin","engine/menuItems/UpgradeMenu.hx",232);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(233)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		Array< ::engine::menuItems::Button > _g1 = this->buttons;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(233)
		while(((_g < _g1->length))){
			HX_STACK_LINE(233)
			::engine::menuItems::Button button = _g1->__get(_g);		HX_STACK_VAR(button,"button");
			HX_STACK_LINE(233)
			++(_g);
			HX_STACK_LINE(234)
			button->onClick(this->get_mouseX(),this->get_mouseY());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(UpgradeMenu_obj,onTouchBegin,(void))

Void UpgradeMenu_obj::nextLevel( ){
{
		HX_STACK_PUSH("UpgradeMenu::nextLevel","engine/menuItems/UpgradeMenu.hx",212);
		HX_STACK_THIS(this);
		HX_STACK_LINE(213)
		this->buttons->splice((int)0,this->buttons->length);
		HX_STACK_LINE(214)
		this->get_parent()->addChild(::game::level::Level_obj::__new(this->renderer));
		HX_STACK_LINE(216)
		this->removeEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null());
		HX_STACK_LINE(220)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null());
		HX_STACK_LINE(223)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,nextLevel,(void))

Void UpgradeMenu_obj::saveAndExit( ){
{
		HX_STACK_PUSH("UpgradeMenu::saveAndExit","engine/menuItems/UpgradeMenu.hx",196);
		HX_STACK_THIS(this);
		HX_STACK_LINE(197)
		this->localData->flush(null());
		HX_STACK_LINE(199)
		this->buttons->splice((int)0,this->buttons->length);
		HX_STACK_LINE(200)
		this->get_parent()->addChild(::engine::menuItems::MainMenu_obj::__new(this->renderer));
		HX_STACK_LINE(202)
		this->removeEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null());
		HX_STACK_LINE(206)
		this->get_stage()->removeEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null());
		HX_STACK_LINE(209)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,saveAndExit,(void))

Void UpgradeMenu_obj::upgradeAggroRadius( ){
{
		HX_STACK_PUSH("UpgradeMenu::upgradeAggroRadius","engine/menuItems/UpgradeMenu.hx",189);
		HX_STACK_THIS(this);
		HX_STACK_LINE(190)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("gold")),::Math_obj::floor((Float(((int)50 * -(((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) - (int)165))))) / Float((int)15))));
		HX_STACK_LINE(191)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("aggroRadius")),(int)15);
		HX_STACK_LINE(192)
		this->localData->flush(null());
		HX_STACK_LINE(193)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,upgradeAggroRadius,(void))

Void UpgradeMenu_obj::upgradeCollectRadius( ){
{
		HX_STACK_PUSH("UpgradeMenu::upgradeCollectRadius","engine/menuItems/UpgradeMenu.hx",182);
		HX_STACK_THIS(this);
		HX_STACK_LINE(183)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("gold")),::Math_obj::floor(((Float(((int)50 * ((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) - (int)10)))) / Float((int)4)) + (int)50)));
		HX_STACK_LINE(184)
		hx::AddEq(this->localData->data->__FieldRef(HX_CSTRING("collectRadius")),(int)4);
		HX_STACK_LINE(185)
		this->localData->flush(null());
		HX_STACK_LINE(186)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,upgradeCollectRadius,(void))

Void UpgradeMenu_obj::upgradeSpeed( ){
{
		HX_STACK_PUSH("UpgradeMenu::upgradeSpeed","engine/menuItems/UpgradeMenu.hx",175);
		HX_STACK_THIS(this);
		HX_STACK_LINE(176)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("gold")),::Math_obj::floor(((int)100 * ((this->localData->data->__Field(HX_CSTRING("speed"),true) - (int)3)))));
		HX_STACK_LINE(177)
		(this->localData->data->__FieldRef(HX_CSTRING("speed")))++;
		HX_STACK_LINE(178)
		this->localData->flush(null());
		HX_STACK_LINE(179)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,upgradeSpeed,(void))

Void UpgradeMenu_obj::upgradeHealth( ){
{
		HX_STACK_PUSH("UpgradeMenu::upgradeHealth","engine/menuItems/UpgradeMenu.hx",167);
		HX_STACK_THIS(this);
		HX_STACK_LINE(168)
		hx::SubEq(this->localData->data->__FieldRef(HX_CSTRING("gold")),::Math_obj::floor(((int)100 * this->localData->data->__Field(HX_CSTRING("health"),true))));
		HX_STACK_LINE(169)
		(this->localData->data->__FieldRef(HX_CSTRING("health")))++;
		HX_STACK_LINE(170)
		this->localData->flush(null());
		HX_STACK_LINE(171)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
		HX_STACK_LINE(172)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,upgradeHealth,(void))

Void UpgradeMenu_obj::refresh( ){
{
		HX_STACK_PUSH("UpgradeMenu::refresh","engine/menuItems/UpgradeMenu.hx",55);
		HX_STACK_THIS(this);
		HX_STACK_LINE(57)
		this->buttons = Array_obj< ::engine::menuItems::Button >::__new();
		HX_STACK_LINE(58)
		this->renderer->clear();
		HX_STACK_LINE(59)
		int cost;		HX_STACK_VAR(cost,"cost");
		HX_STACK_LINE(69)
		this->scale = ::Math_obj::max((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float(::engine::managers::TileRenderer_obj::widths->__get((int)58))),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float(::engine::managers::TileRenderer_obj::heights->__get((int)58))));
		HX_STACK_LINE(70)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)2)),(int)58,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(74)
		this->scale = ::Math_obj::min((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float(::engine::managers::TileRenderer_obj::widths->__get((int)30))),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((((int)5 * ::engine::managers::TileRenderer_obj::heights->__get((int)30))))));
		HX_STACK_LINE(75)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((int)2)),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((int)10)),(int)30,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(77)
		this->scale = (Float(::Math_obj::min((Float(::engine::managers::TileRenderer_obj::stageWidth) / Float(::engine::managers::TileRenderer_obj::widths->__get((int)15))),(Float(::engine::managers::TileRenderer_obj::stageHeight) / Float(::engine::managers::TileRenderer_obj::heights->__get((int)15))))) / Float(10.));
		HX_STACK_LINE(80)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)3)) / Float((int)20)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)17,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(81)
		{
			HX_STACK_LINE(81)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = this->localData->data->__Field(HX_CSTRING("health"),true);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(81)
			while(((_g1 < _g))){
				HX_STACK_LINE(81)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(82)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)18,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(84)
		{
			HX_STACK_LINE(84)
			int _g = this->localData->data->__Field(HX_CSTRING("health"),true);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(84)
			while(((_g < (int)3))){
				HX_STACK_LINE(84)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(85)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)19,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(87)
		if (((this->localData->data->__Field(HX_CSTRING("health"),true) < (int)3))){
			HX_STACK_LINE(88)
			cost = ::Math_obj::floor(((int)100 * this->localData->data->__Field(HX_CSTRING("health"),true)));
			HX_STACK_LINE(89)
			this->scoreboard->renderNumber((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)13)) / Float((int)20)),((Float((::engine::managers::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)) - (Float(::engine::managers::TileRenderer_obj::widths->__get((int)15)) / Float((int)4))),this->scale,cost);
			HX_STACK_LINE(90)
			if (((this->localData->data->__Field(HX_CSTRING("gold"),true) >= cost))){
				HX_STACK_LINE(90)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)15,this->scale,this->renderer,this->upgradeHealth_dyn()));
			}
			else{
				HX_STACK_LINE(92)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)8)) / Float((int)25)),(int)16,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(98)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)3)) / Float((int)20)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)20,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(99)
		{
			HX_STACK_LINE(99)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = ::Math_obj::floor((this->localData->data->__Field(HX_CSTRING("speed"),true) - (int)4));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(99)
			while(((_g1 < _g))){
				HX_STACK_LINE(99)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(100)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)21,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(102)
		{
			HX_STACK_LINE(102)
			int _g = ::Math_obj::floor((this->localData->data->__Field(HX_CSTRING("speed"),true) - (int)4));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(102)
			while(((_g < (int)3))){
				HX_STACK_LINE(102)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(103)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)22,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(105)
		if (((this->localData->data->__Field(HX_CSTRING("speed"),true) < (int)7))){
			HX_STACK_LINE(106)
			cost = ::Math_obj::floor(((int)100 * ((this->localData->data->__Field(HX_CSTRING("speed"),true) - (int)3))));
			HX_STACK_LINE(107)
			this->scoreboard->renderNumber((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)13)) / Float((int)20)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),this->scale,cost);
			HX_STACK_LINE(108)
			if (((this->localData->data->__Field(HX_CSTRING("gold"),true) >= cost))){
				HX_STACK_LINE(108)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)15,this->scale,this->renderer,this->upgradeSpeed_dyn()));
			}
			else{
				HX_STACK_LINE(110)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)11)) / Float((int)25)),(int)16,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(116)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)3)) / Float((int)20)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)23,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(117)
		{
			HX_STACK_LINE(117)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = ::Math_obj::floor((Float(((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) - (int)10))) / Float((int)4)));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(117)
			while(((_g1 < _g))){
				HX_STACK_LINE(117)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(118)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)24,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(120)
		{
			HX_STACK_LINE(120)
			int _g = ::Math_obj::floor((Float(((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) - (int)10))) / Float((int)4)));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(120)
			while(((_g < (int)3))){
				HX_STACK_LINE(120)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(121)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)25,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(123)
		if (((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) < (int)19))){
			HX_STACK_LINE(124)
			cost = ::Math_obj::floor(((Float(((int)50 * ((this->localData->data->__Field(HX_CSTRING("collectRadius"),true) - (int)10)))) / Float((int)4)) + (int)50));
			HX_STACK_LINE(125)
			this->scoreboard->renderNumber((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)13)) / Float((int)20)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),this->scale,cost);
			HX_STACK_LINE(126)
			if (((this->localData->data->__Field(HX_CSTRING("gold"),true) >= cost))){
				HX_STACK_LINE(126)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)15,this->scale,this->renderer,this->upgradeCollectRadius_dyn()));
			}
			else{
				HX_STACK_LINE(128)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)14)) / Float((int)25)),(int)16,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(134)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)3)) / Float((int)20)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)26,this->scale,this->renderer,this->dummyFunction_dyn()));
		HX_STACK_LINE(135)
		{
			HX_STACK_LINE(135)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			int _g = ::Math_obj::floor((Float(-(((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) - (int)150)))) / Float((int)15)));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(135)
			while(((_g1 < _g))){
				HX_STACK_LINE(135)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(136)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)27,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(138)
		{
			HX_STACK_LINE(138)
			int _g = ::Math_obj::floor((Float(-(((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) - (int)150)))) / Float((int)15)));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(138)
			while(((_g < (int)3))){
				HX_STACK_LINE(138)
				int i = (_g)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(139)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * ((3.5 + i)))) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)28,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(141)
		if (((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) > (int)105))){
			HX_STACK_LINE(142)
			cost = ::Math_obj::floor((Float(((int)50 * -(((this->localData->data->__Field(HX_CSTRING("aggroRadius"),true) - (int)165))))) / Float((int)15)));
			HX_STACK_LINE(143)
			this->scoreboard->renderNumber((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)13)) / Float((int)20)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),this->scale,cost);
			HX_STACK_LINE(144)
			if (((this->localData->data->__Field(HX_CSTRING("gold"),true) >= cost))){
				HX_STACK_LINE(144)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)15,this->scale,this->renderer,this->upgradeAggroRadius_dyn()));
			}
			else{
				HX_STACK_LINE(146)
				this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::stageWidth * (int)9)) / Float((int)10)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)17)) / Float((int)25)),(int)16,this->scale,this->renderer,this->dummyFunction_dyn()));
			}
		}
		HX_STACK_LINE(151)
		this->scale = ::Math_obj::min((Float(::engine::managers::TileRenderer_obj::stageHeight) / Float((((int)5 * ::engine::managers::TileRenderer_obj::heights->__get((int)14))))),(Float(::engine::managers::TileRenderer_obj::stageWidth) / Float((((int)2 * ::engine::managers::TileRenderer_obj::widths->__get((int)52))))));
		HX_STACK_LINE(153)
		this->buttons->push(::engine::menuItems::Button_obj::__new((Float((::engine::managers::TileRenderer_obj::widths->__get((int)52) * this->scale)) / Float((int)2)),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)9)) / Float((int)10)),(int)52,this->scale,this->renderer,this->saveAndExit_dyn()));
		HX_STACK_LINE(154)
		this->buttons->push(::engine::menuItems::Button_obj::__new((::engine::managers::TileRenderer_obj::stageWidth - (Float((::engine::managers::TileRenderer_obj::widths->__get((int)14) * this->scale)) / Float((int)2))),(Float((::engine::managers::TileRenderer_obj::stageHeight * (int)9)) / Float((int)10)),(int)14,this->scale,this->renderer,this->nextLevel_dyn()));
		HX_STACK_LINE(157)
		this->scoreboard = ::engine::objects::Scoreboard_obj::__new(this->renderer);
		HX_STACK_LINE(158)
		this->scoreboard->update(this->localData->data->__Field(HX_CSTRING("gold"),true));
		HX_STACK_LINE(161)
		this->healthBar = ::engine::objects::HealthBar_obj::__new(this->renderer);
		HX_STACK_LINE(162)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
		HX_STACK_LINE(164)
		this->renderer->draw();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(UpgradeMenu_obj,refresh,(void))

Void UpgradeMenu_obj::onAddedToStage( ::native::events::Event event){
{
		HX_STACK_PUSH("UpgradeMenu::onAddedToStage","engine/menuItems/UpgradeMenu.hx",33);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(34)
		this->localData = ::native::net::SharedObject_obj::getLocal(HX_CSTRING("greed"),null(),null());
		HX_STACK_LINE(39)
		this->get_stage()->addEventListener(::native::events::TouchEvent_obj::TOUCH_BEGIN,this->onTouchBegin_dyn(),null(),null(),null());
		HX_STACK_LINE(42)
		this->scale = (int)1;
		HX_STACK_LINE(45)
		this->scoreboard = ::engine::objects::Scoreboard_obj::__new(this->renderer);
		HX_STACK_LINE(46)
		this->scoreboard->update(this->localData->data->__Field(HX_CSTRING("gold"),true));
		HX_STACK_LINE(49)
		this->healthBar = ::engine::objects::HealthBar_obj::__new(this->renderer);
		HX_STACK_LINE(50)
		this->healthBar->update(this->localData->data->__Field(HX_CSTRING("health"),true));
		HX_STACK_LINE(52)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(UpgradeMenu_obj,onAddedToStage,(void))


UpgradeMenu_obj::UpgradeMenu_obj()
{
}

void UpgradeMenu_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(UpgradeMenu);
	HX_MARK_MEMBER_NAME(scale,"scale");
	HX_MARK_MEMBER_NAME(healthBar,"healthBar");
	HX_MARK_MEMBER_NAME(scoreboard,"scoreboard");
	HX_MARK_MEMBER_NAME(localData,"localData");
	HX_MARK_MEMBER_NAME(buttons,"buttons");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	super::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void UpgradeMenu_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(scale,"scale");
	HX_VISIT_MEMBER_NAME(healthBar,"healthBar");
	HX_VISIT_MEMBER_NAME(scoreboard,"scoreboard");
	HX_VISIT_MEMBER_NAME(localData,"localData");
	HX_VISIT_MEMBER_NAME(buttons,"buttons");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	super::__Visit(HX_VISIT_ARG);
}

Dynamic UpgradeMenu_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { return scale; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"refresh") ) { return refresh_dyn(); }
		if (HX_FIELD_EQ(inName,"buttons") ) { return buttons; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"nextLevel") ) { return nextLevel_dyn(); }
		if (HX_FIELD_EQ(inName,"healthBar") ) { return healthBar; }
		if (HX_FIELD_EQ(inName,"localData") ) { return localData; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"scoreboard") ) { return scoreboard; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"saveAndExit") ) { return saveAndExit_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"onTouchBegin") ) { return onTouchBegin_dyn(); }
		if (HX_FIELD_EQ(inName,"upgradeSpeed") ) { return upgradeSpeed_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"dummyFunction") ) { return dummyFunction_dyn(); }
		if (HX_FIELD_EQ(inName,"upgradeHealth") ) { return upgradeHealth_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"onAddedToStage") ) { return onAddedToStage_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"upgradeAggroRadius") ) { return upgradeAggroRadius_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"upgradeCollectRadius") ) { return upgradeCollectRadius_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic UpgradeMenu_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"scale") ) { scale=inValue.Cast< Float >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"buttons") ) { buttons=inValue.Cast< Array< ::engine::menuItems::Button > >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::engine::managers::TileRenderer >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"healthBar") ) { healthBar=inValue.Cast< ::engine::objects::HealthBar >(); return inValue; }
		if (HX_FIELD_EQ(inName,"localData") ) { localData=inValue.Cast< ::native::net::SharedObject >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"scoreboard") ) { scoreboard=inValue.Cast< ::engine::objects::Scoreboard >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void UpgradeMenu_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("scale"));
	outFields->push(HX_CSTRING("healthBar"));
	outFields->push(HX_CSTRING("scoreboard"));
	outFields->push(HX_CSTRING("localData"));
	outFields->push(HX_CSTRING("buttons"));
	outFields->push(HX_CSTRING("renderer"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("dummyFunction"),
	HX_CSTRING("onTouchBegin"),
	HX_CSTRING("nextLevel"),
	HX_CSTRING("saveAndExit"),
	HX_CSTRING("upgradeAggroRadius"),
	HX_CSTRING("upgradeCollectRadius"),
	HX_CSTRING("upgradeSpeed"),
	HX_CSTRING("upgradeHealth"),
	HX_CSTRING("refresh"),
	HX_CSTRING("onAddedToStage"),
	HX_CSTRING("scale"),
	HX_CSTRING("healthBar"),
	HX_CSTRING("scoreboard"),
	HX_CSTRING("localData"),
	HX_CSTRING("buttons"),
	HX_CSTRING("renderer"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(UpgradeMenu_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(UpgradeMenu_obj::__mClass,"__mClass");
};

Class UpgradeMenu_obj::__mClass;

void UpgradeMenu_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("engine.menuItems.UpgradeMenu"), hx::TCanCast< UpgradeMenu_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void UpgradeMenu_obj::__boot()
{
}

} // end namespace engine
} // end namespace menuItems
