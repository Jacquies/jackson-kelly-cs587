#include <hxcpp.h>

#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_Enemy
#include <Enemy.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IColliderState
#include <IColliderState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_ITickable
#include <ITickable.h>
#endif
#ifndef INCLUDED_MovableColliderState
#include <MovableColliderState.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_Projectile
#include <Projectile.h>
#endif

Void MovableColliderState_obj::__construct()
{
HX_STACK_PUSH("MovableColliderState::new","MovableColliderState.hx",8);
{
}
;
	return null();
}

MovableColliderState_obj::~MovableColliderState_obj() { }

Dynamic MovableColliderState_obj::__CreateEmpty() { return  new MovableColliderState_obj; }
hx::ObjectPtr< MovableColliderState_obj > MovableColliderState_obj::__new()
{  hx::ObjectPtr< MovableColliderState_obj > result = new MovableColliderState_obj();
	result->__construct();
	return result;}

Dynamic MovableColliderState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< MovableColliderState_obj > result = new MovableColliderState_obj();
	result->__construct();
	return result;}

hx::Object *MovableColliderState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IColliderState_obj)) return operator ::IColliderState_obj *();
	return super::__ToInterface(inType);
}

Void MovableColliderState_obj::collide( ::ICollidable collidingObject,::ICollidable otherCollidingObject){
{
		HX_STACK_PUSH("MovableColliderState::collide","MovableColliderState.hx",10);
		HX_STACK_THIS(this);
		HX_STACK_ARG(collidingObject,"collidingObject");
		HX_STACK_ARG(otherCollidingObject,"otherCollidingObject");
		HX_STACK_LINE(10)
		if (((otherCollidingObject->getCollider()->getType() == ::Collider_obj::TYPE_WALL))){
			HX_STACK_LINE(11)
			if (((collidingObject->getCollider()->getType() == ::Collider_obj::TYPE_PLAYER))){
				HX_STACK_LINE(12)
				(hx::TCast< Player >::cast(collidingObject))->hitWall(otherCollidingObject);
			}
			else{
				HX_STACK_LINE(14)
				if (((collidingObject->getCollider()->getType() == ::Collider_obj::TYPE_ENEMY))){
					HX_STACK_LINE(14)
					(hx::TCast< Enemy >::cast(collidingObject))->hitWall(otherCollidingObject);
				}
				else{
					HX_STACK_LINE(16)
					(hx::TCast< Projectile >::cast(collidingObject))->hitWall(otherCollidingObject);
				}
			}
		}
		else{
			HX_STACK_LINE(19)
			if (((otherCollidingObject->getCollider()->getType() == ::Collider_obj::TYPE_ENEMY))){
				HX_STACK_LINE(19)
				if (((collidingObject->getCollider()->getType() == ::Collider_obj::TYPE_ENEMY))){
					HX_STACK_LINE(20)
					(hx::TCast< Enemy >::cast(collidingObject))->hitOtherEnemy(otherCollidingObject);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(MovableColliderState_obj,collide,(void))


MovableColliderState_obj::MovableColliderState_obj()
{
}

void MovableColliderState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(MovableColliderState);
	HX_MARK_END_CLASS();
}

void MovableColliderState_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic MovableColliderState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic MovableColliderState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void MovableColliderState_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(MovableColliderState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(MovableColliderState_obj::__mClass,"__mClass");
};

Class MovableColliderState_obj::__mClass;

void MovableColliderState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("MovableColliderState"), hx::TCanCast< MovableColliderState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void MovableColliderState_obj::__boot()
{
}

