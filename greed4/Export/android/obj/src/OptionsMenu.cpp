#include <hxcpp.h>

#ifndef INCLUDED_Button
#include <Button.h>
#endif
#ifndef INCLUDED_IMenu
#include <IMenu.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif
#ifndef INCLUDED_MainMenu
#include <MainMenu.h>
#endif
#ifndef INCLUDED_OptionsMenu
#include <OptionsMenu.h>
#endif
#ifndef INCLUDED_TileRenderer
#include <TileRenderer.h>
#endif
#ifndef INCLUDED_UpgradeMenu
#include <UpgradeMenu.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_native_display_DisplayObject
#include <native/display/DisplayObject.h>
#endif
#ifndef INCLUDED_native_display_DisplayObjectContainer
#include <native/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_native_display_IBitmapDrawable
#include <native/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_native_display_InteractiveObject
#include <native/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_native_display_Sprite
#include <native/display/Sprite.h>
#endif
#ifndef INCLUDED_native_display_Stage
#include <native/display/Stage.h>
#endif
#ifndef INCLUDED_native_display_StageAlign
#include <native/display/StageAlign.h>
#endif
#ifndef INCLUDED_native_display_StageScaleMode
#include <native/display/StageScaleMode.h>
#endif
#ifndef INCLUDED_native_events_Event
#include <native/events/Event.h>
#endif
#ifndef INCLUDED_native_events_EventDispatcher
#include <native/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_IEventDispatcher
#include <native/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_native_events_MouseEvent
#include <native/events/MouseEvent.h>
#endif
#ifndef INCLUDED_native_geom_Point
#include <native/geom/Point.h>
#endif
#ifndef INCLUDED_native_net_SharedObject
#include <native/net/SharedObject.h>
#endif

Void OptionsMenu_obj::__construct(::TileRenderer inRenderer)
{
HX_STACK_PUSH("OptionsMenu::new","OptionsMenu.hx",23);
{
	HX_STACK_LINE(24)
	super::__construct();
	HX_STACK_LINE(26)
	this->renderer = inRenderer;
	HX_STACK_LINE(28)
	this->renderer->clear();
	HX_STACK_LINE(30)
	this->addEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null(),null(),null());
}
;
	return null();
}

OptionsMenu_obj::~OptionsMenu_obj() { }

Dynamic OptionsMenu_obj::__CreateEmpty() { return  new OptionsMenu_obj; }
hx::ObjectPtr< OptionsMenu_obj > OptionsMenu_obj::__new(::TileRenderer inRenderer)
{  hx::ObjectPtr< OptionsMenu_obj > result = new OptionsMenu_obj();
	result->__construct(inRenderer);
	return result;}

Dynamic OptionsMenu_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< OptionsMenu_obj > result = new OptionsMenu_obj();
	result->__construct(inArgs[0]);
	return result;}

hx::Object *OptionsMenu_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IMenu_obj)) return operator ::IMenu_obj *();
	return super::__ToInterface(inType);
}

::native::net::SharedObject OptionsMenu_obj::getSharedObject( ){
	HX_STACK_PUSH("OptionsMenu::getSharedObject","OptionsMenu.hx",99);
	HX_STACK_THIS(this);
	HX_STACK_LINE(99)
	return this->sharedObj;
}


HX_DEFINE_DYNAMIC_FUNC0(OptionsMenu_obj,getSharedObject,return )

Void OptionsMenu_obj::setParent( ::String parent){
{
		HX_STACK_PUSH("OptionsMenu::setParent","OptionsMenu.hx",87);
		HX_STACK_THIS(this);
		HX_STACK_ARG(parent,"parent");
		HX_STACK_LINE(87)
		if (((parent == HX_CSTRING("main")))){
			HX_STACK_LINE(88)
			this->fromMain = true;
		}
		else{
			HX_STACK_LINE(90)
			this->fromMain = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsMenu_obj,setParent,(void))

Void OptionsMenu_obj::options( ){
{
		HX_STACK_PUSH("OptionsMenu::options","OptionsMenu.hx",83);
		HX_STACK_THIS(this);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(OptionsMenu_obj,options,(void))

Void OptionsMenu_obj::back( ){
{
		HX_STACK_PUSH("OptionsMenu::back","OptionsMenu.hx",69);
		HX_STACK_THIS(this);
		HX_STACK_LINE(70)
		::haxe::Log_obj::trace(HX_CSTRING("Going back"),hx::SourceInfo(HX_CSTRING("OptionsMenu.hx"),70,HX_CSTRING("OptionsMenu"),HX_CSTRING("back")));
		HX_STACK_LINE(71)
		this->buttons = Array_obj< ::Button >::__new();
		HX_STACK_LINE(72)
		if ((this->fromMain)){
			HX_STACK_LINE(72)
			this->get_parent()->addChild(::MainMenu_obj::__new(this->renderer));
		}
		else{
			HX_STACK_LINE(74)
			this->get_parent()->addChild(::UpgradeMenu_obj::__new(this->renderer));
		}
		HX_STACK_LINE(78)
		this->removeEventListener(::native::events::Event_obj::ADDED_TO_STAGE,this->onAddedToStage_dyn(),null());
		HX_STACK_LINE(80)
		this->get_parent()->removeChild(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(OptionsMenu_obj,back,(void))

Void OptionsMenu_obj::forward( ){
{
		HX_STACK_PUSH("OptionsMenu::forward","OptionsMenu.hx",65);
		HX_STACK_THIS(this);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(OptionsMenu_obj,forward,(void))

Void OptionsMenu_obj::onMouseDown( ::native::events::MouseEvent event){
{
		HX_STACK_PUSH("OptionsMenu::onMouseDown","OptionsMenu.hx",60);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(61)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		Array< ::Button > _g1 = this->buttons;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(61)
		while(((_g < _g1->length))){
			HX_STACK_LINE(61)
			::Button button = _g1->__get(_g);		HX_STACK_VAR(button,"button");
			HX_STACK_LINE(61)
			++(_g);
			HX_STACK_LINE(62)
			button->onClick(this->get_mouseX(),this->get_mouseY(),this->sharedObj);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsMenu_obj,onMouseDown,(void))

Void OptionsMenu_obj::refresh( ){
{
		HX_STACK_PUSH("OptionsMenu::refresh","OptionsMenu.hx",44);
		HX_STACK_THIS(this);
		HX_STACK_LINE(46)
		this->buttons = Array_obj< ::Button >::__new();
		HX_STACK_LINE(47)
		this->renderer->clear();
		HX_STACK_LINE(50)
		this->buttons->push(::Button_obj::__new((Float(this->get_stage()->get_stageWidth()) / Float((int)2)),(Float((this->get_stage()->get_stageHeight() * (int)2)) / Float((int)5)),(int)128,(int)64,hx::ObjectPtr<OBJ_>(this),HX_CSTRING("_mute"),(int)50));
		HX_STACK_LINE(51)
		this->buttons->push(::Button_obj::__new((Float(this->get_stage()->get_stageWidth()) / Float((int)3)),(Float((this->get_stage()->get_stageHeight() * (int)4)) / Float((int)5)),(int)128,(int)64,hx::ObjectPtr<OBJ_>(this),HX_CSTRING("_back"),(int)51));
		HX_STACK_LINE(54)
		{
			HX_STACK_LINE(54)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			Array< ::Button > _g1 = this->buttons;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(54)
			while(((_g < _g1->length))){
				HX_STACK_LINE(54)
				::Button button = _g1->__get(_g);		HX_STACK_VAR(button,"button");
				HX_STACK_LINE(54)
				++(_g);
				HX_STACK_LINE(55)
				button->render(this->renderer,::native::geom::Point_obj::__new((int)0,(int)0));
			}
		}
		HX_STACK_LINE(57)
		this->renderer->draw();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(OptionsMenu_obj,refresh,(void))

Void OptionsMenu_obj::onAddedToStage( ::native::events::Event event){
{
		HX_STACK_PUSH("OptionsMenu::onAddedToStage","OptionsMenu.hx",33);
		HX_STACK_THIS(this);
		HX_STACK_ARG(event,"event");
		HX_STACK_LINE(34)
		this->get_stage()->set_align(::native::display::StageAlign_obj::TOP_LEFT_dyn());
		HX_STACK_LINE(35)
		this->get_stage()->set_scaleMode(::native::display::StageScaleMode_obj::NO_SCALE_dyn());
		HX_STACK_LINE(37)
		this->sharedObj = ::native::net::SharedObject_obj::getLocal(HX_CSTRING("greed"),null(),null());
		HX_STACK_LINE(39)
		this->get_stage()->addEventListener(::native::events::MouseEvent_obj::MOUSE_DOWN,this->onMouseDown_dyn(),null(),null(),null());
		HX_STACK_LINE(41)
		this->refresh();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsMenu_obj,onAddedToStage,(void))


OptionsMenu_obj::OptionsMenu_obj()
{
}

void OptionsMenu_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(OptionsMenu);
	HX_MARK_MEMBER_NAME(fromMain,"fromMain");
	HX_MARK_MEMBER_NAME(mouseDown,"mouseDown");
	HX_MARK_MEMBER_NAME(sharedObj,"sharedObj");
	HX_MARK_MEMBER_NAME(buttons,"buttons");
	HX_MARK_MEMBER_NAME(renderer,"renderer");
	super::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void OptionsMenu_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(fromMain,"fromMain");
	HX_VISIT_MEMBER_NAME(mouseDown,"mouseDown");
	HX_VISIT_MEMBER_NAME(sharedObj,"sharedObj");
	HX_VISIT_MEMBER_NAME(buttons,"buttons");
	HX_VISIT_MEMBER_NAME(renderer,"renderer");
	super::__Visit(HX_VISIT_ARG);
}

Dynamic OptionsMenu_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"back") ) { return back_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"options") ) { return options_dyn(); }
		if (HX_FIELD_EQ(inName,"forward") ) { return forward_dyn(); }
		if (HX_FIELD_EQ(inName,"refresh") ) { return refresh_dyn(); }
		if (HX_FIELD_EQ(inName,"buttons") ) { return buttons; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"fromMain") ) { return fromMain; }
		if (HX_FIELD_EQ(inName,"renderer") ) { return renderer; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"setParent") ) { return setParent_dyn(); }
		if (HX_FIELD_EQ(inName,"mouseDown") ) { return mouseDown; }
		if (HX_FIELD_EQ(inName,"sharedObj") ) { return sharedObj; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"onMouseDown") ) { return onMouseDown_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"onAddedToStage") ) { return onAddedToStage_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"getSharedObject") ) { return getSharedObject_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic OptionsMenu_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"buttons") ) { buttons=inValue.Cast< Array< ::Button > >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"fromMain") ) { fromMain=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"renderer") ) { renderer=inValue.Cast< ::TileRenderer >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"mouseDown") ) { mouseDown=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sharedObj") ) { sharedObj=inValue.Cast< ::native::net::SharedObject >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void OptionsMenu_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("fromMain"));
	outFields->push(HX_CSTRING("mouseDown"));
	outFields->push(HX_CSTRING("sharedObj"));
	outFields->push(HX_CSTRING("buttons"));
	outFields->push(HX_CSTRING("renderer"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("getSharedObject"),
	HX_CSTRING("setParent"),
	HX_CSTRING("options"),
	HX_CSTRING("back"),
	HX_CSTRING("forward"),
	HX_CSTRING("onMouseDown"),
	HX_CSTRING("refresh"),
	HX_CSTRING("onAddedToStage"),
	HX_CSTRING("fromMain"),
	HX_CSTRING("mouseDown"),
	HX_CSTRING("sharedObj"),
	HX_CSTRING("buttons"),
	HX_CSTRING("renderer"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(OptionsMenu_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(OptionsMenu_obj::__mClass,"__mClass");
};

Class OptionsMenu_obj::__mClass;

void OptionsMenu_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("OptionsMenu"), hx::TCanCast< OptionsMenu_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void OptionsMenu_obj::__boot()
{
}

