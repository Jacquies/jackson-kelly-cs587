#include <hxcpp.h>

#ifndef INCLUDED_CollectableColliderState
#include <CollectableColliderState.h>
#endif
#ifndef INCLUDED_Collider
#include <Collider.h>
#endif
#ifndef INCLUDED_GoldPiece
#include <GoldPiece.h>
#endif
#ifndef INCLUDED_ICollidable
#include <ICollidable.h>
#endif
#ifndef INCLUDED_IColliderState
#include <IColliderState.h>
#endif
#ifndef INCLUDED_IRenderable
#include <IRenderable.h>
#endif

Void CollectableColliderState_obj::__construct()
{
HX_STACK_PUSH("CollectableColliderState::new","CollectableColliderState.hx",8);
{
}
;
	return null();
}

CollectableColliderState_obj::~CollectableColliderState_obj() { }

Dynamic CollectableColliderState_obj::__CreateEmpty() { return  new CollectableColliderState_obj; }
hx::ObjectPtr< CollectableColliderState_obj > CollectableColliderState_obj::__new()
{  hx::ObjectPtr< CollectableColliderState_obj > result = new CollectableColliderState_obj();
	result->__construct();
	return result;}

Dynamic CollectableColliderState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< CollectableColliderState_obj > result = new CollectableColliderState_obj();
	result->__construct();
	return result;}

hx::Object *CollectableColliderState_obj::__ToInterface(const type_info &inType) {
	if (inType==typeid( ::IColliderState_obj)) return operator ::IColliderState_obj *();
	return super::__ToInterface(inType);
}

Void CollectableColliderState_obj::collide( ::ICollidable collidingObject,::ICollidable otherCollidingObject){
{
		HX_STACK_PUSH("CollectableColliderState::collide","CollectableColliderState.hx",10);
		HX_STACK_THIS(this);
		HX_STACK_ARG(collidingObject,"collidingObject");
		HX_STACK_ARG(otherCollidingObject,"otherCollidingObject");
		HX_STACK_LINE(10)
		if (((otherCollidingObject->getCollider()->getType() == ::Collider_obj::TYPE_PLAYER))){
			HX_STACK_LINE(11)
			(hx::TCast< GoldPiece >::cast(collidingObject))->collect();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(CollectableColliderState_obj,collide,(void))


CollectableColliderState_obj::CollectableColliderState_obj()
{
}

void CollectableColliderState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(CollectableColliderState);
	HX_MARK_END_CLASS();
}

void CollectableColliderState_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic CollectableColliderState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"collide") ) { return collide_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic CollectableColliderState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void CollectableColliderState_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("collide"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(CollectableColliderState_obj::__mClass,"__mClass");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(CollectableColliderState_obj::__mClass,"__mClass");
};

Class CollectableColliderState_obj::__mClass;

void CollectableColliderState_obj::__register()
{
	Static(__mClass) = hx::RegisterClass(HX_CSTRING("CollectableColliderState"), hx::TCanCast< CollectableColliderState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void CollectableColliderState_obj::__boot()
{
}

