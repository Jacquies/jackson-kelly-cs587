package engine.interfaces;

/**
* @author Jackson Kelly
* 
* Something that needs to be updated every frame.
*/
interface ITickable {
	public function tick(deltaTime:Int):Void;
}