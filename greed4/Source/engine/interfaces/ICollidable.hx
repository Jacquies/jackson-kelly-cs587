package engine.interfaces;
import engine.objects.Collider;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
* @author Jackson Kelly
* 
* An object that can collide with other objects.
*/

interface ICollidable {
	private var collider:Collider;
	
	public function getCollider():Collider;
	public function collide(otherCollidable:ICollidable):Void;
}