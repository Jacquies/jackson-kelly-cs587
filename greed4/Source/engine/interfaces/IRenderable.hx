package engine.interfaces;
import engine.objects.Animation;
import nme.geom.Point;

/**
* @author Jackson Kelly
* 
* Something that needs to be rendered.
*/
interface IRenderable {
	public function render(deltaTime:Int, camera:Point):Void;
}