package engine.menuItems;
import engine.managers.TileRenderer;
import game.level.Level;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.events.TouchEvent;
import nme.geom.Point;
import nme.net.SharedObject;
import nme.system.System;
import nme.Lib;

/**
* @author Jackson Kelly
* 
* The main menu of the game. Has the buttons 'resume game', 'new game', and 'exit'.
*/
class MainMenu extends Sprite {
	
	private var renderer:TileRenderer;							//The renderer with which to render everything.
	private var buttons:Array<Button>;							//The buttons on this menu.
	private var scale:Float;									//The amount that this menu should be scaled to fit the screen.
	private var clickable:Bool;									//Whether or not the buttons in this menu can be clicked.
	
	private static var initialized:Bool;						//Whether or not the game has been initialized (enables different performance for the first time we see the main menu).
	
	public function new(tileRenderer:TileRenderer):Void {
		super();
		
		renderer = tileRenderer;
		
		addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
	}
	
	private function onAddedToStage (event:Event):Void {
		if (MainMenu.initialized) {
			clickable = false;
		}else {
			clickable = true;
			MainMenu.initialized = true;
		}
		
		#if flash
		stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		#elseif android
		stage.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
		stage.addEventListener(TouchEvent.TOUCH_END, onTouchEnd);
		#end
		
		refresh();
	}
	
	public function refresh():Void {
		//Create and add buttons
		buttons = new Array<Button>();
		renderer.clear();
		
		//Background
		#if flash
			for (i in 0...Math.ceil(TileRenderer.stageWidth / TileRenderer.widths[58])+1) {
				for (j in 0...Math.ceil(TileRenderer.stageHeight / TileRenderer.heights[58])+1) {
					buttons.push(new Button(TileRenderer.widths[58] * i, TileRenderer.heights[58] * j, 58, 1, renderer, dummyFunction));
				}
			}
		#elseif android
			scale = Math.max(TileRenderer.stageWidth / TileRenderer.widths[58], TileRenderer.stageHeight / TileRenderer.heights[58]);
			buttons.push(new Button(TileRenderer.stageWidth / 2, TileRenderer.stageHeight / 2, 58, scale, renderer, dummyFunction));
		#end
		//Banner
		scale = Math.min(TileRenderer.stageWidth / TileRenderer.widths[29], TileRenderer.stageHeight * 2 / (5 * TileRenderer.heights[29]));
		buttons.push(new Button(TileRenderer.stageWidth / 2, TileRenderer.stageHeight / 5, 29, scale, renderer, dummyFunction));		//This is the banner. Make a sprite for it and change the ID (and the dimensions);
		//Buttons
		scale = Math.min(TileRenderer.stageWidth / (2 * TileRenderer.widths[47]), TileRenderer.stageHeight / (2 * TileRenderer.heights[47]));
		buttons.push(new Button(TileRenderer.stageWidth / 2, TileRenderer.stageHeight * 3 / 6, 47, scale, renderer, resumeGame));
		buttons.push(new Button(TileRenderer.stageWidth / 2, TileRenderer.stageHeight * 4 / 6, 48, scale, renderer, newGame));
		buttons.push(new Button(TileRenderer.stageWidth / 2, TileRenderer.stageHeight * 5 / 6, 31, scale, renderer, exit));
		
		renderer.draw();
	}
	
	private function resumeGame():Void {
		buttons.splice(0, buttons.length);
		parent.addChild(new Level(renderer));
		
		removeEventListener (Event.ADDED_TO_STAGE, onAddedToStage);
		#if flash
		stage.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		#elseif android
		stage.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
		#end
		
		parent.removeChild(this);
	}
	
	private function newGame():Void {
		var localData:SharedObject;
		localData = SharedObject.getLocal("greed");
		localData.data.newGame = true;
		localData.flush();
		resumeGame();
	}
	
	private function exit():Void {
		System.exit(0);
	}
	
	//This whole thing is awkward. When clicking the main menu button from the death screen, the click/touch is registered here, since this is loaded when it's clicked. In order to avoid this, you have to lift your finger/mouse before it will work.
	#if flash
	public function onMouseDown(event:MouseEvent):Void {
		if (clickable){
			for (button in buttons){
				button.onClick(mouseX, mouseY);
			}
		}
	}
	
	public function onMouseUp(event:MouseEvent):Void {
		clickable = true;
	}
	#elseif android
	public function onTouchBegin(event:TouchEvent):Void {
		if (clickable){
			for (button in buttons){
				button.onClick(mouseX, mouseY);
			}
		}
	}
	
	public function onTouchEnd(event:TouchEvent):Void {
		clickable = true;
	}
	#end
	
	//A dummy function to pass to dummy buttons.
	private function dummyFunction():Void {}
}