package engine.menuItems;
import engine.managers.TileRenderer;
import engine.objects.HealthBar;
import engine.objects.Scoreboard;
import game.level.Level;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.events.TouchEvent;
import nme.geom.Point;
import nme.net.SharedObject;

/**
* @author Jackson Kelly
* 
* The upgrade menu shown between levels. This is where players can spend the money they earned during the levels in order to upgrade themselves.
*/
class UpgradeMenu extends Sprite {
	
	private var renderer:TileRenderer;						//The TileRenderer with which to render everything.
	private var buttons:Array<Button>;						//The buttons on this menu.
	private var localData:SharedObject;						//The local data, in order to determine prices.
	private var scoreboard:Scoreboard;						//The scoreboard, showing your current gold.
	private var healthBar:HealthBar;						//The healthbar, showing your current health.
	private var scale:Float;								//The amount that this menu should be scaled to fit the screen.
	
	public function new(tileRenderer:TileRenderer):Void {
		super();
		
		renderer = tileRenderer;
		
		addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
	}
	
	private function onAddedToStage (event:Event):Void {
		localData = SharedObject.getLocal("greed");
		
		#if flash
		stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		#elseif android
		stage.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
		#end
		
		scale = 1;
		
		//Make scoreboard
		scoreboard = new Scoreboard(renderer);
		scoreboard.update(localData.data.gold);
		
		//Make healthBar
		healthBar = new HealthBar(renderer);
		healthBar.update(localData.data.health);
		
		refresh();
	}
	
	public function refresh():Void {
		//Create and add buttons
		buttons = new Array<Button>();
		renderer.clear();
		var cost:Int;
		
		//Background
		#if flash
			for (i in 0...Math.ceil(TileRenderer.stageWidth / TileRenderer.widths[58])+1) {
				for (j in 0...Math.ceil(TileRenderer.stageHeight / TileRenderer.heights[58])+1) {
					buttons.push(new Button(TileRenderer.widths[58] * i, TileRenderer.heights[58] * j, 58, 1, renderer, dummyFunction));
				}
			}
		#elseif android
			scale = Math.max(TileRenderer.stageWidth / TileRenderer.widths[58], TileRenderer.stageHeight / TileRenderer.heights[58]);
			buttons.push(new Button(TileRenderer.stageWidth / 2, TileRenderer.stageHeight / 2, 58, scale, renderer, dummyFunction));
		#end
		
		//Banner
		scale = Math.min(TileRenderer.stageWidth/TileRenderer.widths[30], TileRenderer.stageHeight/(5*TileRenderer.heights[30]));
		buttons.push(new Button(TileRenderer.stageWidth / 2, TileRenderer.stageHeight / 10, 30, scale, renderer, dummyFunction));
		
		scale = Math.min(TileRenderer.stageWidth / TileRenderer.widths[15], TileRenderer.stageHeight / TileRenderer.heights[15]) / 10.;
		
		//Health (health banner, health icons, price, purchase button).
		buttons.push(new Button(TileRenderer.stageWidth*3/20, TileRenderer.stageHeight * 8 / 25, 17, scale, renderer, dummyFunction));
		for (i in 0...localData.data.health) {
			buttons.push(new Button(TileRenderer.stageWidth*(3.5 + i)/10, TileRenderer.stageHeight * 8 / 25, 18, scale, renderer, dummyFunction));
		}
		for (i in localData.data.health...3) {
			buttons.push(new Button(TileRenderer.stageWidth*(3.5 + i)/10, TileRenderer.stageHeight * 8 / 25, 19, scale, renderer, dummyFunction));
		}
		if (localData.data.health < 3) {
			cost = Math.floor(100 * localData.data.health);
			scoreboard.renderNumber(TileRenderer.stageWidth * 13 / 20, TileRenderer.stageHeight * 8 / 25 - TileRenderer.widths[15] / 4, scale, cost);
			if (localData.data.gold >= cost){
				buttons.push(new Button(TileRenderer.stageWidth*9/10, TileRenderer.stageHeight * 8 / 25, 15, scale, renderer, upgradeHealth));
			}else {
				buttons.push(new Button(TileRenderer.stageWidth*9/10, TileRenderer.stageHeight * 8 / 25, 16, scale, renderer, dummyFunction));
			}
		}
		
		//Speed
		buttons.push(new Button(TileRenderer.stageWidth*3/20, TileRenderer.stageHeight * 11 / 25, 20, scale, renderer, dummyFunction));
		for (i in 0...Math.floor(localData.data.speed - 4)) {
			buttons.push(new Button(TileRenderer.stageWidth*(3.5 + i)/10, TileRenderer.stageHeight * 11 / 25, 21, scale, renderer, dummyFunction));
		}
		for (i in Math.floor(localData.data.speed - 4)...3) {
			buttons.push(new Button(TileRenderer.stageWidth*(3.5 + i)/10, TileRenderer.stageHeight * 11 / 25, 22, scale, renderer, dummyFunction));
		}
		if (localData.data.speed < 7){
			cost = Math.floor(100 * (localData.data.speed - 3));
			scoreboard.renderNumber(TileRenderer.stageWidth*13/20, TileRenderer.stageHeight * 11 / 25, scale, cost);
			if (localData.data.gold >= cost){
				buttons.push(new Button(TileRenderer.stageWidth*9/10, TileRenderer.stageHeight * 11 / 25, 15, scale, renderer, upgradeSpeed));
			}else {
				buttons.push(new Button(TileRenderer.stageWidth*9/10, TileRenderer.stageHeight * 11 / 25, 16, scale, renderer, dummyFunction));
			}
		}
		
		//Collection Radius
		buttons.push(new Button(TileRenderer.stageWidth*3/20, TileRenderer.stageHeight * 14 / 25, 23, scale, renderer, dummyFunction));
		for (i in 0...Math.floor((localData.data.collectRadius-10)/4)) {
			buttons.push(new Button(TileRenderer.stageWidth*(3.5 + i)/10, TileRenderer.stageHeight * 14 / 25, 24, scale, renderer, dummyFunction));
		}
		for (i in Math.floor((localData.data.collectRadius-10)/4)...3) {
			buttons.push(new Button(TileRenderer.stageWidth*(3.5 + i)/10, TileRenderer.stageHeight * 14 / 25, 25, scale, renderer, dummyFunction));
		}
		if (localData.data.collectRadius < 19){
			cost = Math.floor(50 * (localData.data.collectRadius - 10) / 4 + 50);
			scoreboard.renderNumber(TileRenderer.stageWidth*13/20, TileRenderer.stageHeight * 14 / 25, scale, cost);
			if (localData.data.gold >= cost){
				buttons.push(new Button(TileRenderer.stageWidth*9/10, TileRenderer.stageHeight * 14 / 25, 15, scale, renderer, upgradeCollectRadius));
			}else {
				buttons.push(new Button(TileRenderer.stageWidth*9/10, TileRenderer.stageHeight * 14 / 25, 16, scale, renderer, dummyFunction));
			}
		}
		
		//Aggro Radius
		buttons.push(new Button(TileRenderer.stageWidth*3/20, TileRenderer.stageHeight * 17 / 25, 26, scale, renderer, dummyFunction));
		for (i in 0...Math.floor(-(localData.data.aggroRadius-150)/15)) {
			buttons.push(new Button(TileRenderer.stageWidth*(3.5 + i)/10, TileRenderer.stageHeight * 17 / 25, 27, scale, renderer, dummyFunction));
		}
		for (i in Math.floor(-(localData.data.aggroRadius-150)/15)...3) {
			buttons.push(new Button(TileRenderer.stageWidth*(3.5 + i)/10, TileRenderer.stageHeight * 17 / 25, 28, scale, renderer, dummyFunction));
		}
		if (localData.data.aggroRadius > 105){
			cost = Math.floor(50 * -(localData.data.aggroRadius - 165) / 15);
			scoreboard.renderNumber(TileRenderer.stageWidth*13/20, TileRenderer.stageHeight * 17 / 25, scale, cost);
			if (localData.data.gold >= cost){
				buttons.push(new Button(TileRenderer.stageWidth*9/10, TileRenderer.stageHeight * 17 / 25, 15, scale, renderer, upgradeAggroRadius));
			}else {
				buttons.push(new Button(TileRenderer.stageWidth*9/10, TileRenderer.stageHeight * 17 / 25, 16, scale, renderer, dummyFunction));
			}
		}
		
		scale = Math.min(TileRenderer.stageHeight / (5 * TileRenderer.heights[14]), TileRenderer.stageWidth / (2 * TileRenderer.widths[52]));
		
		buttons.push(new Button(TileRenderer.widths[52] * scale / 2, TileRenderer.stageHeight * 9 / 10, 52, scale, renderer, saveAndExit));
		buttons.push(new Button(TileRenderer.stageWidth - TileRenderer.widths[14] * scale / 2, TileRenderer.stageHeight * 9 / 10, 14, scale, renderer, nextLevel));
		
		//Make scoreboard
		scoreboard = new Scoreboard(renderer);
		scoreboard.update(localData.data.gold);
		
		//Make healthBar
		healthBar = new HealthBar(renderer);
		healthBar.update(localData.data.health);
		
		renderer.draw();
	}
	
	private function upgradeHealth():Void {
		localData.data.gold -= Math.floor(100 * localData.data.health);
		localData.data.health++;
		localData.flush();
		healthBar.update(localData.data.health);
		refresh();
	}
	
	private function upgradeSpeed():Void {
		localData.data.gold -= Math.floor(100 * (localData.data.speed - 3));
		localData.data.speed++;
		localData.flush();
		refresh();
	}
	
	private function upgradeCollectRadius():Void {
		localData.data.gold -= Math.floor(50 * (localData.data.collectRadius - 10) / 4 + 50);
		localData.data.collectRadius += 4;
		localData.flush();
		refresh();
	}
	
	private function upgradeAggroRadius():Void {
		localData.data.gold -= Math.floor(50 * -(localData.data.aggroRadius - 165) / 15);
		localData.data.aggroRadius -= 15;
		localData.flush();
		refresh();
	}
	
	private function saveAndExit():Void {
		localData.flush();
		
		buttons.splice(0, buttons.length);
		parent.addChild(new MainMenu(renderer));
		
		removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		#if flash
		stage.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		#elseif android
		stage.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
		#end
		
		parent.removeChild(this);
	}
	
	private function nextLevel():Void {
		buttons.splice(0, buttons.length);
		parent.addChild(new Level(renderer));
		
		removeEventListener (Event.ADDED_TO_STAGE, onAddedToStage);
		#if flash
		stage.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		#elseif android
		stage.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
		#end
		
		parent.removeChild(this);
	}
	
	#if flash
	public function onMouseDown(event:MouseEvent):Void {
		for (button in buttons)
			button.onClick(mouseX, mouseY);
	}
	#elseif android
	public function onTouchBegin(event:TouchEvent):Void {
		for (button in buttons)
			button.onClick(mouseX, mouseY);
	}
	#end
	
	//A dummy function to pass to dummy buttons.
	private function dummyFunction():Void {}
}