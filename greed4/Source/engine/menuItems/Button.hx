package engine.menuItems;
import engine.managers.TileRenderer;
import nme.geom.Point;

/**
* @author Jackson Kelly
* 
* A button to be used in all of the menus.
*/
class Button {
	private var position:Point;						//The position of the button (in screenspace).
	private var width:Float;						//The width of the button.
	private var height:Float;						//The height of the button.
	private var renderer:TileRenderer;				//The TileRenderer with which to render this button.
	private var onPressFunction:Void->Void;			//The function that will be run when this button is pressed.
	
	public function new(xPos:Float, yPos:Float, buttonSpriteID:Int, scale:Float, tileRenderer:TileRenderer, func:Void->Void):Void {
		position = new Point(xPos, yPos);
		#if flash
			//Don't scale the hitbox of the button, since the sprite isn't scaled.
			width = TileRenderer.widths[buttonSpriteID];
			height = TileRenderer.heights[buttonSpriteID];
		#elseif android
			width = TileRenderer.widths[buttonSpriteID] * scale;
			height = TileRenderer.heights[buttonSpriteID] * scale;
		#end
		renderer = tileRenderer;
		
		//Determine what function should be run when this button is pressed.
		onPressFunction = func;
		
		tileRenderer.addTile(xPos, yPos, buttonSpriteID, 0, scale, 2);
	}
	
	public function onClick(clickedX:Float, clickedY:Float):Void {
		//If the click is within the bounds of the button, run the appropriate function.
		if (Math.abs(position.x - clickedX) < width / 2 && Math.abs(position.y - clickedY) < height / 2) {
			onPressFunction();
		}
	}
}