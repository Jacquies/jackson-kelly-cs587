package engine.objects;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
* @author Jackson Kelly
* 
* An data structure that contains all collision data for a collidable object.
*/

class Collider {
	private var boundingBox:Rectangle;						//The box that represents thecollision of this collider.
	private var type:Int;									//The type of this collider (player, enemy, coin, wall, end, projectile).
	
	//The different types of colliders.
	public static var TYPE_PLAYER:Int = 0;
	public static var TYPE_ENEMY:Int = 1;
	public static var TYPE_COIN:Int = 2;
	public static var TYPE_WALL:Int = 3;
	public static var TYPE_END:Int = 4;
	public static var TYPE_PROJECTILE:Int = 5;
	
	//Note that the x and y refer to the center of the collision box, not the upper left corner. This is different from the way Rectangle itself works.
	public function new(x:Float, y:Float, width:Float, height:Float, colliderType:Int):Void {
		boundingBox = new Rectangle(x - width / 2, y - height / 2, width, height);
		
		type = colliderType;
	}
	
	public function getBoundingBox():Rectangle {
		return boundingBox;
	}
	
	public function getType():Int {
		return type;
	}
	
	public function setX(x:Float):Void {
		boundingBox.x = x - boundingBox.width / 2;
	}
	
	public function setY(y:Float):Void {
		boundingBox.y = y - boundingBox.height / 2;
	}
	
	//Whether or not this collider collides with otherCollider.
	public function collides(otherCollider:Collider):Bool {
		return boundingBox.intersects(otherCollider.getBoundingBox());
	}
}