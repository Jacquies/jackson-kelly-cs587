package engine.objects;
import engine.managers.TileRenderer;
import nme.display.Stage;
import nme.geom.Point;

/**
* @author Jackson Kelly
* 
* The scoreboard, which is to be displayed in the top right corner, to let the player know their current score (measured in $).
*/

class Scoreboard {
	private var spriteIDs:Array<Int>;						//The IDs of the sprites used.
	private var renderer:TileRenderer;						//The TileRenderer with which to render everything.
	
	public function new(tileRenderer:TileRenderer):Void {
		renderer = tileRenderer;
		
		spriteIDs = new Array <Int>();
	}
	
	public function update(gold):Void {
		var diff:Int = Math.floor(Math.log(gold) / Math.log(10) + 2) - spriteIDs.length;
		
		//When there's 0 gold, the math gets wonky (log(0)=-INF)
		if (gold == 0)
			diff = 2 - spriteIDs.length;
		
		if (diff > 0) {
			for (i in 0...diff) {
				spriteIDs.push(renderer.addTile(0, 0, 0, 0, 1, 2));
			}
		}else if (diff < 0) {
			for (i in 0... -diff) {
				renderer.remove(spriteIDs[i], 2);
			}
			spriteIDs.splice(0, -diff);
		}
		
		//We should now have the correct number of animation IDs. Update them in the renderer
		for (i in 0...spriteIDs.length - 1) {
			renderer.updateObject(spriteIDs[i], TileRenderer.stageWidth - 28 * (i+.5), 20, Math.floor((gold / Math.pow(10, i)) % 10), 0, 1, 2);
		}
		renderer.updateObject(spriteIDs[spriteIDs.length - 1], TileRenderer.stageWidth - 28 * (spriteIDs.length - .5), 20, 10, 0, 1, 2);
	}
	
	
	//Render a number on the screen.
	public function renderNumber(x:Float, y:Float, scale, num:Int):Void {
		var numDigits = Math.floor(Math.log(num) / Math.log(10) + 1);
		for (i in 0...numDigits) {
			renderer.addTile(x + 28 * (i + .5), y + 16, Math.floor((num / Math.pow(10, numDigits - i - 1)) % 10), 0, scale, 2);
		}
	}
}