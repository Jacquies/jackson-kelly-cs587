package engine.objects;
import engine.interfaces.IRenderable;
import engine.managers.TileRenderer;
import engine.managers.SceneManager;
import nme.geom.Point;

/**
* @author Jackson Kelly
* 
* A series of sprites that are to be rendered and updated every frame.
*/
class Animation implements IRenderable{
	private var startFrame:Int;							//The frame at which this animation starts (in the TileRenderer).
	private var animationLength:Int;					//The length of the animation.
	private var animationSpeed:Int;						//The speed at which the animation will play (in frames per second).
	private var currentFrame:Int;						//The frame that is currently being rendered (from 0 to animationLength).
	private var currentTime:Float;						//The amount of time that has passed since the last time the frame changed.
	private var position:Point;							//The position of this animation, in world coordinates.
	private var rotation:Float;							//The rotation of this animation.
	private var layer:Int;								//The layer on which this animation should be rendered (0 - background, 1 - midground, 2 - foreground).
	private var renderer:TileRenderer;					//The TileRenderer that will be used to render this animation.
	private var ID:Int;									//The ID of this animation in the TileRenderer.
	private var loops:Bool;								//A boolean representing whether or not the animation should loop
	private var rendering:Bool;							//A boolean representing whether or not this animation is currently being rendered.
	private var fixed:Bool;								//A boolean representing whether or not this animation moves with the camera.
	
	public function new(startingFrame:Int, length:Int, framesPerSecond:Int, x:Float, y:Float, rot:Float, displayLayer:Int, tileRenderer:TileRenderer, loop:Bool=true):Void {
		startFrame = startingFrame;
		animationLength = length;
		animationSpeed = framesPerSecond + 1;
		currentFrame = 0;
		currentTime = 0;
		position = new Point(x, y);
		rotation = rot;
		layer = displayLayer;
		loops = (length == 0)?(false):(loop);			//Set loops. If the length of the animation is 0, then it doesn't loop.
		rendering = false;
		fixed = false;
		
		renderer = tileRenderer;
		addToRenderSet();
	}
	
	public function update(x:Float, y:Float, rot:Float):Void {
		position.x = x;
		position.y = y;
		rotation = rot;
	}
	
	public function render(deltaTime:Int, camera:Point):Void {
		//Figure out what frame should be rendered.
		currentTime += deltaTime;
		if (currentTime > 1000. / animationSpeed) {
			currentTime -= 1000. / animationSpeed;
			currentFrame++;
			if (currentFrame > animationLength && loops) {
				currentFrame = 0;
			}else if (currentFrame > animationLength) {
				currentFrame = animationLength;
			}
		}
		
		if (fixed) {
			renderer.updateObject(ID, position.x, position.y, startFrame + currentFrame, rotation, 1, layer);
		}else {
			renderer.updateObject(ID, (position.x - camera.x) * SceneManager.scale + TileRenderer.stageWidth / 2, (position.y - camera.y) * SceneManager.scale + TileRenderer.stageHeight / 2, startFrame + currentFrame, rotation, SceneManager.scale, layer);
		}
	}
	
	//Set whether or not the animation should be fixed to the screen rather than to the level.
	public function setFixed(isFixed:Bool):Void {
		fixed = isFixed;
	}
	
	//Stop this animation from being rendered.
	public function removeFromRenderSet():Void {
		if (rendering){
			renderer.remove(ID, layer);
			rendering = false;
		}
	}
	
	//Begin rendering this animation again.
	public function addToRenderSet():Void {
		if (!rendering) {
			ID = renderer.addTile(position.x * SceneManager.scale, position.y * SceneManager.scale, startFrame, rotation, SceneManager.scale, layer);
			rendering = true;
		}
	}
	
	public function setRotation(rot:Float):Void {
		rotation = rot % 360;
	}
	
	public function getRotation():Float {
		return rotation;
	}
	
	public function setX(x:Float):Void {
		position.x = x;
	}
	
	public function setY(y:Float):Void {
		position.y = y;
	}
	
	public function getPosition():Point {
		return position;
	}
	
	public function getCurrentFrame():Int {
		return currentFrame;
	}
	
	public function isFinished():Bool {
		return (currentFrame == animationLength);
	}
	
	public function restartAnimation():Void {
		currentFrame = 0;
	}
}