package engine.objects;
import engine.managers.SceneManager;
import engine.managers.TileRenderer;
import nme.display.Stage;
import nme.geom.Point;

/**
* @author Jackson Kelly
* 
* The health bar, which is to be displayed in the top left corner, to let the player know their current health (1-3).
*/

class HealthBar {
	private var spriteIDs:Array<Int>;						//The IDs of the sprites used.
	private var renderer:TileRenderer;						//The TileRenderer with which to render everything.
	
	public function new(tileRenderer:TileRenderer):Void {
		renderer = tileRenderer;
		
		spriteIDs = new Array <Int>();
	}
	
	public function update(health):Void {
		var diff:Int = health - spriteIDs.length;
		
		if (diff > 0) {
			for (i in 0...diff) {
				spriteIDs.push(renderer.addTile(0, 0, 0, 0, 1, 2));
			}
		}else if (diff < 0) {
			for (i in 0... -diff) {
				renderer.remove(spriteIDs[i], 2);
			}
			
			//Play the 'heart death' animation.
			var heartDeath:Animation = new Animation(35, 11, 30, 40 * (spriteIDs.length - 0.5), 20, 0, 2, renderer, false);
			heartDeath.setFixed(true);
			SceneManager.currentSceneManager.addToRenderSet(heartDeath);
			spriteIDs.splice(0, -diff);
		}
		
		//We should now have the correct number of animation IDs. Update them in the renderer
		for (i in 0...spriteIDs.length) {
			renderer.updateObject(spriteIDs[i], 40 * (i+.5), 20, 11, 0, 1, 2);
		}
	}
}