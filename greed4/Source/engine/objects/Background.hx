package engine.objects;
import engine.interfaces.IRenderable;
import engine.managers.TileRenderer;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
* @author Jackson Kelly
* 
* The background. Basically the simplest renderable object.
*/
class Background implements IRenderable {
	private var animation:Animation;						//The animation that represents the background
	
	public function new(spawnPoint:Point, tileRenderer:TileRenderer):Void {
		var tempRand:Float = Math.random();
		var spriteNum:Int;
		
		animation = new Animation(53, 0, 30, spawnPoint.x, spawnPoint.y, 90*Math.floor(Math.random()*4), 0, tileRenderer);
	}
	
	public function render(deltaTime:Int, camera:Point):Void {
		animation.render(deltaTime, camera);
	}
	
}