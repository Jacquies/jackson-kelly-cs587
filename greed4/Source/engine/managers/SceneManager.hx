package engine.managers;
import engine.interfaces.ICollidable;
import engine.interfaces.IRenderable;
import engine.interfaces.ITickable;
import engine.objects.HealthBar;
import engine.objects.Scoreboard;
import game.level.Level;
import game.objects.enemy.Projectile;
import nme.geom.Point;
import nme.net.SharedObject;

/**
* @author Jackson Kelly
* 
* Used to manage various things that are happening in the scene. Paired with level, but more general.
*/
 
class SceneManager {
	private var renderer:TileRenderer;					//The TileRenderer
	private var collisionSet:Array<ICollidable>;		//The set of all collidables
	private var renderSet:Array<IRenderable>;			//The set of all things to be rendered
	private var tickableSet:Array<ITickable>;			//The set of all things that need to be ticked
	private var mouseDown:Bool;							//Whether or not the mouse is down
	private var camera:Point;							//A point representing where the center of the camera is pointing
	private var mousePosition:Point;					//A point representing where the mouse (or touch) is [relative to the screen]
	private var localData:SharedObject;					//Data stored on the local machine
	private var idle:Bool;								//A boolean representing whether or not the level has 'started' yet
	private var scoreboard:Scoreboard;					//The scoreboard. Shows the score.
	private var healthBar:HealthBar;					//The health bar. Shows your health.
	#if flash
	//Projectiles were too buggy in android, so they're only enabled in flash.
	private var projectilePool:Array <Projectile>;		//A pool of projectiles. This is so that I don't have to go through the overhead of creating and destroying new projectiles all the time.
	private var availableProjectiles:Array<Bool>;		//An array which tells whether a current projectile from the pool is being used or not.
	#end
	private var levelOver:Bool;							//Determines whether or not the level is over.
	
	public static var scale:Float;						//Represents the amount that everything needs to be scaled.
	public static var usingFlash:Int;					//An Int representing whether the target is flash (1) or android (-1)
	public static var currentSceneManager:SceneManager;	//A reference to the singleton sceneManager object.
	
	public function new(tileRenderer:TileRenderer):Void {
		renderer = tileRenderer;
		currentSceneManager = this;
		#if flash
		usingFlash = 1;
		#else
		usingFlash = -1;
		#end
	}
	
	public function initialize():Void {
		//Determine the scale for the game
		#if flash
			scale = 1;
		#elseif android
			if (TileRenderer.stageHeight > 350 && TileRenderer.stageWidth > 350) {
				scale = 1;
				if (TileRenderer.DPI > 200)
					scale = TileRenderer.DPI / 200;
			}else {
				scale = Math.min(TileRenderer.stageHeight, TileRenderer.stageWidth) / 350;
			}
		#end
		
		idle = true;
		mouseDown = false;
		camera = new Point(0, 0);
		mousePosition = new Point(0, 0);
		levelOver = false;
		
		//Initialize arrays
		collisionSet = new Array<ICollidable>();
		renderSet = new Array<IRenderable>();
		tickableSet = new Array<ITickable>();
		
		//Get local data
		localData = SharedObject.getLocal("greed");
		if (!localData.data.initialized) {
			Level.initializeLocalData(localData);
		}else if (localData.data.newGame) {
			Level.newGame(localData);
		}
		
		//Make scoreboard
		scoreboard = new Scoreboard(renderer);
		scoreboard.update(localData.data.gold);
		
		//Make healthBar
		healthBar = new HealthBar(renderer);
		healthBar.update(localData.data.health);
		
		#if flash
			//Generate projectile pool
			projectilePool = new Array<Projectile>();
			availableProjectiles = new Array<Bool>();
			for (i in 0...Math.floor(5+2*localData.data.difficulty)) {
				projectilePool.push(new Projectile(0, 0, renderer, i));
				availableProjectiles.push(true);
				projectilePool[i].deinitialize();
			}
		#end
		
		idle = false;
	}
	
	public function tick(deltaTime:Int):Void {
		if (levelOver)
			return;
		
		if (!idle){
			//Collide collidables
			for (i in 0...collisionSet.length) {
				for (j in (i+1)...collisionSet.length) {
					collide(collisionSet[i], collisionSet[j]);
				}
			}
			
			//Tick tickables
			for (tickable in tickableSet) {
				tickable.tick(deltaTime);
			}
		}
		
		//Render renderables
		for (renderable in renderSet) {
			renderable.render(deltaTime, camera);
		}
		renderer.draw();
	}
	
	#if flash
	//Get a projectile from the pool
	public function getProjectile(x:Float, y:Float, rotation:Float):Int {
		for (ID in 0...availableProjectiles.length) {
			if (availableProjectiles[ID]) {
				availableProjectiles[ID] = false;
				addToCollisionSet(projectilePool[ID]);
				addToRenderSet(projectilePool[ID]);
				addToTickableSet(projectilePool[ID]);
				projectilePool[ID].setX(x);
				projectilePool[ID].setY(y);
				projectilePool[ID].setRotation(rotation);
				projectilePool[ID].initialize();
				return ID;
			}
		}
		return -1;
	}
	
	//Remove a projectile from the scene
	public function removeProjectile(ID:Int):Void {
		availableProjectiles[ID] = true;
		removeFromCollisionSet(projectilePool[ID]);
		removeFromRenderSet(projectilePool[ID]);
		removeFromTickableSet(projectilePool[ID]);
		projectilePool[ID].deinitialize();
	}
	#end
	
	public function updateScoreboard():Void {
		scoreboard.update(localData.data.gold);
	}
	
	public function updateHealthBar():Void {
		healthBar.update(localData.data.health);
		scoreboard.update(localData.data.gold);				//The scoreboard sometimes acts up when the healthbar is updated.
	}
	
	public function setMouseDown(isMouseDown:Bool):Void {
		mouseDown = isMouseDown;
	}
	
	public function getMouseDown():Bool {
		return mouseDown;
	}
	
	public function setMousePosition(x:Float, y:Float):Void {
		mousePosition.x = x;
		mousePosition.y = y;
	}
	
	public function getMousePosition():Point {
		return mousePosition;
	}
	
	public function getCameraPosition():Point {
		return camera;
	}
	
	public function setCameraX(x:Float):Void {
		camera.x = x;
	}
	
	public function setCameraY(y:Float):Void {
		camera.y = y;
	}
	
	public function addToCollisionSet(collidable:ICollidable):Void {
		collisionSet.push(collidable);
	}
	
	public function removeFromCollisionSet(collidable:ICollidable):Void {
		collisionSet.remove(collidable);
	}
	
	public function clearCollisionSet():Void {
		collisionSet.slice(0, collisionSet.length);
	}
	
	public function addToRenderSet(renderable:IRenderable):Void {
		renderSet.push(renderable);
	}
	
	public function removeFromRenderSet(renderable:IRenderable):Void {
		renderSet.remove(renderable);
	}
	
	public function clearRenderSet():Void {
		renderSet.slice(0, renderSet.length);
	}
	
	public function addToTickableSet(tickable:ITickable):Void {
		tickableSet.push(tickable);
	}
	
	public function removeFromTickableSet(tickable:ITickable):Void {
		tickableSet.remove(tickable);
	}
	
	public function clearTickableSet():Void {
		tickableSet.slice(0, tickableSet.length);
	}
	
	public function getSharedObject():SharedObject {
		return localData;
	}
	
	public function isIdle():Bool {
		return idle;
	}
	
	public function setIdle(idleState:Bool):Void {
		idle = idleState;
	}
	
	public function setLevelOver(isLevelOver:Bool):Void {
		levelOver = isLevelOver;
	}
	
	//Collide two collidable objects
	public static function collide(collidable1:ICollidable, collidable2:ICollidable):Void {
		if (collidable1.getCollider().collides(collidable2.getCollider())) {
			collidable1.collide(collidable2);
			collidable2.collide(collidable1);
		}
	}
	
	public static function distance(point1:Point, point2:Point):Float {
		return Math.sqrt((point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y));
	}
	
	public static function squareDistance(point1:Point, point2:Point):Float {
		return (point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y);
	}
}