package engine.managers;
import nme.display.Bitmap;
import nme.display.DisplayObject;
import nme.display.Graphics;
import nme.display.Sprite;
import nme.display.Stage;
import nme.display.Tilesheet;
import nme.events.MouseEvent;
import nme.Assets;
import nme.geom.Point;
import nme.geom.Rectangle;
import nme.system.Capabilities;

/**
* @author Jackson Kelly
* 
* Uses a TileSheet to render anything that needs to be rendered.
*/
class TileRenderer {
	private var tilesheet:Tilesheet;					//The tilesheet with which to draw everything.
	private var graphics:Graphics;						//The graphics object on which to draw everything
	private var tileData:Array<Array<Float>>;			//The data for all of the sprites that should be drawn. There are three layers (0 - background, 1 - midground, 2 - foreground).
	private var freeIndices:Array<Array<Int>>;			//The available indices in the tileData array. This lets me remove stuff from the tileData array without changing  all of the references to other things in the tileData array.
	
	public static var stageWidth:Float;					//The width of the stage.
	public static var stageHeight:Float;				//The height of the stage.
	public static var widths:Array<Float>;				//The widths of the tiles.
	public static var heights:Array<Float>;				//The heights of the tiles
	public static var DPI:Float;						//The DPI of the device
	
	public function new (inGraphics:Graphics, displayStage:Stage):Void {
		tilesheet = new Tilesheet (Assets.getBitmapData("assets/sheet.png")/*rotation*/);
		
		tileData = new Array<Array<Float>>();
		freeIndices = new Array<Array<Int>>();
		
		for (i in 0...3) {
			tileData.push(new Array<Float>());
			freeIndices.push(new Array<Int>());
		}
		
		graphics = inGraphics;
		
		stageWidth = displayStage.stageWidth;
		stageHeight = displayStage.stageHeight;
		
		widths = new Array<Float>();
		heights = new Array<Float>();
		
		DPI = Capabilities.screenDPI;
		
		loadAllTiles();
	}
	
	public function loadTile(x:Float, y:Float, width:Float, height:Float, centerX:Float, centerY:Float):Void {
		tilesheet.addTileRect(new Rectangle(x, y, width, height), new Point(centerX, centerY));
	}
	
	//Add a tile to be rendered.
	public function addTile(x:Float, y:Float, SpriteID:Int, rot:Float, scale:Float, layer:Int):Int {
		tileData[layer].push(x);
		tileData[layer].push(y);
		tileData[layer].push(SpriteID);
		
		#if android
			//scale and rotation are only working in android for some reason.
			tileData[layer].push(scale);
			tileData[layer].push(rot * Math.PI / 180);
		#end
		
		//Find the next free index in the freeIndices array.
		for (i in 0...freeIndices[layer].length) {
			if (freeIndices[layer][i] == -1) {
				#if flash
					freeIndices[layer][i] = Math.floor(tileData[layer].length / 3) - 1;
				#elseif android
					freeIndices[layer][i] = Math.floor(tileData[layer].length / 5) - 1;
				#end
				return i;
			}
		}
		
		//If there is none, push another index to the array.
		#if flash
			freeIndices[layer].push(Math.floor(tileData[layer].length / 3) - 1);
		#elseif android
			freeIndices[layer].push(Math.floor(tileData[layer].length / 5) - 1);
		#end
		return freeIndices[layer].length - 1;
	}
	
	//Remove a sprite from being rendered
	public function remove(ID:Int, layer:Int):Void {
		#if flash
			tileData[layer].splice(3 * freeIndices[layer][ID], 3);
		#elseif android
			tileData[layer].splice(4 * freeIndices[layer][ID], 5);
		#end
		
		for (i in 0...freeIndices[layer].length) {
			if (freeIndices[layer][i] > freeIndices[layer][ID]) {
				freeIndices[layer][i]--;
			}
		}
		
		freeIndices[layer][ID] = -1;
	}
	
	//Update a sprite that is being rendered.
	public function updateObject(ID:Int, x:Float, y:Float, SpriteID:Int, rotation:Float, scale:Float, layer:Int):Void {
		#if flash
			var index:Int = freeIndices[layer][ID] * 3;
		#elseif android
			var index:Int = freeIndices[layer][ID] * 5;
		#end
		tileData[layer][index] = x;
		tileData[layer][index + 1] = y;
		tileData[layer][index + 2] = SpriteID;
		
		#if android
			tileData[layer][index + 3] = scale;
			tileData[layer][index + 4] = rotation * Math.PI / 180;
		#end
	}
	
	//Render everything
	public function draw():Void {
		graphics.clear();
		
		for (i in 0...3) {
			#if flash
				tilesheet.drawTiles(graphics, tileData[i]);
			#elseif android
				tilesheet.drawTiles(graphics, tileData[i], false, Tilesheet.TILE_SCALE | Tilesheet.TILE_ROTATION);
			#end
		}
	}
	
	public function clear():Void {
		graphics.clear();
		
		for (i in 0...3) {
			tileData[i].splice(0, tileData[i].length);
			freeIndices[i].splice(0, freeIndices[i].length);
		}
	}
	
	//Loads all of the tiles I plan to use
	public function loadAllTiles():Void {
		var xml:Xml = Xml.parse(Assets.getText("assets/sheet.xml"));
		xml = xml.firstChild();
		var it:Iterator<Xml> = xml.elements();
		
		while (it.hasNext()) {
			xml = it.next();
			loadTile(Std.parseFloat(xml.get("x")), Std.parseFloat(xml.get("y")), Std.parseFloat(xml.get("width")), Std.parseFloat(xml.get("height")), Std.parseFloat(xml.get("width")) / 2, Std.parseFloat(xml.get("height")) / 2);
			widths.push(Std.parseFloat(xml.get("width")));
			heights.push(Std.parseFloat(xml.get("height")));
		}
	}
}