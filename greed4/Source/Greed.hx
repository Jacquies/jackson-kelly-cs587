﻿package;
import nme.display.Sprite;
import nme.display.Tilesheet;
import nme.Lib;

import engine.managers.TileRenderer;
import engine.menuItems.MainMenu;

/**
* @author Jackson Kelly
* 
* The entry point of the game.
*/

class Greed extends Sprite {
	private var tilesheet:Tilesheet;
	private var tileData:Array<Float>;
	//ENTRY POINT
    public static function main ()
    {
		//Puts things on the screen
        var renderer:TileRenderer = new TileRenderer(Lib.current.graphics, Lib.current.stage);
		
		Lib.current.stage.addChild (new MainMenu(renderer));
	}
}