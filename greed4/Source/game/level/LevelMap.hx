package game.level;
import nme.geom.Point;

/**
* @author Jackson Kelly
* 
* The map of the level. Determines what the level looks like, and where coins, enemies, walls, and backgrounds should be.
*/
class LevelMap {
	private var map:Array<Array<Bool>>;						//The map of the level.
	private var mapRadius:Int;								//The radius of the map.
	
	public static var mapScale:Int = 180;					//The number of worldSpace units per map square.
	
	public function new(difficulty:Int):Void {
		mapRadius = Math.floor(Math.sqrt(difficulty)) + 1;
		map = new Array<Array<Bool>>();
		var mapWithIslands:Array<Array<Bool>> = new Array<Array<Bool>>();
		
		//Fill both maps with nothing.
		for (i in 0...(2 * mapRadius + 1)) {
			map[i] = new Array<Bool>();
			mapWithIslands[i] = new Array<Bool>();
			for (j in 0...(2 * mapRadius + 1)) {
				map[i][j] = false;
				mapWithIslands[i][j] = false;
			}
		}
		
		//randomly fill mapWithIslands with dungeon space.
		var distanceFromCenter:Int;
		for (i in 0...map.length) {
			for (j in 0...map[i].length) {
				distanceFromCenter = Math.floor(Math.max(Math.abs(mapRadius-i), Math.abs(mapRadius-j))) - 1;
				
				if (Math.random() > distanceFromCenter / (mapRadius + 0.)) {
					mapWithIslands[i][j] = true;
				}
			}
		}
		
		//Remove islands from mapWithIslands and store in map.
		removeIslands(mapRadius, mapRadius, mapWithIslands);
	}
	
	public function generateEnemies(difficulty:Int):Array<Point> {
		return generateEnemiesOrCoins(difficulty, 2);
	}
	
	public function generateCoins(difficulty:Int):Array<Point> {
		return generateEnemiesOrCoins(difficulty, 4);
	}
	
	public function generateLevelEnd(difficulty):Point {
		return new Point(Math.random() * 540 - 270, Math.random() * 540 - 270);
	}
	
	//Generate the positions of objects within the level.
	private function generateEnemiesOrCoins(difficulty:Int, denseness:Int):Array<Point> {
		var points:Array<Point> = new Array<Point>();
		
		var distanceFromCenter:Int;
		for (i in 0...map.length) {
			for (j in 0...map[i].length) {
				if (map[i][j]){
					distanceFromCenter = Math.floor(Math.max(Math.abs(mapRadius - i), Math.abs(mapRadius - j)));
					
					if (Math.random() < (distanceFromCenter / ((5-denseness) * mapRadius + 0.))*(2*difficulty/(difficulty+1))) {
						points.push(new Point((j - mapRadius + Math.random()) * 180 - 90, (i - mapRadius + Math.random()) * 180 - 90));
					}
				}
			}
		}
		
		return points;
	}
	
	//Generate the positions of the walls in the level.
	public function generateWallPositions(wallDirections:Array<Int>):Array<Point> {
		var points:Array<Point> = new Array<Point>();
		wallDirections.splice(0, wallDirections.length);
		
		for (i in 0...map.length) {
			for (j in 0...map[i].length) {
				if (map[i][j]) {
					//Needs left wall?
					if (j == 0 || !map[i][j - 1]) {
						points.push(new Point((j - mapRadius) * 180 - 90, (i - mapRadius) * 180));
						wallDirections.push(0);
					}
					
					//Needs right wall?
					if (j == map[i].length - 1 || !map[i][j + 1]) {
						points.push(new Point((j - mapRadius) * 180 + 90, (i - mapRadius) * 180));
						wallDirections.push(2);
					}
					
					//Needs top wall?
					if (i == 0 || !map[i - 1][j]) {
						points.push(new Point((j - mapRadius) * 180, (i - mapRadius) * 180 - 90));
						wallDirections.push(3);
					}
					
					//Needs bottom wall?
					if (i == map.length - 1 || !map[i + 1][j]) {
						points.push(new Point((j - mapRadius) * 180, (i - mapRadius) * 180 + 90));
						wallDirections.push(1);
					}
				}
			}
		}
		
		return points;
	}
	
	//Generate the positions of the background floor tiles in the level.
	public function generateBackgroundPositions():Array<Point> {
		var points:Array<Point> = new Array<Point>();
		
		for (i in 0...map.length) {
			for (j in 0...map[i].length) {
				if (map[i][j]) {
					points.push(new Point((j - mapRadius) * 180, (i - mapRadius) * 180));
				}
			}
		}
		
		return points;
	}
	
	//Remove islands from a map, so that it's all connected.
	private function removeIslands(i:Int, j:Int, tempMap:Array<Array<Bool>>):Void {
		map[i][j] = true;
		if ((j < 2 * mapRadius && !map[i][j + 1] && tempMap[i][j + 1])) {
			removeIslands(i, j + 1, tempMap);
		}
		if ((j > 0 && !map[i][j - 1] && tempMap[i][j - 1])) {
			removeIslands(i, j - 1, tempMap);
		}
		if ((i < 2 * mapRadius && !map[i + 1][j] && tempMap[i + 1][j])) {
			removeIslands(i + 1, j, tempMap);
		}
		if ((i > 0 && !map[i - 1][j] && tempMap[i - 1][j])) {
			removeIslands(i - 1, j, tempMap);
		}
	}
}