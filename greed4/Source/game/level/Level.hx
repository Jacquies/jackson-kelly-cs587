package game.level;
import engine.managers.SceneManager;
import engine.managers.TileRenderer;
import engine.menuItems.Button;
import engine.menuItems.MainMenu;
import engine.menuItems.UpgradeMenu;
import engine.objects.Background;
import game.objects.EndOfLevel;
import game.objects.Wall;
import game.objects.enemy.Enemy;
import game.objects.enemy.EnemyFactory;
import game.objects.GoldPiece;
import game.objects.Player;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.events.TouchEvent;
import nme.net.SharedObject;
import nme.geom.Point;
import nme.Lib;
 
/**
* @author Jackson Kelly
* 
* The level itself. Takes care of everything in the level that is specific to this game.
*/

class Level extends Sprite {
	private var renderer:TileRenderer;						//The TileRenderer with which to render everything.
	private var sceneManager:SceneManager;					//The SceneManager that will handle a lot of the level.
	private var map:LevelMap;								//The map of the level.
	private var player:Player;								//The player.
	private var enemies:Array<Enemy>;						//The enemies.
	private var coins:Array<GoldPiece>;						//The coins.
	private var levelEnd:EndOfLevel;						//The end of the level.
	private var walls:Array<Wall>;							//The walls.
	private var backgrounds:Array<Background>;				//The backgrounds. (in flash, the background is tiled, so we ned an array. In android, the background is a single tile scaled to cover the whole screen).
	private var deathScreenCountdown:Int;					//The countdown until the deathscreen appears after you die. Thus, there is a pause after you die before the screen gets covered with the deathscreen.
	private var mainMenuButton:Button;						//The button to return to the main menu after you've died.
	private var previousTickTime:Int;						//The time of the previous tick. This helps us keep track of the time between frames.
	
	public function new(tileRenderer:TileRenderer):Void {
		super();
		
		renderer = tileRenderer;
		
		addEventListener (Event.ADDED_TO_STAGE, onAddedToStage);
	}
	
	private function onAddedToStage(event:Event):Void {
		renderer.clear();
		sceneManager = new SceneManager(renderer);
		sceneManager.initialize();
		previousTickTime = Lib.getTimer();
		
		deathScreenCountdown = 45;
		
		addEventListener(Event.ENTER_FRAME, onEnterFrame);
		#if flash
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		#elseif android
			stage.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
			stage.addEventListener(TouchEvent.TOUCH_END, onTouchEnd);
			stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
		#end
		
		//Make background
		#if flash
			for (i in 0...Math.ceil(TileRenderer.stageWidth / TileRenderer.widths[57])+1) {
				for (j in 0...Math.ceil(TileRenderer.stageHeight / TileRenderer.heights[57])+1) {
					renderer.addTile(TileRenderer.widths[57] * i, TileRenderer.heights[57] * j, 57, 0, 1, 0);
				}
			}
		#elseif android
			renderer.addTile(TileRenderer.stageWidth / 2, TileRenderer.stageHeight / 2, 57, 0, Math.max(TileRenderer.stageWidth, TileRenderer.stageHeight) / TileRenderer.widths[57], 0);
		#end
		
		//Generate map
		map = new LevelMap(sceneManager.getSharedObject().data.difficulty);
		
		//Make end
		levelEnd = new EndOfLevel(map.generateLevelEnd(sceneManager.getSharedObject().data.difficulty), renderer, this);
		sceneManager.addToCollisionSet(levelEnd);
		sceneManager.addToRenderSet(levelEnd);
		
		//Make coins
		var points:Array<Point> = map.generateCoins(sceneManager.getSharedObject().data.difficulty);
		coins = new Array<GoldPiece>();
		for (i in 0...points.length) {
			coins.push(new GoldPiece(points[i], renderer));
			sceneManager.addToCollisionSet(coins[i]);
			sceneManager.addToRenderSet(coins[i]);
		}
		
		//Make enemies
		points = map.generateEnemies(sceneManager.getSharedObject().data.difficulty);
		enemies = new Array<Enemy>();
		for (i in 0...points.length) {
			if (Math.random() < .5) {
				enemies.push(EnemyFactory.getEnemy(points[i], Enemy.TYPE_CHASING_ENEMY, renderer));
			}else {
				#if flash
					if (Math.random() < .6) {
						enemies.push(EnemyFactory.getEnemy(points[i], Enemy.TYPE_CHARGING_ENEMY, renderer));
					}else {
						enemies.push(EnemyFactory.getEnemy(points[i], Enemy.TYPE_RANGED_ENEMY, renderer));
					}
				#elseif android
					enemies.push(EnemyFactory.getEnemy(points[i], Enemy.TYPE_CHARGING_ENEMY, renderer));
				#end
			}
			sceneManager.addToCollisionSet(enemies[i]);
			sceneManager.addToRenderSet(enemies[i]);
			sceneManager.addToTickableSet(enemies[i]);
		}
		
		//Make background
		points = map.generateBackgroundPositions();
		backgrounds = new Array<Background>();
		for (i in 0...points.length) {
			backgrounds.push(new Background(points[i], renderer));
			sceneManager.addToRenderSet(backgrounds[i]);
		}
		
		//Make walls (since rotation isn't working on flash [D:<], just use the background as the walls, since [as of now] the background is just black.
		var directions:Array<Int> = new Array<Int>();
		points = map.generateWallPositions(directions);
		walls = new Array<Wall>();
		for (i in 0...points.length) {
			walls.push(new Wall(points[i], directions[i], renderer));
			sceneManager.addToCollisionSet(walls[i]);
			#if android
				sceneManager.addToRenderSet(walls[i]);
			#end
		}
		
		//Generate Player
		player = new Player(renderer);
		sceneManager.addToCollisionSet(player);
		sceneManager.addToRenderSet(player);
		sceneManager.addToTickableSet(player);
		Enemy.player = player;
		
		sceneManager.getSharedObject().data.difficulty++;
	}
	
	private function onEnterFrame(event:Event):Void {
		//Determine if the deathscreen should appear.
		if (sceneManager.isIdle()){
			deathScreenCountdown--;
		}else {
			deathScreenCountdown = 40;
		}
		if (deathScreenCountdown == 0) {
			new Button(TileRenderer.stageWidth / 2, TileRenderer.stageHeight / 2, 61, SceneManager.scale, renderer, dummyFunction);
			mainMenuButton = new Button(TileRenderer.stageWidth / 2, TileRenderer.stageHeight / 2 + TileRenderer.heights[51]*SceneManager.scale/2, 51, SceneManager.scale, renderer, returnToMainMenu);
		}
		
		//Determine deltaTime
		var deltaTime:Int = Lib.getTimer() - previousTickTime;
		previousTickTime = Lib.getTimer();
		
		#if flash
		sceneManager.setMousePosition(stage.mouseX, stage.mouseY);
		#end
		
		sceneManager.tick(deltaTime);
	}
	
	#if flash
	private function onMouseDown(event:MouseEvent):Void {
		sceneManager.setMouseDown(true);
		sceneManager.setMousePosition(mouseX, mouseY);
		if (deathScreenCountdown <= 0) {
			mainMenuButton.onClick(mouseX, mouseY);
		}
	}
	
	private function onMouseUp(event:MouseEvent):Void {
		sceneManager.setMouseDown(false);
	}
	#elseif android
	private function onTouchBegin(event:TouchEvent):Void {
		sceneManager.setMouseDown(true);
		sceneManager.setMousePosition(mouseX, mouseY);
		if (deathScreenCountdown <= 0) {
			mainMenuButton.onClick(mouseX, mouseY);
		}
	}
	
	private function onTouchEnd(event:TouchEvent):Void {
		sceneManager.setMouseDown(false);
	}
	
	private function onTouchMove(event:TouchEvent):Void {
		sceneManager.setMousePosition(event.localX, event.localY);
	}
	#end
	
	public function endLevel():Void {
		cleanup();
		
		parent.addChild(new UpgradeMenu(renderer));
		
		parent.removeChild(this);
	}
	
	private function returnToMainMenu():Void {
		cleanup();
		
		parent.addChild(new MainMenu(renderer));
		
		parent.removeChild(this);
	}
	
	private function cleanup():Void {
		sceneManager.setLevelOver(true);
		
		removeEventListener (Event.ADDED_TO_STAGE, onAddedToStage);
		removeEventListener (Event.ENTER_FRAME, onEnterFrame);
		#if flash
			stage.removeEventListener (MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.removeEventListener (MouseEvent.MOUSE_UP, onMouseUp);
		#elseif android
			stage.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
			stage.removeEventListener(TouchEvent.TOUCH_END, onTouchEnd);
			stage.removeEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
		#end
		
		sceneManager.clearCollisionSet();
		sceneManager.clearRenderSet();
		sceneManager.clearTickableSet();
		
		renderer.clear();
		
		sceneManager.getSharedObject().flush();
	}
	
	public static function initializeLocalData(localData:SharedObject):Void {
		localData.data.initialized = true;
		localData.data.mostGold = 0;
		newGame(localData);
	}
	
	public static function newGame(localData:SharedObject):Void {
		//mostGold and totalGold are meant to be used in a 'high score' feature, but I didn't have time to implement it.
		localData.data.gold = 0;
		localData.data.totalGold = 0;
		localData.data.difficulty = 1;
		localData.data.newGame = false;
		//Add stats as needed, just to make sure they all start at 0 (or an appropriate value) with a new game
		localData.data.health = 1;
		localData.data.speed = 4;
		localData.data.collectRadius = 10;
		localData.data.aggroRadius = 150;
		
		localData.flush();
	}
	
	//A dummy function to pass to dummy buttons.
	private function dummyFunction():Void{}
}