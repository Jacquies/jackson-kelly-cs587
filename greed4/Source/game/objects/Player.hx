package game.objects;
import engine.managers.SceneManager;
import engine.interfaces.ICollidable;
import engine.interfaces.IRenderable;
import engine.interfaces.ITickable;
import engine.managers.TileRenderer;
import engine.objects.Animation;
import engine.objects.Collider;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
* @author Jackson Kelly
* 
* The player itself.
*/
class Player implements ICollidable, implements IRenderable, implements ITickable {
	private var collider:Collider;						//The collider.
	private var animation:Animation;					//The animation.
	private var position:Point;							//The position of the player.
	private var rotation:Float;							//The rotation of the player.
	private var currentAnimationID:Int;					//The animation that is currently being played.d
	private var hitCountdown:Int;						//The countdown before you can get hit again (that way, you don't get insta-killed by getting hit in a bunch of frames in a row).
	private var animations:Array<Animation>;			//The animations that can be shown (0 - idle, 1 - dying)
	private var renderer:TileRenderer;					//The TileRenderer with which to render everything.
	
	public function new(tileRenderer:TileRenderer):Void {
		renderer = tileRenderer;
		collider = new Collider(0, 0, 20, 20, Collider.TYPE_PLAYER);
		
		position = new Point(0, 0);
		rotation = 0;
		currentAnimationID = 0;
		hitCountdown = 0;
		
		animations = new Array<Animation>();
		animations.push(new Animation(62, 0, 30, 0, 0, 0, 1, tileRenderer));
		animations.push(new Animation(70, 3, 10, 0, 0, 0, 1, tileRenderer, false));
		
		for (i in 1...animations.length) {
			animations[i].removeFromRenderSet();
		}
		
		animation = animations[currentAnimationID];
	}
	
	public function getCollider():Collider {
		return collider;
	}
	
	public function getPosition():Point {
		return position;
	}
	
	public function setX(x:Float):Void {
		position.x = x;
	}
	
	public function setY(y:Float):Void {
		position.y = y;
	}
	
	public function getRotation():Float {
		return rotation;
	}
	
	public function setRotation(rot:Float):Void {
		rotation = rot;
	}
	
	public function render(deltaTime:Int, camera:Point):Void {
		animation.render(deltaTime, camera);
	}
	
	public function translateX(dx:Float):Void {
		animation.setX(animation.getPosition().x + dx);
	}
	
	public function translateY(dy:Float):Void {
		animation.setY(animation.getPosition().y + dy);
	}
	
	public function tick(deltaTime:Int):Void {
		if (animation.isFinished())
			switchAnimation(0);
		
		if (hitCountdown > 0)
			hitCountdown--;
		
		if (SceneManager.currentSceneManager.getMouseDown()) {
			//Rotate player toward mouse
			if (SceneManager.currentSceneManager.getMousePosition().y > TileRenderer.stageHeight / 2) {
				rotation = SceneManager.usingFlash * (180 - 180 * Math.atan((SceneManager.currentSceneManager.getMousePosition().x - TileRenderer.stageWidth / 2) / (SceneManager.currentSceneManager.getMousePosition().y - TileRenderer.stageHeight / 2)) / Math.PI * deltaTime * 3 / 100);
			}else if (SceneManager.currentSceneManager.getMousePosition().y < TileRenderer.stageHeight / 2) {
				rotation = SceneManager.usingFlash * ( -180 * Math.atan((SceneManager.currentSceneManager.getMousePosition().x - TileRenderer.stageWidth / 2) / (SceneManager.currentSceneManager.getMousePosition().y - TileRenderer.stageHeight / 2)) / Math.PI * deltaTime * 3 / 100);
			}
			
			animation.setRotation(rotation);
			
			//Move player forward
			position.x += SceneManager.currentSceneManager.getSharedObject().data.speed * Math.sin(SceneManager.usingFlash * rotation * Math.PI / 180);
			position.y -= SceneManager.currentSceneManager.getSharedObject().data.speed * Math.cos(SceneManager.usingFlash * rotation * Math.PI / 180);
			
			//Update the animation, collider, and camera position to the player's position (since the camera follows the player).
			animation.setX(position.x);
			animation.setY(position.y);
			collider.setX(position.x);
			collider.setY(position.y);
			SceneManager.currentSceneManager.setCameraX(position.x);
			SceneManager.currentSceneManager.setCameraY(position.y);
		}
		
		if (SceneManager.currentSceneManager.getSharedObject().data.health <= 0) {
			switchAnimation(1);
		}
	}
	
	//Switch the animation that is currently playing.
	private function switchAnimation(ID:Int):Void {
		if (currentAnimationID != ID){
			animation.removeFromRenderSet();
			animation = animations[ID];
			animation.setX(position.x);
			animation.setY(position.y);
			animation.setRotation(rotation);
			animation.addToRenderSet();
			currentAnimationID = ID;
		}
	}
	
	public function decreaseHealth():Void {
		if (hitCountdown <= 0) {	//Make sure the player can't get hit a bunch of times in a row
			hitCountdown = 30;
			//Hit the player
			if (SceneManager.currentSceneManager.getSharedObject().data.health > 0) {
				SceneManager.currentSceneManager.getSharedObject().data.health--;
				SceneManager.currentSceneManager.updateHealthBar();
			}
			
			if (SceneManager.currentSceneManager.getSharedObject().data.health <= 0) {
				SceneManager.currentSceneManager.setIdle(true);
				SceneManager.currentSceneManager.getSharedObject().data.newGame = true;
			}
		}
	}
	
	public function getHitCountdown():Int {
		return hitCountdown;
	}
	
	public function collide(otherCollidable:ICollidable):Void {
		if (otherCollidable.getCollider().getType() == Collider.TYPE_WALL) {
			//Player hit a wall
			var wall:Wall = cast(otherCollidable, Wall);
			
			if (SceneManager.currentSceneManager.getMouseDown()){
				switch(wall.getDirection()) {
					case 0:
						position.x += wall.getCollider().getBoundingBox().intersection(collider.getBoundingBox()).width + 1;
					case 1:
						position.y -= wall.getCollider().getBoundingBox().intersection(collider.getBoundingBox()).height + 1;
					case 2:
						position.x -= wall.getCollider().getBoundingBox().intersection(collider.getBoundingBox()).width + 1;
					case 3:
						position.y += wall.getCollider().getBoundingBox().intersection(collider.getBoundingBox()).height + 1;
				}
			}
		}else if (otherCollidable.getCollider().getType() == Collider.TYPE_PROJECTILE) {
			decreaseHealth();
		}
	}
}