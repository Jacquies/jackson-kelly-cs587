package game.objects.enemy;
import engine.managers.SceneManager;
import engine.managers.TileRenderer;
import engine.objects.Animation;
import game.objects.enemy.ai.ChargingEnemyState;
import game.objects.enemy.ai.ChasingEnemyState;
import game.objects.enemy.ai.IEnemyState;
import game.objects.enemy.ai.RangedAttackEnemyState;
import game.objects.enemy.ai.StrafingEnemyState;
import nme.geom.Point;

/**
* @author Jackson Kelly
* 
* A factory for generating different types of enemies.
*/

class EnemyFactory {
	public static function getEnemy(position:Point, type:Int, tileRenderer:TileRenderer):Enemy {
		var states:Array<IEnemyState> = new Array<IEnemyState>();						//The states that the generated enemy will have.
		var animations:Array<Animation> = new Array<Animation>();						//The animations that the generated enemy will have.
		
		switch (type) {
			case Enemy.TYPE_CHASING_ENEMY:
				states.push(new ChasingEnemyState(SceneManager.currentSceneManager.getSharedObject().data.difficulty));
				animations.push(new Animation(93, 0, 30, position.x, position.y, 0, 1, tileRenderer));
				animations.push(new Animation(93, 1, 15, position.x, position.y, 0, 1, tileRenderer));
				animations.push(new Animation(94, 9, 30, position.x, position.y, 0, 1, tileRenderer, false));
			case Enemy.TYPE_CHARGING_ENEMY:
				states.push(new ChargingEnemyState());
				animations.push(new Animation(104, 0, 30, position.x, position.y, 0, 1, tileRenderer));
				animations.push(new Animation(104, 1, 15, position.x, position.y, 0, 1, tileRenderer));
				animations.push(new Animation(105, 9, 30, position.x, position.y, 0, 1, tileRenderer, false));
			case Enemy.TYPE_RANGED_ENEMY:
				states.push(new StrafingEnemyState());
				states.push(new RangedAttackEnemyState());
				animations.push(new Animation(115, 0, 30, position.x, position.y, 0, 1, tileRenderer));
				animations.push(new Animation(115, 1, 15, position.x, position.y, 0, 1, tileRenderer));
				animations.push(new Animation(116, 9, 30, position.x, position.y, 0, 1, tileRenderer, false));
		}
		
		return new Enemy(position, states, animations, tileRenderer);
	}
}