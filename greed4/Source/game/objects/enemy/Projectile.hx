package game.objects.enemy;
import engine.managers.SceneManager;
import engine.interfaces.ICollidable;
import engine.interfaces.IRenderable;
import engine.interfaces.ITickable;
import engine.managers.TileRenderer;
import engine.objects.Animation;
import engine.objects.Collider;
import nme.geom.Point;

/**
* @author Jackson Kelly
* 
* A deadly projectile, shot by enemies, whch travels in a straight line until it hits the player or a wall.
*/

class Projectile implements ICollidable, implements IRenderable, implements ITickable {
	private var collider:Collider;						//The collider.
	private var animation:Animation;					//The animation.
	private var renderer:TileRenderer;					//The TileRenderer with which to render everything.
	private var position:Point;							//The position of this projectile.
	private var rotation:Float;							//The rotation of this projectile.
	private var poolID:Int;								//The ID of this projectile in the projectile pool in the SceneManager.
	
	//Since projectiles will always be held in a pool, each projectile will keep track of where it is in the pool so that it can remove itself from the scene
	public function new(x:Float, y:Float, tileRenderer:TileRenderer, ID:Int):Void {
		poolID = ID;
		
		position = new Point(x, y);
		
		collider = new Collider(x, y, 10, 10, Collider.TYPE_PROJECTILE);
		animation = new Animation(33, 0, 30, x, y, 0, 1, tileRenderer);
		
		animation.removeFromRenderSet();
		
		renderer = tileRenderer;
	}
	
	public function initialize():Void {
		animation.addToRenderSet();
	}
	
	public function deinitialize():Void {
		animation.removeFromRenderSet();
	}
	
	public function setX(x:Float):Void {
		position.x = x;
	}
	
	public function setY(y:Float):Void {
		position.y = y;
	}
	
	public function setRotation(rot:Float):Void {
		rotation = rot;
		animation.setRotation(rot);
	}
	
	public function getCollider():Collider {
		return collider;
	}
	
	public function render(deltaTime:Int, camera:Point):Void {
		animation.render(deltaTime, camera);
	}
	
	public function tick(deltaTime:Int):Void {
		//Move projectile forward
		position.x += 7 * Math.sin(SceneManager.usingFlash * rotation * Math.PI / 180)/* * deltaTime / 30*/;
		position.y -= 7 * Math.cos(SceneManager.usingFlash * rotation * Math.PI / 180)/* * deltaTime / 30*/;
		
		//Update the animation and collider position.
		animation.setX(position.x);
		animation.setY(position.y);
		collider.setX(position.x);
		collider.setY(position.y);
	}
	
	public function collide(otherCollidable:ICollidable):Void {
		#if flash
		if (otherCollidable.getCollider().getType() == Collider.TYPE_WALL) {
			SceneManager.currentSceneManager.removeProjectile(poolID);
		}
		#end
	}
}