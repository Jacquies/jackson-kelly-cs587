package game.objects.enemy.ai;
import engine.interfaces.ICollidable;
import game.objects.enemy.Enemy;

/**
* @author Jackson Kelly
* 
* The state of an enemy.
*/

interface IEnemyState {
	public function tick(deltaTime:Int, enemy:Enemy):Void;
	public function hitWall(wallCollidable:ICollidable):Void;
}