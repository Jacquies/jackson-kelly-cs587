package game.objects.enemy.ai;
import engine.managers.SceneManager;
import engine.interfaces.ICollidable;
import game.objects.enemy.Enemy;

/**
* @author Jackson Kelly
* 
* The state of an enemy that chases the player.
*/

class ChasingEnemyState implements IEnemyState {
	private var speed:Float;						//The speed of thie enemy.
	
	public function new(difficulty:Int):Void {
		speed = 4 - 3 / Math.sqrt(difficulty);		//Determine the speed based on the difficulty.
	}
	
	public function tick(deltaTime:Int, enemy:Enemy):Void {
		//Rotate enemy toward player
		if (Enemy.player.getPosition().y > enemy.getPosition().y) {
			enemy.setRotation(SceneManager.usingFlash*(180 - 180 * Math.atan((Enemy.player.getPosition().x - enemy.getPosition().x) / (Enemy.player.getPosition().y - enemy.getPosition().y)) / Math.PI));
		}else if (Enemy.player.getPosition().y < enemy.getPosition().y){
			enemy.setRotation(SceneManager.usingFlash*(-180 * Math.atan((Enemy.player.getPosition().x - enemy.getPosition().x) / (Enemy.player.getPosition().y - enemy.getPosition().y)) / Math.PI));
		}
		
		//Move enemy forward
		enemy.setX(enemy.getPosition().x + speed * Math.sin(SceneManager.usingFlash * enemy.getRotation() * Math.PI / 180) * deltaTime * 3 / 100);
		enemy.setY(enemy.getPosition().y - speed * Math.cos(SceneManager.usingFlash * enemy.getRotation() * Math.PI / 180) * deltaTime * 3 / 100);
	}
	
	//All of the functionality for this enemy hitting a wall is handles in the enemy class.
	public function hitWall(wallCollidable:ICollidable):Void {}
}