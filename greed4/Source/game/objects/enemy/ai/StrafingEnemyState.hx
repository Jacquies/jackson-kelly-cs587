package game.objects.enemy.ai;
import engine.managers.SceneManager;
import engine.interfaces.ICollidable;
import game.objects.enemy.Enemy;

/**
* @author Jackson Kelly
* 
* The state of an enemy that strafes around the player.
*/

class StrafingEnemyState implements IEnemyState {
	private var strafeDirection:Int;						//The direction in which this enemy is strafing.
	private var strafeAngle:Float;							//The angle at which this enemy is strafing.
	
	public function new():Void {
		strafeDirection = Math.floor(Math.random() * 2);
		if (strafeDirection == 0)
			strafeDirection = -1;
		strafeAngle = 75;
	}
	
	public function tick(deltaTime:Int, enemy:Enemy):Void {
		var distanceSquared:Float = SceneManager.squareDistance(enemy.getPosition(), Enemy.player.getPosition());
		
		//If the player is too far away, strafe at an angle that brings the enemy closer to the player.
		if (distanceSquared > 90000){
			strafeAngle = 15;
		}else {
			strafeAngle = 75;
		}
		
		//Rotate enemy away from player in the direction of the strafing.
		if (Enemy.player.getPosition().y > enemy.getPosition().y) {
			enemy.setRotation(SceneManager.usingFlash*(180 - 180 * Math.atan((Enemy.player.getPosition().x - enemy.getPosition().x) / (Enemy.player.getPosition().y - enemy.getPosition().y)) / Math.PI));
		}else if (Enemy.player.getPosition().y < enemy.getPosition().y){
			enemy.setRotation(SceneManager.usingFlash*(-180 * Math.atan((Enemy.player.getPosition().x - enemy.getPosition().x) / (Enemy.player.getPosition().y - enemy.getPosition().y)) / Math.PI));
		}
		
		enemy.setRotation(enemy.getRotation() + strafeDirection * strafeAngle);
		
		//Move enemy forward
		enemy.setX(enemy.getPosition().x + 2 * Math.sin(SceneManager.usingFlash * enemy.getRotation() * Math.PI / 180) * deltaTime * 3 / 100);
		enemy.setY(enemy.getPosition().y - 2 * Math.cos(SceneManager.usingFlash * enemy.getRotation() * Math.PI / 180) * deltaTime * 3 / 100);
		
		//Rotate enemy toward player
		if (Enemy.player.getPosition().y > enemy.getPosition().y) {
			enemy.setRotation(SceneManager.usingFlash*(180 - 180 * Math.atan((Enemy.player.getPosition().x - enemy.getPosition().x) / (Enemy.player.getPosition().y - enemy.getPosition().y)) / Math.PI));
		}else if (Enemy.player.getPosition().y < enemy.getPosition().y){
			enemy.setRotation(SceneManager.usingFlash*(-180 * Math.atan((Enemy.player.getPosition().x - enemy.getPosition().x) / (Enemy.player.getPosition().y - enemy.getPosition().y)) / Math.PI));
		}
	}
	
	public function hitWall(wallCollidable:ICollidable):Void {
		//When this enemy hits the wall, it should turn around and strafe the other way.
		strafeDirection *= -1;
	}
}