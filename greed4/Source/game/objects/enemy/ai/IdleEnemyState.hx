package game.objects.enemy.ai;
import engine.managers.SceneManager;
import engine.interfaces.ICollidable;
import game.objects.enemy.Enemy;

/**
* @author Jackson Kelly
* 
* The state of an enemy that has not yet seen the player, and thus does nothing.
*/

class IdleEnemyState implements IEnemyState {
	private var nextStates:Array<IEnemyState>;						//The states that enemy will have once it sees the player.
	
	public function new(states:Array<IEnemyState>):Void {
		nextStates = states;
	}
	
	public function tick(deltaTime:Float, enemy:Enemy):Void {
		//If the player is close enough and the scenemanager is not idle
		if ((SceneManager.squareDistance(enemy.getPosition(), Enemy.player.getPosition()) < Math.pow(SceneManager.currentSceneManager.getSharedObject().data.aggroRadius, 2)) && !SceneManager.currentSceneManager.isIdle()) {
			//Transition to appropriate state(s)
			enemy.exitState(this);
			
			enemy.switchAnimation(1);
			
			for (state in nextStates) {
				enemy.enterState(state);
			}
		}
	}
	
	//All of the functionality for this enemy hitting a wall is handles in the enemy class. Also, I don't expect this enemy to hit the wall very often, seeign as it doesn't move.
	public function hitWall(wallCollidable:ICollidable):Void {}
}