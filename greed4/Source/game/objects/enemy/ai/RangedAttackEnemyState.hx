package game.objects.enemy.ai;
import engine.managers.SceneManager;
import engine.interfaces.ICollidable;
import game.objects.enemy.Enemy;

/**
* @author Jackson Kelly
* 
* The state of an enemy that fires projectiles.
*/

class RangedAttackEnemyState implements IEnemyState {
	private var fireCooldown:Int;						//The cooldown before this enemy can fire again.
	
	public function new():Void {
		fireCooldown = 35;
	}
	
	public function tick(deltaTime:Int, enemy:Enemy):Void {
		fireCooldown--;
		
		if (fireCooldown <= 0) {
			fireCooldown = 35;
			//Spawn projectile
			#if flash
			SceneManager.currentSceneManager.getProjectile(enemy.getPosition().x, enemy.getPosition().y, enemy.getRotation());
			#end
		}
	}
	
	//All of the functionality for this enemy hitting a wall is handled in the enemy class.
	public function hitWall(wallCollidable:ICollidable):Void {}
}