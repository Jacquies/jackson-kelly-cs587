package game.objects.enemy.ai;
import engine.managers.SceneManager;
import engine.interfaces.ICollidable;
import game.objects.enemy.Enemy;

/**
* @author Jackson Kelly
* 
* The state of an enemy who charges in a random direction.
*/

class ChargingEnemyState implements IEnemyState {
	private var speed:Float;						//The speed of the associated enemy.
	private var rotation:Float;						//The rotation of the associated enemy.
	
	public function new():Void {
		speed = 1;
		rotation = -1;
	}
	
	public function tick(deltaTime:Int, enemy:Enemy):Void {
		//If the rotation hasn't been changed, use the enemy's current rotation.
		if (rotation == -1) {
			rotation = enemy.getRotation();
		}else{
			enemy.setRotation(rotation);
		}
		
		//Move enemy forward
		enemy.setX(enemy.getPosition().x + speed * Math.sin(SceneManager.usingFlash * enemy.getRotation() * Math.PI / 180) * deltaTime * 3 / 100);
		enemy.setY(enemy.getPosition().y - speed * Math.cos(SceneManager.usingFlash * enemy.getRotation() * Math.PI / 180) * deltaTime * 3 / 100);
		
		if (speed < 10){
			speed *= 1.075;
		}else {
			speed = 10;
		}
	}
	
	//When this enemy hits a wall, slow down, change direction randomly, and start charging again.
	public function hitWall(wallCollidable:ICollidable):Void {
		speed = 1;
		rotation = Math.random() * 360;
	}
}