package game.objects.enemy;
import engine.interfaces.ICollidable;
import engine.interfaces.IRenderable;
import engine.interfaces.ITickable;
import engine.managers.TileRenderer;
import engine.objects.Animation;
import engine.objects.Collider;
import game.objects.enemy.ai.IdleEnemyState;
import game.objects.enemy.ai.IEnemyState;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
* @author Jackson Kelly
* 
* The enemy. Tries to kill the player.
*/
class Enemy implements ICollidable, implements IRenderable, implements ITickable {
	private var collider:Collider;						//The collider.
	private var animation:Animation;					//The animation.
	private var enemyStates:Array<IEnemyState>;			//A collection of this enemy's current states.
	private var position:Point;							//The current position of the enemy.
	private var rotation:Float;							//The rotation of the enemy.
	private var currentAnimationID:Int;					//The animation that is currently being played.
	private var animations:Array<Animation>;			//A collection of the animations that can be played.
	
	//The types of enemies, and a static reference to the player, since all of the enemies are chasing the same palyer.
	public static var TYPE_CHASING_ENEMY:Int = 0;
	public static var TYPE_RANGED_ENEMY:Int = 1;
	public static var TYPE_CHARGING_ENEMY:Int = 2;
	public static var player:Player;
	
	public function new(spawnPoint:Point, states:Array<IEnemyState>, enemyAnimations:Array<Animation>, tileRenderer:TileRenderer):Void {
		collider = new Collider(spawnPoint.x, spawnPoint.y, 20, 20, Collider.TYPE_ENEMY);
		
		currentAnimationID = 0;
		position = spawnPoint;
		rotation = Math.random() * 360;
		animations = enemyAnimations;
		animation = animations[0];
		for (i in 1...animations.length) {
			animations[i].removeFromRenderSet();
		}
		
		//Add the idle state to the enemy
		enemyStates = new Array<IEnemyState>();
		enemyStates.push(new IdleEnemyState(states));
	}
	
	public function getPosition():Point {
		return position;
	}
	
	public function setX(x:Float):Void {
		position.x = x;
	}
	
	public function setY(y:Float):Void {
		position.y = y;
	}
	
	public function getRotation():Float {
		return rotation;
	}
	
	public function setRotation(rot:Float):Void {
		rotation = rot;
	}
	
	public function getCollider():Collider {
		return collider;
	}
	
	public function render(deltaTime:Int, camera:Point):Void {
		animation.render(deltaTime, camera);
	}
	
	public function tick(deltaTime:Int):Void {
		//If the enemy is playing an attack animation, it shouldn't be doing whatever its states want it to be doing. If it's done playing an attack animation, the player should lose health.
		if (currentAnimationID == 2 && animation.isFinished()) {
			switchAnimation(1);
			player.decreaseHealth();
		}
		
		if (currentAnimationID != 2){
			for (state in enemyStates) {
				state.tick(deltaTime, this);
			}
		}
		
		//Update the animation and collider with the enemy's position and rotation.
		animation.setX(position.x);
		animation.setY(position.y);
		animation.setRotation(rotation - 90);
		collider.setX(position.x);
		collider.setY(position.y);
	}
	
	public function enterState(state:IEnemyState):Void {
		enemyStates.push(state);
	}
	
	public function exitState(state:IEnemyState):Void {
		enemyStates.remove(state);
	}
	
	//Switch the animation that is currently playing.
	public function switchAnimation(ID:Int):Void {
		if (currentAnimationID != ID) {
			animation.restartAnimation();
			animation.removeFromRenderSet();
			animation = animations[ID];
			animation.setX(position.x);
			animation.setY(position.y);
			animation.setRotation(rotation);
			animation.addToRenderSet();
			currentAnimationID = ID;
		}
	}
	
	public function collide(otherCollidable:ICollidable):Void {
		if (otherCollidable.getCollider().getType() == Collider.TYPE_ENEMY) {
			//Enemy hit another enemy
			var collisionVector:Point = new Point(collider.getBoundingBox().intersection(otherCollidable.getCollider().getBoundingBox()).x, collider.getBoundingBox().intersection(otherCollidable.getCollider().getBoundingBox()).y);
			
			if (collisionVector.x < collisionVector.y) {
				if (position.x < cast(otherCollidable, Enemy).getPosition().x) {
					position.x --;
				}else {
					position.x ++;
				}
			}else {
				if (position.y < cast(otherCollidable, Enemy).getPosition().y) {
					position.y --;
				}else {
					position.y ++;
				}
			}
		}else if (otherCollidable.getCollider().getType() == Collider.TYPE_WALL) {
			//Enemy hit a wall
			var wall:Wall = cast(otherCollidable, Wall);
			
			switch(wall.getDirection()) {
				case 0:
					position.x += wall.getCollider().getBoundingBox().intersection(collider.getBoundingBox()).width;
				case 1:
					position.y -= wall.getCollider().getBoundingBox().intersection(collider.getBoundingBox()).height;
				case 2:
					position.x -= wall.getCollider().getBoundingBox().intersection(collider.getBoundingBox()).width;
				case 3:
					position.y += wall.getCollider().getBoundingBox().intersection(collider.getBoundingBox()).height;
			}
			
			for (state in enemyStates) {
				state.hitWall(otherCollidable);
			}
		}else if (otherCollidable.getCollider().getType() == Collider.TYPE_PLAYER) {
			if (player.getHitCountdown()<=0)
				switchAnimation(2);
		}
	}
}