package game.objects;
import engine.interfaces.ICollidable;
import engine.interfaces.IRenderable;
import engine.managers.TileRenderer;
import engine.objects.Animation;
import engine.objects.Collider;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
* @author Jackson Kelly
* 
* Walls in the level to keep the player within the level itself.
*/
class Wall implements IRenderable, implements ICollidable {
	private var collider:Collider;						//The collider.
	private var animation:Animation;					//The animation.
	private var direction:Int;							//The direction this wall is facing - 0=left wall, facing right, 1=bottom wall, facing up, 2=right wall, facing left, 3=top wall, facing down
	
	public function new(spawnPoint:Point, facingDirection:Int, tileRenderer:TileRenderer):Void {
		direction = facingDirection;
		
		if (direction%2==0){
			collider = new Collider(spawnPoint.x, spawnPoint.y, 16, 160, Collider.TYPE_WALL);
		}else {
			collider = new Collider(spawnPoint.x, spawnPoint.y, 160, 16, Collider.TYPE_WALL);
		}
		#if android
		animation = new Animation(56, 0, 30, spawnPoint.x, spawnPoint.y, 90 * direction, 0, tileRenderer);
		#end
	}
	
	public function getCollider():Collider {
		return collider;
	}
	
	public function render(deltaTime:Int, camera:Point):Void {
		animation.render(deltaTime, camera);
	}
	
	public function getDirection():Int {
		return direction;
	}
	
	//The wall doesn't do anything when something hits it.
	public function collide(otherCollidable:ICollidable):Void {}
}