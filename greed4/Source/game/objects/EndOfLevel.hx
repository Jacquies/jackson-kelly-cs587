package game.objects;
import engine.interfaces.ICollidable;
import engine.interfaces.IRenderable;
import engine.managers.TileRenderer;
import engine.objects.Animation;
import engine.objects.Collider;
import game.level.Level;
import nme.geom.Point;

/**
* @author Jackson Kelly
* 
* An object in the level which allows the player to proceed to the next level when they reach it.
*/
class EndOfLevel implements ICollidable, implements IRenderable {
	private var collider:Collider;						//The collider for this object.
	private var animation:Animation;					//The animation for this object.
	private var parent:Level;							//The parent of this object (since hitting this object ends the level, this object must have access to the level itself).
	
	public function new(spawnPoint:Point, tileRenderer:TileRenderer, parentLevel:Level):Void {
		collider = new Collider(spawnPoint.x, spawnPoint.y, 20, 20, Collider.TYPE_END);
		animation = new Animation(32, 0, 30, spawnPoint.x, spawnPoint.y, 0, 1, tileRenderer);
		parent = parentLevel;
	}
	
	public function getCollider():Collider {
		return collider;
	}
	
	public function render(deltaTime:Int, camera:Point):Void {
		animation.render(deltaTime, camera);
	}
	
	public function endLevel():Void {
		//End the level
		parent.endLevel();
	}
	
	public function collide(otherCollidable:ICollidable):Void {
		if (otherCollidable.getCollider().getType() == Collider.TYPE_PLAYER) {
			parent.endLevel();
		}
	}
}