package game.objects;
import engine.managers.SceneManager;
import engine.interfaces.ICollidable;
import engine.interfaces.IRenderable;
import engine.managers.TileRenderer;
import engine.objects.Animation;
import engine.objects.Collider;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
* @author Jackson Kelly
* 
* The coins in the level which can be collected by the player to earn money to buy upgrades.
*/
class GoldPiece implements ICollidable, implements IRenderable {
	private var collider:Collider;						//The collider.
	private var animation:Animation;					//The animation.
	private var collected:Bool;							//Whether or not this coin has been collected yet.
	private var renderer:TileRenderer;					//The TileRenderer with which to render everything.
	private var value:Int;								//The value of this coin
	
	public function new(spawnPoint:Point, tileRenderer:TileRenderer):Void {
		//Randomly give a piece more value.
		if (Math.random() > .9) {
			value = 25;
			animation = new Animation(34, 0, 30, spawnPoint.x, spawnPoint.y, 0, 1, tileRenderer);
		}else {
			value = 5;
			animation = new Animation(74, 13, 20, spawnPoint.x, spawnPoint.y, 0, 1, tileRenderer);
		}
		
		collider = new Collider(spawnPoint.x, spawnPoint.y, SceneManager.currentSceneManager.getSharedObject().data.collectRadius*2, SceneManager.currentSceneManager.getSharedObject().data.collectRadius*2, Collider.TYPE_COIN);
		collected = false;
		renderer = tileRenderer;
	}
	
	public function getCollider():Collider {
		return collider;
	}
	
	public function render(deltaTime:Int, camera:Point):Void {
		//If the coin is collected and done playing its 'disappear' animation, remove it. Otherwise, render it.
		if (collected && animation.isFinished()) {
			SceneManager.currentSceneManager.removeFromRenderSet(this);
			animation.removeFromRenderSet();
		}else{
			animation.render(deltaTime, camera);
		}
	}
	
	public function collide(otherCollidable:ICollidable):Void {
		if (otherCollidable.getCollider().getType() == Collider.TYPE_PLAYER) {
			//Collect this coin
			if (!collected) {
				SceneManager.currentSceneManager.getSharedObject().data.gold += value;
				SceneManager.currentSceneManager.getSharedObject().data.totalGold += value;
				collected = true;
				
				#if flash
				//For some reason this doesn't work on android...
				SceneManager.currentSceneManager.removeFromCollisionSet(this);
				#end
				
				var position:Point = animation.getPosition();
				animation.removeFromRenderSet();
				if (value == 5){
					animation = new Animation(126, 5, 30, position.x, position.y, 0, 1, renderer, false);
				}else {
					SceneManager.currentSceneManager.removeFromRenderSet(this);
				}
				SceneManager.currentSceneManager.updateScoreboard();
			}
		}
	}
}